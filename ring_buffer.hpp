#ifndef CUES_RING_BUFFER_HPP
#define CUES_RING_BUFFER_HPP


//! single-producer, single-consumer FIFO queue (ring buffer)
//! does not have iterators, because it is not meant to be iterated.
//! takes advantage of unsigned arithmetic and has specialized modulo
//! for capacities that are a power of two.

//! Differs from the interface of std::queue significantly.
//! In particular, because it has fixed capacity and does not itself
//! make use of exceptions, many functions change the signature to indicate
//! success or failure.  Further, in order to quickly free up space,
//! font() and pop() are often combined as a consume() function.
//! front() itself is removed due to difficulty in ensuring ahead of time
//! that it will be safe; peek_front() is provided instead to indicate that
//! the return value is not the same as front(); it is in fact an access token
//! which may not refer to a value.

//! differs from boost::lockfree::spsc_queue in that:
//! 1) users are allowed to specify size_type (this can impact performance
//!    for example on Harvard Architecture devices where size_t may be
//!    larger than the native data word size)
//! 2) users can specify alignment of both indices and data, which may have performance implications
//!    for some devices
//! 3) does not use branches to deal with index modulo arithmetic to avoid branch mis-prediction
//!    (boost does as recently as 1.74).  Buffers of size 2^N use bitmasking, other buffer sizes use modulo
//!    operator (devices without hardware division should consider limiting buffers to sizes 2^N)
//! 4) does not require the element type to be default-constructible or copy-constructible
//!    (boost does as recently as 1.74)
//!    and uses cues::flattened_placement_new to avoid the additional space needed by
//!    array placement new if the element type is itself a raw array
//! 5) users have access to make intermediate changes between construction using placement new
//!    and publishing.  This makes it simpler to interface with C APIs that expect to write to
//!    an existing struct through a pointer.  This is done using something like a smart pointer
//!    rather than an additional data member, but comes with the restriction that it can only
//!    be done for one element at a time.

//! Differs from boost::circular_buffer in that:
//! 1) adding new data fails if the buffer is full, instead of overwriting oldest element.
//!    This can be preferable in situations where failing to handle data should be considered
//!    an error, but in situations where only recent data is needed, boost::circular_buffer
//!    may be preferable.
//! 2) It does not attempt to be STL-compliant; in particular, it is not suitable for iteration
//!    and does not provide iterators, nor is it allocator-aware.
//! 3) It is inherently thread-safe for single-producer, single-consumer use
//! 4) It does not attempt to support older language standards
//! 5) Other than the raw storage space, it only needs two atomic size_type values to track
//!    size and positioning, as opposed to four pointers and one size_type (as of boost 1.79)
//! 6) elements can be constructed in-place using using perfect forwarding
//!    (boost requires lvalue or rvalue reference as of 1.79)
//! 7) users have access to make intermediate changes between construction using placement new
//!    and publishing, as noted previously.


//! based in part on the algorithm with two pointers increasing atomically, as analyzed in:
//! Sutter, Herb. "Lock-Free Code: A False Sense of Security", Dr. Dobb's Journal, September 08, 2008
//! https://www.drdobbs.com/cpp/lock-free-code-a-false-sense-of-security/210600279
//! Sutter, Herb. "Writing Lock-Free Code: A Corrected Queue", Dr. Dobb's Journal, September 29, 2008
//! https://www.drdobbs.com/parallel/writing-lock-free-code-a-corrected-queue/210604448
//! regarding work from:
//! Marginean, Petru. "Lock-Free Queues", Dr. Dobb's Journal, july 1, 2008
//! https://www.drdobbs.com/parallel/lock-free-queues/208801974
//! though this queue uses the algorithm of incrementing pointers atomically in a somewhat different
//! manner; in particular, because the underlying storage is an array instead of a linked list,
//! it uses integer offsets instead of whole pointers, allowing for cheaper atomic operations
//! on platforms where the native integer size is smaller than the pointer size and adds some
//! mathematical improvements for treatment of index overflow for queue sizes 2^n;
//! the container also adds various emplacement template functions, provides a mechanism to access and modify data
//! before publishing, and provides functions to produce or consume more than one element at a time,

#include "cues/version_helpers.hpp"

#if (__cplusplus < 201703L && (!defined ( __cpp_lib_launder) || !defined(__cpp_lib_if_constexpr)))
    #error "cues::ring_buffer requires both std::launder and if constexpr"
#endif

#include <cstddef>
#include <cstdint>
#include <atomic>
#include <type_traits>
#include <algorithm>
#include <iterator>
#include <stdexcept>

#if __cplusplus < 202002L
    #include <array>
    #include <vector>
    #if __cplusplus >= 201703L
    #include <string>
    #include <string_view>
    #endif
#endif


#include "cues/constexpr_math.hpp"
#include "cues/array_proxy.hpp"
#include "cues/access_token.hpp"
#include "cues/new.hpp"
#include "cues/type_traits.hpp"
#include "cues/integer_selector.hpp"

namespace cues {


template<class T, std::size_t N, typename index_type = std::size_t, std::size_t DATA_ALIGN = alignof(T), std::size_t INDEX_ALIGN = alignof(index_type)>
class ring_buffer
{
    public:

        using value_type      = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
        using size_type       = index_type;
        //
        using difference_type = typename cues::select_least_integer_by_digits<
            // start aiming for the same size as the size_type
            std::numeric_limits<size_type>::digits
            // if we can store N with one less digit, pass that to the selector so it can pick
            // a signed type of the same width.  Otherwise, it will bump the size of the
            // difference type up to avoid overflows.
            - ((cues::minimum_bits(N) < std::numeric_limits<size_type>::digits) ? 1u : 0u)
            , integer_selector_signedness::use_signed
        >::type;

        using innermost_type  = typename std::remove_all_extents<value_type>::type;
        using pointer         = innermost_type *;
        using const_pointer   = innermost_type const *;

        using reference = typename std::conditional<
            cues::is_fixed_size_array<value_type>::value,
            cues::array_proxy<value_type>,
            typename std::add_lvalue_reference<value_type>::type
        >::type;

        using const_reference = typename std::conditional<
            cues::is_fixed_size_array<value_type>::value,
            cues::array_proxy<typename std::add_const<value_type>::type>,
            typename std::add_lvalue_reference<typename std::add_const<value_type>::type>::type
        >::type;

        using const_access_token     = access_token_impl<ring_buffer, value_type, true>;
        using nonowning_access_token = access_token_impl<ring_buffer, value_type, false>;
        // only need owning token for producer
        using access_token           = unpublished_placement_access_token<ring_buffer, value_type, false>;

        static_assert(!std::is_array<T>::value || cues::is_fixed_size_array<T>::value, "ring_buffer can only store fixed-size arrays.");
        static_assert(N < (std::numeric_limits<size_type>::max)(), "the provided index type cannot represent the requested buffer size (one spare increment is necessary).");
        static_assert(std::is_unsigned<index_type>::value, "ring buffer implementation relies on modulo arithmetic of indices; an unsigned type is thus required.");


        //! must be constructed before concurrent access
        constexpr ring_buffer() noexcept
        : producer_idx(0)
        , consumer_idx(0)
        {}

        //! most likely copying would be a mistake.  If you are more interested in copying than
        //! in concurrent access, try one of the standard library containers.
        ring_buffer(ring_buffer const &) = delete;
        //! moving this structure not possible concurrently.
        ring_buffer(ring_buffer &&) = delete;

        //! PRECONDITION: concurrent access should be finished (or not yet started) before destructor is called
        //! (accessing a destroyed object is always invalid anyway)
        ~ring_buffer()
        noexcept(noexcept(flattened_destruct_backward_from<value_type>(nullptr,0)))
        {
            // make sure both values are updated
            std::atomic_thread_fence(std::memory_order_acquire);
            for (size_type m_begin = atomic_load_explicit(&consumer_idx, std::memory_order_relaxed) ,
                 m_end = std::atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
                 m_begin != m_end;
                 ++m_begin)
            {
                flattened_destruct_backward_from<value_type>(index_to_ptr(m_begin), extent_sequence<value_type>::product()-1);
            }
        }

        //! callable from any context
        static constexpr size_type max_size() noexcept
        {
            return N;
        }

        //! callable from any context.
        //! NOTE: because it uses memory_order_relaxed, may provide a slightly out-of-date value
        //! and cannot be used for synchronizing.
        size_type size() const noexcept
        {
            return static_cast<size_type>(std::atomic_load_explicit(&producer_idx, std::memory_order_relaxed)
            - std::atomic_load_explicit(&consumer_idx, std::memory_order_relaxed));
        }


        //! callable from any context
        //! NOTE: because it uses memory_order_relaxed, may provide slightly out-of-date value
        //! and cannot be used for synchronizing.
        bool has_unconsumed_data() const noexcept
        {
            return !empty();
        }

        //! callable from any context.
        //! NOTE: because it uses memory_order_relaxed, may provide slightly out-of-date value
        //! and cannot be used for synchronizing.  However, may be faster to check than functions
        //! using memory orders that would synchronize.
        bool empty() const noexcept
        {
            return std::atomic_load_explicit(&producer_idx, std::memory_order_relaxed)
                == std::atomic_load_explicit(&consumer_idx, std::memory_order_relaxed);
        }

        //! PRECONDITION: try_emplace may only be called from producer context
        //! Takes care of synchronization.
        //! differs from std container emplace() signature in that it returns a bool indicating success or failure,
        //! hence the name "try_emplace" rather than "emplace".
        //! This change allows it to avoid both exceptions (unless the constructor throws)
        //! and dynamic memory allocation.
        //! Note that try_emplace is optimistic; that is, it uses an atomic_load with memory_order_acquire,
        //! rather than an atomic_load with memory_order_relaxed followed by an atomic_thread_fence with
        //! memory_order_acquire if there is something to do.
        //! Since the load imposes fewer constraints than the fence, this may be more efficient on some cases
        //! (if the buffer is not usually full, since the load blocks fewer compiler optimizations on some platforms).
        //! However, if the buffer is usually full, or if you are waiting on it to be empty, you could
        //! check whether size() == max_size() instead, since size() only uses memory_order_relaxed

        //! emplace overload is for when each argument can construct one of the "elements", and any remaining elements can be default-constructed
        template<typename... Args, typename std::enable_if<
            cues::is_constructible_some_from_each_and_remainder_default<value_type, Args...>::value
            // prefer the other overload for a single-element variadic pack that can construct the whole thing
            && ((sizeof...(Args) > 1) || !is_constructible_first_from_all_and_remainder_default<value_type, Args...>::value),
            bool
            >::type = true
        >
        bool try_emplace_back(Args && ... args)
        noexcept(cues::is_nothrow_constructible_some_from_each_and_remainder_default<value_type, Args...>::value)
        {
            return commit(try_construct_data(std::forward<Args>(args)...));
        }

        //! emplace overload when a set of one or more arguments can construct a single
        //! "element" and any remaining can be default-constructed
        template< class U, class... Args, typename std::enable_if<
                is_constructible_first_from_all_and_remainder_default<value_type, U, Args...>::value,
                bool
            >::type = true
        >
        bool try_emplace_back(U && first_arg, Args && ... args )
        noexcept(is_nothrow_constructible_first_from_all_and_remainder_default<value_type, U, Args...>::value)
        {
            return commit(try_construct_data(std::forward<U>(first_arg), std::forward<Args>(args)...));
        }

        //! emplace overload for when each element of an initializer list can construct one of the "elements", and any remaining can be default-constructed
        template< class U, typename std::enable_if<
                is_constructible_from_one_and_remainder_default<T, U>::value,
                bool
            >::type = true
        >
        bool try_emplace_back(std::initializer_list<U> il)
        noexcept(is_nothrow_constructible_from_one_and_remainder_default<value_type,U>::value)
        {
            return commit(try_construct_data(il));
        }

        //! emplace overload for when an entire initializer_list and additional arguments can construct one
        //! "element", and any remaining can be default-constructed
        template<typename U, typename...Args, typename std::enable_if<
            is_constructible_first_from_list_and_remainder_default<value_type, U, Args...>::value
            , bool
            >::type = true
        >
        bool try_emplace_back(std::initializer_list<U> il, Args && ... args)
        noexcept(is_nothrow_constructible_first_from_list_and_remainder_default<value_type, U, Args...>::value)
        {
            return commit(try_construct_data(il, std::forward<Args>(args)...));
        }



        //! try_construct is an alternative to emplace for cases where the caller may need
        //! to modify the object after construction but before publishing.
        //! If you don't need that, using emplace instead means fewer constraints
        //! you need to worry about.
        //! May return a token with has_value() == false if there is not room in
        //! the buffer.
        //! PRECONDITION:  only called from producer context
        //! PRECONDITION:  no other try_construct function has been called since the
        //!                most recent call to commit or rollback, whichever is more recent
        //! POSTCONDITION: producer calls this function at most once before
        //!                calling commit(access_token) or
        //!                rollback(access_token), and does not
        //!                call any other functions that add items to the buffer
        //!                before calling one of those

        //! overload when all values can be default constructed
        template<typename V = value_type, typename std::enable_if<
                std::is_default_constructible<V>::value,
                bool
            >::type = true
        >
        access_token try_construct_data()
        noexcept(std::is_nothrow_default_constructible<value_type>::value)
        {
            size_type construction_idx = atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
            if (index_can_be_written_to(construction_idx))
            {
                auto ptr = index_to_ptr(construction_idx);
                flattened_default_construct_remaining<value_type>(ptr, 0);
                return access_token(nonowning_access_token(ptr));
            }
            return access_token(nonowning_access_token(nullptr));
        }


        //! overload for when each argument can construct one "element" and any remaining can be
        //! default-constructed.
        template<typename... Args, typename std::enable_if<
                ( sizeof...(Args) > 0
                &&
                cues::is_constructible_some_from_each_and_remainder_default<value_type, Args...>::value),
                bool
            >::type = true
        >
        access_token try_construct_data(Args && ... args)
        noexcept(is_nothrow_constructible_some_from_each_and_remainder_default<value_type, Args...>::value)
        {
            size_type construction_idx = atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
            if (index_can_be_written_to(construction_idx))
            {
                auto ptr = index_to_ptr(construction_idx);
                flattening_placement_new<value_type, Args...>(ptr, std::forward<Args>(args)...);
                return access_token(nonowning_access_token(ptr));
            }
            return access_token(nonowning_access_token(nullptr));
        }

        //! overload when all arguments can construct a single "element" and any remaining
        //! can be default-constructed
        template< class U, class... Args, typename std::enable_if<
                is_constructible_first_from_all_and_remainder_default<value_type, U, Args...>::value,
                bool
            >::type = true
        >
        access_token try_construct_data(U && first_arg, Args &&... args )
        noexcept(is_nothrow_constructible_first_from_all_and_remainder_default<value_type, U, Args...>::value)
        {
            size_type construction_idx = atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
            if (index_can_be_written_to(construction_idx))
            {
                auto ptr = index_to_ptr(construction_idx);
                flattening_placement_new<value_type, U, Args...>(ptr, std::forward<U>(first_arg), std::forward<Args>(args)...);
                return access_token(nonowning_access_token(ptr));
            }
            return access_token(nonowning_access_token(nullptr));
        }

        //! overload for when each element of initializer_list can construct one "element" and remaining
        //! can be default-constructed
        template< class U, typename std::enable_if<
                is_constructible_from_one_and_remainder_default<value_type, U>::value,
                bool
            >::type = true
        >
        access_token try_construct_data(std::initializer_list<U> il)
        noexcept(is_nothrow_constructible_from_one_and_remainder_default<value_type,U>::value)
        {
            size_type construction_idx = atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
            if (index_can_be_written_to(construction_idx))
            {
                auto ptr = index_to_ptr(construction_idx);
                flattening_placement_new<value_type, U>(ptr, il);
                return access_token(nonowning_access_token(ptr));
            }
            return access_token(nonowning_access_token(nullptr));
        }

        //! overload for when an entire initializer_list with supplemental parameters can construct one "element"
        //! and any remaining can be default-constructed
        template< class U, class... Args, typename std::enable_if<
                is_constructible_first_from_list_and_remainder_default<value_type, U, Args...>::value,
                bool
            >::type = true
        >
        access_token try_construct(
            std::initializer_list<U> il,
            Args && ... args
        )
        noexcept( is_nothrow_constructible_first_from_list_and_remainder_default<value_type, U, Args...>::value)
        {
            size_type construction_idx = atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
            if (index_can_be_written_to(construction_idx))
            {
                auto ptr = index_to_ptr(construction_idx);
                flattening_placement_new<value_type, std::initializer_list<U>, Args...>(ptr, il, std::forward<Args>(args)...);
                return access_token(nonowning_access_token(ptr));
            }
            return access_token(nonowning_access_token(nullptr));
        }

        //! overload for taking reference to array
        template<class U, typename std::enable_if<
                std::is_same<U, value_type>::value
                && std::is_array<U>::value,
                bool
            >::type = true
        >
        access_token try_construct_data (U const & array_value) noexcept(noexcept(flattening_placement_new<U>(&(this->raw_data[0]), array_value)))
        {
            size_type construction_idx = atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
            if (index_can_be_written_to(construction_idx))
            {
                auto ptr = index_to_ptr(construction_idx);
                flattening_placement_new<U>(ptr, array_value);
                return access_token(nonowning_access_token(ptr));
            }
            return access_token(nonowning_access_token(nullptr));

        }


        //! PRECONDITION: only called from producer context
        //! PRECONDITION: only call with a token returned by the last call to try_construct,
        //!               though it is permitted to pass one that does not have a value
        bool commit(access_token && item) noexcept
        {
            size_type pending_idx = atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
             assert(static_cast<typename access_token::pointer>(item) == nullptr
                   || reinterpret_cast<cues::raw_byte_type *>(static_cast<typename access_token::pointer>(item)) == index_to_ptr(pending_idx));
            assert((!item.has_value() || index_can_be_written_to(pending_idx)));
            return item.has_value() ?
            // rebind token to nullptr so it doesn't destruct the object
            ((item.rebind(nonowning_access_token(nullptr)), publish_to_index(pending_idx)), true)
            : false;
        }

        //! PRECONDITION: only called from producer context
        //! PRECONDITION: only called with a token returned by the last call to try_construct,
        //!               though it is permitted to pass one that does not have a value
        void roll_back(access_token && item) noexcept
        {
            size_type pending_idx = atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
            assert(static_cast<typename access_token::pointer>(item) == nullptr
                   || reinterpret_cast<cues::raw_byte_type *>(static_cast<typename access_token::pointer>(item)) == index_to_ptr(pending_idx));
            // let RAII of access_token take care of destruction
        }

        //! PRECONDITION: only called from producer context
        //! POSTCONDITION: if non-const iterators are passed, this function
        //!                will prefer to move from those iterators rather than copy
        //!                (leaving them valid but possibly in another state)

        //! publishes as many objects from range as will fit in the buffer;
        //! only updates indices after construction of all possible items.
        //! returns iterator to the first object that could not be inserted;
        //! if none can be inserted, returns src_begin; if all can be inserted,
        //! returns src_end; if some can be inserted, returns something between
        //! src_begin and src_end.
        //! If the object's constructor throws, will destruct any already constructed
        //! objects and rethrow the exception.
        template<class ITERATOR_TYPE>
        ITERATOR_TYPE push_back_n(ITERATOR_TYPE src_begin, ITERATOR_TYPE src_end)
        noexcept(
             cues::can_memcpy_via_iterator<ITERATOR_TYPE, value_type>::value ?
                noexcept(src_begin += 0)
                 && noexcept(*src_begin)
             :
             (is_const_iterator<ITERATOR_TYPE>::value ?
                noexcept(flattening_placement_new<value_type>(nullptr, *src_begin))
                :
                noexcept(flattening_placement_new<value_type>(nullptr, std::move(*src_begin)))
             )
             && noexcept(flattened_destruct_backward_from<value_type>(nullptr, 0))
        )
        {
            auto ret_itr = src_begin;

            size_type const cached_producer_idx    = std::atomic_load_explicit(&producer_idx, std::memory_order_relaxed);
            size_type const cached_consumer_idx    = std::atomic_load_explicit(&consumer_idx, std::memory_order_acquire);
            size_type working_index      = cached_producer_idx;
            size_type remaining_capacity = max_size() - static_cast<size_type>(cached_producer_idx - cached_consumer_idx);

            try
            {
                // std::uninitialized_copy, std::uninitialized_move, or even std::memcpy could
                // potentially batch the copy and be more efficient.
                // however, std::uninitialized_copy and std::uninitialized_move act as though it
                // dereferences the destination "iterator", which in this case is a pointer,
                // which should be undefined behavior since the destination object doesn't exist yet
                // memcpy would work, c++20 (retroactively) specifies that it suffices to begin object
                // lifetime, but needs a contiguous iterator (or raw pointer assumed to be contiguous)
                // can_memcpy_via_iterator checks both for contiguous memory usage,
                // trivially copyable, and that the type of the dereferenced iterator is the
                // same as the value_type (possible with extents removed)
                if constexpr (cues::can_memcpy_via_iterator<ITERATOR_TYPE, value_type>::value)
                {
                    // there are two concerns with the bulk copy.
                    // first, do we have enough space?
                    // second, do we need to handle modulo index (two copies at opposite ends vs one copy)
                    // first copy starts at the write index

                    // note we already checked that iterators are to contiguous memory
                    // so they should support binary operator- and it should be fast
                    auto src_size = src_end - src_begin;
                    size_type actual_copy_count = static_cast<size_type>(std::min(static_cast<std::size_t>(src_size), static_cast<std::size_t>(remaining_capacity)));
                    size_type begin_idx = modulo_index(cached_producer_idx);
                    bool wraparound = (actual_copy_count) > (max_size() - begin_idx);
                    size_type end_idx = wraparound ? max_size() : (begin_idx + actual_copy_count);
                    // one unconditional memcpy for this part of the range
                    // we are assuming the objects do not overlap; because ring_buffer does not
                    // provide iterators (pointers can be obtained with some effort)
                    // it can reasonably be assumed that only intentional misuse could result
                    // in overlapping ranges here.  Strictly speaking, the result of pointer
                    // comparisons would be unspecified (other than == and !=)
                    // if they are not elements in the same array, so there is no easy way here
                    // to reliably validate all cases of range-checking anyway.
                    std::memcpy(
                        &(raw_data[begin_idx*sizeof(value_type)]),
                        std::addressof(*src_begin), // strictly speaking, even a contiguous iterator is not necessarily a pointer
                        (end_idx - begin_idx)*sizeof(value_type)
                    );
                    // second memcpy only if we overlapped
                    if (wraparound)
                    {
                        size_type previously_copied_elements = end_idx - begin_idx;
                        // advance working_index in case second memcpy throws to
                        // destruct already constructed elements
                        working_index = cached_producer_idx + previously_copied_elements;

                        // always wrap to beginning of our data
                        std::memcpy(
                            &(raw_data[0]),
                            std::addressof(*(src_begin + previously_copied_elements)),
                            (actual_copy_count - previously_copied_elements)*sizeof(value_type)
                        );
                    }
                    working_index = cached_producer_idx + actual_copy_count;
                    ret_itr = src_begin + actual_copy_count;

                }
                else
                {
                    // cannot bulk copy; loop and hope compiler optimizes as best it can
                    for (ret_itr = src_begin;
                         (remaining_capacity > 0) && (ret_itr != src_end);
                         --remaining_capacity, ++ret_itr, ++working_index
                    )
                    {
                        assert(index_can_be_written_to(working_index));
                        // post-increment here because we want to call with the original value
                        // switch between copy and move based on const-ness of iterator provided
                        if constexpr (is_const_iterator<ITERATOR_TYPE>::value)
                        {
                            flattening_placement_new<value_type, decltype(*ret_itr)>(index_to_ptr(working_index), *ret_itr);
                        }
                        else
                        {
                            flattening_placement_new<value_type, decltype(std::move(*ret_itr))>(index_to_ptr(working_index), std::move(*ret_itr));
                        }
                    }
                }
            }
            catch(...)
            {
                // cleanup and rethrow
                // note: memcpy version could also throw for example if memcpy throws
                for (;
                    working_index != cached_producer_idx;
                    // pre-increment here because we want to call with the new value;
                    // on loop entry, working_index is the index of the element that threw the exception,
                    // and thus was not constructed.  the one before it is the last constructed one (assuming we
                    // didn't have working_index == cached_unpublished_index, in which case none were constructed).
                    // So, we destruct the previous one, and then repeat to get all the newly constructed ones.
                    flattened_destruct_backward_from<value_type>(index_to_ptr(--working_index), extent_sequence<value_type>::product()-1))
                {
                    ;
                }
                // disable warning on throw in noexcept if we verified that the call is noexcept
                if constexpr (!noexcept(push_back_n(src_begin, src_end)))
                {
                    throw;
                }
            }

            std::atomic_store_explicit(&producer_idx, working_index, std::memory_order_release);

            return ret_itr;
        }

        //! PRECONDITION: only called from consumer context
        //! POSTCONDITION: if there was data ready to be read (at the front), up to max_objects number of objects
        //! will be assigned to the passed argument and removed from the queue.  Returns the number of objects
        //! consumed.
        template<typename ITERATOR_TYPE,
            typename std::enable_if<
                std::is_assignable<
                    typename std::iterator_traits<ITERATOR_TYPE>::value_type,
                    typename std::conditional<
                        (1u == extent_sequence<value_type>::product()),
                        // although we know that the extent is 1, this could still be a single-element array,
                        // so check for assignable using innermost_type instead
                        typename std::add_rvalue_reference<innermost_type>::type,
                        reference
                    >::type
                >::value,
                bool
            >::type = true>
        size_type consume_n(ITERATOR_TYPE dest, size_type max_objects)
        {
            cues::raw_byte_type * ptr;
            size_type const cached_consumer_idx = atomic_load_explicit(&consumer_idx, std::memory_order_relaxed);
            size_type const cached_producer_idx = atomic_load_explicit(&producer_idx, std::memory_order_acquire);
            size_type read_idx_advancement = 0;

            try
            {
                // if we can bulk copy
                if constexpr (cues::can_memcpy_via_iterator<ITERATOR_TYPE, value_type>::value)
                {
                    // very similar to push_back_n but from read side
                    size_type unconsumed_elements = static_cast<size_type>(cached_producer_idx - cached_consumer_idx);
                    size_type actual_copy_count = std::min(unconsumed_elements, max_objects);
                    size_type begin_idx = modulo_index(cached_consumer_idx);
                    bool wraparound = (max_size() - begin_idx) < actual_copy_count;
                    size_type end_idx = wraparound ? max_size() : (begin_idx + actual_copy_count);

                    std::memcpy(
                        std::addressof(*dest),
                        &raw_data[begin_idx*sizeof(value_type)],
                        (end_idx - begin_idx)*sizeof(value_type)
                    );

                    read_idx_advancement = (end_idx - begin_idx);

                    if (wraparound)
                    {
                        std::memcpy(
                            std::addressof(*(dest + read_idx_advancement)),
                            &raw_data[0], // always wraps around to beginning
                            (actual_copy_count - read_idx_advancement)*sizeof(value_type)
                        );

                        read_idx_advancement = actual_copy_count;
                    }
                }
                else
                {
                    // manually copy
                    for (size_type idx = cached_consumer_idx;
                        idx != cached_producer_idx && read_idx_advancement < max_objects;
                        ++idx, ++read_idx_advancement, ++dest)
                    {
                        ptr = index_to_ptr(idx);

                        // if just one object, move it directly
                        if constexpr (1u == extent_sequence<value_type>::product())
                        {
                            *dest = std::move(* reinterpret_ptr(ptr));
                        }
                        else // otherwise let the array proxy take care of it
                        {
                            *dest = reference(ptr, extent_sequence<value_type>());
                        }
                        // destruct
                        flattened_destruct_backward_from<value_type>(ptr, extent_sequence<value_type>::product()-1);
                    }
                }
            }
            catch(...)
            {
                // update to account for any we've already consumed
                std::atomic_store_explicit(&consumer_idx, cached_consumer_idx+read_idx_advancement, std::memory_order_release);
                if constexpr (!noexcept(consume_n(dest, max_objects)))
                {
                    throw;
                }
                // otherwise we threw in a noexcept which calls std::terminate
            }

            // publish
            std::atomic_store_explicit(&consumer_idx, cached_consumer_idx+read_idx_advancement, std::memory_order_release);
            return read_idx_advancement;
        }


        bool consume(value_type & into_here )
        {
            // forward to "iterator" overload for single element
            return consume_n(std::addressof(into_here), 1) == 1;
        }

        template<typename U = value_type,
            class Enable = typename std::enable_if<
                   !std::is_array<U>::value
                && std::is_move_constructible<U>::value
            >::type
        >
        U consume()  noexcept(std::is_nothrow_move_assignable<value_type>::value
                                          && std::is_nothrow_move_constructible<value_type>::value)
        {
            size_type read_idx = std::atomic_load_explicit(&consumer_idx, std::memory_order_relaxed);
            assert(index_can_be_read_from(read_idx));

            U temp{std::move(*(reinterpret_index(read_idx)))};
            pop_from_index(read_idx);
            // not moving here because compiler can already select move constructor
            // or can elide the copy
            return temp;
        }

        //! PRECONDITION:  only called from consumer context
        //! POSTCONDITION: do not continue to use the pointer after any call that consumes the data
        //! (try_pop, etc).  may return nullptr if there is no data.
        const_access_token peek_front() const noexcept
        {
            size_type read_idx = atomic_load_explicit(&consumer_idx, std::memory_order_relaxed);
            return const_access_token(index_can_be_read_from(read_idx) ? index_to_ptr(read_idx) : nullptr);
        }
        //! same as peek_front() const, using "c" prefix like std::cbegin to specify
        //! const overload on non-const object without casting
        const_access_token cpeek_front() const noexcept
        {
            return peek_front();
        }

        //! non-const overload
        //! PRECONDITION:  same as const overload
        //! POSTCONDITION: same as const overload
        nonowning_access_token peek_front() noexcept
        {
            size_type read_idx = atomic_load_explicit(&consumer_idx, std::memory_order_relaxed);
            return nonowning_access_token(index_can_be_read_from(read_idx) ? index_to_ptr(read_idx) : nullptr);
        }

        //! PRECONDITION:  only call from consumer context
        //! PRECONDITION:  only pass non-null pointer from last call to try_front()
        //! POSTCONDITION: do not use the pointer again.
        //! note: you could also call pop_front(), but this is a little harder to use incorrectly
        void mark_consumed(const_access_token && data)
        noexcept(noexcept(flattened_destruct_backward_from<value_type>(nullptr, 0)))
        {
            size_type read_idx = atomic_load_explicit(&consumer_idx, std::memory_order_relaxed);
            assert(data != nullptr);
            assert(ptr_to_index(reinterpret_cast<cues::raw_byte_type const *>(data.as_pointer())) == modulo_index(read_idx));
            assert(index_can_be_read_from(read_idx));
            pop_from_index(read_idx);
        }

        //! PRECONDITION: only call from consumer context
        //! PRECONDITION: !empty()
        //! POSTCONDITION: data at the front is destructed
        void pop_front()
        noexcept(noexcept(flattened_destruct_backward_from<value_type>(nullptr, extent_sequence<value_type>::product())))
        {
            size_type read_idx = atomic_load_explicit(&consumer_idx, std::memory_order_relaxed);
            assert(index_can_be_read_from(read_idx));
            pop_from_index(read_idx);
        }


    private:

        void publish_to_index(size_type idx) noexcept
        {
            assert(index_can_be_written_to(idx));
            atomic_store_explicit(&producer_idx, ++idx, std::memory_order_release);
        }

        void pop_from_index(size_type idx)
        noexcept(noexcept(flattened_destruct_backward_from<value_type>(nullptr, 0)))
        {
            assert(index_can_be_read_from(idx));
            flattened_destruct_backward_from<value_type>(index_to_ptr(idx), extent_sequence<value_type>::product()-1);
            atomic_store_explicit(&consumer_idx, ++idx, std::memory_order_release);
        }

        bool index_can_be_written_to(size_type idx) const noexcept
        {
            // using memory_order_acquire here instead of memory_order_relaxed;
            // this may be less efficient than memory_order_relaxed followed by a fence with memory_order_acquire
            // if the buffer is full and the producer is waiting on the consumer,
            // but may be more efficient if the buffer is not full, since the fence imposes stronger constraints and
            // thus prohibits more optimizations than the load.
            // if it is expected that the buffer will be full, checking size() (which uses memory_order_relaxed) before
            // attempting to add a new element may be preferable.
            return (static_cast<size_type>(idx - atomic_load_explicit(&consumer_idx, std::memory_order_acquire)) < max_size());
        }

        bool index_can_be_read_from(size_type idx) const noexcept
        {
            return atomic_load_explicit(&producer_idx, std::memory_order_acquire) != idx;
        }

        template<size_type FULL_SIZE = N, typename std::enable_if<!cues::is_pow2(FULL_SIZE), bool>::type USE_MASK = false>
        constexpr size_type modulo_index(size_type raw_idx) const noexcept
        {
            return raw_idx % static_cast<size_type>(N);
        }

        // optimization where modulus can be implemented as bitmask
        template<size_type FULL_SIZE = N, typename std::enable_if< cues::is_pow2(FULL_SIZE), bool>::type USE_MASK = true>
        constexpr size_type modulo_index(size_type raw_idx) const noexcept
        {
            return static_cast<size_type>(N-1u) & raw_idx;
        }

        // PRECONDITION: pointer is within array
        // NOTE: due to modulo nature, answer is not necessarily unique.
        // you get the value of the index for the first "pass" through the storage area
        constexpr size_type ptr_to_index(cues::raw_byte_type const * ptr) const noexcept
        {
            // relying on compiler optimizations to implement this as right shift when
            // appropriate.
            return static_cast<size_type>((ptr - &raw_data[0]) / sizeof(value_type));
        }

        // NOTE: index need not refer to an existing element,
        // thus flattened_get is not usable
        constexpr cues::raw_byte_type const * index_to_ptr(size_type index) const noexcept
        {
            // using sizeof rather than alignof on purpose.
            // the whole array should already have the appropriate alignment
            // and padding should take care of aligning subsequent elements as
            // with an array of value_type
            return &raw_data[modulo_index(index)*sizeof(value_type)];
        }

        constexpr cues::raw_byte_type * index_to_ptr(size_type index) noexcept
        {
            return const_cast<cues::raw_byte_type *>(static_cast<ring_buffer const &>(*this).index_to_ptr(index));
        }

        const_pointer reinterpret_ptr(cues::raw_byte_type const * ptr) const noexcept
        {
            return std::launder(reinterpret_cast<const_pointer>(ptr));
        }

        pointer reinterpret_ptr(cues::raw_byte_type * ptr) noexcept
        {
            return const_cast<pointer>(static_cast<ring_buffer const &>(*this).reinterpret_ptr(ptr));
        }

        // PRECONDITION: index refers to an existing element
        const_pointer reinterpret_index(size_type index) const noexcept
        {
            return reinterpret_ptr(index_to_ptr(index));
        }

        // PRECONDITION: index refers to valid element
        pointer reinterpret_index(size_type index) noexcept
        {
            return const_cast<pointer>(static_cast<ring_buffer const &>(*this).reinterpret_index(index));
        }

        alignas(INDEX_ALIGN) std::atomic<size_type> producer_idx;
        alignas(INDEX_ALIGN) std::atomic<size_type> consumer_idx;
        alignas(DATA_ALIGN)  cues::raw_byte_type    raw_data[N*sizeof(value_type)];

};

} // namespace cues

#endif // CUES_RING_BUFFER_HPP
