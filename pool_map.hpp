#ifndef CUES_POOL_MAP_HPP
#define CUES_POOL_MAP_HPP

#include "cues/version_helpers.hpp"

#if (__cplusplus < 201703L && !defined(__cpp_lib_if_constexpr))
    #error "cues::pool_list requires if constexpr, but it is not available"
#endif

#include <cstddef>
#include <utility>
#include <functional>
#include <algorithm>
#include <initializer_list>
#include <memory>


#include "cues/integer_selector.hpp"
#include "cues/constexpr_math.hpp"

#include "cues/allocator_traits.hpp"
#include "cues/memory.hpp"
#include "cues/functional.hpp"
#include "cues/associative_detail.hpp"

namespace cues {

// forward declare iterator
template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
class pool_map_iterator;

// forward declare map
template<
    typename Key,
    typename T,
    class Compare,
    class Container
>
class pool_map;

// forward declare multimap
template<
    typename Key,
    typename T,
    class Compare,
    class Container
>
class pool_multimap;

// common implementation
//! NOTE: for a given comparison functor (std::less, std::greater),
//! you most likely want to supply cues::compare_first_or_value<comparison, value_type>
//! as the compare, unless you are making a custom comparator wrapper
//! to permit lookups by key only but to also sort on some aspect of mapped_type
template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP = false
>
class pool_map_impl
{

public:

    using key_type        = Key;
    using mapped_type     = T;
    using value_type      = std::pair<key_type const, mapped_type>;
    using size_type       = typename Container::size_type;
    using difference_type = typename std::iterator_traits<typename Container::iterator>::difference_type;
    using reference       = value_type &;
    using const_reference = value_type const &;
    using pointer         = value_type *;
    using const_pointer   = value_type const *;

    static_assert(std::is_same<value_type, typename Container::value_type>::value, "assumption violation: build the map on an underlying container with the same value_type");

    using iterator        = pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, false>;
    using const_iterator  = pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, true>;

    using reverse_iterator       = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using underlying_container_type = Container;
    using allocator_type       = typename underlying_container_type::allocator_type;

    using node_type       = basic_node_handle<value_type, allocator_type>;

    static_assert(cues::provides_allocator<node_type>::value, "node type is expected to provide an allocator");

    allocator_type get_allocator() const
    {
        return this->underlying_container.get_allocator();
    }

    using pair_compare = typename cues::compare_first_or_value<Compare, value_type>;

    using value_compare = typename cues::compare_or_indirect_compare<
        pair_compare,
        underlying_container_type
    >::type;

    using key_compare = Compare;


    value_compare value_comp() const
    {
        return this->comp;
    }

    key_compare key_comp() const
    {
        return key_compare{this->comp};
    }

    bool empty() const noexcept
    {
        return underlying_container.empty();
    }

    size_type size() const noexcept
    {
        return underlying_container.size();
    }

    size_type max_size() const noexcept
    {
        return underlying_container.max_size();
    }

    void clear() noexcept
    {
        underlying_container.clear();
    }

    iterator erase( const_iterator pos )
    {
        return iterator{this->underlying_container.erase(
            static_cast<underlying_const_iterator_type>(pos)
        )};
    }

    iterator erase( const_iterator first, const_iterator last )
    {
        return iterator{this->underlying_container.erase(
            static_cast<underlying_const_iterator_type>(first),
            static_cast<underlying_const_iterator_type>(last)
        )};
    }

    iterator erase( key_type const & key  )
    {
        auto limits = this->idx_equal_range(key);
        return iterator{this->underlying_container.erase(
            this->underlying_container.cbegin() += limits.first,
            this->underlying_container.cbegin() += limits.second
        )};
    }

    const_iterator cbegin() const
    {
        return static_cast<const_iterator >(underlying_container.cbegin());
    }

    const_iterator begin() const
    {
        return static_cast<const_iterator >(underlying_container.cbegin());
    }

    iterator begin()
    {
        return static_cast<iterator >(underlying_container.begin());
    }

    const_iterator cend() const
    {
        return static_cast<const_iterator >(underlying_container.cend());
    }

    const_iterator end() const
    {
        return static_cast<const_iterator >(underlying_container.cend());
    }

    iterator end()
    {
        return static_cast<iterator >(underlying_container.end());
    }

    const_reverse_iterator crbegin() const
    {
        return const_reverse_iterator{cend()};
    }

    const_reverse_iterator rbegin() const
    {
        return const_reverse_iterator{cend()};
    }

    reverse_iterator rbegin()
    {
        return reverse_iterator{end()};
    }

    const_reverse_iterator crend() const
    {
        return const_reverse_iterator{cbegin()};
    }

    const_reverse_iterator rend() const
    {
        return const_reverse_iterator{cbegin()};
    }

    reverse_iterator rend()
    {
        return reverse_iterator{begin()};
    }

    const_iterator lower_bound(key_type const & key) const
    {
        return cbegin() += idx_lower_bound(key);
    }

    iterator lower_bound(key_type const & key)
    {
        return begin() += idx_lower_bound(key);
    }

    const_iterator upper_bound( key_type const & key ) const
    {
        return cbegin() += idx_upper_bound(key);
    }

    iterator upper_bound(key_type const & key)
    {
        return begin() += idx_upper_bound(key);
    }

    std::pair<const_iterator, const_iterator> equal_range(key_type const & key) const
    {
        std::pair<size_type, size_type> idx_range = idx_equal_range(key);
        return {
            cbegin() += idx_range.first,
            cbegin() += idx_range.second
        };
    }

    std::pair<iterator, iterator> equal_range(key_type const & key)
    {
        std::pair<size_type, size_type> idx_range = idx_equal_range(key);
        return {
            begin() += idx_range.first,
            begin() += idx_range.second
        };
    }

    size_type count(key_type const & key) const
    {
        std::pair<size_type, size_type> idx_range = idx_equal_range(key);
        return idx_range.second - idx_range.first;
    }

    const_iterator find( key_type const & key ) const
    {
        const_iterator lb_itr = lower_bound(key);
        // lower_bound finds element not less than key,
        // aka element >= key
        // we want only == , so check comparison
        // again; if key < element, return end instead

        if (lb_itr != cend() && comp(key, *lb_itr))
        {
            return cend();
        }
        return lb_itr;
    }

    iterator find(key_type const & key)
    {
        iterator lb_itr = lower_bound(key);
        // lower_bound finds element not less than key,
        // aka element >= key
        // we want only == , so check comparison
        // again; if key < element, return end instead
        if (lb_itr != end() && comp(key, *lb_itr))
        {
            return end();
        }
        return lb_itr;
    }

    bool contains( key_type const & key ) const
    {
        return (find(key) != cend());
    }

    node_type extract(const_iterator position) noexcept
    {
        if (position == cend())
        {
            return node_type{};
        }

        typename underlying_container_type::node_type unode = this->underlying_container.extract(
            static_cast<underlying_const_iterator_type>(position)
        );

        if (!static_cast<bool>(unode))
        {
            return node_type{};
        }

        if constexpr (cues::can_construct_from_extracted_node<node_type, typename underlying_container_type::node_type>::value)
        {
            return node_type{std::move(unode)};
        }
        else
        {
            node_type node{&(*unode), unode.get_allocator()};
            unode.release();
            return node;
        }
    }

    template< template<typename...> class Other_Impl, class Any_Compare, class Any_Container>
    typename std::enable_if<
        (
             std::is_same<
                typename std::remove_const<
                    typename std::remove_reference<
                        Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>
                    >::type
                >::type,
                pool_map<key_type, mapped_type, Any_Compare, Any_Container>
            >::value
            ||
            std::is_same<
                typename std::remove_const<
                    typename std::remove_reference<
                        Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>
                    >::type
                >::type,
                pool_multimap<key_type, mapped_type, Any_Compare, Any_Container>
            >::value
        )
        &&
        std::is_same<
            allocator_type,
            typename Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>::allocator_type
        >::value
    >::type merge( Other_Impl<key_type, mapped_type, Any_Compare, Any_Container> & other )
    {
        merge(other, other.cbegin(), other.cend());
    }

    template< template<typename...> class Other_Impl, class Any_Compare, class Any_Container>
    typename std::enable_if<
        (
             std::is_same<
                typename std::remove_const<
                    typename std::remove_reference<
                        Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>
                    >::type
                >::type,
                pool_map<key_type, mapped_type, Any_Compare, Any_Container>
            >::value
            ||
            std::is_same<
                typename std::remove_const<
                    typename std::remove_reference<
                        Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>
                    >::type
                >::type,
                pool_multimap<key_type, mapped_type, Any_Compare, Any_Container>
            >::value
        )
        &&
        std::is_same<
            allocator_type,
            typename Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>::allocator_type
        >::value
    >::type merge( Other_Impl<key_type, mapped_type, Any_Compare, Any_Container> & other,
          typename Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>::const_iterator ext_begin,
          typename Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>::const_iterator ext_limit
    ) {
        using other_iterator_type = typename Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>::const_iterator;
        using other_diff_type  = typename std::iterator_traits<other_iterator_type>::difference_type;

        if (static_cast<void *>(this) == static_cast<void *>(&other)) {return;}

        other_diff_type skipped_elements = ext_begin - other.cbegin();
        other_diff_type ext_range = ext_limit - ext_begin;

        underlying_indirect_const_iterator_type this_end = cues::direct_or_indirect_end(this->underlying_container);
        underlying_indirect_const_iterator_type this_begin = cues::direct_or_indirect_begin(this->underlying_container);
        underlying_indirect_const_iterator_type dest_pos;

        for (  typename Other_Impl<key_type, mapped_type, Any_Compare, Any_Container>::underlying_indirect_const_iterator_type
            src_begin = cues::direct_or_indirect_begin(other.underlying_container) + skipped_elements,
            other_end = src_begin + ext_range;
            // are there still elements to use?
            src_begin != other_end;
            // update invalidated iterators
            this_begin = cues::direct_or_indirect_begin(this->underlying_container),
            this_end = cues::direct_or_indirect_end(this->underlying_container),
            src_begin = cues::direct_or_indirect_begin(other.underlying_container) + skipped_elements,
            other_end = src_begin + ext_range
        ) {
            // mixing destination and source, get value
            // from other instead of passing potentially indirect iterator
            // if compare had matched, we could have searched from dest_pos,
            // but we can't guarantee that, and have to search the full range each time
            // for multimap, upper_bound is always the correct answer; for map,
            // lower_bound might be more useful to check for equivalence, but
            // since map is unique, lower_bound is either upper_bound or
            // upper_bound - 1
            dest_pos = std::upper_bound(
                this_begin,
                this_end,
               *cues::make_direct(other.underlying_container, src_begin),
                this->comp
            );
            // do we need to check for duplicates?
            if CUES_CONSTEXPR_OR_RUNTIME_IF (!IS_MULTIMAP)
            {
                // upper_bound returns first dest_pos such that comp(src_begin, dest_pos)
                // for map, elements are unique and in order, so if there is an equal
                // element, it is at dest_pos - 1.  we know that comp(dest_pos - 1, dest_pos),
                // and !comp(src_begin, dest_pos - 1), because then dest_pos -1 would have been
                // returned by upper_bound instead of dest_pos
                // so, if upper_bound is not begin(), and !comp(dest_pos - 1, src_begin)
                // dest_pos -1 is equivalent
                if (dest_pos != this_begin
                    && !this->comp(*(dest_pos - 1), *cues::make_direct(other.underlying_container, src_begin))
                ) {
                    ++skipped_elements;
                    // also remove this element from the remaining range
                    --ext_range;
                    continue;
                }
            }
            else
            {
                (void)skipped_elements; //< unwarn unused, let optimizer eliminate
            }

            ext_range -= this->splice_or_batch_splice(
                dest_pos,
                other,
                src_begin,
                other_end
            );
        }

    }

    protected:

    template<
        typename FK,
        typename FT,
        class FCmp,
        class FCnt,
        bool Fmm
    >
    friend class pool_map_impl;

    pool_map_impl() = default;

    static_assert(
        std::is_same<
            typename cues::compare_constructor_proxy<pair_compare, underlying_container_type>::compare_type,
            typename underlying_container_type::indirect_comparator<pair_compare>
        >::value,
        "unexpected compare type"
    );

    explicit pool_map_impl(allocator_type a)
    : underlying_container(std::move(a))
    , comp(cues::compare_constructor_proxy<pair_compare, underlying_container_type>(pair_compare(Compare{}), underlying_container))
    {}

    // not virtual; do not fail to destruct the implementation by deleting base pointer
    ~pool_map_impl() = default;
    // declaring the destructor, in this case just to make it protected,
    // means the default move constructor is not automatically generated
    // unless we ask for it
    pool_map_impl(pool_map_impl &&) = default;
    pool_map_impl & operator=(pool_map_impl &&) = default;
    // declaring the move constructor means the default copy constructor
    // is not automatically generated unless we ask for it
    pool_map_impl(pool_map_impl const &) = default;
    pool_map_impl & operator=(pool_map_impl const &) = default;

    pool_map_impl(Compare c, allocator_type a)
    : underlying_container(std::move(a))
    , comp(cues::compare_constructor_proxy<pair_compare, underlying_container_type>(pair_compare(c), underlying_container))
    {}



    // helper functions
    using underlying_iterator_type = typename iterator::underlying_iterator;
    using underlying_const_iterator_type = typename const_iterator::underlying_iterator;
    using underlying_indirect_iterator_type = typename direct_or_indirect_iterator<underlying_container_type>::iterator;
    using underlying_indirect_const_iterator_type = typename direct_or_indirect_iterator<underlying_container_type>::const_iterator;

    std::pair<size_type, size_type> idx_equal_range(key_type const & key) const
    {
        auto bg = cues::direct_or_indirect_begin(this->underlying_container);
        auto temp = std::equal_range(
            bg,
            cues::direct_or_indirect_end(this->underlying_container),
            key,
            comp
        );
        return std::pair<size_type, size_type>{temp.first - bg, temp.second - bg};
    }

    size_type idx_lower_bound(key_type const & key) const
    {
        return std::lower_bound(
            cues::direct_or_indirect_begin(this->underlying_container),
            cues::direct_or_indirect_end(this->underlying_container),
            key,
            comp
        ) - cues::direct_or_indirect_begin(this->underlying_container);
    }

    size_type idx_upper_bound(key_type const & key) const
    {
        return std::upper_bound(
            cues::direct_or_indirect_begin(this->underlying_container),
            cues::direct_or_indirect_end(this->underlying_container),
            key,
            comp
        ) - cues::direct_or_indirect_begin(this->underlying_container);
    }

    iterator checked_insert(underlying_const_iterator_type position, node_type && node)
    {
        if constexpr (cues::has_node_positional_splice<underlying_container_type, node_type>::value)
        {
            this->underlying_container.splice(position, std::move(node));
            return begin() += (position - this->underlying_container.cbegin());
        }
        else
        {
            return static_cast<iterator>(
                underlying_iterator_type{this->underlying_container.insert(position, std::move(node))}
            );
        }
    }

    template< template<typename...> class Other_Map, class Any_Container>
    typename std::enable_if<
        cues::has_iterator_range_splice<
                underlying_container_type,
                Any_Container
        >::value,
        size_type
    >::type splice_or_batch_splice(
        underlying_indirect_const_iterator_type dest_pos,
        Other_Map<key_type, mapped_type, Compare, Any_Container> & other,
        typename Other_Map<key_type, mapped_type, Compare, Any_Container>::underlying_indirect_const_iterator_type src_begin,
        typename Other_Map<key_type, mapped_type, Compare, Any_Container>::underlying_indirect_const_iterator_type src_limit
    )
    {
         using omap_type = Other_Map<key_type, mapped_type, Compare, Any_Container>;
         using foreign_underlying_const_iterator = typename omap_type::underlying_const_iterator_type;

         typename omap_type::underlying_indirect_const_iterator_type src_end;


        if (dest_pos == cues::direct_or_indirect_end(this->underlying_container))
        {
            // if we are a multimap, or the other is a map, and our comparison is the same,
            // we can grab the entire remaining range, because it is already sorted properly
            // and either we accept duplicates or they won't have any
            // if inserting to the end, grab the whole rest
            if CUES_CONSTEXPR_OR_RUNTIME_IF (IS_MULTIMAP
            || std::is_same<omap_type, pool_map<key_type, mapped_type, Compare, Any_Container>>::value
            )  {
                src_end = src_limit;
            }
            else // otherwise just insert one, because we need to check the others
            {
                src_end = src_begin + 1;
            }
        }
        else
        {
            // for multimap, we want the open-ended range to end with the first value >=, to maintain
            // relative ordering,  which is also what want for map, since the open end is excluded
            src_end = std::lower_bound(
                src_begin,
                src_limit,
                *cues::make_direct(this->underlying_container, dest_pos),
                comp
            );
        }

        size_type const previous_size = this->underlying_container.size();
        // splice will remove elements from the container
        this->underlying_container.splice(
            static_cast<underlying_const_iterator_type>(cues::make_direct(this->underlying_container, dest_pos)),
            other.underlying_container,
            static_cast<foreign_underlying_const_iterator>(cues::make_direct(other.underlying_container, src_begin)),
            static_cast<foreign_underlying_const_iterator>(cues::make_direct(other.underlying_container, src_end))
        );
        return this->underlying_container.size() - previous_size;
    }


    template<class Other_Map>
    typename std::enable_if<
        !std::is_same<typename Other_Map::key_compare, key_compare>::value
        ||
        !cues::has_iterator_range_splice<
                underlying_container_type,
                typename Other_Map::underlying_container_type
        >::value,
        size_type
    >::type splice_or_batch_splice(
        underlying_indirect_const_iterator_type dest_pos,
        Other_Map & other,
        typename Other_Map::underlying_indirect_const_iterator_type src_pos,
        typename Other_Map::underlying_indirect_const_iterator_type src_limit
    )
    {
        if (src_limit == src_pos) {return static_cast<size_type>(0u);}
        using foreign_underlying_const_iterator = typename Other_Map::underlying_const_iterator_type;
        size_type const previous_size = this->underlying_container.size();

        // splice directly via underlying container
        if constexpr (
            cues::has_container_positional_splice<
                underlying_container_type,
                typename Other_Map::underlying_container_type
            >::value
        )
        {
            this->underlying_container.splice(
                static_cast<underlying_const_iterator_type>(cues::make_direct(this->underlying_container, dest_pos)),
                other.underlying_container,
                static_cast<foreign_underlying_const_iterator>(cues::make_direct(other.underlying_container, src_pos))
            );
        }
        // extract and insert
        else
        {
            this->underlying_container.insert(
                 static_cast<underlying_const_iterator_type>(cues::make_direct(this->underlying_container, dest_pos)),
                 other.underlying_container.extract(
                     static_cast<foreign_underlying_const_iterator>(cues::make_direct(other.underlying_container, src_pos))
                 )
            );
        }

        return this->underlying_container.size() - previous_size;
    }

    // if we have the same key comparison,
    // assignable value type,
    // and random access iterator, permit batch insertion
    template<typename Itr>
    void insert_sorted_range(Itr src_begin, Itr src_end)
    {
        Itr subset_end;

        for( auto this_end = cues::direct_or_indirect_end(*this),
                dest_pos = cues::direct_or_indirect_begin(*this);
            src_begin != src_end;
            this_end = cues::direct_or_indirect_end(*this),
            src_begin = subset_end
        ) {
            if CUES_CONSTEXPR_OR_RUNTIME_IF (IS_MULTIMAP)
            {
                // mixing destination and source, get value
                // from other instead of passing potentially indirect iterator
                dest_pos = std::lower_bound(
                    dest_pos,
                    this_end,
                   *src_begin,
                    // our value_compare also accepts keys and indirect iterators,
                    // so pass that.
                    this->comp
                );

                if (dest_pos != this_end && !comp(*dest_pos, *src_begin))
                {
                    subset_end = src_begin + 1;
                    continue;
                }
            }
            else
            {
                dest_pos = std::upper_bound(
                    dest_pos,
                    this_end,
                   *src_begin,
                    // our value_compare also accepts keys and indirect iterators,
                    // so pass that.
                    this->comp
                );
            }

            subset_end = std::lower_bound(
                src_begin,
                src_end,
                *cues::make_direct(*this, dest_pos),
                this->comp
            );

            this->underlying_containter.insert(
                static_cast<underlying_const_iterator_type>(cues::make_direct(*this, dest_pos)),
                src_begin,
                subset_end
            );
        }
    }

    underlying_container_type underlying_container;
    value_compare comp;

};


template<
    typename Key,
    typename T,
    class Compare,
    class Container
>
class pool_map : private pool_map_impl<Key, T, Compare, Container, false>
{

    using impl_type = pool_map_impl<Key, T, Compare, Container, false>;
    using typename impl_type::underlying_iterator_type;
    using typename impl_type::underlying_const_iterator_type;
    using typename impl_type::underlying_indirect_iterator_type;
    using typename impl_type::underlying_indirect_const_iterator_type;

    template<typename FK, typename FT, class FCmp, class FCtr>
    friend class pool_map;

    template<typename FK, typename FT, class FCmp, class FCtr>
    friend class pool_multimap;

    template<typename FK, typename FT, class FCmp, class FCtr, bool MM>
    friend class pool_map_impl;

    enum class hint_quality
    {
        inaccurate,
        blocked,
        insertable
    };

    public:

    using typename impl_type::key_type;
    using typename impl_type::mapped_type;
    using typename impl_type::value_type;
    using typename impl_type::size_type;
    using typename impl_type::difference_type;
    using typename impl_type::reference;
    using typename impl_type::const_reference;
    using typename impl_type::pointer;
    using typename impl_type::const_pointer;

    using typename impl_type::iterator;
    using typename impl_type::const_iterator;

    using typename impl_type::reverse_iterator;
    using typename impl_type::const_reverse_iterator;

    using typename impl_type::node_type;
    struct insert_return_type {
        iterator     position;
        bool         inserted;
        node_type    node;
    };

    using typename impl_type::underlying_container_type;
    using typename impl_type::allocator_type;

    using typename impl_type::value_compare;
    using typename impl_type::key_compare;

    using impl_type::value_comp;
    using impl_type::key_comp;


    using impl_type::empty;
    using impl_type::size;
    using impl_type::max_size;
    using impl_type::clear;
    using impl_type::erase;

    using impl_type::cbegin;
    using impl_type::begin;
    using impl_type::cend;
    using impl_type::end;

    using impl_type::crbegin;
    using impl_type::rbegin;
    using impl_type::crend;
    using impl_type::rend;

    using impl_type::lower_bound;
    using impl_type::upper_bound;
    using impl_type::equal_range;
    using impl_type::count;
    using impl_type::find;
    using impl_type::contains;

    using impl_type::extract;

    using impl_type::merge;


    pool_map() = default;

    explicit pool_map(allocator_type const & a)
    : impl_type(a)
    {}

    pool_map(Compare const & c, allocator_type const & a)
    : impl_type(c, a)
    {}

    pool_map(std::initializer_list<value_type> il, Compare const & c, allocator_type const & a )
    : impl_type(c, a)
    {
        this->insert(il.begin(), il.end());
    }

    // leave compiler-generated copy, move constructors

    friend void swap(pool_map & lhs, pool_map & rhs) noexcept(noexcept(swap(lhs.underlying_container, rhs.underlying_container)))
    {
        swap(lhs.underlying_container, rhs.underlying_container);
    }

    // permit comparison with maps of other container types
    // if the container types can be compared for equality
    template<class Other_Container>
    typename std::enable_if<
        cues::is_equality_comparable<Container, Other_Container>::value,
    bool >::type equals(pool_map<key_type, mapped_type, Compare, Other_Container> const & other) const
    {
        return this->underlying_container == other.underlying_container;
    }

    template<class Other_Container>
    typename std::enable_if<
        !cues::is_equality_comparable<Container, Other_Container>::value,
    bool >::type equals(pool_map<key_type, mapped_type, Compare, Other_Container> const & other) const
    {
        if (this->size() != other.size())
        {
            return false;
        }
        auto this_itr  = cbegin();
        auto this_end  = cend();
        auto other_itr = other.cbegin();
        auto other_end = other.cend();

        for (;
            this_itr != this_end && other_itr != other_end;
            ++this_itr, ++other_itr
        )
        {
            if (*this_itr != *other_itr)
            {
                return false;
            }
        }
        return true;
    }

    template<class Key_Ref>
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<typename std::remove_reference<Key_Ref>::type>::type,
            key_type
        >::value,
    mapped_type>::type & operator[](Key_Ref && key)
    {
        return this->try_emplace(std::forward<Key_Ref>(key)).first->second;
    }

    mapped_type const & at( key_type const & key ) const
    {
        auto itr = this->find(key);
        if (itr == this->cend())
        {
            throw std::out_of_range("map has no element with specified key.");
        }
        return itr->second;
    }

    mapped_type & at( key_type const & key )
    {
        return const_cast<mapped_type &>(const_cast<pool_map const *>(this)->at(key));
    }

    template< class... Args >
    typename std::enable_if<
        std::is_constructible<value_type, Args...>::value
    , std::pair<iterator,bool> >::type emplace( Args && ... args )
    {
        node_type potential_node {cues::allocate_unique<value_type>(this->underlying_container.get_allocator(), std::forward<Args>(args)...)};

        assert(((void)"node should be valid or should have thrown std::bad_alloc", static_cast<bool>(potential_node)));

        auto potential_position = cues::direct_or_indirect_begin(this->underlying_container)+ this->idx_lower_bound(potential_node.key());

        // if we would go to the end, or found a spot that is not equal
        if (potential_position == cues::direct_or_indirect_end(this->underlying_container)
            ||
            this->comp(
                 static_cast<key_type const &>(potential_node.key()),
                 *potential_position
            )
        ) {
            return {this->checked_insert(
                     cues::make_direct(this->underlying_container, potential_position),
                     std::move(potential_node)
                ), true
            };
        }

        return {iterator{cues::make_direct(this->underlying_container,potential_position)}, false};
    }

    template< class... Args >
    typename std::enable_if<
        std::is_constructible<value_type, Args...>::value
    , iterator>::type emplace_hint( const_iterator hint, Args && ... args )
    {
        node_type potential_node {cues::allocate_unique<value_type>(this->underlying_container.get_allocator(), std::forward<Args>(args)...)};

        assert(((void)"node should be valid or should have thrown std::bad_alloc", static_cast<bool>(potential_node)));

        hint_quality hq = this->check_hint_quality(
            cues::make_indirect(this->underlying_container, static_cast<underlying_const_iterator_type>(hint)),
            potential_node.key()
        );

        switch(hq)
        {
        case hint_quality::inaccurate:
            return this->insert(
                (
                    this->cbegin() += this->idx_lower_bound(potential_node.key())
                ),
                std::move(potential_node)
            );
        case hint_quality::insertable:
            return this->checked_insert(
                    static_cast<underlying_const_iterator_type>(hint),
                    std::move(potential_node)
            );
        case hint_quality::blocked:
            break;
        default:
            assert(((void)"failed to consider all switch types", false));
        }
        // blocked can return if either hint or hint-1 is equivalent,
        // so we know one of two cases is true:
        // !comp(hint, key) && !comp(key, hint)
        // or !comp(hint-1, key) && !comp(key, hint-1)
        // we also know that the map is ordered, meaning that if hint is
        // not the blocking element, comp(key, hint) must be true
        return begin() + (
            (
                ( hint == this->cend() || this->comp(static_cast<key_type const &>(potential_node.key()), *hint) ) ?
                --hint
                :
                hint
            )
         - cbegin()
        );

    }


    template<class Key_Ref, class ... Args >
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<
                typename std::remove_reference<Key_Ref>::type
            >::type,
            key_type
        >::value,
    std::pair<iterator, bool> >::type try_emplace( Key_Ref && key, Args && ... args )
    {
        auto potential_position = cues::direct_or_indirect_begin(this->underlying_container)+ this->idx_lower_bound(key);
        if (potential_position == cues::direct_or_indirect_end(this->underlying_container)
            ||
            this->comp(key, *potential_position)
        )
        {
            node_type node {cues::allocate_unique<value_type>(
                this->underlying_container.get_allocator(),
                std::piecewise_construct,
                std::forward_as_tuple(std::forward<Key_Ref>(key)),
                std::forward_as_tuple(std::forward<Args>(args)...)
            )};
            return {this->checked_insert(
                    cues::make_direct(this->underlying_container, potential_position),
                    std::move(node)
                ), true
            };
        }
        return {iterator{cues::make_direct(this->underlying_container, potential_position)}, false};
    }

    template<class Key_Ref, class ... Args >
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<typename std::remove_reference<Key_Ref>::type>::type,
            key_type
        >::value,
    iterator >::type try_emplace( const_iterator hint, Key_Ref && key, Args && ... args )
    {
        auto indirect_underlying_itr = cues::make_indirect(this->underlying_container, static_cast<underlying_const_iterator_type>(hint));
        hint_quality hq = this->check_hint_quality(
           indirect_underlying_itr,
           key
        );

        switch(hq)
        {
        case hint_quality::inaccurate:
            return try_emplace(std::forward<Key_Ref>(key), std::forward<Args>(args)...).first;
        case hint_quality::insertable:
            {
            node_type node {cues::allocate_unique<value_type>(
                this->underlying_container.get_allocator(),
                std::piecewise_construct,
                std::forward_as_tuple(std::forward<Key_Ref>(key)),
                std::forward_as_tuple(std::forward<Args>(args)...)
            )};
            return this->checked_insert(static_cast<underlying_const_iterator_type>(hint), std::move(node));
            }
        case hint_quality::blocked:
            break;
        default:
            assert(((void)"failed to consider all switch types", false));
        }
        return begin() +
        (
             (
                (( cues::direct_or_indirect_end( const_cast<underlying_container_type const &>(this->underlying_container) )
                  == indirect_underlying_itr
                 )
                 || this->comp(key, *indirect_underlying_itr)) ?
                --indirect_underlying_itr
                :
                indirect_underlying_itr
            )
            - cues::direct_or_indirect_begin( const_cast<underlying_container_type const &>(this->underlying_container) )
        );

    }


    template<typename VALUE_REF>
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<
                typename std::remove_reference<VALUE_REF>::type
            >::type
            ,
            value_type
        >::value,
    std::pair<iterator, bool>>::type insert( VALUE_REF && value )
    {
        auto lb_itr = cues::direct_or_indirect_begin(this->underlying_container) + this->idx_lower_bound(value.first);
        // does it already exist?
        // lower bound means key <= element
        // !(key < element), thus equal, thus blocked
        if ( lb_itr != cues::direct_or_indirect_end(this->underlying_container)
            && !this->comp(value.first, *lb_itr)
        ) {
            return {iterator{cues::make_direct(this->underlying_container, lb_itr)}, false};
        }
        // otherwise less; we can insert
        return
        {
            // return our iterator, not its
            iterator{
                this->underlying_container.insert(
                    // give it its iterator, not ours
                    cues::make_direct(this->underlying_container, lb_itr),
                    std::forward<VALUE_REF>(value)
                )
            },
            true
        };
    }

    template<typename CONVERTIBLE>
    typename std::enable_if<
        std::is_constructible<value_type, CONVERTIBLE &&>::value
        &&
        !std::is_same<
            typename std::remove_const<
                typename std::remove_reference<CONVERTIBLE>::type
            >::type
            ,
            value_type
        >::value,
    std::pair<iterator, bool>>::type insert( CONVERTIBLE && v )
    {
        return this->emplace(std::forward<CONVERTIBLE>(v));
    }

    template<typename VALUE_REF>
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<
                typename std::remove_reference<VALUE_REF>::type
            >::type
            ,
            value_type
        >::value,
    iterator>::type insert( const_iterator hint, VALUE_REF && value )
    {
        hint_quality hq = check_hint_quality(
             cues::make_indirect(this->underlying_container, static_cast<underlying_const_iterator_type>(hint)),
             value.first
        );

        switch (hq)
        {
            case hint_quality::inaccurate:
                return insert(std::forward<VALUE_REF>(value)).first;
            case hint_quality::insertable:
                return iterator{
                    this->underlying_container.insert(
                        static_cast<underlying_const_iterator_type>(hint),
                        std::forward<VALUE_REF>(value)
                    )
                };
            case hint_quality::blocked:
                break;
            default:
                assert(((void)"failed to consider all switch types", false));
        }
        return begin() + (
            (
                ( hint == this->cend()  || this->comp(value.first, *hint)) ?
                --hint
                :
                hint
            )
        - cbegin());
    }

    template<typename CONVERTIBLE>
    typename std::enable_if<
        std::is_constructible<value_type, CONVERTIBLE &&>::value
        &&
        !std::is_same<
            typename std::remove_const<
                typename std::remove_reference<CONVERTIBLE>::type
            >::type
            ,
            value_type
        >::value,
    std::pair<iterator, bool>>::type insert( const_iterator hint, CONVERTIBLE && v )
    {
        return this->emplace_hint(hint, std::forward<CONVERTIBLE>(v));
    }

    // if we have the same key comparison,
    // assignable value type,
    // and random access iterator, permit batch insertion
    template<class Other_Associative>
    typename std::enable_if<
           is_associative_with_same_comparison<pool_map, Other_Associative>::value
        && std::is_base_of<
            std::random_access_iterator_tag,
            typename std::iterator_traits<typename Other_Associative::const_iterator>::iterator_category
        >::value
    >::type insert(typename Other_Associative::const_iterator src_begin, typename Other_Associative::const_iterator src_end)
    {
        this->insert_sorted_range(src_begin, src_end);
    }

    template<class Other_Container>
    typename std::enable_if<
           !is_associative_with_same_comparison<pool_map, Other_Container>::value
        || !std::is_base_of<
            std::random_access_iterator_tag,
            typename std::iterator_traits<typename Other_Container::const_iterator>::iterator_category
        >::value
    >::type insert( typename Other_Container::const_iterator first, typename Other_Container::const_iterator last )
    {
        for( ;
            first != last;
            ++first
        ) {
            insert(*first);
        }
    }

    void insert(std::initializer_list<value_type> il)
    {
        insert(il.begin(), il.end());
    }


    insert_return_type insert(node_type && node)
    {
        if (node.empty() || node.get_allocator() != this->get_allocator())
        {
            return {
                end(),
                false,
                std::move(node)
            };
        }
        auto lb_itr = cbegin() + this->idx_lower_bound(node.key());

        if (lb_itr == cend() || this->comp(static_cast<key_type const &>(node.key()), *lb_itr))
        {
            return {
                this->checked_insert(
                    static_cast<underlying_const_iterator_type const &>(lb_itr),
                    std::move(node)
                ),
                true,
                node_type{nullptr, this->get_allocator()}
            };
        }

        return {
            begin() + (lb_itr - cbegin()),
            false,
            std::move(node)
        };
    }

    iterator insert(const_iterator hint, node_type && node)
    {
        if (node.empty() || node.get_allocator() != this->get_allocator())
        {
            return end();
        }
        hint_quality hq = check_hint_quality(
             cues::make_indirect(this->underlying_container, static_cast<underlying_const_iterator_type const &>(hint)),
             (*node).first
        );

        switch (hq)
        {
            case hint_quality::inaccurate:
                return insert(std::move(node)).position;
            case hint_quality::insertable:
                return this->checked_insert(static_cast<underlying_const_iterator_type>(hint), std::move(node));
            case hint_quality::blocked:
                break;
            default:
                assert(((void)"failed to consider all switch types", false));
        }
        return begin() + (
            (
                ( hint == this->cend()  || this->comp((*node).first, *hint)) ?
                --hint
                :
                hint
            )
       - cbegin()) ;
    }

    template <class Key_Ref, class Convertible>
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<typename std::remove_reference<Key_Ref>::type>::type,
            key_type
        >::value,
    std::pair<iterator, bool> >::type insert_or_assign( Key_Ref && key, Convertible && obj )
    {
        size_type maybe_idx = this->idx_lower_bound(key);
        if (maybe_idx < this->size() && !comp(key, *(this->cbegin() + maybe_idx)))
        {
            iterator other = this->begin() + maybe_idx;
            *other = std::forward<Convertible>(obj);
            return {other, false};
        }
        return {
            this->insert(
                this->begin() + maybe_idx,
                value_type{std::forward<Key_Ref>(key), std::forward<Convertible>(obj)}
            ),
            true
        };
    }

    template <class Key_Ref, class Convertible>
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<typename std::remove_reference<Key_Ref>::type>::type,
            key_type
        >::value,
    std::pair<iterator, bool> >::type insert_or_assign( const_iterator hint, Key_Ref && key, Convertible && obj )
    {
        hint_quality hq = this->check_hint_quality(
            cues::make_indirect(this->underlying_container, hint),
            key
        );
    }


    private:



    hint_quality check_hint_quality(
        underlying_indirect_const_iterator_type hint,
        key_type const & key
    ) {
        auto const ind_end   = cues::direct_or_indirect_end(this->underlying_container);
        auto const ind_begin = cues::direct_or_indirect_begin(this->underlying_container);
        // hinted at the end, or to a spot comparing greater than key
        if (hint == ind_end || this->comp(key, *hint))
        {
            // hinted at beginning, or to a spot where the previous
            // spot compares less than key
            if (hint == ind_begin || this->comp(*(hint-1u), key))
            {
                return hint_quality::insertable;
            } // not at the beginning, and the previous
            // spot is neither less than key, nor greater than key
            else if (hint != ind_begin && !this->comp(key, *(hint-1u)))
            {
                return hint_quality::blocked;
            }
        }
        // not at the end, and the spot is neither greater than key,
        // nor less than key
        else if (hint != ind_end && !this->comp(*hint, key))
        {
            return hint_quality::blocked;
        }
        // hint is neither to insertable spot or equivalent element
        return hint_quality::inaccurate;
    }

};

template<
    typename Key,
    typename T,
    class Compare,
    class Other_Compare,
    class Container,
    class Other_Container,
    template<typename, typename, typename, typename> class Other_Map
>
typename std::enable_if<
    // a map that we can't trust the comparison on to be unique in our map
    (
        std::is_same<Other_Map<Key, T, Other_Compare, Other_Container >,
                      pool_map<Key, T, Other_Compare, Other_Container>
        >::value
        && !std::is_same<Compare, Other_Compare>::value
    )
    ||
    // or a multimap, which we can't trust to be unique even if the comparison is the same
    std::is_same< Other_Map<Key, T, Other_Compare, Other_Container >,
              pool_multimap<Key, T, Other_Compare, Other_Container>
    >::value
    ,
bool >::type operator==(pool_map< Key, T, Compare,       Container       > const & lhs,
                        Other_Map<Key, T, Other_Compare, Other_Container > const & rhs)
{
    if (lhs.size() != rhs.size())
    {
        return false;
    }
    // lhs has unique keys, so we can iterate and find its keys in the other, then compare values
    // even if the other is multimap, if the size is the same, and each key in the map
    // has a match in the multimap such that key and value compare equal, the containers
    // can be considered to be equivalent
    for (auto litr = lhs.cbegin();
        litr != lhs.cend();
        ++litr
    ) {
        auto ritr = rhs.find(litr->first);
        if (ritr == rhs.cend()
            || *ritr != *litr
        ) {
            return false;
        }
    }
    return true;
}

// the (map, map) and (map, multimap) cases are covered already,
// but we should also have multimap, map
template<
    typename Key,
    typename T,
    class Compare,
    class Other_Compare,
    class Container,
    class Other_Container
>
bool operator==(pool_multimap<Key, T, Other_Compare, Other_Container > const & lhs,
                pool_map<     Key, T, Compare,       Container       > const & rhs)
{
    return rhs == lhs;
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    class Other_Container
>
bool operator==(pool_map<Key, T, Compare, Container> const & lhs, pool_map<Key, T, Compare, Other_Container> const & rhs)
{
    return lhs.equals(rhs);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Other_Compare,
    class Container,
    class Other_Container
>
bool operator!=(pool_map<Key, T, Compare, Container> const & lhs, pool_map<Key, T, Other_Compare, Other_Container> const & rhs)
{
    return !(lhs == rhs);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Other_Compare,
    class Container,
    class Other_Container
>
bool operator!=(pool_map<Key, T, Compare, Container> const & lhs, pool_multimap<Key, T, Other_Compare, Other_Container> const & rhs)
{
    return !(lhs == rhs);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Other_Compare,
    class Container,
    class Other_Container
>
bool operator!=(pool_multimap<Key, T, Compare, Container> const & lhs, pool_map<Key, T, Other_Compare, Other_Container> const & rhs)
{
    return !(rhs == lhs);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container
>
class pool_multimap : private pool_map_impl<Key, T, Compare, Container, true>
{

    enum class hint_quality
    {
        inaccurate,
        insertable
    };

    using impl_type = pool_map_impl<Key, T, Compare, Container, true>;
    using typename impl_type::underlying_iterator_type;
    using typename impl_type::underlying_const_iterator_type;
    using typename impl_type::underlying_indirect_iterator_type;
    using typename impl_type::underlying_indirect_const_iterator_type;

    template<typename FK, typename FT, class FCmp, class FCtr, bool MM>
    friend class pool_map_impl;

    template<typename FK, typename FT, class FCmp, class FCtr>
    friend class pool_map;

    template<typename FK, typename FT, class FCmp, class FCtr>
    friend class pool_multimap;

    public:

    using typename impl_type::key_type;
    using typename impl_type::mapped_type;
    using typename impl_type::value_type;
    using typename impl_type::size_type;
    using typename impl_type::difference_type;
    using typename impl_type::reference;
    using typename impl_type::const_reference;
    using typename impl_type::pointer;
    using typename impl_type::const_pointer;

    using typename impl_type::iterator;
    using typename impl_type::const_iterator;

    using typename impl_type::reverse_iterator;
    using typename impl_type::const_reverse_iterator;

    using typename impl_type::node_type;

    using typename impl_type::underlying_container_type;
    using typename impl_type::allocator_type;

    using typename impl_type::value_compare;
    using typename impl_type::key_compare;

    using impl_type::value_comp;
    using impl_type::key_comp;


    using impl_type::empty;
    using impl_type::size;
    using impl_type::max_size;
    using impl_type::clear;
    using impl_type::erase;

    using impl_type::cbegin;
    using impl_type::begin;
    using impl_type::cend;
    using impl_type::end;

    using impl_type::crbegin;
    using impl_type::rbegin;
    using impl_type::crend;
    using impl_type::rend;

    using impl_type::lower_bound;
    using impl_type::upper_bound;
    using impl_type::equal_range;
    using impl_type::count;
    using impl_type::find;
    using impl_type::contains;

    using impl_type::extract;
    using impl_type::merge;


public:

    pool_multimap() = default;

    explicit pool_multimap(allocator_type const & a)
    : impl_type(a)
    {}

    pool_multimap(Compare const & c, allocator_type const & a)
    : impl_type(c, a)
    {}

    pool_multimap(std::initializer_list<value_type> il, Compare const & c, allocator_type const & a )
    : impl_type(c, a)
    {
        this->insert(il.begin(), il.end());
    }

    template< class... Args >
    iterator emplace( Args && ... args )
    {
        node_type potential_node {cues::allocate_unique<value_type>(this->underlying_container.get_allocator(), std::forward<Args>(args)...)};

        return this->checked_insert(
              this->underlying_container.cbegin() + this->idx_upper_bound((*potential_node).first),
              std::move(potential_node)
        );
    }

    template< class... Args >
    iterator emplace_hint(const_iterator hint,  Args && ... args )
    {
        node_type potential_node {cues::allocate_unique<value_type>(this->underlying_container.get_allocator(), std::forward<Args>(args)...)};

        hint_quality hq = this->check_hint_quality(
            cues::make_indirect(
                this->underlying_container,
                static_cast<underlying_const_iterator_type>(hint)
            ),
            (*potential_node).first
        );

        if (hq == hint_quality::inaccurate)
        {
            hint = this->upper_bound((*potential_node).first);
        }

        return this->checked_insert(
            static_cast<underlying_const_iterator_type>(hint),
            std::move(potential_node)
        );
    }


    template<typename VALUE_REF>
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<
                typename std::remove_reference<VALUE_REF>::type
            >::type
            ,
            value_type
        >::value,
    iterator>::type insert(VALUE_REF && value)
    {
        auto dest_pos = std::upper_bound(
            cues::direct_or_indirect_begin(this->underlying_container),
            cues::direct_or_indirect_end(this->underlying_container),
            value.first,
            this->comp
        );

        return iterator{
            this->underlying_container.insert(
                cues::make_direct(this->underlying_container, dest_pos),
                std::forward<VALUE_REF>(value)
            )
        };
    }

    template<typename CONVERTIBLE>
    typename std::enable_if<
        std::is_constructible<value_type, CONVERTIBLE &&>::value
        &&
        !std::is_same<
            typename std::remove_const<
                typename std::remove_reference<CONVERTIBLE>::type
            >::type
            ,
            value_type
        >::value,
    iterator>::type insert( CONVERTIBLE && v )
    {
        return this->emplace(std::forward<CONVERTIBLE>(v));
    }

    template<typename VALUE_REF>
    typename std::enable_if<
        std::is_same<
            typename std::remove_const<
                typename std::remove_reference<VALUE_REF>::type
            >::type
            ,
            value_type
        >::value,
    iterator>::type insert( const_iterator hint, VALUE_REF && value )
    {
        hint_quality hq = check_hint_quality(
            cues::make_indirect(
                this->underlying_container,
                static_cast<underlying_const_iterator_type>(hint)
            ),
            value.first
        );

        if(hq == hint_quality::insertable)
        {
            return iterator{this->underlying_container.insert(
                static_cast<underlying_const_iterator_type>(hint),
                std::forward<VALUE_REF>(value)
            )};
        }
        return insert(std::forward<VALUE_REF>(value));
    }

    template<typename CONVERTIBLE>
    typename std::enable_if<
        std::is_constructible<value_type, CONVERTIBLE &&>::value
        &&
        !std::is_same<
            typename std::remove_const<
                typename std::remove_reference<CONVERTIBLE>::type
            >::type
            ,
            value_type
        >::value,
    iterator>::type insert( const_iterator hint, CONVERTIBLE && v )
    {
        return this->emplace_hint(hint, std::forward<CONVERTIBLE>(v));
    }

    template<class Other_Associative>
    typename std::enable_if<
           is_associative_with_same_comparison<pool_multimap, Other_Associative>::value
        && std::is_base_of<
            std::random_access_iterator_tag,
            typename std::iterator_traits<typename Other_Associative::const_iterator>::iterator_category
        >::value
    >::type insert(typename Other_Associative::const_iterator src_begin, typename Other_Associative::const_iterator src_end)
    {
        this->insert_sorted_range(src_begin, src_end);
    }

    template<class Other_Associative>
    typename std::enable_if<
           !is_associative_with_same_comparison<pool_multimap, Other_Associative>::value
        || !std::is_base_of<
            std::random_access_iterator_tag,
            typename std::iterator_traits<typename Other_Associative::const_iterator>::iterator_category
        >::value
    >::type insert(typename Other_Associative::const_iterator src_begin, typename Other_Associative::const_iterator src_end)
    {
        for( ;
            src_begin != src_end;
            ++src_begin
        ) {
            insert(*src_begin);
        }
    }

    void insert(std::initializer_list<value_type> il)
    {
        insert(il.begin(), il.end());
    }

    iterator insert(node_type && node)
    {
        if (node.empty() || node.get_allocator() != this->get_allocator())
        {
            return end();
        }

        return this->checked_insert(
              this->underlying_container.cbegin() + this->idx_upper_bound(node.key()),
              std::move(node)
        );
    }

    iterator insert(const_iterator hint, node_type && node)
    {
        if (node.empty() || node.get_allocator() != this->get_allocator())
        {
            return end();
        }
        hint_quality hq = check_hint_quality(
             cues::make_indirect(this->underlying_container, static_cast<underlying_const_iterator_type>(hint)),
             (*node).first
        );

        if(hq == hint_quality::insertable)
        {
            return this->checked_insert(
                    static_cast<underlying_const_iterator_type>(hint),
                    std::move(node)
                );
        }

        return insert(std::move(node));
    }

    template<class Other_Container>
    typename std::enable_if<
        cues::is_equality_comparable<Container, Other_Container>::value,
    bool >::type equals(pool_multimap<key_type, mapped_type, Compare, Other_Container> const & other) const
    {
        return this->underlying_container == other.underlying_container;
    }

    template<class Other_Container>
    typename std::enable_if<
        !cues::is_equality_comparable<Container, Other_Container>::value,
    bool >::type equals(pool_multimap<key_type, mapped_type, Compare, Other_Container> const & other) const
    {
        if (this->size() != other.size())
        {
            return false;
        }
        auto this_itr  = cbegin();
        auto this_end  = cend();
        auto other_itr = other.cbegin();
        auto other_end = other.cend();

        for (;
            this_itr != this_end && other_itr != other_end;
            ++this_itr, ++other_itr
        )
        {
            if (*this_itr != *other_itr)
            {
                return false;
            }
        }
        return true;
    }

    private:


    hint_quality check_hint_quality(
        underlying_indirect_const_iterator_type hint,
        key_type const & key
    ) {
        auto const ind_end = cues::direct_or_indirect_end(this->underlying_container);
        auto const ind_begin = cues::direct_or_indirect_begin(this->underlying_container);

        // if the hint compares greater or we are at the end
        if (hint == ind_end || this->comp(key, *hint))
        {
            // hinted at beginning, or to a spot where the previous
            // spot compares less or equal to key aka
            // ! key < previous
            if (hint == ind_begin || !this->comp(key, *(hint-1u)))
            {
                return hint_quality::insertable;
            }
        }
        return hint_quality::inaccurate;

    }

};

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    class Other_Container
>
bool operator==(pool_multimap<Key, T, Compare, Container> const & lhs, pool_multimap<Key, T, Compare, Other_Container> const & rhs)
{
    return lhs.equals(rhs);
}


template<
    typename Key,
    typename T,
    class Compare,
    class Other_Compare,
    class Container,
    class Other_Container
>
typename std::enable_if<
    !std::is_same<Compare, Other_Compare>::value,
bool>::type operator==(pool_multimap< Key, T, Compare,       Container       > const & lhs,
                       pool_multimap< Key, T, Other_Compare, Other_Container > const & rhs)
{
    using lhs_type = typename std::remove_reference<decltype(lhs)>::type;
    using rhs_type = typename std::remove_reference<decltype(rhs)>::type;
    typename rhs_type::size_type const rsz = rhs.size();
    if (lhs.size() != rsz)
    {
        return false;
    }
    // sizes are the same; if one is zero, both are zero,
    // and empty containers should compare equal
    else if (rsz == 0)
    {
        return true;
    }

    // keys may not be unique, and sorting order may be different.
    // we need to check for equal value_type, but this is not trivial.
    // simply finding a match in rhs for each element in lhs could mean that multimaps with
    // the same unique elements, but different counts, would compare equal
    // (for example, {{1,2},{1,2},{2,3}} would compare equal to {{1,2},{2,3},{2,3}},
    // so what we actually need to check is equal ranges for size and equal mapped_types
    // but, we also want to verify the count of each mapped type, so that, for example,
    // {{1,2},{1,2},{1,3}} does not compare equal to {{1,2},{1,3},{1,3}}, which
    // gets trickier as there is not necessarily an ordering on the mapped_type;
    // the previous example could just as easily be, for example:
    // {{1,3},{1,2},{1,2}} and {{1,3},{1,2},{1,3}}
    // further, we don't want to change the order of existing elements,
    // so we don't want to sort these in-place.  we could use additional memory
    // to sort them, which would speed up checking them, but we don't know in advance
    // how much memory we would need, and, worst case, that means a trip to the heap
    // to get more memory at each key range.

    std::pair<typename lhs_type::const_iterator, typename lhs_type::const_iterator> lhs_range =
        lhs.equal_range(lhs.cbegin()->first);
    std::pair<typename rhs_type::const_iterator, typename rhs_type::const_iterator> rhs_range;


    unary_equal_to<typename lhs_type::value_type> value_equivalence;

    // loop ranges until done (shrinks number of comparisons to make)
    for ( ;
        lhs_range.first != lhs.cend();
        lhs_range = ((lhs_range.second == lhs.cend()) ?
                     std::make_pair(lhs.cend(), lhs.cend())
                     : lhs.equal_range(lhs_range.second->first))
    ) {

        // need to loop elements in lhs subrange
        typename lhs_type::const_iterator before_next_itr;
        for (auto lhs_itr = lhs_range.first;
            lhs_itr != lhs_range.second;
            lhs_itr = before_next_itr + 1
        )
        {
            // update rhs_range
            rhs_range = rhs.equal_range(lhs_itr->first);
            // if no matches, rhs doesn't have the key at all, and we know the containers cannot be equal
            // note that we cannot simply check that the size of the ranges is the same, because the comparison
            // might not even group keys the same way, and we did not
            // put any constraints on how the comparisons would group values.
            // consider: std::less(substr(key, 3)) and one with std::less(strlen(key))
            // with values: {{"abc",1},{"abcd",1},{"abcde",1}}} with each comparison
            // using less(substr) would give an equal range to "abc" of size 3,
            // where less(strlen) would give an equal range to "abc" of size 1,
            // yet the values in the containers are identical
            if ((rhs_range.second - rhs_range.first) == 0)
            {
                return false;
            }

            // could use count_if, but then we loop the subrange twice.
            // optimize for the case where lhs_type::compare orders keys in such a way
            // that keys that compare equal with operator== (or std::equal) are adjacent
            // do linear search because lhs_type::key_compare may order differently than
            // operator== (and thus we cannot guarantee that we won't test this same
            // key again, we just hope to reduce the likelihood of that happening)
            value_equivalence = *lhs_itr;
            before_next_itr = lhs_range.first;
            typename lhs_type::size_type lhs_duplicates = 0;
            bool can_keep_advancing = true;

            for (auto itr = lhs_range.first;
                itr != lhs_range.second;
                ++itr
            )
            {
                bool const equivalent = value_equivalence(*itr);
                lhs_duplicates += static_cast<typename lhs_type::size_type>(equivalent);
                // we need to count elements in the entire subrange, but we only want to advance
                // the next position if we're past our current iterator
                if (itr > lhs_itr && can_keep_advancing)
                {
                    if (equivalent)
                    {
                        before_next_itr = itr;
                    }
                    else
                    {
                        can_keep_advancing = false;
                    }
                }
            }

            // now we can check that rhs has at least that many in the range; since the key
            // compared equal using std::equal_to, however it was sorted, it should have the same number of matches
            // when sorted some other way.
            if (rhs_range.second - rhs_range.first < lhs_duplicates)
            {
                return false;
            }
            // we're not skipping through rhs, so count_if is appropriate here
            typename rhs_type::size_type rhs_matches = std::count_if(rhs_range.first, rhs_range.second, value_equivalence);
            if (lhs_duplicates != rhs_matches)
            {
                return false;
            }
        }

    }
    return true;
}

template<
    typename Key,
    typename T,
    class Compare,
    class Other_Compare,
    class Container,
    class Other_Container
>
bool operator!=(pool_multimap< Key, T, Compare,       Container       > const & lhs,
                pool_multimap< Key, T, Other_Compare, Other_Container > const & rhs
)
{
    return !(lhs == rhs);
}

// inheriting from the iterator to the underlying container lets us
// make use of its functionality, in particular permitting easy
// conversion for passing to member functions of the underlying container
template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
class pool_map_iterator
{
    public:

    using map_impl_type = pool_map_impl<Key, T, Compare, Container, IS_MULTIMAP>;
    friend map_impl_type;

    using map_type = typename std::conditional<
        IS_MULTIMAP,
        pool_multimap<Key, T, Compare, Container>,
        pool_map<Key, T, Compare, Container>
    >::type;
    friend map_type;

    using underlying_container_type = typename map_impl_type::underlying_container_type;
    using underlying_iterator = typename std::conditional<
        IS_CONST_ITERATOR,
        typename underlying_container_type::const_iterator,
        typename underlying_container_type::iterator
    >::type;

    using size_type       = typename Container::size_type;
    using value_type      = typename std::conditional<
        IS_CONST_ITERATOR,
        typename std::add_const<typename map_impl_type::value_type>::type,
        typename map_impl_type::value_type
    >::type;
    using pointer         = typename std::add_pointer<value_type>::type;
    using reference       = typename std::add_lvalue_reference<value_type>::type;
    using difference_type = typename std::iterator_traits<underlying_iterator>::difference_type;

    using iterator_category = typename std::iterator_traits<underlying_iterator>::iterator_category;

    pool_map_iterator() = default;

    explicit constexpr pool_map_iterator(std::nullptr_t) noexcept
    : impl(nullptr)
    {}

    friend class pool_map_iterator<Key,T,Compare,Container,IS_MULTIMAP,!IS_CONST_ITERATOR>;

    //! conditionally enable converting constructor
    template<bool ENABLE_CONST_CONVERSION = IS_CONST_ITERATOR>
    constexpr pool_map_iterator(
        typename std::enable_if<
            ENABLE_CONST_CONVERSION,
            pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, false>
        >::type const & other
    ) noexcept
    : impl(other.impl)
    {}

    constexpr pool_map_iterator & operator=(std::nullptr_t) noexcept
    {
        impl.operator=(nullptr);
        return *this;
    }

    pool_map_iterator & operator--()
    {
        impl.operator--();
        return *this;
    }

    pool_map_iterator operator--(int)
    {
        pool_map_iterator temp{*this};
        operator--();
        return *this;
    }

    pool_map_iterator & operator-=(difference_type val)
    {
        impl.operator-=(val);
        return *this;
    }


    pool_map_iterator & operator++()
    {
        impl.operator++();
        return *this;
    }

    pool_map_iterator operator++(int)
    {
        pool_map_iterator temp{*this};
        operator++();
        return *this;
    }

    pool_map_iterator & operator+=(difference_type val)
    {
        impl.operator+=(val);
        return *this;
    }

    reference operator*() const
    {
        return impl.operator*();
    }

    reference operator[](difference_type n) const
    {
        return impl.operator[](n);
    }

    pointer operator->() const
    {
        return impl.operator->();
    }


    friend constexpr bool operator== (pool_map_iterator const & lhs, std::nullptr_t) noexcept
    {
        return lhs.impl == nullptr;
    }

    friend constexpr bool operator== (pool_map_iterator const & lhs, pool_map_iterator const & rhs) noexcept
    {
        return lhs.impl == rhs.impl;
    }

    //! PRECONDITION: only valid for iterators from the same container
    friend constexpr bool operator< (pool_map_iterator const & lhs, pool_map_iterator const & rhs) noexcept
    {
        return lhs.impl < rhs.impl;
    }

    explicit operator underlying_iterator const & () const
    {
        return impl;
    }

    private:

    explicit pool_map_iterator(underlying_iterator const &i)
    : impl(i)
    {}

    underlying_iterator impl;

};

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
typename pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP,IS_CONST_ITERATOR>::difference_type
operator-(pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP,IS_CONST_ITERATOR> const & lhs,
          pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP,IS_CONST_ITERATOR> const & rhs) noexcept
{
    using underlying_iterator = typename pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP,IS_CONST_ITERATOR>::underlying_iterator;
    return static_cast<underlying_iterator>(lhs) - static_cast<underlying_iterator>(rhs);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> operator+(
    pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> lhs,
    typename pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR>::difference_type rhs)
{
    lhs += rhs;
    return lhs;
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> operator+(
    typename pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR>::difference_type lhs,
    pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> rhs)
{
    return rhs + lhs;
}


template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR>
 operator-(pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> lhs,
           typename pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR>::difference_type rhs)
{
    lhs -= rhs;
    return lhs;
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
constexpr bool operator>  (pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & lhs,
                           pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & rhs) noexcept
{
    return rhs < lhs;
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
constexpr bool operator<= (pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & lhs,
                           pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & rhs) noexcept
{
    return !(rhs < lhs);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
constexpr bool operator>= (pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & lhs,
                           pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & rhs) noexcept
{
    return !(lhs < rhs);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
>
constexpr bool operator!= (pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & lhs,
                           pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & rhs) noexcept
{
    return !(lhs == rhs);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
> constexpr bool operator!= (pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & lhs,
                             std::nullptr_t) noexcept
{
    return !(lhs == nullptr);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
> constexpr bool operator!= (std::nullptr_t,
                             pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & rhs ) noexcept
{
    return !(rhs == nullptr);
}

template<
    typename Key,
    typename T,
    class Compare,
    class Container,
    bool IS_MULTIMAP,
    bool IS_CONST_ITERATOR
> constexpr bool operator== (std::nullptr_t,
                             pool_map_iterator<Key, T, Compare, Container, IS_MULTIMAP, IS_CONST_ITERATOR> const & rhs ) noexcept
{
    return rhs == nullptr;
}

} // namespace cues

#endif // CUES_POOL_MAP_HPP
