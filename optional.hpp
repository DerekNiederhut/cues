#ifndef CUES_OPTIONAL_HPP
#define CUES_OPTIONAL_HPP

//! alternative to std::optional for certain niche uses.
//!
//! In particular, it is meant for use with raw arrays (as opposed to std::array).
//! To accomplish this, it uses proxy types as the reference type aliases
//! which also changes the interface comapred to std::optional, particularly for
//! value() and value_or().
//!
//! Note that the use of the proxy type requires std::launder from c++17,
//! so this code will also make use of other c++17 features, like if constexpr
//!
//! It also allows for specifying larger alignments than what
//! would normally be required by the contained type.  Be cautious
//! using this with an array of optionals, since the additional
//! boolean flag would most likely result in padding.

#if ((__cplusplus < 201703L ) && (!defined ( __cpp_lib_launder) || !defined( __cpp_lib_optional)))
    #error "cues::optional requires std::launder and std::in_place_t"
#endif

#include <new>
#include <initializer_list>
#include <utility>
#include <cstddef>
#include <type_traits>
#include <cassert>
#include <optional> // for std::nullopt_t
#include <cstring>  // for std::memcpy

#include "cues/array_type_traits.hpp"
#include "cues/variadic_type_traits.hpp"
#include "cues/array_proxy.hpp"
#include "cues/new.hpp"

namespace cues {

// helper classes for special function generation
class optional_move_and_copy
{};

class optional_move_only
{
    protected:
    optional_move_only() = default;
    optional_move_only(optional_move_only const &) = delete;
    optional_move_only & operator=(optional_move_only const &) = delete;
    optional_move_only(optional_move_only &&) = default;
    optional_move_only & operator=(optional_move_only &&) = default;
};

class optional_neither_move_nor_copy
{
    protected:
    optional_neither_move_nor_copy() = default;
    optional_neither_move_nor_copy(optional_neither_move_nor_copy const &) = delete;
    optional_neither_move_nor_copy(optional_neither_move_nor_copy &&) = delete;
    optional_neither_move_nor_copy & operator=(optional_neither_move_nor_copy const &) = delete;
    optional_neither_move_nor_copy & operator=(optional_neither_move_nor_copy &&) = delete;
};


// helper class for common implementation and proper special function generation
template<class T, std::size_t DATA_ALIGN>
class optional_base
{
protected:

    using value_type = T;

    using innermost_type = typename std::remove_all_extents<value_type>::type;

    using reference = typename std::conditional<
        cues::is_fixed_size_array<value_type>::value,
        cues::array_proxy<value_type>,
        typename std::add_lvalue_reference<value_type>::type
    >::type;

    using const_reference = typename std::conditional<
        cues::is_fixed_size_array<value_type>::value,
        cues::array_proxy<typename std::add_const<value_type>::type>,
        typename std::add_lvalue_reference<typename std::add_const<value_type>::type>::type
    >::type;

    using extent_sequence_type       = cues::extent_sequence<T>;
    using const_extent_sequence_type = cues::extent_sequence<T const>;

    static_assert(!std::is_array<value_type>::value || cues::is_fixed_size_array<value_type>::value,
        "cues::optional can only store arrays that are fixed in size."
    );
    static_assert(!std::is_reference<T>::value, "cues::optional is not meant to hold a reference (try std::reference_wrapper instead).");
    static_assert(!std::is_same<T, std::in_place_t>::value, "in_place_t is used for tag dispatch, which precludes an optional<in_place_t>.");
    static_assert(!std::is_same<T, std::nullopt_t>::value, "nullopt is used to construct an empty optional, which precludes an optional<nullopt>.");

    template<bool USE_PROXY = std::is_array<value_type>::value>
    typename std::enable_if<USE_PROXY, const_reference>::type as_const_reference() const
    {
        return const_reference(reinterpret_cast<cues::raw_byte_type const *>(indexed_pointer(0)), const_extent_sequence_type{});
    }

    template<bool USE_PROXY = std::is_array<value_type>::value>
    typename std::enable_if<!USE_PROXY, const_reference>::type as_const_reference() const
    {
        return *indexed_pointer(0);
    }
    // note: because array types use a proxy object instead of a reference to the type,
    // we can't use the usual const_cast overload trick here to reduce code duplication,
    // because array_proxy<T const> and array_proxy<T> are distinct types
    template<bool USE_PROXY = std::is_array<value_type>::value>
    typename std::enable_if<USE_PROXY, reference>::type as_reference()
    {
        return reference(reinterpret_cast<cues::raw_byte_type *>(indexed_pointer(0)), extent_sequence_type{});
    }

    template<bool USE_PROXY = std::is_array<value_type>::value>
    typename std::enable_if<!USE_PROXY, reference>::type as_reference()
    {
        return const_cast<reference>(this->as_const_reference());
    }



    optional_base() noexcept
    : obj_flag{false}
    {}

    optional_base(optional_base && other) noexcept(std::is_nothrow_move_constructible<innermost_type>::value)
    : obj_flag{false}
    {
        this->move_or_copy_construct(std::move(other));
    }

    optional_base(optional_base const & other) noexcept(std::is_nothrow_copy_constructible<innermost_type>::value)
    : obj_flag{false}
    {
        this->move_or_copy_construct(other);
    }

    optional_base & operator=(optional_base && from) noexcept(std::is_nothrow_move_assignable<innermost_type>::value)
    {
        // self-move assignment is a programmer error; you have said
        // that we have the only relevant reference to the moved-from object,
        // yet are obviously keeping it if you are assigning to it.
        // If you move assign from an empty, you're left with the result of reset().
        // If you move assign from yourself, you're left with the result of reset().
        // if you move assign from an optional other than this that has a value, you destroy the
        // old contents of this and assign from the contents of the "from".
        // Note that "from" is not marked as having
        // no data here; that is left to the destructor of from.  This provides sensible behavior
        // for both trivial data types (where moves are copies) and types where move truly does
        // "steal" the data (like std::vector or std::unique_ptr).
        this->reset();
        this->move_or_copy_construct(std::move(from));
        return *this;
    }

    optional_base & operator=(optional_base const & from) noexcept(std::is_nothrow_copy_assignable<innermost_type>::value)
    {
        // self-copy assignment is wasteful, but not necessarily an error.
        if (this != &from)
        {
            this->reset();
            this->move_or_copy_construct(from);
        }
        return *this;
    }

    ~optional_base() noexcept(noexcept(optional_base::reset()))
    {
        reset();
    }

    bool has_value() const noexcept
    {
        return obj_flag;
    }

    void reset() noexcept(std::is_nothrow_destructible<innermost_type>::value)
    {
        if (this->obj_flag)
        {
            destruct_backward_from(extent_sequence_type::product()-1);
            this->obj_flag = false;
        }
    }

    //! This is an intentional departure from the interface of std::optional.  Though std::optional
    //! always returns a reference to T, this value() can instead return a proxy type (if T is an array).
    //! This should still be usable when calling operator[] on the resulting array proxy.
    const_reference value() const
    {
        return this->has_value() ? as_const_reference() : throw std::bad_optional_access();
    }

    reference value()
    {
        return this->has_value() ? as_reference() : throw std::bad_optional_access();
    }

    //! This is an intentional departure from the interface of std::optional.
    //! Because arrays cannot be returned by value, and an array could be T (aka value_type),
    //! value_or cannot return value_type.  It can, however, return const_reference, which may
    //! in actuality be a proxy type.  Because of this, it must also take a const_reference.
    //! This will work similarly to std::optional's value_or in a subset of cases.
    //! In particular, it should still work when default_val is a temporary,
    //! since binding to const reference extends the life of a temporary.
    //! For non-array types, it should still work when the result of value_type is used in
    //! assigning to another value_type.  For arrays, the result should still be usable
    //! to call operator[].
    const_reference value_or(const_reference default_val) const
    {
        return this->has_value() ? (this->value()) : default_val;
    }

    //! emplace overload where each member of an initializer list can
    //! be used to construct one type, and any remaining types can be default
    //! constructed.  Will ignore extra initializer list elements.
    template< class U, class Enable = typename std::enable_if<
            is_constructible_from_one_and_remainder_default<T, U>::value
        >::type
    >
    reference emplace(std::initializer_list<U> il)
    noexcept(is_nothrow_constructible_from_one_and_remainder_default<T,U>::value)
    {

        reset();
        flattening_placement_new<T, decltype(il)>(&raw_mem[0], il);
        obj_flag = true;
        return as_reference();
    }

    template<class U, class Enable = typename std::enable_if<
            std::is_same<U, value_type>::value
            && std::is_array<U>::value
        >::type
    >
    reference emplace (U const & array_value) noexcept(noexcept(flattening_placement_new<U>(&(this->raw_mem[0]), array_value)))
    {
        reset();
        flattening_placement_new<U>(&raw_mem[0], array_value);
        obj_flag = true;
        return as_reference();
    }

    //! emplace new value into optional, overload for when innermost_type is constructible
    //! from all arguments and any remaining innermost_type are default constructible
    template< class U, class... Args, class Enable = typename std::enable_if<
            is_constructible_first_from_all_and_remainder_default<T, U, Args...>::value
        >::type
     >
    reference emplace(U first_arg, Args && ... args )
    noexcept(is_nothrow_constructible_first_from_all_and_remainder_default<T, U, Args...>::value)
    {
        reset();
        flattening_placement_new<T, U, Args...>(&raw_mem[0], std::forward<U>(first_arg), std::forward<Args>(args)...);
        obj_flag = true;
        return as_reference();
    }

    //! emplace value when each argument can construct one innermost_type,
    //! and any remaining innermost_types can be default constructed
    template<class... Args, class Enable =  typename std::enable_if<
            cues::is_constructible_some_from_each_and_remainder_default<T, Args...>::value
        >::type
    >
    reference emplace(Args && ... args)
    noexcept(is_nothrow_constructible_some_from_each_and_remainder_default<T, Args...>::value)
    {
        reset();
        flattening_placement_new<T, Args...>(&raw_mem[0], std::forward<Args>(args)...);
        obj_flag = true;
        return as_reference();
    }


    //! emplace overload when single innermost type is constructible from initializer_list and
    //! other template args, and any remaining innermost_type are default constructible
    template< class U, class... Args, class Enable = typename std::enable_if<
            is_constructible_first_from_list_and_remainder_default<T, U, Args...>::value
    >::type>
    reference emplace(
        std::initializer_list<U> il,
        Args && ... args
    )
    noexcept( is_nothrow_constructible_first_from_list_and_remainder_default<T, U, Args...>::value)
    {
        reset();
        flattening_placement_new<T, U, Args...>(&raw_mem[0], il, std::forward<Args>(args)...);
        obj_flag = true;
        return as_reference();
    }

    innermost_type const * indexed_pointer (std::size_t index) const noexcept
    {
        // reinterpreting std::byte pointers is permitted by the c++ aliasing rules
        // to allow for things like byte-by-byte inspection and placement new.
        // so, pointer math is done on the std::byte *, in sizeof(value_type)
        // increments, to get around the fact that we never "create" a value_type array as c++
        // understands it, but rather individual value_type objects located contiguously in the
        // byte array of memory.
        return (has_value() && (index < extent_sequence_type::product())) ?
        flattened_get<T>(&raw_mem[0], index)
        : nullptr;
    }

    innermost_type * indexed_pointer (std::size_t index) noexcept
    {
        // removing const is ok, because "this" was not const before we added const
        return const_cast<innermost_type *>((static_cast<optional_base const &>(*this).indexed_pointer(index)));
    }


    private:

    template<typename OPT_REF>
    void move_or_copy_construct(OPT_REF && other) noexcept(std::is_nothrow_constructible<innermost_type, OPT_REF>::value
                                                            && std::is_nothrow_destructible<innermost_type>::value)
   {
        if (other.has_value())
        {
            std::size_t index = 0;
            try{
                for (std::size_t const index_end = extent_sequence_type::product();
                    index < index_end;
                    ++index)
                {
                    // move or copy construct
                    new (&raw_mem[0] + index*sizeof(innermost_type)) innermost_type{
                        // like std::forward, but for a type other than the function argument type
                        static_cast<
                            typename std::conditional<
                                std::is_rvalue_reference<decltype(other)>::value,
                                innermost_type &&,
                                innermost_type const &
                            >::type
                        >(*other.indexed_pointer(index))
                    };
                }
                obj_flag = true;
            }
            catch(...)
            {
                destruct_backward_from(index);
                if constexpr (!noexcept(this->move_or_copy_construct(std::forward<OPT_REF>(other))))
                {
                    throw;
                }
            }
        }
   }

    void default_construct_remaining(std::size_t index)
    noexcept(std::is_nothrow_default_constructible<value_type>::value)
    {
        flattened_default_construct_remaining<T>(&raw_mem[0], index);
        obj_flag = true;
    }

    void destruct_backward_from(std::size_t index) noexcept(std::is_nothrow_destructible<innermost_type>::value)
    {
        flattened_destruct_backward_from<T>(&raw_mem[0], index);
    }

    static constexpr std::size_t data_alignment = (DATA_ALIGN < alignof(value_type)) ?
            alignof(value_type)
            : DATA_ALIGN;

    alignas(data_alignment) cues::raw_byte_type raw_mem[sizeof(T)];
    bool obj_flag;
};

// forward-declare
template<
    class T,
    std::size_t DATA_ALIGN = alignof(typename std::remove_reference<T>::type)
>
class optional;

template<class T, class U>
struct optional_is_convertible_from
: public std::integral_constant<
    bool,
    !std::is_same<
        std::in_place_t,
        typename std::remove_cv<
            typename std::remove_reference<U>::type
        >::type
    >::value
    &&
    !std::is_same<
        optional<U>,
        typename std::remove_cv<
            typename std::remove_reference<U>::type
        >::type
    >::value
    && std::is_constructible<T, U &&>::value
>
{};


template<
    class T,
    std::size_t DATA_ALIGN
>
class optional
: private optional_base<T, DATA_ALIGN>
, private std::conditional<
    std::is_copy_constructible<
        typename std::remove_all_extents<
            typename std::remove_reference<T>::type
        >::type
    >::value,
    optional_move_and_copy,
    typename std::conditional<
        std::is_move_constructible<
            typename std::remove_all_extents<
                typename std::remove_reference<T>::type
            >::type
        >::value,
        optional_move_only,
        optional_neither_move_nor_copy
    >::type
>::type
{
public:

    using base_type       = optional_base<T, DATA_ALIGN>;

    using reference       = typename base_type::reference;

    using const_reference = typename base_type::const_reference;

    using innermost_type  = typename base_type::innermost_type;

    using value_type      = typename base_type::value_type;

    using base_type::reset;

    using base_type::has_value;

    using base_type::value;

    // see note about value_or in base class
    using base_type::value_or;

    using base_type::emplace;

    constexpr explicit operator bool () const noexcept(noexcept(optional::has_value))
    {
        return this->has_value();
    }

    const_reference operator*() const
    {
        assert(((void)"Attempted to de-reference optional that does not have a value", has_value()));
        return this->as_const_reference();
    }

    reference operator*()
    {
        assert(((void)"Attempted to de-reference optional that does not have a value", has_value()));
        return this->as_reference();
    }

    optional() noexcept
    : base_type()
    {}

    optional(std::nullopt_t) noexcept
    : base_type()
    {}

    //! constructor for array type that does not match other constructors
    template<typename U=value_type, class Enable = typename std::enable_if<
            std::is_same<U, value_type>::value
            && std::is_array<U>::value
        >::type
    >
    optional(U const & array_val)
    : base_type()
    {
        emplace(array_val);
    }

    //! assignment for array
    template<typename U=value_type, class Enable = typename std::enable_if<
            std::is_same<U, value_type>::value
            && std::is_array<U>::value
        >::type
    >
    optional & operator=(U const & array_val)
    {
        this->reset();
        emplace(array_val);
        return *this;
    }

    //! conversion constructor from std::optional for convenience
    //! implicit to simplify use with std::optional since the difference is primarily in implementation.
    template<typename U=value_type, class Enable = typename std::enable_if<optional_is_convertible_from<value_type, U>::value>::type>
    optional(std::optional<U> const & s_opt)
    : base_type()
    {
        if (s_opt.has_value())
        {
            this->emplace(s_opt.value());
        }
    }
    //! conversion assignment from std::optional
    template<typename U=value_type, class Enable = typename std::enable_if<optional_is_convertible_from<value_type, U>::value>::type>
    optional & operator=(std::optional<U> const & s_opt)
    {
        this->reset();
        if (s_opt.has_value())
        {
            this->emplace(s_opt.value());
        }
        return *this;
    }

    //! conversion to std::optional, enabled for non-array types (std::optional cannot be used with arrays)
    template<typename U=value_type, class Enable = typename std::enable_if<!std::is_array<U>::value>::type>
    operator std::optional<U> () const
    {
        return (this->has_value()) ? std::optional<U>(std::in_place, this->value()) : std::optional<U>(std::nullopt);
    }

    optional(optional && other) = default;

    optional(optional const & other) = default;

    optional & operator=(std::nullopt_t) noexcept(std::is_nothrow_destructible<innermost_type>::value)
    {
        this->reset();
        return *this;
    }

    optional & operator=(optional && from) = default;

    optional & operator=(optional const & from) = default;

    template<typename U = value_type>
    optional & operator=(typename std::enable_if<optional_is_convertible_from<value_type,U>::value, U >::type && val)
    {
        reset();
        this->emplace(std::forward<U>(val));
        return *this;
    }

    template<typename U = value_type>
    optional(
        typename std::enable_if<
            optional_is_convertible_from<value_type,U>::value,
        U >::type && val
    )
    noexcept(std::is_nothrow_constructible<value_type, U>::value)
    : base_type()
    {
        this->emplace(std::forward<U>(val));
    }

    //! construct when single innermost_type is constructible from all arguments
    //! and any remaining innermost_type are default constructible
    //! requires at least one argument.  This helps with disambiguation
    //! regarding the overload where the arguments indidividually construct some
    //! innermost_types.  This overload is preferred for exactly one argument.
    //! The overload for each is preferred for exactly zero arguments.
    //! when there are more than one, SFINAE will disambiguate.
    template< class U, class... Args >
    optional(std::in_place_t,
        typename std::enable_if<
            is_constructible_first_from_all_and_remainder_default<T, U, Args...>::value,
            U
        >::type first_arg,
        Args... args)
    noexcept(is_nothrow_constructible_first_from_all_and_remainder_default<T, U, Args...>::value)
    : base_type()
    {
        this->emplace(std::forward<U>(first_arg), std::forward<Args>(args)...);
    }

    //! construct when each argument can construct one innermost_type,
    //! and any remaining innermost_types can be default constructed
    template<class... Args >
    optional(std::in_place_t,
         typename std::enable_if<
            cues::is_constructible_some_from_each_and_remainder_default<T, Args...>::value
            , Args
        >::type ... args
    )
    noexcept(is_nothrow_constructible_some_from_each_and_remainder_default<T, Args...>::value)
    : base_type()
    {
        this->emplace(std::forward<Args>(args)...);
    }

    //! construct when single innermost_type is constructible from initializer_list and other template args,
    //! and any remaining innermost_type are default cosntructible
    template< class U, class... Args>
    optional(
        std::in_place_t,
        std::initializer_list<U> il,
        typename std::enable_if<
            is_constructible_first_from_list_and_remainder_default<T, U, Args...>::value,
            Args
        >::type ... args
    )
    noexcept( is_nothrow_constructible_first_from_list_and_remainder_default<T, U, Args...>::value)
    : base_type()
    {
        this->emplace(il, std::forward<Args>(args)...);
    }

    //! construct when each value in an initializer list can construct an innermost_type
    //! and the type can be default constructed.  The default constructor will be called
    //! for indices not covered by the initializer list.  If the initializer list is
    //! too long, those values will be ignored.
    template< class U>
    optional(
        std::in_place_t,
        typename std::enable_if<
            is_constructible_from_one_and_remainder_default<T, U>::value,
            std::initializer_list<U>
        >::type
        il
    )
    noexcept(is_nothrow_constructible_from_one_and_remainder_default<T,U>::value)
    : base_type()
    {
        this->emplace(il);
    }


};


template<class T>
constexpr bool operator==(optional<T> const & lhs, std::nullopt_t)
{
    return !lhs.has_value();
}

template<class T>
constexpr bool operator==(std::nullopt_t lhs, optional<T> const & rhs)
{
    return (rhs == lhs);
}

template<class T>
constexpr bool operator!=(optional<T> const & lhs, std::nullopt_t rhs)
{
    return !(lhs == rhs);
}

template<class T>
constexpr bool operator!=(std::nullopt_t , optional<T> const & rhs)
{
    return !(rhs == std::nullopt);
}

template<class T>
constexpr bool operator<(optional<T> const &, std::nullopt_t)
{
    return false;
}

template<class T>
constexpr bool operator<(std::nullopt_t, optional<T> const & rhs )
{
    return rhs.has_value();
}

template<class T>
constexpr bool operator>(optional<T> const & lhs, std::nullopt_t rhs)
{
    return rhs < lhs;
}

template<class T>
constexpr bool operator>(std::nullopt_t lhs, optional<T> const & rhs )
{
    return rhs < lhs;
}

template<class T>
constexpr bool operator<=(optional<T> const & lhs, std::nullopt_t rhs)
{
    return !(rhs < lhs);
}

template<class T>
constexpr bool operator<=(std::nullopt_t lhs, optional<T> const & rhs )
{
    return !(rhs < lhs);
}

template<class T>
constexpr bool operator>=(optional<T> const & lhs, std::nullopt_t rhs)
{
    return !(lhs < rhs);
}

template<class T>
constexpr bool operator>=(std::nullopt_t lhs, optional<T> const & rhs )
{
    return !(lhs < rhs);
}

template<class T, class U>
constexpr bool operator==(optional<T> const & lhs, U const & rhs)
{
    return lhs.has_value() ? (*lhs == rhs) : false;
}

template<class T, class U>
constexpr bool operator==(T const & lhs, optional<U> const & rhs)
{
    return (rhs == lhs);
}

template<class T, class U>
constexpr bool operator!=(optional<T> const & lhs, U const & rhs)
{
    return !(lhs == rhs);
}

template<class T, class U>
constexpr bool operator!=(T const & lhs, optional<U> const & rhs)
{
    return !(rhs == lhs);
}

template<class T, class U>
constexpr bool operator<(optional<T> const & lhs, U const & rhs)
{
    return lhs.has_value() ? (*lhs < rhs) : true;
}

template<class T, class U>
constexpr bool operator<(T const & lhs, optional<U> const & rhs)
{
    return rhs.has_value() ? (lhs < *rhs) : false;
}

template<class T, class U>
constexpr bool operator>(optional<T> const & lhs, U const & rhs)
{
    return (rhs < lhs);
}

template<class T, class U>
constexpr bool operator>(T const & lhs, optional<U> const & rhs)
{
    return (rhs < lhs);
}

template<class T, class U>
constexpr bool operator<=(optional<T> const & lhs, U const & rhs)
{
    return !(rhs < lhs);
}

template<class T, class U>
constexpr bool operator<=(T const & lhs, optional<U> const & rhs)
{
    return !(rhs < lhs);
}

template<class T, class U>
constexpr bool operator>=(optional<T> const & lhs, U const & rhs)
{
    return !(lhs < rhs);
}

template<class T, class U>
constexpr bool operator>=(T const & lhs, optional<U> const & rhs)
{
    return !(lhs < rhs);
}

template<class T, class U>
constexpr bool operator==(optional<T> const & lhs, optional<U> const &rhs)
{
    return lhs.has_value() ?
        (*lhs == rhs)
        :
        (std::nullopt == rhs)
    ;
}

template<class T, class U>
constexpr bool operator!=(optional<T> const & lhs, optional<U> const &rhs)
{
    return !(lhs == rhs);
}

template<class T, class U>
constexpr bool operator<(optional<T> const & lhs, optional<U> const &rhs)
{
    return lhs.has_value() ?
        (*lhs < rhs)
        :
        (std::nullopt < rhs)
    ;
}

template<class T, class U>
constexpr bool operator>(optional<T> const & lhs, optional<U> const &rhs)
{
    return rhs < lhs;
}

template<class T, class U>
constexpr bool operator<=(optional<T> const & lhs, optional<U> const &rhs)
{
    return !(rhs < lhs);
}

template<class T, class U>
constexpr bool operator>=(optional<T> const & lhs, optional<U> const &rhs)
{
    return !(lhs < rhs);
}

// avoid overload ambiguity
template<class T, class U>
constexpr bool operator==(optional<T> const & lhs, std::optional<U> const & rhs)
{
    return lhs.has_value() ?
        (*lhs == rhs)
        :
        (std::nullopt == rhs)
    ;
}

template<class T, class U>
constexpr bool operator==(std::optional<T> const & lhs, optional<U> const & rhs)
{
    return (rhs == lhs);
}

template<class T, class U>
constexpr bool operator!=(optional<T> const & lhs, std::optional<U> const & rhs)
{
    return !(lhs == rhs);
}

template<class T, class U>
constexpr bool operator!=(std::optional<T> const & lhs, optional<U> const & rhs)
{
    return !(rhs == lhs);
}

template<class T, class U>
constexpr bool operator<(optional<T> const & lhs, std::optional<U> const & rhs)
{
    return lhs.has_value() ?
        (*lhs < rhs)
        :
        (std::nullopt < rhs)
    ;
}

template<class T, class U>
constexpr bool operator<(std::optional<T> const & lhs, optional<U> const & rhs)
{
    return lhs.has_value() ?
        (*lhs < rhs)
        :
        (std::nullopt < rhs)
    ;
}

template<class T, class U>
constexpr bool operator>(optional<T> const & lhs, std::optional<U> const & rhs)
{
    return (rhs < lhs);
}

template<class T, class U>
constexpr bool operator>(std::optional<T> const & lhs, optional<U> const & rhs)
{
    return (rhs < lhs);
}

template<class T, class U>
constexpr bool operator<=(optional<T> const & lhs, std::optional<U> const & rhs)
{
    return !(rhs < lhs);
}

template<class T, class U>
constexpr bool operator<=(std::optional<T> const & lhs, optional<U> const & rhs)
{
    return !(rhs < lhs);
}

template<class T, class U>
constexpr bool operator>=(optional<T> const & lhs, std::optional<U> const & rhs)
{
    return !(lhs < rhs);
}

template<class T, class U>
constexpr bool operator>=(std::optional<T> const & lhs, optional<U> const & rhs)
{
    return !(lhs < rhs);
}

template<typename T>
optional<typename std::decay<T>::type> make_optional(T && value)
{
    return optional<typename std::decay<T>::type>(std::forward<T>(value));
}

template<typename T, typename...Args>
optional<T> make_optional(Args &&... args)
{
    return optional<T>(std::in_place, std::forward<Args>(args)...);
}

template<typename T, typename U, typename...Args>
optional<T> make_optional(std::initializer_list<U> il, Args &&... args)
{
    return optional<T>(std::in_place, il, std::forward<Args>(args)...);
}

} // namespace cues

#endif // CUES_OPTIONAL_HPP
