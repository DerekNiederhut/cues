
#include "utf8_iterator_test.hpp"
#include <algorithm>

namespace cues {
namespace test {


#ifndef CUES_HAS_NON_CONST_CONSTEXPR

std::size_t check_utf8_iterator_ascii_data()
{
    return std::distance(utf8_begin(valid_ascii_sequence), utf8_end(valid_ascii_sequence))
    != std::distance(&valid_ascii_sequence[0], &valid_ascii_sequence[0]+sizeof(valid_ascii_sequence));
}

std::size_t check_utf8_iterator_valid_data()
{
    return
      static_cast<std::size_t>(!verify_fewer_code_points_than_characters(valid_utf8_sequence_1))
    + static_cast<std::size_t>(!verify_fewer_code_points_than_characters(valid_utf8_sequence_2))
    + static_cast<std::size_t>(!verify_fewer_code_points_than_characters(valid_utf8_sequence_3))
    ;
}

std::size_t check_utf8_iterator_invalid_data_noexcept()
{
    std::size_t acc = 0;
    acc += static_cast<std::size_t>(!verify_skip_fewer_than_replaced(invalid_utf8_sequence_1));
    acc += static_cast<std::size_t>(!verify_skip_fewer_than_replaced(invalid_utf8_sequence_2));
    acc += static_cast<std::size_t>(!verify_skip_fewer_than_replaced(invalid_utf8_sequence_3));
    acc += static_cast<std::size_t>(!verify_utf8_invalid(invalid_utf8_sequence_4));
    acc += static_cast<std::size_t>(!verify_utf8_invalid(invalid_utf8_sequence_5));
    acc += static_cast<std::size_t>(!verify_utf8_invalid(invalid_utf8_sequence_6));
    acc += static_cast<std::size_t>(!verify_fewer_code_points_than_characters(invalid_utf8_sequence_7));
    acc += static_cast<std::size_t>(!verify_utf8_invalid(invalid_utf8_sequence_8));
    acc += static_cast<std::size_t>(!verify_skip_fewer_than_replaced(invalid_utf8_sequence_9));
    acc += static_cast<std::size_t>(!verify_skip_fewer_than_replaced(invalid_utf8_sequence_10));

    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_1) == maximal_subpart_replacement_count_invalid_tests[0]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_2) == maximal_subpart_replacement_count_invalid_tests[1]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_3) == maximal_subpart_replacement_count_invalid_tests[2]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_4) == maximal_subpart_replacement_count_invalid_tests[3]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_5) == maximal_subpart_replacement_count_invalid_tests[4]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_6) == maximal_subpart_replacement_count_invalid_tests[5]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_7) == maximal_subpart_replacement_count_invalid_tests[6]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_8) == maximal_subpart_replacement_count_invalid_tests[7]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_9) == maximal_subpart_replacement_count_invalid_tests[8]))
    acc += static_cast<std::size_t>(!(count_replacement_chars(invalid_utf8_sequence_10) == maximal_subpart_replacement_count_invalid_tests[9]))

    return acc;
}

#endif

template<typename char_type, std::size_t N>
std::size_t require_immediate_replacement(char_type (&str)[N])
{
    using container_type = typename std::remove_reference<decltype(str)>::type;
    std::size_t running_errors = 0;
    try {
        running_errors += static_cast<std::size_t>(unicode_replacement_code !=
            *utf8_begin<container_type, throw_on_invalid_unicode>(str)
        );

        ++running_errors;
    }
    catch(invalid_utf8 const &)
    {}
    return running_errors;
}

template<typename char_type, std::size_t N>
std::size_t require_any_invalid(char_type (&str)[N])
{
    using container_type = typename std::remove_reference<decltype(str)>::type;
    std::size_t running_errors = 0;
    try {
        running_errors += static_cast<std::size_t>(0 == distance(
            utf8_begin<container_type, throw_on_invalid_unicode>(str),
            utf8_end<container_type, throw_on_invalid_unicode>(str)
        ));

        ++running_errors;
    }
    catch(invalid_utf8 const &)
    {}
    return running_errors;
}

template<typename char_type, std::size_t N>
std::size_t require_valid_non_empty(char_type (&str)[N])
{
    using container_type = typename std::remove_reference<decltype(str)>::type;
    std::size_t running_errors = 0;
    try {
        running_errors += static_cast<std::size_t>(0 == distance(
            utf8_begin<container_type, throw_on_invalid_unicode>(str),
            utf8_end<container_type, throw_on_invalid_unicode>(str)
        ));
    }
    catch(invalid_utf8 const &)
    {
        ++running_errors;
        throw;
    }
    return running_errors;
}

template<typename char_type, std::size_t N>
std::size_t require_valid_no_replacement_chars(char_type (&str)[N] )
{
    using container_type = typename std::remove_reference<decltype(str)>::type;
    std::size_t running_errors = 0;
    try {
        running_errors += count_replacement_chars(
            utf8_begin<container_type, throw_on_invalid_unicode>(str),
            utf8_end<container_type, throw_on_invalid_unicode>(str)
        );
    }
    catch(invalid_utf8 const &)
    {
        ++running_errors;
        throw;
    }
    return running_errors;
}

std::size_t check_utf8_iterator_exceptions()
{
    // verify that valid data sets do not throw exceptions
    std::size_t acc = 0;

    acc += require_valid_no_replacement_chars(valid_ascii_sequence);

    acc += require_valid_non_empty(valid_utf8_sequence_1);
    acc += require_valid_non_empty(valid_utf8_sequence_2);
    acc += require_valid_non_empty(valid_utf8_sequence_3);

    acc += require_any_invalid(invalid_utf8_sequence_1);
    acc += require_immediate_replacement(invalid_utf8_sequence_2);
    acc += require_immediate_replacement(invalid_utf8_sequence_3);
    acc += require_any_invalid(invalid_utf8_sequence_4);
    acc += require_any_invalid(invalid_utf8_sequence_5);
    acc += require_any_invalid(invalid_utf8_sequence_6);
    acc += require_any_invalid(invalid_utf8_sequence_7);
    acc += require_any_invalid(invalid_utf8_sequence_8);
    acc += require_immediate_replacement(invalid_utf8_sequence_9);
    acc += require_immediate_replacement(invalid_utf8_sequence_10);

    return acc;
}


std::size_t check_utf8_iterator_test_runtime_assertions()
{
    return
    #ifndef CUES_HAS_NON_CONST_CONSTEXPR
    check_utf8_iterator_ascii_data()
    + check_utf8_iterator_valid_data()
    + check_utf8_iterator_invalid_data_noexcept()
    +
    #endif
    check_utf8_iterator_exceptions()
    ;
}


}
}
