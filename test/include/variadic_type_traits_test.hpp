#ifndef CUES_VARIADIC_TYPE_TRAITS_TEST_HPP
#define CUES_VARIADIC_TYPE_TRAITS_TEST_HPP

#include <type_traits>
#include <cstdint>

#include "cues/variadic_type_traits.hpp"
#include "helper_types.hpp"

namespace cues {
namespace test
{

    static_assert(cues::conjunction<std::true_type, std::true_type, std::true_type>::value, "error: conjunction of true types was false.");
    static_assert(cues::conjunction<std::true_type>::value, "error: conjunction of one true types was false.");
    static_assert(!cues::conjunction<std::false_type>::value, "error: conjunction of one false type was true.");
    static_assert(!cues::conjunction<std::false_type, std::false_type, std::false_type>::value, "error: conjunction of false types was true");
    static_assert(!cues::conjunction<std::true_type, std::false_type, std::true_type>::value, "error: conjunction of mixed types was true.");

    static_assert(!cues::disjunction<std::false_type, std::false_type, std::false_type>::value, "error: disjunction of false types was true");
    static_assert(!cues::disjunction<std::false_type>::value, "error: disjunction of one false type was true");
    static_assert(cues::disjunction<std::true_type>::value, "error: disjunction of one true type was false");
    static_assert(cues::disjunction<std::false_type, std::true_type, std::false_type>::value, "error: disjunction of mixed types was false");
    static_assert(cues::disjunction<std::true_type, std::true_type, std::true_type>::value, "error: disjunction of true types was false");


    static_assert(cues::is_same_as_all<int, int, int, int>::value, "false negative: all types identical");
    static_assert(cues::is_same_as_all<std::false_type, std::false_type, std::false_type, std::false_type>::value, "false negative: all types identical");

    static_assert(!cues::is_same_as_all<int, int, char, int>::value, "false positive: mismatch in check list");
    static_assert(!cues::is_same_as_all<char, int, int, int>::value, "false positive: mismatch in first type");

    static_assert(cues::is_same_as_any<int, char, void, int, unsigned long long>::value, "false negative: one type matches");
    static_assert(!cues::is_same_as_any<int, char, void, unsigned int, unsigned long long>::value, "false positive: no type matches");

    static_assert(cues::is_same_as_none<int, char, void, unsigned int, unsigned long long>::value, "false negative: no types match");
    static_assert(!cues::is_same_as_none<int, char, void, int, unsigned long long>::value, "false positive: one type matches");




    static_assert(cues::any_is_base_of<cues::test::random_test_failure_exception, void, std::runtime_error, cues::test::no_random_failure_tag>::value,
                  "false negative, random test failure exception derives from std::runtime_error");
    static_assert(!cues::any_is_base_of<cues::test::counted_then_random_failure, cues::test::random_test_failure_exception, void, std::runtime_error, cues::test::no_random_failure_tag>::value,
                  "false positive, counted_then_random_failure has base classes but they are not among the list");
    static_assert(cues::is_base_of_any<std::runtime_error, void,  cues::test::random_test_failure_exception, cues::test::no_random_failure_tag>::value,
                  "false negative, random test failure exception derives from std::runtime_error");
    static_assert(!cues::any_is_base_of<std::runtime_error,  cues::test::counted_then_random_failure, cues::test::scoped_only, void, cues::test::no_random_failure_tag>::value,
                  "false positive, no listed class inherits from runtime_error");

    static_assert(cues::all_are_unique<int>::value, "false negative, single type is unique");

    static_assert(!cues::all_are_unique<char, int, int>::value, "false negative, duplicate int type");

    static_assert(cues::all_are_unique<std::runtime_error, void, cues::test::random_test_failure_exception, std::exception, cues::test::no_random_failure_tag, cues::test::scoped_only, cues::test::move_only>::value
                  , "false negative: types have inheritance but are distinct");

    static_assert(!cues::all_are_unique<std::runtime_error, void, cues::test::random_test_failure_exception, std::exception, void, cues::test::no_random_failure_tag, cues::test::scoped_only, cues::test::move_only>::value
                  , "false positive: void matches void");


    static_assert(
        std::is_same<typename cues::nth_type<2, char, short, int, long int>::type, int>::value,
        "false negative on nth type selection"
    );
    static_assert(
        !std::is_same<typename cues::nth_type<0,char, short, int, long int>::type, int>::value,
        "false positive on nth type selection"
    );


    using signed_integral_type_list = cues::type_list<char, short, int, long>;
    using unsigned_integral_type_list = cues::type_list<unsigned char, unsigned short, unsigned int, unsigned long>;
    using nested_integral_type_list = cues::type_list<signed_integral_type_list, unsigned_integral_type_list>;
    using combined_integral_type_list = cues::recursive_concatenate_type_list<bool, signed_integral_type_list, unsigned_integral_type_list>;


    static_assert(cues::is_type_list<signed_integral_type_list>::value, "false negative on signed type list");
    static_assert(!cues::is_type_list<char, short, int, long>::value, "false positive on multiple types that are not a type_list");
    static_assert(!cues::is_nested_type_list<signed_integral_type_list>::value, "false positive on non-nested type list");
    static_assert(cues::is_nested_type_list<nested_integral_type_list>::value, "false negative on nested type list");
    static_assert(!cues::is_nested_type_list<combined_integral_type_list>::value, "false positive on recursively flattened and concatenated type list");


    //! simplest case: identity
    static_assert(
        cues::first_match_index<char, char>::value == 0,
        "first_match_index failed identity case"
    );

    //! non-empty, but still first
    static_assert(
        cues::first_match_index<char, char, unsigned char, signed char>::value == 0,
        "first_match_index failed simple case"
    );

    //! no types to search
    static_assert(
        cues::first_match_index<char>::value == 0,
        "first_match_index failed empty case"
    );

    //! non-empty, not first
    static_assert(
        cues::first_match_index<char, signed char, unsigned char, void, char, char>::value == 3,
        "first_match_index failed moderate test case"
    );

    //! no match case
    static_assert(
        cues::first_match_index<char, void>::value == 1,
        "first_match_index failed not present, non-empty test case"
    );

    static_assert(
        cues::first_match_index<char, void, signed char, unsigned char, unsigned int>::value == 4,
        "first_match_index failed not present, non-empty test case"
    );

    template<typename LHS, typename RHS>
    struct size_is_less : public std::integral_constant<
        bool,
        sizeof(LHS) < sizeof(RHS)
    >
    {};

    using char_arr_2_type = char [2];

    static_assert(
        3 ==
        first_trait_match_index<
            size_is_less,
            char,
            char, signed char, unsigned char, char_arr_2_type
        >::value,
        "first trait match failed case with match at end"
    );
    static_assert(
        0 ==
        first_trait_match_index<
            size_is_less,
            char,
            char_arr_2_type, char, signed char, unsigned char, char_arr_2_type
        >::value,
        "first trait match failed case with match at beginning"
    );

    static_assert(
        0 ==
        first_trait_match_index<
            size_is_less,
            char
        >::value,
        "first trait match failed case with empty haystack"
    );

    static_assert(
        3 ==
        first_trait_match_index<
            size_is_less,
            char,
            char, signed char, unsigned char
        >::value,
        "first trait match failed case with no match when non-empty"
    );


}
} // namespace cues

#endif // CUES_VARIADIC_TYPE_TRAITS_TEST_HPP
