#ifndef CUES_TEST_HELPER_TYPES_HPP_INCLUDED
#define CUES_TEST_HELPER_TYPES_HPP_INCLUDED

#include <cstdint>
#include <atomic>
#include <random>
#include <stdexcept>
#include <limits>
#include <utility> // std::in_place_t
#include <cassert>

#include "cues/version_helpers.hpp"
#include "cues/type_traits.hpp"

namespace cues
{

#if CUES_HAS_IN_PLACE_T
    using in_place_t = std::in_place_t;
#else
    struct in_place_t {};
#endif

namespace test
{

class scoped_only
{
public:
    scoped_only() = default;
    scoped_only(scoped_only const &) = delete;
    scoped_only & operator=(scoped_only const &) = delete;
    scoped_only(scoped_only &&) = delete;
    scoped_only & operator=(scoped_only &&) = delete;
};

class move_only
{
    public:
    move_only() = default;
    move_only(move_only const &) = delete;
    move_only & operator=(move_only const &) = delete;
    move_only(move_only &&) = default;
    move_only & operator=(move_only &&) = default;
};

inline constexpr bool operator==(move_only const & , move_only const & )
{
    return true;
}

inline constexpr bool operator!=(move_only const & lhs, move_only const & rhs)
{
    return !(lhs == rhs);
}

inline constexpr bool operator<(move_only const & , move_only const & )
{
    return false;
}

inline constexpr bool operator>(move_only const & lhs, move_only const & rhs)
{
    return rhs < lhs;
}

inline constexpr bool operator<=(move_only const & lhs, move_only const & rhs)
{
    return !(rhs < lhs);
}

inline constexpr bool operator>=(move_only const & lhs, move_only const & rhs)
{
    return !(lhs < rhs);
}

struct alignas(8) empty_aligned_to_8
{
};

struct alignas(16) empty_aligned_to_16
{
};

using counted_counter_type = std::uint_fast32_t;

template<typename COUNTER_TYPE = counted_counter_type>
class counted
{
public:

    counted() noexcept
    {
        std::atomic_fetch_add_explicit(&times_default_constructed, 1u, std::memory_order_acq_rel);
    }
    counted(counted const &) noexcept
    {
        std::atomic_fetch_add_explicit(&times_copy_constructed, 1u, std::memory_order_acq_rel);
    }

    counted(counted &&) noexcept
    {
        std::atomic_fetch_add_explicit(&times_move_constructed, 1u, std::memory_order_acq_rel);
    }

    explicit counted(in_place_t) noexcept
    {
        std::atomic_fetch_add_explicit(&times_in_place_constructed, 1u, std::memory_order_acq_rel);
    }

    counted & operator=(counted const &)
    {
       std::atomic_fetch_add_explicit(&times_copy_assigned, 1u, std::memory_order_acq_rel);
       return *this;
    }

    counted & operator=(counted &&) noexcept
    {
       std::atomic_fetch_add_explicit(&times_move_assigned, 1u, std::memory_order_acq_rel);
       return *this;
    }

    ~counted()
    {
        COUNTER_TYPE volatile previous_destructed = destructed_count();
        COUNTER_TYPE volatile times_constructed   = total_constructed_count();
        assert(previous_destructed < times_constructed && "destruction > construction");
        std::atomic_fetch_add_explicit(&times_destructed, 1u, std::memory_order_acq_rel);
    }

    static COUNTER_TYPE default_constructed_count() noexcept
    {
        return std::atomic_load_explicit(&times_default_constructed, std::memory_order_acquire);
    }

    static COUNTER_TYPE copy_constructed_count() noexcept
    {
        return std::atomic_load_explicit(&times_copy_constructed, std::memory_order_acquire);
    }

    static COUNTER_TYPE move_constructed_count() noexcept
    {
        return std::atomic_load_explicit(&times_move_constructed, std::memory_order_acquire);
    }

    static COUNTER_TYPE in_place_constructed_count() noexcept
    {
        return std::atomic_load_explicit(&times_in_place_constructed, std::memory_order_acquire);
    }

    static COUNTER_TYPE default_copy_move_constructed_count() noexcept
    {
        return default_constructed_count() + copy_constructed_count() + move_constructed_count();
    }

    //! WARNING: the sum is not atomic, only individual values are.  Be careful using this function.
    static COUNTER_TYPE total_constructed_count() noexcept
    {
        return default_copy_move_constructed_count() + in_place_constructed_count();
    }

    static COUNTER_TYPE copy_assigned_count() noexcept
    {
        return std::atomic_load_explicit(&times_copy_assigned, std::memory_order_acquire);
    }

    static COUNTER_TYPE move_assigned_count() noexcept
    {
        return std::atomic_load_explicit(&times_move_assigned, std::memory_order_acquire);
    }

    //! NOTE: this is not atomic as a whole.  To avoid unreliable data,
    //! only call after multi-context access has finished.
    static bool constructor_destructor_mismatch() noexcept
    {
        return destructed_count() !=
        (
            default_constructed_count()
          + copy_constructed_count()
          + move_constructed_count()
          + in_place_constructed_count()
         );
    }

    static COUNTER_TYPE destructed_count() noexcept
    {
        return std::atomic_load_explicit(&times_destructed, std::memory_order_acquire);
    }

private:

    static std::atomic<COUNTER_TYPE> times_default_constructed;
    static std::atomic<COUNTER_TYPE> times_copy_constructed;
    static std::atomic<COUNTER_TYPE> times_move_constructed;
    static std::atomic<COUNTER_TYPE> times_in_place_constructed;
    static std::atomic<COUNTER_TYPE> times_destructed;
    static std::atomic<COUNTER_TYPE> times_copy_assigned;
    static std::atomic<COUNTER_TYPE> times_move_assigned;

};

template<typename COUNTER_TYPE>
std::atomic<COUNTER_TYPE> counted<COUNTER_TYPE>::times_default_constructed = CUES_ATOMIC_VAR_INIT(0);

template<typename COUNTER_TYPE>
std::atomic<COUNTER_TYPE> counted<COUNTER_TYPE>::times_copy_constructed    = CUES_ATOMIC_VAR_INIT(0);

template<typename COUNTER_TYPE>
std::atomic<COUNTER_TYPE> counted<COUNTER_TYPE>::times_move_constructed    = CUES_ATOMIC_VAR_INIT(0);

template<typename COUNTER_TYPE>
std::atomic<COUNTER_TYPE> counted<COUNTER_TYPE>::times_in_place_constructed = CUES_ATOMIC_VAR_INIT(0);

template<typename COUNTER_TYPE>
std::atomic<COUNTER_TYPE> counted<COUNTER_TYPE>::times_destructed          = CUES_ATOMIC_VAR_INIT(0);

template<typename COUNTER_TYPE>
std::atomic<COUNTER_TYPE> counted<COUNTER_TYPE>::times_copy_assigned       = CUES_ATOMIC_VAR_INIT(0);

template<typename COUNTER_TYPE>
std::atomic<COUNTER_TYPE> counted<COUNTER_TYPE>::times_move_assigned       = CUES_ATOMIC_VAR_INIT(0);

class no_random_failure_tag
{};

class random_test_failure_exception : public std::runtime_error
{
    public:
    explicit random_test_failure_exception(char const * msg)
    : std::runtime_error(msg)
    {}
    random_test_failure_exception() = default;
};


class randomly_fails_construction
{
    public:

    using counter_type = counted_counter_type;
    using integer_type = counted_counter_type;

    static constexpr integer_type const threshold = (std::numeric_limits<integer_type>::max)()/10 + (std::numeric_limits<integer_type>::min)();

    randomly_fails_construction()
    {
        if (post_processor(internal_engine) < threshold)
        {
            std::atomic_fetch_add_explicit(&times_default_thrown, 1u, std::memory_order_release);
            throw random_test_failure_exception("default constructor random failure");
        }
    }

    explicit constexpr randomly_fails_construction(no_random_failure_tag) noexcept
    {}

    randomly_fails_construction(randomly_fails_construction const &)
    {
        if (post_processor(internal_engine) < threshold)
        {
            std::atomic_fetch_add_explicit(&times_copy_thrown, 1u, std::memory_order_release);
            throw random_test_failure_exception("copy constructor random failure");
        }
    }

    randomly_fails_construction(randomly_fails_construction &&) noexcept
    {}

    randomly_fails_construction & operator=(randomly_fails_construction const &) = default;

    static counter_type default_constructor_thrown_count()
    {
        return std::atomic_load_explicit(&times_default_thrown, std::memory_order_acquire);
    }

    static counter_type copy_constructor_thrown_count()
    {
        return std::atomic_load_explicit(&times_copy_thrown, std::memory_order_acquire);
    }

    private:
        static std::mt19937 internal_engine;
        static std::uniform_int_distribution<integer_type> post_processor;

        static std::atomic<counter_type> times_default_thrown;
        static std::atomic<counter_type> times_copy_thrown;

};

// base class constructors go in left to right order

class counted_then_random_failure : public counted<counted_counter_type>, public randomly_fails_construction
{
    public:
    using INTEGER_TYPE = counted_counter_type;
    counted_then_random_failure() = default;
    // to permit construction where we don't want to test behavior when throwing
    explicit counted_then_random_failure(no_random_failure_tag t) noexcept
        : counted<INTEGER_TYPE>()
        , randomly_fails_construction(t)
    {}
    // to defer construction in cases of perfect forwarding
    explicit counted_then_random_failure(in_place_t tag)
        : counted<INTEGER_TYPE>(tag)
        , randomly_fails_construction()
    {}

    counted_then_random_failure(counted_then_random_failure const &) = default;
    counted_then_random_failure(counted_then_random_failure &&) = default;

    counted_then_random_failure & operator=(counted_then_random_failure const &) = default;
};


class random_failure_then_counted : public randomly_fails_construction, public counted<counted_counter_type>
{
    public:
    using INTEGER_TYPE = counted_counter_type;
    random_failure_then_counted() = default;
    explicit random_failure_then_counted(no_random_failure_tag t) noexcept
        : randomly_fails_construction(t)
        , counted<INTEGER_TYPE>()
    {}

    #if CUES_HAS_IN_PLACE_T
    explicit random_failure_then_counted(std::in_place_t tag)
        : randomly_fails_construction()
        , counted<INTEGER_TYPE>(tag)
    {}
    #endif

    random_failure_then_counted(random_failure_then_counted const &) = default;
    random_failure_then_counted(random_failure_then_counted &&) = default;

    random_failure_then_counted & operator=(random_failure_then_counted const &) = default;
};

template<class BUFFER_TYPE>
bool buffer_size_check(BUFFER_TYPE const & buffer)
{
    counted_counter_type total_ctors = counted<counted_counter_type>::total_constructed_count();
    counted_counter_type total_dtors = counted<counted_counter_type>::destructed_count();
    assert((((total_ctors - total_dtors) == buffer.size()) && "buffer size does not match the existing objects"));
    return ((total_ctors - total_dtors) != buffer.size());
}

// in c++20, static_cast will allow conversion from the first element of an aggregate
// to the aggregate.  Prior to that, we need our own trick.
// overload for when types are directly constructible

// trivial case, type is exactly the same
template<typename T>
T make_value(T const & v)
{
    return v;
}
// case where T does not have a member "first",
// and can be constructed from a U const &
template<typename T, typename U>
constexpr typename std::enable_if<
    !std::is_same<T, typename std::remove_const<typename std::remove_reference<U>::type>::type>::value
    // not a pair (or our trivial_aggregate)
    && std::is_same<T, typename cues::type_or_first<T>::type>::value
    && std::is_constructible<T, U const &>::value
, T>::type make_value(U const & v)
{
    return static_cast<T>(v);
}

// case where T does not have a member "first",
// and cannot be constructed from a U const &
// but is default constructible
template<typename T, typename U>
constexpr typename std::enable_if<
    !std::is_same<T, typename std::remove_const<typename std::remove_reference<U>::type>::type>::value
    // not a pair (or our trivial_aggregate)
    && std::is_same<T, typename cues::type_or_first<T>::type>::value
    && !std::is_constructible<T, U const &>::value
    && std::is_default_constructible<T>::value
, T>::type make_value(U const & )
{
    return T{};
}

// case where T has a member "first" and "second",
// and T.first can be constructed from a U const &
template<typename T, typename U>
constexpr typename std::enable_if<
       !std::is_same<T, typename std::remove_const<typename std::remove_reference<U>::type>::type>::value
    && !std::is_same<T, typename cues::type_or_first<T>::type>::value
    &&  std::is_constructible<typename cues::type_or_first<T>::type, U const &>::value
    &&  std::is_same<void, cues::void_t<decltype(std::declval<T>().second)>>::value
, T>::type make_value(U const & v)
{
    using first_type = typename cues::type_or_first<T>::type;
    using second_type = typename std::remove_reference<decltype(std::declval<T>().second)>::type;
    return T{static_cast<first_type>(v), second_type{}};
}

// case where T has a member "first" and "second", and
// T::first also has a member first,
// and T.first.first can be constructed from U
template<typename T, typename U>
constexpr typename std::enable_if<
       !std::is_same<T, typename std::remove_const<typename std::remove_reference<U>::type>::type>::value
    && !std::is_same<T, typename cues::type_or_first<T>::type>::value
    && !std::is_same<
        typename cues::type_or_first<T>::type,
        typename cues::type_or_first<
            typename cues::type_or_first<T>::type
        >::type
    >::value
    &&  std::is_constructible<
        typename cues::type_or_first<
            typename cues::type_or_first<T>::type
        >::type,
        U const &
    >::value
    &&  std::is_same<void, cues::void_t<decltype(std::declval<T>().second)>>::value
, T>::type make_value(U const & v)
{
    using first_type = typename cues::type_or_first<T>::type;
    using second_type = typename std::remove_reference<decltype(std::declval<T>().second)>::type;
    using nested_first_type = typename cues::type_or_first<first_type>::type;
    using nested_second_type = typename std::remove_reference<decltype(std::declval<first_type>().second)>::type;
    return T{{static_cast<nested_first_type>(v), nested_second_type{}}, second_type{}};
}



struct trivial_aggregate
{
    std::uint_least16_t first;
    std::uint_least8_t  second;
    // intentional potential padding for alignment
};

inline bool operator==(trivial_aggregate const & lhs, trivial_aggregate const & rhs)
{
    return (lhs.first == rhs.first) && (lhs.second == rhs.second);
}

inline bool operator!=(trivial_aggregate const & lhs, trivial_aggregate const & rhs)
{
    return !(lhs == rhs);
}

inline bool operator<(trivial_aggregate const & lhs, trivial_aggregate const & rhs)
{
    return (lhs.first < rhs.first) || ((lhs.first == rhs.first) && (lhs.second < rhs.second));
}

inline bool operator>(trivial_aggregate const & lhs, trivial_aggregate const & rhs)
{
    return rhs < lhs;
}

inline bool operator<=(trivial_aggregate const & lhs, trivial_aggregate const & rhs)
{
    return !(rhs < lhs);
}

inline bool operator>=(trivial_aggregate const & lhs, trivial_aggregate const & rhs)
{
    return !(lhs < rhs);
}

static_assert(std::is_trivial<trivial_aggregate>::value, "type that needs to be trivial is not.");

struct multiple_aggregate
{
    trivial_aggregate  * agg_1;
    trivial_aggregate  * agg_2;
    std::uint_least16_t  u8_1;
    std::uint_least16_t  u8_2;
};



template<typename Itr, class Compare>
typename std::enable_if<
    std::is_convertible<
        decltype((std::declval<Compare>())(*(std::declval<Itr>()), *(std::declval<Itr>()))),
        bool
    >::value,
std::size_t >::type container_sort_errors(Itr bg, Itr nd, Compare const & comp)
{
    std::size_t error_count = 0;
    for (auto itr = bg;
        nd - itr > 1;
        ++itr
    ) {
        bool const not_decreasing = !comp(*(itr +1), *(itr ));
        assert(((void)"not sorted", not_decreasing));
        error_count += static_cast<std::size_t>(!not_decreasing);
    }
    return error_count;
}

template<typename Itr, class Compare>
typename std::enable_if<
    !std::is_convertible<
        decltype((std::declval<Compare>())(*(std::declval<Itr>()), *(std::declval<Itr>()))),
        bool
    >::value
    &&
    std::is_convertible<
        decltype((std::declval<Compare>())(*(std::declval<Itr>()).first, *(std::declval<Itr>()).first)),
        bool
    >::value,
std::size_t >::type container_sort_errors(Itr bg, Itr nd, Compare const & comp)
{
    std::size_t error_count = 0;
    for (auto itr = bg;
        nd - itr > 1;
        ++itr
    ) {
        bool const not_decreasing = !comp(*(itr +1).first, *(itr ).first);
        assert(((void)"not sorted", not_decreasing));
        error_count += static_cast<std::size_t>(!not_decreasing);
    }
    return error_count;
}

}
}

#endif // CUES_TEST_HELPER_TYPES_HPP_INCLUDED
