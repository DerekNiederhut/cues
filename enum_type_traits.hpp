#ifndef CUES_ENUM_TYPE_TRAITS_HPP_INCLUDED
#define CUES_ENUM_TYPE_TRAITS_HPP_INCLUDED

#pragma once

#include <type_traits>  // base type trait functionality
#include "cues/type_traits.hpp"

namespace cues
{
    template<typename T, class Enable=void>
    struct type_or_underlying
    {
        using type = T;
    };
    // specialization for enumerations
    template<typename T>
    struct type_or_underlying<T, cues::void_t<typename std::enable_if< std::is_enum<T>::value>::type>>
    {
        using type = typename std::underlying_type<T>::type;
    };
    // specialization for types that provide an underlying_type typedef
    template<typename T>
    struct type_or_underlying<T, cues::void_t<typename T::underlying_type>>
    {
        using type = typename T::underlying_type;
    };

    //! convenience wrapper for cases where either an intger or enum is expected
    template<typename T>
    struct is_integral_or_enum : public std::integral_constant<
        bool,
        std::is_integral<T>::value || std::is_enum<T>::value
        || (std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer)
    >
    {};

    //! convenience wrapper that combines information from std::numeric_limits
    //! on whether the underlying type is unsigned, for example to use when hashing
    //! or other associative key
    template<typename T>
    struct is_or_underlying_is_unsigned
    : public std::integral_constant<
        bool,
            std::numeric_limits<typename cues::type_or_underlying<T>::type>::is_specialized
        && !std::numeric_limits<typename cues::type_or_underlying<T>::type>::is_signed
    >
    {};

    template<typename T>
    struct is_or_underlying_is_signed
    : public std::integral_constant<
        bool,
            std::numeric_limits<typename cues::type_or_underlying<T>::type>::is_specialized
        &&  std::numeric_limits<typename cues::type_or_underlying<T>::type>::is_signed
    >
    {};


}

#endif // CUES_ENUM_TYPE_TRAITS_HPP_INCLUDED
