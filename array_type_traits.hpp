#ifndef CUES_ARRAY_TYPE_TRAITS_HPP_INCLUDED
#define CUES_ARRAY_TYPE_TRAITS_HPP_INCLUDED

#include <cstddef>
#include <type_traits>
#include <limits>


#include "cues/sequence.hpp"
#include "cues/variadic_type_traits.hpp"

namespace cues
{

//! like std::is_array, but rejects arrays of unknown size
template<class T>
struct is_fixed_size_array : public std::false_type {};

template<class T, std::size_t N>
struct is_fixed_size_array<T[N]> : public std::true_type {};

//! like std::is_array, but includes std::array
template<class T>
struct is_array_or_std_array : public std::integral_constant<bool, std::is_array<T>::value>  {};

template<class T, std::size_t N>
struct is_array_or_std_array<std::array<T,N>> : public std::true_type {};

//! like is_fixed_size_array, but includes std::array
template<class T>
struct is_fixed_size_array_or_std_array : public is_fixed_size_array<T> {};

template<class T, std::size_t N>
struct is_fixed_size_array_or_std_array<std::array<T,N>> : public std::true_type {};

//! empty extent sequence by default
template<class T>
struct extent_sequence : public sequence<std::size_t>
{
    static_assert(!std::is_array<T>::value, "Cannot generate a sequence of extents for array of unknown extent.");
};

//! fixed-size arrays inherit from the smaller type's prepend_type, prepending their extent
//! so that the extends are in the same order as std::extent<T, N>
template<class T, std::size_t N  >
struct extent_sequence<T[N] >
: public extent_sequence<
    typename std::remove_extent<T[N]>::type
  >::prepend_type<std::extent<T[N]>::value>
{
};


template<class T, class... Args>
struct is_constructible_first_from_all_and_remainder_default
: public std::integral_constant<
    bool,
    std::is_constructible<typename std::remove_all_extents<T>::type, Args...>::value
    && (1 == cues::extent_sequence<T>::product()
        || std::is_default_constructible<typename std::remove_all_extents<T>::type>::value
    )
>
{};

template<class T, class... Args>
struct is_nothrow_constructible_first_from_all_and_remainder_default
: public std::integral_constant<
    bool,
    std::is_nothrow_constructible<typename std::remove_all_extents<T>::type, Args...>::value
    && (1 == cues::extent_sequence<T>::product()
        || std::is_nothrow_default_constructible<typename std::remove_all_extents<T>::type>::value
    )
>
{};

template<class T, class... Args>
struct is_constructible_some_from_each_and_remainder_default
: public std::integral_constant<
    bool,
    cues::is_constructible_from_each<typename std::remove_all_extents<T>::type, Args...>::value
     &&
    (    (sizeof...(Args)) == cues::extent_sequence<T>::product()
    ||( ((sizeof...(Args)) <  cues::extent_sequence<T>::product())
         && std::is_default_constructible<typename std::remove_all_extents<T>::type>::value)
     )
>
{};

template<class T, class... Args>
struct is_nothrow_constructible_some_from_each_and_remainder_default
: public std::integral_constant<
    bool,
    cues::is_nothrow_constructible_from_each<typename std::remove_all_extents<T>::type, Args...>::value
     &&
    (    (sizeof...(Args)) == cues::extent_sequence<T>::product()
    ||( ((sizeof...(Args)) <  cues::extent_sequence<T>::product())
         && std::is_nothrow_default_constructible<typename std::remove_all_extents<T>::type>::value)
     )
>
{};

template<class T, class U, class... Args>
struct is_constructible_first_from_list_and_remainder_default
: public std::integral_constant<
    bool,
     std::is_constructible<typename std::remove_all_extents<T>::type, std::initializer_list<U>, Args...>::value
    && (   1 == extent_sequence<T>::product()
        || std::is_default_constructible<typename std::remove_all_extents<T>::type>::value
    )
>
{};

template<class T, class U, class... Args>
struct is_nothrow_constructible_first_from_list_and_remainder_default
: public std::integral_constant<
    bool,
     std::is_nothrow_constructible<typename std::remove_all_extents<T>::type, std::initializer_list<U>, Args...>::value
    && (   1 == extent_sequence<T>::product()
        || std::is_nothrow_default_constructible<typename std::remove_all_extents<T>::type>::value
    )
>
{};

template<class T, class U>
struct is_constructible_from_one_and_remainder_default
: public std::integral_constant<
    bool,
    std::is_constructible<typename std::remove_all_extents<T>::type, U>::value
            && std::is_default_constructible<typename std::remove_all_extents<T>::type>::value
>
{};

template<class T, class U>
struct is_nothrow_constructible_from_one_and_remainder_default
: public std::integral_constant<
    bool,
    std::is_nothrow_constructible<typename std::remove_all_extents<T>::type, U>::value
            && std::is_nothrow_default_constructible<typename std::remove_all_extents<T>::type>::value
>
{};





}

#endif // CUES_ARRAY_TYPE_TRAITS_HPP_INCLUDED
