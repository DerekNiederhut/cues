#ifndef CUES_TEST_SEQUENCE_TEST_HPP_INCLUDED
#define CUES_TEST_SEQUENCE_TEST_HPP_INCLUDED

#include "cues/sequence.hpp"

namespace cues
{
namespace test
{
    struct wrapped_value
    {
        public:
        using value_type = int_least32_t;
        constexpr wrapped_value() : val(0) {}
        wrapped_value(wrapped_value const &) = default;
        wrapped_value & operator=(wrapped_value const &) = default;

        explicit constexpr wrapped_value(value_type conv) : val(conv) {}
        constexpr operator value_type() const {return val;}

        friend constexpr wrapped_value operator+(wrapped_value, wrapped_value const &);
        friend constexpr wrapped_value operator*(wrapped_value, wrapped_value const &);

        value_type val;
    };

    constexpr wrapped_value operator+(wrapped_value const & lhs, wrapped_value const & rhs)
    {
        return wrapped_value(lhs.val + rhs.val);
    }

    constexpr wrapped_value operator*(wrapped_value const & lhs, wrapped_value const & rhs)
    {
        return wrapped_value(lhs.val * rhs.val);
    }

    using index_like_test_sequence = cues::sequence<std::size_t, 0,1,2,3,4,5>;

    using null_test_sequence = cues::sequence<int>;

    #if __cpp_nontype_template_args >= 201911L
    using wrapped_test_sequence = cues::sequence<wrapped_value, wrapped_value{-20}, wrapped_value{11}, wrapped_value{-1} wrapped_value{-10}, wrapped_value{21}>;
    #endif

    using integer_test_sequence = cues::sequence<long int, -20, 11, -1, -10, 21>;

    using addition_overflow_test_sequence = cues::sequence<long long int, (std::numeric_limits<long long int>::min)() + 1, -1, -1, -1, -1>;

    using multiplicative_overflow_test_sequence = cues::sequence<long long int, (std::numeric_limits<long long int>::max)(), (std::numeric_limits<long long int>::min)()>;


       static_assert(cues::test::index_like_test_sequence::size()    == 6,
        "Assertion violation: index_like_test_sequence incorrect length");
    static_assert(cues::test::index_like_test_sequence::sum()     == 0+1+2+3+4+5,
        "Assertion violation: index_like_test_sequence sum incorrect");
    static_assert(cues::test::index_like_test_sequence::product() == 0*1*2*3*4*5,
        "Assertion violation: index_like_test_sequence product incorrect");
    static_assert(cues::test::index_like_test_sequence::nth<0>() == 0,
        "Assertion violation: index_like_test_sequence nth<0> incorrect");
    static_assert(cues::test::index_like_test_sequence::nth<1>() == 1,
        "Assertion violation: index_like_test_sequence nth<0> incorrect");
    static_assert(cues::test::index_like_test_sequence::nth<2>() == 2,
        "Assertion violation: index_like_test_sequence nth<0> incorrect");
    static_assert(cues::test::index_like_test_sequence::nth<3>() == 3,
        "Assertion violation: index_like_test_sequence nth<0> incorrect");
    static_assert(cues::test::index_like_test_sequence::nth<4>() == 4,
        "Assertion violation: index_like_test_sequence nth<0> incorrect");
    static_assert(cues::test::index_like_test_sequence::nth<5>() == 5,
        "Assertion violation: index_like_test_sequence nth<0> incorrect");


    static_assert(cues::test::null_test_sequence::size()    == 0,
        "Assertion violation: null_test_sequence incorrect length");
    static_assert(cues::test::null_test_sequence::sum()     == 0,
        "Assertion violation: null_test_sequence sum incorrect");
    static_assert(cues::test::null_test_sequence::product() == 1,
        "Assertion violation: null_test_sequence product incorrect");


    static_assert(cues::test::integer_test_sequence::size() == 5,
        "Assertion violation: integer_test_sequence incorrect length");
    static_assert(cues::test::integer_test_sequence::sum() == -20 + 11 - 1 - 10 + 21,
        "Assertion violation: integer_test_sequence sum incorrect");
    static_assert(cues::test::integer_test_sequence::product() == -20L*11L*-1L*-10L*21L,
        "Assertion violation: integer_test_sequence product incorrect");

    static_assert(cues::test::addition_overflow_test_sequence::product() == (std::numeric_limits<long long int>::min)() +1,
        "Assertion violation: multiplication error near numeric extremes");

    static_assert(cues::test::multiplicative_overflow_test_sequence::sum() <= 0
                  && cues::test::multiplicative_overflow_test_sequence::sum() >= -1,
        "Assertion violation: addition error near numeric extremes.");
}
}

#endif // CUES_TEST_SEQUENCE_TEST_HPP_INCLUDED
