#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <cassert>

#include "type_traits_test.hpp"
#include "enum_type_traits_test.hpp"
#include "variadic_type_traits_test.hpp"
#include "selector_test.hpp"
#include "dereference_test_types.hpp"
#include "sequence_test.hpp"
#include "array_type_traits_test.hpp"
#include "array_proxy_test.hpp"
#include "optional_test.hpp"
#include "triple_buffer_test.hpp"
#include "constexpr_math_test.hpp"
#include "pair_unsigned_hash_test.hpp"
#include "ring_buffer_test.hpp"
#include "fnv_test.hpp"
#include "homogenous_pool_test.hpp"
#include "pool_list_test.hpp"
#include "pool_map_test.hpp"
#include "utf8_iterator_test.hpp"
// #include "fixed_point_test.hpp"

int main(int argc, char * argv[])
{
    std::size_t assertion_failures = 0;
    /*************************************************************************
    * test enum_type_traits
    **************************************************************************/

    // all tests are static_assertions in header file
    assertion_failures +=
        #if defined(CUES_TEST_ENUM_TYPE_TRAITS_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    /*************************************************************************
    * test integer_selector
    **************************************************************************/

    // all tests are static_assertions in header file
    assertion_failures +=
        #if defined(CUES_TEST_SELECTOR_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    /*************************************************************************
    * dereferenced operator
    **************************************************************************/

    // header contains static assertions; failing to check them is a failure
    assertion_failures +=
        #if defined(CUES_TEST_DEREFERENCE_TEST_TYPES_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    assertion_failures += cues::test::check_dereference_nontrivial_iterators();

    /*************************************************************************
    * sequence
    **************************************************************************/

    // header contains static assertions; failing to check them is a failure
    assertion_failures +=
        #if defined(CUES_TEST_SEQUENCE_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    /*************************************************************************
    * array proxy
    **************************************************************************/

    assertion_failures +=
        #if defined(CUES_TEST_ARRAY_PROXY_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    assertion_failures += cues::test::check_array_proxy_runtime_assertions();

    /*************************************************************************
    * optional
    **************************************************************************/

    assertion_failures +=
        #if defined(CUES_TEST_OPTIONAL_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    assertion_failures += cues::test::check_optional_test_runtime_assertions();

    /*************************************************************************
    * triple_buffer
    **************************************************************************/

    assertion_failures +=
        #if defined(CUES_TEST_TRIPLE_BUFFER_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    assertion_failures += cues::test::check_triple_buffer_test_runtime_assertions();

    /*************************************************************************
    * test constexpr math
    **************************************************************************/

    // all tests are static_assertions in header file
    assertion_failures +=
        #if defined(CUES_TEST_CONSTEXPR_MATH_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;



    /*************************************************************************
    * test pair unsigned hash math
    **************************************************************************/

    assertion_failures +=
        #if defined (CUES_TEST_PAIR_UNSIGNED_HASH_TEST_HPP)
        0u
        #else
        1u
        #endif
    ;

    /*************************************************************************
    * test ring buffer
    **************************************************************************/

    assertion_failures +=
        #if defined(CUES_TEST_RING_BUFFER_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    assertion_failures += cues::test::check_ring_buffer_test_runtime_assertions();

    /*************************************************************************
    * test fnv
    **************************************************************************/

     assertion_failures +=
        #if defined(CUES_TEST_FNV_TEST_HPP)
        0u
        #else
        1u
        #endif
    ;

    /*************************************************************************
    * test homogenous pool
    **************************************************************************/


    assertion_failures +=
        #if defined(CUES_TEST_HOMOGENOUS_POOL_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;
    assertion_failures += cues::test::check_homogenous_pool_test_runtime_assertions();

    /*************************************************************************
    * test pool list
    **************************************************************************/

    assertion_failures +=
        #if defined(POOL_LIST_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    assertion_failures += cues::test::check_pool_list_test_runtime_assertions();

    /*************************************************************************
    * test pool map
    **************************************************************************/

    assertion_failures +=
        #if defined(POOL_MAP_TEST_HPP_INCLUDED)
        0u
        #else
        1u
        #endif
    ;

    assertion_failures += cues::test::check_pool_map_test_runtime_assertions();

    /*************************************************************************
    * test utf 8
    **************************************************************************/

    assertion_failures +=
    #if defined(UTF8_ITERATOR_TEST_HPP_INCLUDED)
    0u
    #else
    1u
    #endif
    ;

    assertion_failures += cues::test::check_utf8_iterator_test_runtime_assertions();

    std::cout << "Total assertion failures: " << assertion_failures << std::endl;
    return (assertion_failures == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
