
#include "optional_test.hpp"

#include "array_proxy_test.hpp"

namespace cues
{
namespace test
{
#if CUES_TEST_OPTIONAL

std::size_t check_optional_test_runtime_assertions()
{
    std::size_t assertion_failures = 0;

    // scoped can't move or copy, but should have no value when constructed
    // default or with nullopt, but have value when cosntructed with arguments
    {
        cues::optional<cues::test::scoped_only> scoped_opt;
        if (scoped_opt.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional default constructor did not result in empty optional")));
    }
    {
        cues::optional<cues::test::scoped_only> scoped_opt{std::nullopt};
        if (scoped_opt.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("nullopt optional constructor did not result in empty optional")));
    }
    {
        cues::optional<cues::test::scoped_only> scoped_opt{std::in_place};
        if (!scoped_opt.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional in-place default constructor resulted in empty optional")));
    }
    {
        cues::optional<cues::test::move_only> movee{};
        // move construct from temporary constructed in-place from no arguments
        cues::optional<cues::test::move_only> mover{cues::optional<cues::test::move_only>{std::in_place}};
        if (movee.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional bracketed default constructor resulted in empty optional")));
        if (!mover.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional in-place default constructor resulted in empty optional")));
        // move assign from other
        movee = std::move(mover);
        if (!movee.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional did not have value after move-assignment from valid optional")));
        // move assign from nullopt
        movee = std::nullopt;
        if (movee.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional had value after assignment from nullopt")));
    }

    {
        cues::optional<float> var1;
        cues::optional<float> var2{2.125f};
        cues::optional<float> var3(var2);

        if (static_cast<bool>(var1)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional converted to true when it should not have contained a value")));
        if (!static_cast<bool>(var2)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional did not convert to true when it should have contained a value")));
        if (!static_cast<bool>(var3)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional did not convert to true when it should have contained a value")));

        if (var2 == var1) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that did not have a value compared equal to optional that did")));
        if (var2.value() != 2.125f) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional value() did not return the value the optional contained")));
        if (var2.value_or(5) != 2.125) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional value_or() did not return the value the optional contained")));
        if (var1.value_or(5) != 5) {++assertion_failures;}
        assert((0 == assertion_failures) && (("optional value_or() did not return the default value for an empty optional")));
        // copy assign
        var1 = var2;
        // verify
        if (!static_cast<bool>(var1)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional still empty after copy-assignment")));
        // equality and inequality
        if (var2 != var1) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional compared unequal after copy assignment")));
        if (!(var1 == var3)){++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional did not compare equal after copy assignment")));


        // explicit conversion assignment
        var2 = 3.399f;
        // re-check
        if (!static_cast<bool>(var2)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional did not have value after conversion assignment from value_type")));
        // equality and inequality with another valid optional
        if (var2 == var1) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optionals containing different values compared equal")));
        if (var2 < var1) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional with larger value compared less")));
        if (var3 > var2) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional with smaller value compared greater")));
        if (!(var1 <= var3)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional with smaller value did not compare less or equal")));
        if (!(var3 >= var1)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional with larger value did not compare greater or equal")));
        // equality and inequality with optional and constant
        if (!(2.125f == var1) || !(var1 == 2.125f)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional did not compare equal to literal of its contents")));
        if (var3 != 2.125f || 2.125f != var3) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional compared unequal to literal of its contents")));
        if (var3 < 0.5f || 0.25f > var3) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional compared less than a smaller value")));
        if (var2 >= 10.5f || 8.25f <= var2) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional compared greater than or equal to a larger literal")));
        // equality, valid optional and nullopt
        if (var2 == std::nullopt || std::nullopt == var2) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional containing value compared equal to nullopt")));
        if (var2 < std::nullopt || std::nullopt > var2) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional containing value compared less than nullopt")));
        if (var1 <= std::nullopt || std::nullopt >= var1) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional containing value compared less than or equal to nullopt")));

        // reset
        var3.reset();
        if (static_cast<bool>(var3)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional contains value after reset")));
        // inequality, valid optional and invalid optional
        if (var3 > var1 || var2 < var3) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that contains value compares less than optional that does not")));
        if (var2 <= var3 || var3 >= var1) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that contains value compares less than or equal to optional that does not")));
        // inequality, invalid optional and value
        if (0.5f <= var3 || var3 >= 0.5f) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that contains value compares greater than or equal to literal")));
        if (-1000 < var3 || var3 > -1000) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that contains value compares greater or equal to literal")));
        // equality and inequality, invalid optional and nullopt
        if (std::nullopt != var3 || var3 != std::nullopt) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that does not contain value does compares not equal to nullopt")));
        if (var3 < std::nullopt || std::nullopt < var3) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that does not contain value and nullopt compare less")));
        if (std::nullopt > var3 || var3 > std::nullopt) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that does not contain value and nullopt compare greater")));
        if (!(var3 >= std::nullopt) || !(std::nullopt >= var3)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional that does not contain value and nullopt compare greater than or equal to")));

        var3 = std::optional<float>(2.125f);
        if (var3 != var1) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional did not compare equal to one containing the same value after assignment from std::optional")));
        if (var3 != std::optional<float>(2.125f)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional did not compare equal to std::optional containing the same value")));
        std::optional<float> std_var3 = var3;
        if (std_var3 != 2.125f) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("std::optional did not survive round-trip through optional")));
    }

    {
        cues::test::u16_proxy_3darray_type real_array;
        cues::test::u16_proxy_3darray_type alternate_array;
        // construct proxy with reference to raw array
        cues::array_proxy<cues::test::u16_proxy_3darray_type> proxy_to_real{real_array};
        cues::array_proxy<cues::test::u16_proxy_3darray_type> proxy_to_alternate{alternate_array};
        // fill real array through proxy
        cues::test::fill_3d_forward_via_bracket(proxy_to_real);
        cues::optional<cues::test::u16_proxy_3darray_type> opt_3d_arr_1;
        // construct optional from reference to raw array
        cues::optional<cues::test::u16_proxy_3darray_type> opt_3d_arr_2(real_array);

        // verify default-constructed optional array doesn't have value
        if (opt_3d_arr_1.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional array default constructed as having a value")));
        // verify conversion-constructed optional does
        if (!opt_3d_arr_2.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional array did not have value after conversion construction")));
        // verify that constructed optional compares equal to array
        // note: because real arrays do not support operator==, we are using the proxy here
        if (opt_3d_arr_2 != proxy_to_real || !(proxy_to_real == opt_3d_arr_2)) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("array and optional array did not compare equal")));
        // verify empty array not equal
        if (opt_3d_arr_1 == opt_3d_arr_2) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional arrays equal to empty optional array")));
        // assignment
        opt_3d_arr_1 = real_array;
        if (!opt_3d_arr_1.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional array did not have value after conversion assignment")));

        if (opt_3d_arr_1 != opt_3d_arr_2) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional arrays unequal after assignment from same array")));

        opt_3d_arr_1.value()[0][0][3] += 9;
        if (opt_3d_arr_1 == opt_3d_arr_2) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional arrays equal after modifying one")));
        // copy through temporary proxy
        proxy_to_alternate = opt_3d_arr_1.value_or(proxy_to_real);
        if (proxy_to_alternate == proxy_to_real) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional array with value returning default in value_or")));

        // reset
        opt_3d_arr_1.reset();
        {
            // don't copy, just use proxy to compare
            auto maybe_proxy = opt_3d_arr_1.value_or(proxy_to_real);
            if (maybe_proxy != proxy_to_real) {++assertion_failures;}
            assert(((0 == assertion_failures) && ("optional array without value did not return default in value_or")));
        }
    }

    {
        cues::optional<std::vector<int>> opt_int_vector{{10,0,20,-127,25,90,127}};
        std::size_t const initial_size = opt_int_vector.value().size();
        opt_int_vector.value().push_back(11);
        opt_int_vector.value().push_back(76);
        if (opt_int_vector.value().size() <= initial_size) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("unable to modify optional<vector> through reference")));
    }

    {
        cues::optional<cues::test::move_only_array_type> mo_array_1{std::in_place};
        cues::optional<cues::test::move_only_array_type> mo_array_2;
        if (!mo_array_1.has_value() || mo_array_2.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("optional of move-only array not constructing properly")));

        if (mo_array_1 == mo_array_2) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("empty optional compares equal to non-empty optional")));

        mo_array_2 = std::move(mo_array_1);
        if (!mo_array_2.has_value()) {++assertion_failures;}
        assert(((0 == assertion_failures) && ("moved optional array not valid")));
    }


    return assertion_failures;
}
#else

std::size_t check_optional_test_runtime_assertions()
{
    return 0;
}

#endif
}
}

