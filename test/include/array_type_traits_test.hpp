#ifndef CUES_TEST_ARRAY_TYPE_TRAITS_TEST_HPP_INCLUDED
#define CUES_TEST_ARRAY_TYPE_TRAITS_TEST_HPP_INCLUDED

#include <cstdint>
#include "cues/array_type_traits.hpp"

namespace cues
{
namespace test
{
    using uint_16_nonarray           = std::uint_least16_t;
    using uint_16_array_unknown      = std::uint_least16_t[];
    using uint_16_array_32           = std::uint_least16_t[32];
    using uint_32_array_8_8          = std::uint_least32_t[8][8];
    using uint_32_array_unknown_8    = std::uint_least32_t[][8];
    using uint_16_array_4_8_16       = std::uint_least16_t[4][8][16];
    using uint_16_array_unknown_8_16 = std::uint_least16_t[][8][16];

    static_assert(cues::is_fixed_size_array<uint_16_array_32>::value,
        "Assertion violation: fixed size array not recognized as being fixed in size.");
    static_assert(cues::is_fixed_size_array<uint_32_array_8_8>::value,
        "Assertion violation: fixed size array not recognized as being fixed in size.");
    static_assert(cues::is_fixed_size_array<uint_16_array_4_8_16>::value,
        "Assertion violation: fixed size array not recognized as being fixed in size.");
    static_assert(!cues::is_fixed_size_array<uint_16_array_unknown>::value,
        "Assertion violation: unknown size array recognized as being fixed in size.");
    static_assert(!cues::is_fixed_size_array<uint_32_array_unknown_8>::value,
        "Assertion violation: unknown size array recognized as being fixed in size.");
    static_assert(!cues::is_fixed_size_array<uint_16_array_unknown_8_16>::value,
        "Assertion violation: unknown size array recognized as being fixed in size.");
    static_assert(!cues::is_fixed_size_array<uint_16_nonarray>::value,
        "Assertion violation: non-array type recognized as being fixed in size.");


    using sequence_non_array            = cues::extent_sequence<uint_16_nonarray>;
    using sequence_uint_16_array_32     = cues::extent_sequence<uint_16_array_32>;
    using sequence_uint_32_array_8_8    = cues::extent_sequence<uint_32_array_8_8>;
    using sequence_uint_16_array_4_8_16 = cues::extent_sequence<uint_16_array_4_8_16>;

    static_assert(   sequence_non_array::size() == 0,
        "Assertion violation: single value is not considered a null extent sequence.");

    static_assert(   sequence_uint_16_array_32::size()    == std::rank<uint_16_array_32>::value
                  && sequence_uint_16_array_32::nth<0>()  == std::extent<uint_16_array_32, 0>::value
                  && sequence_uint_16_array_32::product() ==
                  sizeof(uint_16_array_32)/sizeof(typename std::remove_all_extents<uint_16_array_32>::type),
        "Assertion violation: extent_sequence has failed for single-dimension fixed array.");

    static_assert(   sequence_uint_32_array_8_8::size()    == std::rank<uint_32_array_8_8>::value,
        "Assertion violation: sequence size failed for 2-D fixed array");

    static_assert(   sequence_uint_32_array_8_8::nth<0>()  == std::extent<uint_32_array_8_8, 0>::value
                  && sequence_uint_32_array_8_8::nth<1>()  == std::extent<uint_32_array_8_8, 1>::value,
          "Assertion violation: extents failed for 2-D fixed array");

    static_assert( sequence_uint_32_array_8_8::product() ==
                  sizeof(uint_32_array_8_8)/sizeof(typename std::remove_all_extents<uint_32_array_8_8>::type),
        "Assertion violation: total extent size has failed for two-dimensional fixed array.");

    static_assert(   sequence_uint_16_array_4_8_16::size()    == std::rank<uint_16_array_4_8_16>::value,
        "Assertion violation: sequence size failed for 3-D fixed array");

    static_assert(   sequence_uint_16_array_4_8_16::nth<0>()  == std::extent<uint_16_array_4_8_16, 0>::value
                  && sequence_uint_16_array_4_8_16::nth<1>()  == std::extent<uint_16_array_4_8_16, 1>::value
                  && sequence_uint_16_array_4_8_16::nth<2>()  == std::extent<uint_16_array_4_8_16, 2>::value,
          "Assertion violation: extents failed for 3-D fixed array");

    static_assert( sequence_uint_16_array_4_8_16::product() ==
                  sizeof(uint_16_array_4_8_16)/sizeof(typename std::remove_all_extents<uint_16_array_4_8_16>::type),
        "Assertion violation: total extent size has failed for 3-D fixed array.");
}

}

#endif // CUES_TEST_ARRAY_TYPE_TRAITS_TEST_HPP_INCLUDED
