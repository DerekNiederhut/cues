#ifndef CUES_VOID_T_HPP_INCLUDED
#define CUES_VOID_T_HPP_INCLUDED


//! std::void_t if available, otherwise proposed std::void_t implementation
//! from WG21 N3911, 2014-02-23, Walter E. Brown
//! put in a separate header from other type traits because cues::type_traits
//! relies on cues::integer_selector and both of them need void_t
#if __cplusplus > 201703L || ( defined __cpp_lib_void_t && __cpp_lib_void_t >= 201411)

    #include <type_traits>
    namespace cues
    {

    template< typename ... Types>
    using void_t = std::void_t<Types...>;
    }
#else
    namespace cues
    {
    // for defect in c++11 such that template alias would not work by itself
    template<typename ... Types>
    struct voider
    {
        using type = void;
    };

    template< typename ... Types>
    using void_t = typename voider<Types...>::type;
    }
#endif

#endif // CUES_VOID_T_HPP_INCLUDED
