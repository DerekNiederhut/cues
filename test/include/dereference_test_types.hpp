#ifndef CUES_TEST_DEREFERENCE_TEST_TYPES_HPP_INCLUDED
#define CUES_TEST_DEREFERENCE_TEST_TYPES_HPP_INCLUDED

#include <cstdint>
#include <array>
#include <utility>
#include<limits>

#include "cues/dereferenced_operators.hpp"

namespace cues
{
namespace test
{

#if __cplusplus >= 201402L
    template<typename T>
    using less = std::less<T>;
#else
    //! WARNING: not suitable for pointers on implementations that do not
    //! provide a flat address space.
    template<typename T>
    struct less{
        constexpr bool operator()(T const & lhs, T const & rhs) const
        {
            return lhs < rhs;
        }
    };
#endif

using dereferenced_test_type = std::array<std::int_least8_t, 8> ;

constexpr dereferenced_test_type dereferenced_test_set =
{ 127, 126, 1, 0, -1, -64, -127, -128};

static_assert(std::numeric_limits<double>::has_quiet_NaN, "Test data includes quiet_NaN but the type does not support it.");
static_assert(std::numeric_limits<double>::has_infinity, "Test data includes quiet_NaN but the type does not support it.");

using pair_test_type = std::array<std::pair<std::int_least8_t, double>, 8>;

constexpr pair_test_type pair_test_set =
{{
    {127, std::numeric_limits<double>::quiet_NaN()},
    {64,  std::numeric_limits<double>::lowest()},
    {32, -1e6},
    {0,  -1e-12},
    {-32, 0},
    {-64, 1e-12},
    {-127, 1e9},
    {-128, std::numeric_limits<double>::infinity()}
}};


template<class T, class FUNCTOR>
constexpr bool recursive_array_ptr_check_by_ptr(T const * begin_ptr, std::size_t N)
{
    // zero and singel-element containers are sorted by default
    return (N <= 1) ?
        true
        :
        ((FUNCTOR{})(
            &(begin_ptr[N-1]),
            &(begin_ptr[N-2])
        ) && recursive_array_ptr_check_by_ptr<T,FUNCTOR>(begin_ptr, N-1)
    );
}

template<class T, class FUNCTOR>
constexpr bool recursive_array_pair_check_by_ptr(T const * begin_ptr, std::size_t N)
{
    // zero and singel-element containers are sorted by default
    return (N <= 1) ?
        true
        :
        ((FUNCTOR{})(
            (begin_ptr[N-1]),
            (begin_ptr[N-2])
        )
         &&
         (FUNCTOR{})(
            (begin_ptr[N-1].first),
            (begin_ptr[N-2])
        )
          &&
         (FUNCTOR{})(
            (begin_ptr[N-1]),
            (begin_ptr[N-2].first)
        )
         && recursive_array_pair_check_by_ptr<T,FUNCTOR>(begin_ptr, N-1)
    );
}

template<class Itr>
constexpr bool recursive_array_check_by_iterator(Itr current_itr, Itr end_itr)
{
    return (current_itr == end_itr || ++(Itr{current_itr}) == end_itr) ?
            (true) :
            (
                cues::operator_wrapper_dereference< Itr, decltype(*current_itr)>(
                    current_itr,
                    ++(Itr{current_itr})
                )
                && recursive_array_check_by_iterator( ++(Itr{current_itr}), end_itr)
             );
}


// check once without the functor wrapper
static_assert(
    cues::operator_wrapper_dereference<
        cues::test::dereferenced_test_type::value_type const *,
        cues::test::dereferenced_test_type::value_type,
        cues::test::less<cues::test::dereferenced_test_type::value_type>
    >(&(cues::test::dereferenced_test_set[1]), &(cues::test::dereferenced_test_set[0])),
    "Assertion violation: operator_wrapper_dereference failed on pointer "
);
// check all elements with the functor wrapper

static_assert(
    cues::test::recursive_array_ptr_check_by_ptr<
        cues::test::dereferenced_test_type::value_type,
        cues::functor_wrapper_dereference<
            cues::test::dereferenced_test_type::value_type const *,
            cues::test::dereferenced_test_type::value_type,
            cues::test::less<cues::test::dereferenced_test_type::value_type>
        >
    >(
        // .data() not constexpr until c++17
        &(cues::test::dereferenced_test_set[0]),
        cues::test::dereferenced_test_set.size()
    ),
    "Assertion violation: functor_wrapper_dereference failed on pointer."
);

// test pair without functor wrapper: one element
static_assert(
    cues::operator_wrapper_first_of_pair<
        pair_test_type::value_type::first_type,
        pair_test_type::value_type::second_type,
        cues::test::less<pair_test_type::value_type::first_type>
    >(cues::test::pair_test_set[1], cues::test::pair_test_set[0])
    && cues::operator_wrapper_first_of_pair<
        pair_test_type::value_type::first_type,
        pair_test_type::value_type::second_type,
        cues::test::less<pair_test_type::value_type::first_type>
    >(cues::test::pair_test_set[1].first, cues::test::pair_test_set[0])
    && cues::operator_wrapper_first_of_pair<
        pair_test_type::value_type::first_type,
        pair_test_type::value_type::second_type,
        cues::test::less<pair_test_type::value_type::first_type>
    >(cues::test::pair_test_set[1], cues::test::pair_test_set[0].first)
    ,
    "Assertion violation: operator_wrapper_first_of_pair"
);

// test pair with functor: full array
static_assert(
    cues::test::recursive_array_pair_check_by_ptr<
        cues::test::pair_test_type::value_type,
        cues::functor_wrapper_first_of_pair<
            cues::test::pair_test_type::value_type::first_type,
            cues::test::pair_test_type::value_type::second_type,
            cues::test::less<cues::test::pair_test_type::value_type::first_type>
        >
    >(
        // .data() not constexpr until c++17
        &(cues::test::pair_test_set[0]),
        cues::test::pair_test_set.size()
    ),
    "Assertion violation: functor_wrapper_first_of_pair failed."
);

std::size_t check_dereference_nontrivial_iterators();

}
}
#endif // CUES_TEST_DEREFERENCE_TEST_TYPES_HPP_INCLUDED
