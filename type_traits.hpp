#ifndef CUES_TYPE_TRAITS_HPP_INCLUDED
#define CUES_TYPE_TRAITS_HPP_INCLUDED

#include <type_traits>
#include <utility>
#include <tuple>
#include <cstddef>
#include <limits>
#include <iterator>

#if __cplusplus < 202002L
    #include <array>
    #include <vector>
    #if __cplusplus >= 201703L
    #include <string>
    #include <string_view>
    #else
    #include <functional>
    #endif
#endif

#if __cplusplus >= 201703L || defined ( __cpp_lib_launder)
#include "cues/array_proxy.hpp"
#endif

#include "cues/void_t.hpp"
#include "cues/integer_selector.hpp"

namespace cues
{

//! unlike std::make_unsigned, this will pick an integer for any type
//! for which std::numeric_limits<> is specialized based on digits.
//! Note that the underlying mechanism tries to use (pardon the regular expression) std::u?int_least.+_t
//! to pick the smallest possible type (falling back on std::u?intmax_t if it cannot find one that fits),
//! which, depending on compiler (has been seen with gcc), may mean that
//! std::make_unsigned<T>::type differs from cues::make_unsigned<T>::type
//! even for fundamental integer types; for example,
//! cues::make_unsigned<long long int>::type may alias to a different type than unsigned long long
//! (in particular, comparing via std::is_same<> could give you false instead of true)
//! though you should expect the type it picks to have the same number of digits and size on any
//! sane implementation.
template<typename T, typename std::enable_if<std::numeric_limits<T>::is_specialized, bool>::type = true>
struct make_unsigned
{
    using type = typename std::conditional<
        std::numeric_limits<T>::is_integer && !std::numeric_limits<T>::is_signed
        , T
        , typename cues::select_least_integer_by_digits<
            std::numeric_limits<T>::digits,
            cues::integer_selector_signedness::use_unsigned
        >::type
    >::type;
};

//! unlike std::make_signed, this will pick an integer for any type
//! for which std::numeric_limits<> is specialized, and it will
//! pick one with enough digits, not equal size, meaning it may pick
//! a signed type larger than the provided unsigned type in order to fit
//! all possible values of the unsigned type.
template<typename T, typename std::enable_if<std::numeric_limits<T>::is_specialized, bool>::type = true>
struct make_signed
{
    using type = typename std::conditional<
        std::numeric_limits<T>::is_integer && std::numeric_limits<T>::is_signed
        , T
        , typename cues::select_least_integer_by_digits<
            std::numeric_limits<T>::digits,
            cues::integer_selector_signedness::use_signed
        >::type
    >::type;
};

template<typename T>
struct type_or_promoted
{
    using type = typename std::conditional<
        std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer,
        typename std::conditional<
            std::numeric_limits<T>::is_signed,
            typename std::conditional<
                std::numeric_limits<T>::digits < std::numeric_limits<int>::digits,
                int,
                T
            >::type,
            typename std::conditional<
                std::numeric_limits<T>::digits < std::numeric_limits<unsigned int>::digits,
                unsigned int,
                T
            >::type
        >::type,
        T
    >::type;
};

//! std::is_pod is deprecated in c++20, but is convenient in particular
//! since it covers the requirements of std::basic_string and std::string_view.
template<class T>
struct is_pod : public std::integral_constant<
    bool,
    std::is_trivial<T>::value && std::is_standard_layout<T>::value
>
{};

template<class T, class U>
struct pod_or
{
    using type = typename std::conditional<is_pod<T>::value, T, U>::type;
};

//! convenience wrapper
template<class T, class U>
struct is_constructible_or_convertible :
    public std::integral_constant<bool,
        std::is_constructible<T, U>::value
        || std::is_convertible<T,U>::value
    >
{};

template<class ITERATOR_TYPE>
struct is_const_iterator : public std::integral_constant<
    bool,
    std::is_const< typename std::remove_reference< typename std::iterator_traits<ITERATOR_TYPE>::reference>::type>::value
    || std::is_const< typename std::remove_pointer< typename std::iterator_traits<ITERATOR_TYPE>::pointer>::type>::value
    // covers adapter-like iterators that have proxy objects for a reference but still report
    // const value_type
    || std::is_const<typename std::iterator_traits<ITERATOR_TYPE>::value_type>::value
>
{};

template<class ITERATOR_TYPE>
struct is_iterator_to_contiguous_memory : public std::integral_constant<
    bool,
    // first, check via traits
    #if __cplusplus >= 202002L
        // simply ask if contiguous via traits
           std::is_base_of<typename std::iterator_traits<FROM_ITERATOR_TYPE>::iterator_category, std::contiguous_iterator_tag>::value
        || std::is_base_of<typename std::iterator_traits<FROM_ITERATOR_TYPE>::iterator_concept,  std::contiguous_iterator_tag>::value
    #else
        // start with random access and then check if it is a container type that would imply contiguous
        (std::is_base_of<typename std::iterator_traits<ITERATOR_TYPE>::iterator_category, std::random_access_iterator_tag>::value
            && (
                std::is_pointer<ITERATOR_TYPE>::value //< for c-style arrays
                // std::array is also templated on the size, but it would be perverse for
                // std::array to use anything but value_type * as its iterator
                || std::is_same<ITERATOR_TYPE, typename std::vector<typename std::iterator_traits<ITERATOR_TYPE>::value_type>::iterator>::value
                || std::is_same<ITERATOR_TYPE, typename std::vector<typename std::iterator_traits<ITERATOR_TYPE>::value_type>::const_iterator>::value
                // std::span would also fit but is part of c++20 at which point the iterator traits check is sufficient
                // and cleaner
                // string is only guaranteed to be contiguous starting in c++17,
                // string_view doesn't exist until c++17
                #if __cplusplus >= 201703L || ( defined ( __cpp_lib_string_view ) && __cpp_lib_string_view >= 201606L )
                || (   is_pod<typename std::iterator_traits<ITERATOR_TYPE>::value_type>::value
                    &&  (
                         // the short-circuit avoids evaluation, but does not remove the template isntantiation
                         // so we ensure a valid template via conditionally switching in a char type,
                         // using the same is_pod check that will be false if we aren't checking the string
                           std::is_same<ITERATOR_TYPE, typename std::basic_string<typename pod_or<typename std::iterator_traits<ITERATOR_TYPE>::value_type, char>::type>::iterator>::value
                        || std::is_same<ITERATOR_TYPE, typename std::basic_string<typename pod_or<typename std::iterator_traits<ITERATOR_TYPE>::value_type, char>::type>::const_iterator>::value
                        || std::is_same<ITERATOR_TYPE, typename std::basic_string_view<typename pod_or<typename std::iterator_traits<ITERATOR_TYPE>::value_type, char>::type>::iterator>::value
                        || std::is_same<ITERATOR_TYPE, typename std::basic_string_view<typename pod_or<typename std::iterator_traits<ITERATOR_TYPE>::value_type, char>::type>::const_iterator>::value
                    )
                )
                #endif
            )
        )
    #endif
    #if __cplusplus >= 201703L || defined ( __cpp_lib_launder)
        // special case for array_proxy_iterator regardless of version, which lays its elements out contiguously but
        // does not necessarily have defined behavior for the value_type * pointer math required of a
        // contiguous iterator (it uses std::byte * pointer math instead), and so provides
        // random_access_iterator_tag to the trait despite contiguous layout because contiguous iterator
        // assumes value_type * pointer math is defined
        || std::is_array<typename std::iterator_traits<ITERATOR_TYPE>::value_type>::value
        &&
        (
         // same short-circuit problem as with std::string_view
           std::is_same<
                ITERATOR_TYPE,
                typename std::conditional<
                    std::is_array<typename std::iterator_traits<ITERATOR_TYPE>::value_type>::value,
                    array_proxy_iterator<typename std::iterator_traits<ITERATOR_TYPE>::value_type>,
                    void
                >::type
           >::value
        || std::is_same<
                ITERATOR_TYPE,
                typename std::conditional<
                    std::is_array<typename std::iterator_traits<ITERATOR_TYPE>::value_type>::value,
                    array_proxy_iterator<typename std::iterator_traits<ITERATOR_TYPE>::value_type, true>,
                    void
                >::type
            >::value
        )
    #endif
>
{};


template<class ITERATOR_TYPE, typename value_type = typename std::iterator_traits<ITERATOR_TYPE>::value_type>
struct can_memcpy_via_iterator
: public std::integral_constant<
    bool,
    // can the type safely be copied by memcpy
    std::is_trivially_copyable<value_type>::value
    // is the same type (or an element of an array of the same type)
    // this will always be true for value_type = iterator_trats::value_type
    // but the trait may be queried for a type not known to be the same
    && (std::is_same<typename std::remove_cv<
            typename std::remove_reference<decltype(*std::declval<ITERATOR_TYPE>())>::type
        >::type, value_type>::value
    ||  std::is_same<typename std::remove_cv<
            typename std::remove_reference<decltype(*std::declval<ITERATOR_TYPE>())>::type
        >::type, typename std::remove_all_extents<value_type>::type>::value)
    &&
    // is laid out contiguously in memory
    cues::is_iterator_to_contiguous_memory<ITERATOR_TYPE>::value

>
{};

//! generates type of pointer to member function from variadic arguments
template<typename OBJ, typename RETURN, bool IS_NOEXCEPT, typename... Args>
struct pointer_to_member_function
{
    using type = RETURN (OBJ::*)(Args...) noexcept(IS_NOEXCEPT);
};

namespace impl
{
    template<typename T>
    struct impl_remove_noexcept
    {
        using type = T;
    };

    template <typename RETURN, typename ...Args>
    struct impl_remove_noexcept<RETURN(Args...) noexcept>
    {
        using type = RETURN(Args...);
    };

    template <typename OBJ, typename RETURN, typename ...Args>
    struct impl_remove_noexcept<RETURN (OBJ::*)(Args...) noexcept>
    {
        using type = RETURN (OBJ::*)(Args...);
    };
}

template<typename T>
struct remove_noexcept
{
private:
        using fully_stripped_type = typename cues::impl::impl_remove_noexcept<typename std::remove_cv<typename std::remove_reference<T>::type>::type>::type;

        using const_restored_type = typename std::conditional<
            std::is_const<T>::value,
            typename std::add_const<fully_stripped_type>::type,
            fully_stripped_type
        >::type;

        using cv_restored_type = typename std::conditional<
            std::is_volatile<T>::value,
            typename std::add_volatile<const_restored_type>::type,
            const_restored_type
        >::type;

        using reference_to_cv_restored_type = typename std::conditional<
            std::is_lvalue_reference<T>::type,
            typename std::add_lvalue_reference<cv_restored_type>::type,
            typename std::conditional<
                std::is_rvalue_reference<T>::type,
                typename std::add_rvalue_reference<cv_restored_type>::type,
                cv_restored_type
            >::type
        >::type;

public:

    using type = reference_to_cv_restored_type;
};

template<typename T>
struct is_noexcept
: public std::integral_constant<
    bool,
    !std::is_same<T, remove_noexcept<T>>::value
>
{};

template<typename T>
struct has_tuple_element : public std::false_type
{};

template<typename T1, typename T2>
struct has_tuple_element<std::pair<T1, T2>>
: public std::true_type
{};

template<typename ... TALL>
struct has_tuple_element<std::tuple<TALL...>>
: public std::true_type
{};

// useful for finding key values for set or map iterators
template<typename T, typename Selector = void>
struct type_or_first
{
    using type = T;
};

template<typename T>
struct type_or_first<
    T,
    cues::void_t<
        typename std::enable_if<
            has_tuple_element<T>::value
        >::type
    >
>
{
    using type = typename std::tuple_element<0u,T>::type;
};
// for types that do not support tuple_element but have a member named "first"
template<typename T>
struct type_or_first<T,
    cues::void_t<
        typename std::enable_if<
            !has_tuple_element<T>::value
        >::type,
        decltype(std::declval<T>().first)
    >
>
{
    using type = typename std::remove_reference<decltype(std::declval<T>().first)>::type;
};

template<typename T>
constexpr typename std::enable_if<
    std::is_same<
        T,
        typename type_or_first<T>::type
    >::value,
    T
>::type const & value_or_first (T const & val)
{return val;}

template<typename T>
constexpr typename std::enable_if<
    !has_tuple_element<T>::value &&
    std::is_same<
        typename std::remove_reference<decltype(std::declval<T>().first)>::type,
        typename type_or_first<T>::type
    >::value,
    typename type_or_first<T>::type
>::type const & value_or_first (T const & val)
{return val.first;}

template<typename T>
constexpr typename std::enable_if<
    has_tuple_element<T>::value,
    typename type_or_first<T>::type
>::type const & value_or_first (T const & val)
{return std::get<0>(val);}

template<typename T, typename Selector = void>
struct type_or_second
{
    using type = T;
};

template<typename T>
struct type_or_second<T, cues::void_t<decltype(std::declval<T>().second)>>
{
    using type = typename std::remove_reference<decltype(std::declval<T>().second)>::type;
};


template<typename T, typename U>
struct is_similar : public std::integral_constant<
    bool,
        // same, other than top-level cv qualifiers
       std::is_same<typename std::remove_cv<T>::type, typename std::remove_cv<U>::type>::value
     // or, pointers and the pointed-to types are similar
     || (
            std::is_pointer<T>::value && std::is_pointer<U>::value
            && std::is_same<
                typename std::remove_cv<typename std::remove_pointer<T>::type>::type,
                typename std::remove_cv<typename std::remove_pointer<U>::type>::type
            >::value
        )
>
{};

// specialization for pointer to members that forwards to the general template
template<typename CLASS, typename T, typename U>
struct is_similar<T CLASS::*, U CLASS::*> : public is_similar<T,U>
{};

// specialization for fixed arrays
template<typename T, typename U, std::size_t N>
struct is_similar<T[N], U[N]> : public is_similar<T,U>
{};

// specialization for unbounded arrays
template<typename T, typename U>
struct is_similar<T[], U[]> : public is_similar<T,U>
{};

// since c++20, one fixed and one unbounded is also permitted
#if __cplusplus >= 202002L
template<typename T, typename U, std::size_t N>
struct is_similar<T[N], U[]> : public is_similar<T,U>
{};

template<typename T, typename U, std::size_t N>
struct is_similar<T[], U[N]> : public is_similar<T,U>
{};

#endif

template<typename TEST, typename MATCH, class ENABLE=void>
struct is_signed_equivalent : public std::false_type
{};

template<typename TEST, typename MATCH>
struct is_signed_equivalent<
    TEST,
    MATCH,
    cues::void_t<typename std::enable_if<
        ((std::is_integral<TEST>::value && !std::is_same<TEST, bool>::value) || std::is_enum<TEST>::value)
    >::type>
>
: public std::integral_constant<
    bool,
    std::is_same<typename std::make_signed<TEST>::type, MATCH>::value
>
{};

template<typename TEST, typename MATCH, typename ENABLE=void>
struct is_unsigned_equivalent : public std::false_type
{};

template<typename TEST, typename MATCH>
struct is_unsigned_equivalent<TEST, MATCH,
    cues::void_t<typename std::enable_if<
        (std::is_integral<TEST>::value && !std::is_same<TEST, bool>::value) || std::is_enum<TEST>::value
    >::type>
>
: public std::integral_constant<
    bool,
    std::is_same<typename std::make_unsigned<TEST>::type, MATCH>::value
>
{};

template<typename ALIASED_TYPE>
struct is_always_safe_to_alias : public std::integral_constant<
    bool,
    // char and byte may alias anything
       std::is_same<typename std::remove_const<ALIASED_TYPE>::type, char>::value
    || std::is_same<typename std::remove_const<ALIASED_TYPE>::type, unsigned char>::value
    || std::is_same<typename std::remove_const<ALIASED_TYPE>::type, signed char>::value
    #if defined __cpp_lib_byte
    || std::is_same<typename std::remove_const<ALIASED_TYPE>::type, std::byte>::value
    #endif
>
{};

template<typename ALIASED_PTR>
struct ptr_is_always_safe_to_alias : public std::integral_constant<
    bool,
       std::is_pointer<ALIASED_PTR>::value
    && cues::is_always_safe_to_alias<typename std::remove_pointer<ALIASED_PTR>::type>::value
>
{};


//! may be useful for example for SFINAE for low-level structures
//! that use aliasing on raw memory blocks.
template<typename ALIASED_TYPE, typename DYNAMIC_TYPE>
struct is_safe_to_alias : public std::integral_constant<
    bool,
    (
     // types are similar
     cues::is_similar<ALIASED_TYPE, DYNAMIC_TYPE>::value
     // or aliased is (possible cv-qualified) signed variant
     || cues::is_signed_equivalent<typename std::remove_cv<ALIASED_TYPE>::type, DYNAMIC_TYPE>::value
     // or aliased is (possibly cv-qualified) unsigned variant
     || cues::is_unsigned_equivalent<typename std::remove_cv<ALIASED_TYPE>::type, DYNAMIC_TYPE>::value
     || cues::is_always_safe_to_alias<ALIASED_TYPE>::value
    )
>
{};

template<typename ALIASED_PTR, typename DYNAMIC_PTR>
struct ptr_is_safe_to_alias :  public std::integral_constant<
    bool,
       std::is_pointer<ALIASED_PTR>::value
    && std::is_pointer<DYNAMIC_PTR>::value
    && cues::is_safe_to_alias<
        typename std::remove_pointer<ALIASED_PTR>::type,
        typename std::remove_pointer<DYNAMIC_PTR>::type
    >::value
>
{};

template<typename T, typename Selector = void>
struct is_container : public std::false_type
{};

template<typename T>
struct is_container<
    T,
    cues::void_t<
        typename T::value_type,
        typename T::iterator,
        typename T::const_iterator,
        typename T::size_type,
        typename T::difference_type,
        typename std::enable_if<
            std::is_same<
                decltype(std::declval<T &>().begin()),
                typename T::iterator
            >::value
        >::type,
        typename std::enable_if<
            std::is_same<
                decltype(std::declval<T &>().end()),
                typename T::iterator
            >::value
        >::type,
        typename std::enable_if<
            std::is_same<
                decltype(std::declval<T const &>().begin()),
                typename T::const_iterator
            >::value
        >::type,
        typename std::enable_if<
            std::is_same<
                decltype(std::declval<T const &>().end()),
                typename T::const_iterator
            >::value
        >::type,
        typename std::enable_if<
            std::is_same<
                decltype(std::declval<T const &>().size()),
                typename T::size_type
            >::value
        >::type,
        typename std::enable_if<
            std::is_same<
                decltype(std::declval<T const &>().max_size()),
                typename T::size_type
            >::value
        >::type,
        typename std::enable_if<
            std::is_same<
                decltype(std::declval<T const &>().empty()),
                bool
            >::value
        >::type

    >
> : public std::true_type
{};

template<typename T, typename Selector = void>
struct is_associative_container : public std::false_type
{};

template<typename T>
struct is_associative_container<
    T,
    cues::void_t<
        typename std::enable_if<is_container<T>::value>::type,
        typename T::key_type,
        typename T::key_compare,
        typename T::value_compare,
        typename std::enable_if<
            std::is_same<typename T::key_compare, decltype(std::declval<T>().key_comp())>::value
        >::type,
        typename std::enable_if<
            std::is_same<typename T::value_compare, decltype(std::declval<T>().value_comp())>::value
        >::type
    >
>
: public std::true_type
{};

template<typename T, typename U, typename Selector = void>
struct is_associative_with_same_comparison : public std::false_type
{};

template<typename T, typename U>
struct is_associative_with_same_comparison<
    T,
    U,
    cues::void_t<
        typename std::enable_if<
               is_associative_container<T>::value
            && is_associative_container<U>::value
            && std::is_same<typename T::key_type, typename U::key_type>::value
        >::type
    >
>
: public std::true_type
{};

//! like std::equality_comparable, but only checks for operator==,
//! not operator!=, and exists prior to c++20.
template<typename T, typename U = T, typename Selector = void>
struct is_equality_comparable : public std::false_type
{};

template<typename T, typename U>
struct is_equality_comparable<
    T,
    U,
    cues::void_t<typename std::enable_if<
        std::is_same<
            bool,
            typename std::remove_reference<decltype(std::declval<T const &>() == std::declval<U const &>())>::type
        >::value
        &&
        std::is_same<
            bool,
            typename std::remove_reference<decltype(std::declval<U const &>() == std::declval<T const &>())>::type
        >::value
    >::type>
> : public std::true_type
{};

template<typename T, typename Selector = void>
struct is_transparent : public std::false_type
{};

template<typename T>
struct is_transparent<
    T,
    cues::void_t<typename T::is_transparent>
> : public std::true_type
{};

//! note: may be const_iterator if CONTAINER is a const type;
//! if you need a non-const iterator, make sure the type you pass
//! is not const (for example via typename std::remove_const<>::type if necessary)
template<typename CONTAINER>
struct iterator_type
{
    using type = typename std::remove_reference<
        // single reference used here to prevent types without
        // r-value reference qualifier from becoming rvalues, which in turn
        // bind to const reference in std::begin, always returning a const_iterator.
        // reference collapsing allows const types and rvalue types to continue to
        // bind to const reference, but allows types without such qualifiers to pass
        // as non-vonst lvalue references an return an iterator
        decltype(std::begin(std::declval<CONTAINER &>()))
    >::type;
};

//! will add const if necessary
template<typename CONTAINER>
struct const_iterator_type
{
    using type = typename std::remove_reference<
        decltype(std::begin(std::declval<typename std::remove_reference<CONTAINER>::type const &>()))
    >::type;
};


//! NOTE: only works for type parameters
template<class T, typename ... Args>
struct has_exact_template_parameters : public std::false_type
{};


template<template <typename...> class T, typename U, typename ... EXTRAS>
struct has_exact_template_parameters<T<U, EXTRAS...>, U, EXTRAS...> : public std::true_type
{};

//! when the parameters are not important, and we just want to know
//! if a type is a specialization of a template:
template<template<typename ...> class T, class U>
struct is_template_of : public std::false_type
{};

template<template <typename...> class T, typename ...Args>
struct is_template_of<T, T<Args...>> : public std::true_type
{};

template<class T, typename ... OTHERS>
struct parameter_swapped_or_original
{
    using type = T;
};

template< template <typename...> class T, typename ORIG, typename OTHER>
struct parameter_swapped_or_original<T<ORIG>, OTHER>
{
    using type = T<OTHER>;
};

template<typename T>
struct is_character_type : public std::false_type
{};

template<>
struct is_character_type<char> : public std::true_type
{};

template<>
struct is_character_type<signed char> : public std::true_type
{};

template<>
struct is_character_type<unsigned char> : public std::true_type
{};

#if defined (__cpp_char8_t) || __cplusplus >= 202002L
template<>
struct is_character_type<char8_t> : public std::true_type
{};
#endif

template<>
struct is_character_type<wchar_t> : public std::true_type
{};

#if defined (__cpp_unicode_characters) || __cplusplus >= 201103L
template<>
struct is_character_type<char16_t> : public std::true_type
{};

template<>
struct is_character_type<char32_t> : public std::true_type
{};
#endif




}

#endif // CUES_TYPE_TRAITS_HPP_INCLUDED
