#ifndef CUES_FUNCTIONAL_HPP_INCLUDED
#define CUES_FUNCTIONAL_HPP_INCLUDED

#include <cstdint>
#include <type_traits>
#include <utility>
#include <functional>
#include <cassert>

#include "cues/version_helpers.hpp"

#include "cues/type_traits.hpp"

namespace cues
{

#if __cplusplus >= 201402L || defined ( __cpp_lib_transparent_operators )
    using less = std::less<void>;
#else
    struct less
    {
        using is_transparent = std::true_type;
        // if at least one is not a pointer, try to forward to operator<
        template<typename T, typename U>
        constexpr
        typename std::enable_if<
            !std::is_pointer<typename std::remove_reference<T>::type>::value
            ||
            !std::is_pointer<typename std::remove_reference<U>::type>::value
        bool >::type
        operator()(T&& lhs, U&& rhs) const
        {
            return static_cast<bool>(std::forward<T>(lhs) < std::forward<U>(rhs));
        }

        // pointers may not compare correctly with operator<
        // they will compare correctly with std::less, but they could
        // be pointers to different types, with different cv qualifications,
        // so use less<void const volatile *>
        template<typename T, typename U>
        constexpr
        typename std::enable_if<
            std::is_pointer<typename std::remove_reference<T>::type>::value
            &&
            std::is_pointer<typename std::remove_reference<U>::type>::value
        bool >::type
        operator()(T lhs, U rhs) const
        {
            return (std::less<void const volatile *>{}).operator()(
                lhs,
                rhs
            );
        }
    };
#endif

// allows sorting on second element of a tuple or pair;
// can be used for example to change sorting of a std::vector<pair<T1,T2>>
// instead of maintaining a boost::bimap or two boost::flat_maps.
// if you have small containers, and rarely change the sorting, the
// reduced memory footprint may be worth the additional time to resort.
template<class Compare, class T, bool transparent = false>
struct compare_second : public Compare
{
    compare_second() = default;

    constexpr compare_second(Compare const & c)
    : Compare(c)
    {}

    constexpr bool operator() (T const & lhs, T const & rhs) const
    {
        return Compare::operator()(std::get<1>(lhs), std::get<1>(rhs));
    }

    template<typename U = typename std::tuple_element<1, T>::type >
    constexpr bool operator() (U const & lhs, T const & rhs) const
    {
        return Compare::operator()(lhs, std::get<1>(rhs));
    }

    template<typename U = typename std::tuple_element<1, T>::type >
    constexpr bool operator() (T const & lhs, U const & rhs) const
    {
        return Compare::operator()(std::get<1>(lhs), rhs);
    }

    template<typename U = typename std::tuple_element<1, T>::type, typename V = typename std::tuple_element<1, T>::type>
    constexpr bool operator() (U const & lhs, V const & rhs) const
    {
        return Compare::operator()(lhs, rhs);
    }
};

template<class Compare, class T >
struct compare_second<
    Compare, T,
    std::is_same<void, cues::void_t<typename Compare::is_transparent> >::value
>
: public compare_second<Compare, T, false>
{
    using is_transparent = typename Compare::is_transparent;

    compare_second() = default;

    compare_second(Compare const & c)
    : compare_second<Compare, T, false>(c)
    {}

    using compare_second<Compare, T, false>::operator();

};

template<class Compare, class T, bool transparent = false>
struct compare_first_or_value : public Compare
{
    compare_first_or_value() = default;

    constexpr compare_first_or_value(Compare const & c)
    : Compare(c)
    {}

    constexpr bool operator() (T const & lhs, T const & rhs) const
    {
        return Compare::operator()(cues::value_or_first(lhs), cues::value_or_first(rhs));
    }

    template<typename U >
    constexpr bool operator() (U const & lhs, T const & rhs) const
    {
        return Compare::operator()(lhs, cues::value_or_first(rhs));
    }

    template<typename U >
    constexpr bool operator() (T const & lhs, U const & rhs) const
    {
        return Compare::operator()(cues::value_or_first(lhs), rhs);
    }

    template<typename U , typename V >
    constexpr bool operator() (U const & lhs, V const & rhs) const
    {
        return Compare::operator()(lhs, rhs);
    }
};

template<class Compare, class T >
struct compare_first_or_value<
    Compare, T,
    std::is_same<void, cues::void_t<typename Compare::is_transparent> >::value
>
: public compare_first_or_value<Compare, T, false>
{
    using is_transparent = typename Compare::is_transparent;

    compare_first_or_value() = default;

    compare_first_or_value(Compare const & c)
    : compare_first_or_value<Compare, T, false>(c)
    {}

    using compare_first_or_value<Compare, T, false>::operator();

};

// can provide compare type for use with compare_first_or_value when
// the provided compare type is for the pair, instead of for the
// first_type, allowing subcomparisons of a compare rather than
// applying the compare to a subtype of the value
template<class Compare, class T, bool FORCE_ACCEPT_DEFAULTS = false>
struct subcompare_first
{
    using type = typename std::conditional<
        // if the comparison is transparent, we don't need to modify it
        is_transparent<Compare>::value,
        Compare,
        // otherwise instantiate the template used for comparison
        // against the first type instead of the entire type
        typename parameter_swapped_or_original<
            Compare,
            typename type_or_first<T>::type
        >::type
    >::type;

    // the only way this would be correct is if the other template parameters have defaults, and
    // the defaults are correct for *both* Compare<T> and Compare<typename type_or_first<T>::type>
    // which might coincidentally be true, but is not generally true (hence the template parameter
    // FORCE_ACCEPT_DEFAULTS and its default value of false)
    static_assert(is_transparent<Compare>::value
                  || has_exact_template_parameters<Compare, T>::value
                  || FORCE_ACCEPT_DEFAULTS,
        "parameter swap will only work if the template has default parameters that are ok for both types, "
        "and the only way for the template to know this is if you specify parameter FORCE_ACCEPT_DEFAULTS = true");

};

// helper template to decide how to construct the selected compare
template<typename SELECTED_COMPARE, typename ORIGINAL_COMPARE >
typename std::enable_if<
    std::is_constructible<SELECTED_COMPARE, ORIGINAL_COMPARE const &>::value,
    SELECTED_COMPARE
>::type make_selected_compare(ORIGINAL_COMPARE const & in)
{
    return SELECTED_COMPARE(in);
}


template<typename SELECTED_COMPARE, typename ORIGINAL_COMPARE >
typename std::enable_if<
    !std::is_constructible<SELECTED_COMPARE, ORIGINAL_COMPARE const &>::value,
    SELECTED_COMPARE
>::type make_selected_compare(ORIGINAL_COMPARE const & )
{
    return SELECTED_COMPARE{};
}

//! subrange_on_first is useful when you want something like viewing a
//! std::set<std::pair<T1,T2>> as a std::multimap<T1, T2>; that is, you have
//! pairs of existing values that are ordered first on pair's first_type, then on second_type,
//! and you want to find all matches for a given value of first_type regardless of second_type,
//! without changing the existing pairs or copying them somewhere else, because they are already
//! in the right order, it is just the comparison that needs to be relaxed.

//! Creates a new comparator that will work on the same range, but ignores the second_type of a pair;
//! if the original comparison functor is transparent, it will re-use that comparison, but only
//! pass first_type to it, instead of the pair.  If the comparison functor is not transparent, it will
//! attempt to define a comparison functor being the same template as first_type; that is, if your
//! comparison was std::greater<std::pair<T1,T2>>, it will instantiate std::greater<T1> and use
//! that.  If the new comparator can be constructed from the old, it will do so, to allow for
//! custom comparators that have state (so long as an appropriate constructor is defined);
//! otherwise it will default-construct the new comparator, which is fine for stateless comparators,
//! but means stateful, non-transparent comparators that cannot be constructed from each other will not work.

//! In order to supply its own comparison functor, it makes use of std::upper_bound and std::lower_bound,
//! rather than container-provided functions, which means it requires random access iterators.
//! As a result, it will not work with std::map or std::set, but will work with:
//! sorted random-access containers (vector, array, deque after for example appropriate std::sort),
//! as well as associative containers built on random-access storage, like
//! boost::flat_map / boost::flat_set, or cues::pool_map
//! An alternate implementation could take advantage of inputs where:
//! 1) second_type has a specialization for std::numeric_limits
//! 2) first_type is copy-constructible
//! 3) the comparison is either std::less<> aka less<void>, std::less<key_type>,
//!      std::greater<> aka std::greater<void>, or std::greater<key_type>,
//! in which case it could create dummy keys of {subkey, std::numeric_limits<second_type>::min}
//! and {subkey, std::numeric_limits<second_type>::max}, which could be passed to container
//! lower_bound and upper_bound functions (depending on whether std::less or std::greater is used)
//! but such an implementation is not currently provided here.

template<typename CONTAINER_TYPE>
std::pair<
    typename CONTAINER_TYPE::const_iterator,
    typename CONTAINER_TYPE::const_iterator
> subrange_on_first(CONTAINER_TYPE const & container, typename cues::type_or_first<typename CONTAINER_TYPE::key_type>::type const & subkey)
{
    using container_type = CONTAINER_TYPE;
    using const_iterator = typename container_type::const_iterator;

    static_assert(
        std::is_base_of<typename std::iterator_traits<const_iterator>::iterator_category, std::random_access_iterator_tag>::value,
        "subrange_on_first implementation requires random access iterators."
    );

    // either container_type::key_compare,
    // or the template of container_type::key_compare<> but templated on
    // key_type::first_type
    using first_compare_type = typename cues::subcompare_first<
        typename container_type::key_compare,
        typename container_type::key_type
    >::type;

    // we are now comparing keys as though only the first_type mattered
    using altered_key_compare_type = cues::compare_first_or_value<
        first_compare_type,
        typename container_type::key_type
    >;

    // but, we don't just need to compare keys, because we will get pointers to value_type,
    // which for maps is itself a pair, so we need to ignore *that* second type,
    // if it exists.
    using altered_value_compare_type = cues::compare_first_or_value<
        altered_key_compare_type,
        typename container_type::value_type
    >;

    static_assert(
        std::is_constructible<first_compare_type, typename container_type::key_compare const &>::value
        || std::is_default_constructible<first_compare_type>::value,
        "subrange_on_first implementation requires comparison that can either be default constructed"
        " or constructed from the container key_compare."
    );

    // ideally, if there were state, we would copy it.
    // try to do this by constructing our comparison from the first_compare_type,
    // which is in turn constructed by container.key_comp() if possible, and is
    // otherwise default-constructed.
    altered_value_compare_type compare{
        altered_key_compare_type {make_selected_compare<first_compare_type>(container.key_comp())}
    };

    std::pair< const_iterator, const_iterator > retval;

    retval.first = std::lower_bound(
        container.cbegin(),
        container.cend(),
        subkey,
        compare
    );

    retval.second = (retval.first == container.cend()) ?
        retval.first
        :
        std::upper_bound(
            retval.first,
            container.cend(),
            subkey,
            compare
        )
    ;

    return retval;
}

template<typename CONTAINER_TYPE>
std::pair<
    typename CONTAINER_TYPE::iterator,
    typename CONTAINER_TYPE::iterator
> subrange_on_first(CONTAINER_TYPE & container, typename cues::type_or_first<typename CONTAINER_TYPE::key_type>::type const & subkey)
{
    // use const implementation
    auto const_pair = subrange_on_first(const_cast<CONTAINER_TYPE const &>(container), subkey);
    // use the non-const container to make iterators from the const_iterators
    return {
        container.begin() + (const_pair.first  - container.cbegin()),
        container.begin() + (const_pair.second - container.cbegin())
    };
}


template<typename T>
struct unary_equal_to
{
    using is_transparent = std::true_type;

    //! POSTCONDITION: must be assigned from a valid unary_equal_to,
    //! or T const &, before being used in comparisons (otherwise nullptr dereference)
    unary_equal_to()
    : v(nullptr)
    {}

    explicit unary_equal_to(T const & val)
    : v(&val)
    {}

    unary_equal_to operator=(T const & val)
    {
        v = &val;
        return *this;
    }

    constexpr bool operator()(T const & t) const
    {
        assert(v != nullptr);
        // play nicely with std::equal_to specialization
        return (std::equal_to<T>{}).operator()(*v, t);
    }

    template<typename U>
    constexpr bool operator()(U && u) const
    {
        assert(v != nullptr);
        // play nicely with std::equal_to specialization if equal_to<void> exists
        #if __cplusplus >= 201402L || defined ( __cpp_lib_transparent_operators )
        return (std::equal_to<void>{}).operator()(*v, std::forward<U>(u));
        #else
        return *v == std::forward<U>(u);
        #endif
    }

private:
    T const * v;
};



//! used similarly to std::function, except
//! the target is specified at compile-time and thus
//! can be constexpr
template<class T, class Ret, class... Args>
class forwarding_to_member_functor
{
    public:
        using Fn_type = typename std::conditional<std::is_const<T>::value, Ret(T::*)(Args...) const, Ret(T::*)(Args...)>;

        //! note: caller is responsible for managing the lifetime of these objects.
        constexpr forwarding_to_member_functor(T * obj, Fn_type fn) noexcept
        : mfn(fn),
        mobj(obj)
        {}

        constexpr forwarding_to_member_functor(forwarding_to_member_functor const &) noexcept = default;
        constexpr forwarding_to_member_functor(forwarding_to_member_functor &&) noexcept = default;

        constexpr Ret operator() (Args && ... args)
        {
            return mobj->*mfn(std::forward<Args>(args)...);
        }

    private:
        // don't even construct if you don't have the pointer.
        forwarding_to_member_functor() = delete;
        Fn_type mfn;
        T * mobj;
};

template<class Ret, class... Args>
class forwarding_to_free_functor
{
 public:
        using Fn_type = Ret(*)(Args...);

        constexpr forwarding_to_free_functor(Fn_type fn) noexcept
        : mfn(fn)
        {}

        constexpr forwarding_to_free_functor(forwarding_to_free_functor const &) noexcept = default;
        constexpr forwarding_to_free_functor(forwarding_to_free_functor &&) noexcept = default;

        constexpr Ret operator() (Args && ... args)
        {
            return mfn(std::forward<Args>(args)...);
        }

 private:
        // don't even construct if you don't have the pointer.
        forwarding_to_free_functor() = delete;
        Fn_type mfn;
};

template<class Ret, class... Args>
class defaulting_functor
{
    public:
        using Fn_type = void;

        constexpr defaulting_functor() noexcept = default;
        constexpr Ret operator() (Args && ...)
        {return Ret{};}
};


enum class functor_select_mode : std::uint_least8_t
{
    select_default,
    select_forward_to_member_fn,
    select_forward_to_free_fn
};

template<functor_select_mode MODE, class T, class Ret, class... Args>
struct select_default_or_forwarding
{
    using type = typename std::conditional<
        functor_select_mode::select_default == MODE,
        defaulting_functor<Ret, Args...>,
        typename std::conditional<
            functor_select_mode::select_forward_to_member_fn == MODE,
            forwarding_to_member_functor<T, Ret, Args...>,
            forwarding_to_free_functor<Ret, Args...>
        >::type
    >::type;

};


//! make_functor is a helper function to make a functor
//! for example selected based on select_default_or_forwarding
//! with or without constructor arguments
//! In particular this allows an easy compile-time switch
//! between constructing defaulting_functor, and constructing
//! either of the other functors, passing the argument that the
//! non-default one would need and ignoring it as appropriate.

// no parameter, void parameter, or not constructible from parameters
template<typename FUNCTOR, typename... CONSTRUCTOR_ARGS>
static constexpr typename std::enable_if<std::is_constructible<FUNCTOR, CONSTRUCTOR_ARGS...>::value, FUNCTOR>::type
make_functor(CONSTRUCTOR_ARGS ... args)
{
    return std::move(FUNCTOR{std::forward<CONSTRUCTOR_ARGS>(args)...});
}

template<typename FUNCTOR, typename... CONSTRUCTOR_ARGS>
static constexpr typename std::enable_if<!std::is_constructible<FUNCTOR, CONSTRUCTOR_ARGS...>::value, FUNCTOR>::type
make_functor(CONSTRUCTOR_ARGS...)
{
    return std::move(FUNCTOR{});
}

//! overloading functor, with fallback when c++17 variadic using is not present.
//! other implementations of this exist:
//! see (2014): https://stackoverflow.com/questions/27183568/c-how-to-introduce-overload-set-from-variadic-number-of-bases
//! and (2015): P0195R0, the justification for variadic using, at: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0195r0.html
//! this implementation differs in that:
//! 1) zero-type case has static assertion for failure; if you expected to inherit from something,
//!    then inheriting from nothing should be an error and would result in a useless class anyway.
//! 2) instead of leaving construction to aggregate initialization--which requires that
//!    each composing functor have no user-provided constructors (possibly even no
//!    user-declared constructors in c++20), which in turn places strict limitations
//!    on the kind of state functors can have (for example std::function is unusable
//!    along with other functors that need to copy state in order to work)--
//!    this implementation instead follows std::function practice and only requires
//!    that they are copy-constructible (and/or move-constructible).
//!    The overloading functor has a user-defined constructor taking each functor by value
//!    and forwarding, as well as a variadic constructor that will forward its inputs
//!    to each base, for functors that share common construction arguments.
//!    Note that these user-defined constructors this will result in
//!    the compiler not generating the default constructor, and it is not declared
//!    as defaulted either, to allow for functors that have no default constructor.
//!    Instead, a template "make_default_overloaded_functor" is provided to
//!    reduce typing in uses cases where all functors are default-constructible,
//!    which works by copies from temporary default constructed functors, which
//!    modern compiler optimizations are expected to elide.

// variadic using requires c++17, otherwise requires additional recursion step
#if __cplusplus >= 201703L || defined ( __cpp_variadic_using )

template< typename ... TYPES>
struct overloaded_functor : public TYPES...
{
    static_assert(sizeof...(TYPES) > 0, "no types present, thus no operator(), thus no functor.");
    using TYPES::operator()...;

    overloaded_functor(TYPES ... args)
    : TYPES(std::forward<TYPES>(args))...
    {}

    template<typename...Args>
    overloaded_functor(Args && ... args)
    : TYPES{std::forward<Args>(args)...}...
    {}

};
#else

// primary template, handles case if provided zero types from the start,
// which for the purposes of creating a functor, seems like it would have to be
// a mistake on the part of the programmer, and thus a compile-time error is sensible
template< typename TYPES...>
struct functor_inheritance_helper
{
    static_assert(sizeof...(TYPES) > 0, "no types present, thus no operator(), thus no functor.");
};

// specialization: exactly one, terminating
template< typename TYPE>
struct functor_inheritance_helper<TYPE> : public TYPE
{
    using TYPE::operator();

    functor_inheritance_helper(TYPE arg)
    : TYPE(std::forward<TYPE>(arg))
    {}

    template<typename...Args>
    functor_inheritance_helper(Args && ... args)
    : TYPES(std::forward<Args>(args)...)
    {}

};

// specialization: two or more, recursing case
template< typename FIRST_TYPE, typename SECOND_TYPE, typename... OTHER_TYPES>
struct functor_inheritance_helper<FIRST_TYPE, SECOND_TYPE, OTHER_TYPES...>
: public FIRST_TYPE
, public functor_inheritance_helper<SECOND_TYPE, OTHER_TYPES...>
{
    using FIRST_TYPE::operator();
    using functor_inheritance_helper<SECOND_TYPE, OTHER_TYPES...>::operator();

    functor_inheritance_helper(FIRST_TYPE first_arg, SECOND_TYPE second_arg, OTHER_TYPES ... other_args)
    : FIRST_TYPE(std::forward<FIRST_TYPE>(first_arg))
    , functor_inheritance_helper<SECOND_TYPE, OTHER_TYPES...>(std::forward<SECOND_TYPE>(second_arg), std::forward<OTHER_TYPES>(other_args)...)
    {}

    template<typename...Args>
    functor_inheritance_helper(Args && ... args)
    : FIRST_TYPE(std::forward<Args>(args)...)
    , functor_inheritance_helper<SECOND_TYPE, OTHER_TYPES...>(std::forward<Args>(args)...)
    {}


};

// effectively the same number of operator() introduced as the c++17 method
template< typename ... TYPES>
struct overloaded_functor : public functor_inheritance_helper<TYPES...>
{
    using functor_inheritance_helper<TYPES...>::operator();

    overloaded_functor(TYPES ... args)
    : functor_inheritance_helper<TYPES...>(std::forward<TYPES>(args)...)
    {}

    template<typename...Args>
    overloaded_functor(Args && ... args)
    : functor_inheritance_helper<TYPES...>(std::forward<Args>(args)...)
    {}
};

#endif

template<typename ... TYPES>
overloaded_functor<TYPES...> make_default_overloaded_functor()
{
    return overloaded_functor<TYPES...>(TYPES{}...);
}
}

#endif // CUES_FUNCTION_SELECTION_HPP_INCLUDED
