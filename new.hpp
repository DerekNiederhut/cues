#ifndef CUES_NEW_HPP_INCLUDED
#define CUES_NEW_HPP_INCLUDED

#include <cstddef>
#include <cassert>
#include <cstring>  //< for std::memcpy
#include <new>
#include <type_traits>
#include <initializer_list>
#include <utility>
#include <memory>
#include "cues/version_helpers.hpp"
#include "cues/variadic_type_traits.hpp"
#include "cues/array_type_traits.hpp"
#include "cues/void_t.hpp"

namespace cues
{

//! helper function to recover object pointer from byte array
//! PRECONDITION: the object has been constructed at this index
template<typename ACTUAL_TYPE>
typename std::remove_all_extents<ACTUAL_TYPE>::type const *
flattened_get(cues::raw_byte_type const * mem_begin, std::size_t index)
{
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;
    return ((mem_begin != nullptr) && (index < extent_sequence<ACTUAL_TYPE>::product())) ?
        (std::launder(reinterpret_cast<innermost_type const *>(mem_begin + index*sizeof(innermost_type))))
        : nullptr;
}

//! non-const overload
template<typename ACTUAL_TYPE>
typename std::remove_all_extents<ACTUAL_TYPE>::type *
flattened_get(cues::raw_byte_type * mem_begin, std::size_t index)
{
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;
    return const_cast<innermost_type *>(flattened_get<ACTUAL_TYPE>(static_cast<cues::raw_byte_type const *>(mem_begin), index));
}

//! helper function for when construction fails, or to destruct array
//! PRECONDITION: index is within bounds of array
//! PRECONDITION: memory provided points to the beginning of the array or is nullptr.
template<typename ACTUAL_TYPE>
void flattened_destruct_backward_from(cues::raw_byte_type * mem_begin, std::size_t index )
noexcept(std::is_nothrow_destructible<typename std::remove_all_extents<ACTUAL_TYPE>::type>::value)
{
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;
    if (mem_begin == nullptr) {return;}
    assert((index < extent_sequence<ACTUAL_TYPE>::product() && "attempted to destruct location outside array"));
    // destruct, iterating backwards
    // increment index once to act like "reverse iterator",
    // where it refers to the element *before* the index
    for (++index;
        index > 0;
        flattened_get<ACTUAL_TYPE>(mem_begin, --index)->~innermost_type()
    )
    {}
}

//! helper function to in-place construct flattened array with default-constructed elements
template<typename ACTUAL_TYPE>
void flattened_default_construct_remaining(cues::raw_byte_type * mem_begin, std::size_t index )
noexcept(std::is_nothrow_default_constructible<typename std::remove_all_extents<ACTUAL_TYPE>::type>::value)
{
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;

    try
    {
        for (std::size_t const index_end = extent_sequence<ACTUAL_TYPE>::product();
             index < index_end;
             ++index
        )
        {
            new (mem_begin + index*sizeof(innermost_type)) innermost_type{};
        }
    }
    catch(...)
    {
        if (index > 0)
        {
            flattened_destruct_backward_from<ACTUAL_TYPE>(mem_begin, --index);
        }

        // noexcept specifier for this function is based on is_nothrow_constructible
        // so we are noexcept iff the constructor is noexcept.
        // If the noexcept constructor throws,
        // but is marked noexcept, it will call std::terminate itself, in which case we do not need to rethrow,
        // because we won't make it to the catch block anyway.
        // If the constructor is not noexcept, this function is also not noexcept, so it will rethrow
        // after cleaning up.
        if constexpr (!noexcept(flattened_default_construct_remaining<ACTUAL_TYPE>(mem_begin, index)))
        {
            throw;
        }
    }
}

//! helper function for recursively constructing in-place from variadic arguments.
//! this represents the termination condition for the variadic constructor, where there are no more arguments
template<typename ACTUAL_TYPE, std::size_t IDX, typename... Args>
std::size_t flattened_in_place_construct_at(cues::raw_byte_type * , Args && ... ) noexcept
{
    return IDX;
}

//! helper function to recursively construct in-place from variadic arguments,
//! specialization for when there is at least one argument
template<typename ACTUAL_TYPE, std::size_t IDX, typename U, typename... Args>
std::size_t flattened_in_place_construct_at(cues::raw_byte_type * mem_begin, U && first_arg, Args && ... args)
noexcept(std::is_nothrow_constructible<typename std::remove_all_extents<ACTUAL_TYPE>::type, U>::value
 && noexcept(flattened_in_place_construct_at<ACTUAL_TYPE, IDX+1, Args...>(static_cast<cues::raw_byte_type *>(nullptr), std::forward<Args>(args)...))
)
{
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;

    static_assert(IDX < extent_sequence<ACTUAL_TYPE>::product(), "attempted to construct out of bounds.");
    try
    {
        new (mem_begin+IDX*sizeof(innermost_type)) innermost_type(first_arg);
        return flattened_in_place_construct_at<ACTUAL_TYPE, IDX+1, Args...>(mem_begin, std::forward<Args>(args)...);
    }
    catch(...)
    {
        // in this case, the recursive calls to in_place_construct_at
        // will catch and rethrow, destructing along the way, so
        // the iterative destruct_backward is not needed and would
        // destruct an object more than once.
        if constexpr (IDX > 0)
        {
            flattened_get<ACTUAL_TYPE>(mem_begin, IDX-1)->~innermost_type();
        }

        if constexpr (
            !(
              std::is_nothrow_constructible<typename std::remove_all_extents<ACTUAL_TYPE>::type, U>::value
              && noexcept(flattened_in_place_construct_at<ACTUAL_TYPE, IDX+1, Args...>(static_cast<cues::raw_byte_type *>(nullptr), std::forward<Args>(args)...))
            )
        )
        {
            throw;
        }
    }
    return IDX;
}


template<typename ACTUAL_TYPE, typename... Args, class Enable =  typename std::enable_if<
        is_constructible_some_from_each_and_remainder_default<ACTUAL_TYPE, Args...>::value
    >::type
>
typename std::remove_all_extents<ACTUAL_TYPE>::type *
flattening_placement_new(cues::raw_byte_type * mem_begin, Args && ... args)
noexcept(is_nothrow_constructible_some_from_each_and_remainder_default<ACTUAL_TYPE, Args...>::value)
{
    // construct in-place each argument, then default-construct the rest
    std::size_t elements_constructed_by_arguments =
    flattened_in_place_construct_at<ACTUAL_TYPE, 0, Args...>(mem_begin, std::forward<Args>(args)...);
    assert(((elements_constructed_by_arguments == sizeof...(Args)) && "logic error constructing from variadic arguments"));
    if constexpr (sizeof...(Args) < extent_sequence<ACTUAL_TYPE>::product())
    {
        flattened_default_construct_remaining<ACTUAL_TYPE>(mem_begin, elements_constructed_by_arguments);
    }

    return flattened_get<ACTUAL_TYPE>(mem_begin, 0);
}

//! replacement for array placement new that does not use additional space to store extra information.
//! PRECONDITION: the memory pointed to is not null and is large enough to hold the type being constructed.
//! this overload is for an initializer list where each element can construct one element of the array,
//! and remaining elements can be default-constructed
template<typename ACTUAL_TYPE, typename PROVIDED, class Enable = typename std::enable_if<
            !std::is_reference<PROVIDED>::value
            && is_constructible_from_one_and_remainder_default<ACTUAL_TYPE, typename std::remove_reference<PROVIDED>::type>::value
        >::type>
typename std::remove_all_extents<ACTUAL_TYPE>::type *
flattening_placement_new( cues::raw_byte_type * mem_begin, std::initializer_list<PROVIDED> il)
noexcept(is_nothrow_constructible_from_one_and_remainder_default<ACTUAL_TYPE,PROVIDED>::value)
{
    assert((mem_begin != nullptr && "Trying to construct object at nullptr." ));
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;
    std::size_t index = 0;
    std::size_t const index_end = extent_sequence<ACTUAL_TYPE>::product();

    try
    {
        for (auto list_itr = il.begin();
            index < index_end
            && list_itr != il.end();
            ++index, ++list_itr
        )
        {
            new (mem_begin+index*sizeof(innermost_type)) innermost_type(*list_itr);
        }
    }
    catch(...)
    {
        if (index > 0)
        {
            flattened_destruct_backward_from<ACTUAL_TYPE>(mem_begin, --index);
        }

        if constexpr (!is_nothrow_constructible_from_one_and_remainder_default<ACTUAL_TYPE,PROVIDED>::value)
        {
            throw;
        }
    }

    flattened_default_construct_remaining<ACTUAL_TYPE>(mem_begin, index);

    return flattened_get<ACTUAL_TYPE>(mem_begin, 0);
}

//! overload where first element can be constructed by all the arguments,
//! and the remainder can be default-constructed
//! PRECONDITION: memory is large enough to hold object and is not null
template<typename ACTUAL_TYPE, typename U, typename... Args, class Enable = typename std::enable_if<
        is_constructible_first_from_all_and_remainder_default<ACTUAL_TYPE, U, Args...>::value
    >::type
>
typename std::remove_all_extents<ACTUAL_TYPE>::type *
flattening_placement_new(cues::raw_byte_type * mem_begin, U && first_arg, Args && ... args )
noexcept(is_nothrow_constructible_first_from_all_and_remainder_default<ACTUAL_TYPE, U, Args...>::value)
{
    assert((mem_begin != nullptr && "Trying to construct object at nullptr." ));
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;

    new (mem_begin) innermost_type(std::forward<U>(first_arg), std::forward<Args>(args)...);

    // conditionally removing this call means that it can be used
    // for exact-length arguments if value_type is not default-constructible
    if constexpr(extent_sequence<ACTUAL_TYPE>::product() > 1)
    {
        // default_construct_remaining will take care of cleanup if a subsequent constructor throws
        flattened_default_construct_remaining<ACTUAL_TYPE>(mem_begin, 1u);
    }

    return flattened_get<ACTUAL_TYPE>(mem_begin, 0);
}

//! overload where the first element can be constructed with the (whole) initializer_list and
//! all remaining arguments, and any remaining elements can be default-constructed.
//! PRECONDITION: memory is large enough and not null
template<typename ACTUAL_TYPE, typename U, typename... Args, class Enable = typename std::enable_if<
        !std::is_reference<U>::value
        && is_constructible_first_from_list_and_remainder_default<ACTUAL_TYPE, typename std::remove_reference<U>::type, Args...>::value
>::type>
typename std::remove_all_extents<ACTUAL_TYPE>::type *
flattening_placement_new(
    cues::raw_byte_type * mem_begin,
    std::initializer_list<U> il,
    Args && ... args
)
noexcept( is_nothrow_constructible_first_from_list_and_remainder_default<ACTUAL_TYPE, U, Args...>::value)
{
    assert((mem_begin != nullptr && "Trying to construct object at nullptr." ));
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;

    new (mem_begin) innermost_type{il, std::forward<Args>(args)...};

    flattened_default_construct_remaining<ACTUAL_TYPE>(mem_begin,1u);

    return flattened_get<ACTUAL_TYPE>(mem_begin, 0);
}

//! overload when type is an array and you are passing a reference to const array
//! PRECONDITION: memory is large enough and not null
template<typename ACTUAL_TYPE>
typename std::remove_all_extents<ACTUAL_TYPE>::type *
flattening_placement_new(
    cues::raw_byte_type * mem_begin,
    typename std::enable_if<std::is_array<ACTUAL_TYPE>::value,
        typename std::add_lvalue_reference<typename std::add_const<ACTUAL_TYPE>::type>::type
    >::type other
)
noexcept(std::is_nothrow_copy_constructible<typename std::remove_all_extents<ACTUAL_TYPE>::type>::value)
{
    using innermost_type = typename std::remove_all_extents<ACTUAL_TYPE>::type;
    assert((mem_begin != nullptr && "Trying to construct object at nullptr." ));
    // potential optimization here
    if constexpr (std::is_trivially_copyable<ACTUAL_TYPE>::value)
    {
        // c++20 clarified this to start object lifetime for trivial objects
        // and applied that clarification retroactively back to c++98.
        std::memcpy(mem_begin, reinterpret_cast<cues::raw_byte_type const *>(other), sizeof(ACTUAL_TYPE));
    }
    else
    {
        // take pointer to entire array, so that adjustments to the pointer within the range of the array
        // are well-defined.
        cues::raw_byte_type const * other_begin = reinterpret_cast<cues::raw_byte_type const *>(&other);
        std::size_t index = 0;
        try{
            for (std::size_t const index_end = extent_sequence<ACTUAL_TYPE>::product();
                index < index_end;
                ++index)
            {
                new (mem_begin    + index*sizeof(innermost_type)) innermost_type(
                    *std::launder(reinterpret_cast<innermost_type const *>(
                        other_begin + index*sizeof(innermost_type)
                    ))
                );
            }
        }
        catch(...)
        {
            if (index > 0)
            {
                flattened_destruct_backward_from<ACTUAL_TYPE>(mem_begin, --index);
            }
            if constexpr (!noexcept(flattening_placement_new<ACTUAL_TYPE>(mem_begin, other)))
            {
                throw;
            }
        }
    }
    return flattened_get<ACTUAL_TYPE>(mem_begin, 0);
}


//! convenience template type for making deleter part of the unique_ptr type
//! instead of storing an additional member.  For memory that is already handled
//! some other way.
template<typename T>
class placement_destruct
{
    public:
    void operator()(T * obj) const {obj->~T();}
};


//! WARNING: it is possible to receive a nullptr if enough / aligned space is not provided,
//! so check for null before using unless you know you have guaranteed enough aligned storage.
//! NOTE: does not free the memory nor ensure its lifetime, making this useful primarily
//! for temporary objects whose memory is managed by the caller and known to be released.
//! Convenience function for making a unique_ptr that only destructs its
//! "owned" object rather than freeing the memory.  Can be useful for example
//! in implementing polymorphic clone methods, where a pointer to stack memory
//! is provided to the clone function.  Note that some other mechanism of telling
//! the caller how much memory is needed (and perhaps the alignment) would also
//! be required.

// overload for non array types only.
template<typename POINTEE, typename CONSTRUCTAS=POINTEE, typename DESTRUCTAS=POINTEE, class... Args>
typename std::enable_if<!std::is_array<POINTEE>::value, std::unique_ptr<POINTEE, placement_destruct<DESTRUCTAS> > >::type
make_placement(void * mem_addr, std::size_t mem_size, Args && ... args)
{
    static_assert(std::is_same<DESTRUCTAS,POINTEE>::value
        // permit also polymorphic destruction
        || (std::is_base_of<DESTRUCTAS,POINTEE>::value && std::has_virtual_destructor<DESTRUCTAS>::value)
        , "provided types to make_placement will not destruct properly."
   );
    // permit also polymorphic pointers for derived types
   static_assert(std::is_same<POINTEE,CONSTRUCTAS>::value
        || (std::is_base_of<POINTEE,CONSTRUCTAS>::value && std::has_virtual_destructor<POINTEE>::value),
        "provide types will not construct properly"
    );

    void * aligned_ptr = std::align(alignof(CONSTRUCTAS), sizeof(CONSTRUCTAS), mem_addr, mem_size);
    if (nullptr == aligned_ptr) {return nullptr;}

    return std::unique_ptr<POINTEE, placement_destruct<DESTRUCTAS> >(
         new (aligned_ptr)
         CONSTRUCTAS(std::forward<Args>(args)...)
     );
}



// pad a type to an alignment so that an array of them,
// with the array properly aligned, will align each element
template<typename T, std::size_t ALIGN, class Enable = void>
class padded_type
{
    T original_type;
};

template<typename T, std::size_t ALIGN>
class padded_type<T, ALIGN, cues::void_t<typename std::enable_if<(ALIGN > sizeof(T))>::type>>
{
    T original_type;
    cues::raw_byte_type pad [ALIGN - sizeof(T)];
};

}
#endif // CUES_NEW_HPP_INCLUDED
