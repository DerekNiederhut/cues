#ifndef TYPE_TRAITS_TEST_HPP_INCLUDED
#define TYPE_TRAITS_TEST_HPP_INCLUDED

#include <cstdint>
#include <type_traits>
#include <limits>
#include <utility>
#include "cues/type_traits.hpp"
#include "helper_types.hpp"

#include <array>
#include <vector>
#include <deque>
#include <map>
#include <set>
#include <forward_list>
#include <list>
#include <unordered_map>
#include <unordered_set>

namespace cues
{
namespace test
{

    template<typename INTEGER_TYPE>
    struct signed_test
    {
        using signed_fit_type = typename cues::make_signed<INTEGER_TYPE>::type;

        static_assert(
            std::numeric_limits<signed_fit_type>::digits >=
            std::numeric_limits<INTEGER_TYPE>::digits,
            "make_signed did not pick enough digits"
        );

        static_assert(
            std::numeric_limits<signed_fit_type>::is_integer && std::numeric_limits<signed_fit_type>::is_signed,
            "make_signed did not pick a signed type"
        );

        static_assert(
            !std::numeric_limits<INTEGER_TYPE>::is_integer
            || !std::numeric_limits<INTEGER_TYPE>::is_signed
            || std::is_same<INTEGER_TYPE, signed_fit_type>::value,
            "make_signed changed the type of a signed integer"
        );

    };

    template<typename INTEGER_TYPE>
    struct unsigned_test
    {
        using unsigned_fit_type = typename cues::make_unsigned<INTEGER_TYPE>::type;

        static_assert(
            std::numeric_limits<unsigned_fit_type>::digits >=
            std::numeric_limits<INTEGER_TYPE>::digits,
            "make_unsigned did not pick enough digits"
        );

        static_assert(
            std::numeric_limits<unsigned_fit_type>::is_integer && !std::numeric_limits<unsigned_fit_type>::is_signed,
            "make_unsigned did not pick an unsigned type"
        );

        static_assert(
            !std::numeric_limits<INTEGER_TYPE>::is_integer
            || std::numeric_limits<INTEGER_TYPE>::is_signed
            || std::is_same<INTEGER_TYPE, unsigned_fit_type>::value,
            "make_unsigned changed the type of an unsigned integer"
        );

    };

    constexpr signed_test<std::uint_least8_t>  mksu08_test;
    constexpr signed_test<std::uint_least16_t> mksu16_test;
    constexpr signed_test<std::uint_least32_t> mksu32_test;
    // we don't know that we have a signed type that can fit
    // all the values of an unsigned 64
    using potential_unsigned64_type = typename std::conditional<
        std::numeric_limits<std::intmax_t>::digits >=
        std::numeric_limits<std::uint_least64_t>::digits,
        std::uint_least64_t,
        std::uint_least32_t
    >::type;

    constexpr signed_test<potential_unsigned64_type> mksu64_test;

    constexpr signed_test<std::int_least8_t>  mkss08_test;
    constexpr signed_test<std::int_least16_t> mkss16_test;
    constexpr signed_test<std::int_least32_t> mkss32_test;
    constexpr signed_test<std::int_least64_t> mkss64_test;
    constexpr signed_test<std::intmax_t>      mkssmx_test;


    constexpr unsigned_test<std::uint_least8_t>  mkuu08_test;
    constexpr unsigned_test<std::uint_least16_t> mkuu16_test;
    constexpr unsigned_test<std::uint_least32_t> mkuu32_test;
    constexpr unsigned_test<std::uint_least64_t> mkuu64_test;
    constexpr unsigned_test<std::uintmax_t>      mkuumx_test;
    constexpr unsigned_test<std::int_least8_t>   mkus08_test;
    constexpr unsigned_test<std::int_least16_t>  mkus16_test;
    constexpr unsigned_test<std::int_least32_t>  mkus32_test;
    constexpr unsigned_test<std::int_least64_t>  mkus64_test;
    constexpr unsigned_test<std::intmax_t>       mkusmx_test;

    template<typename T>
    struct basic_alias_test
    {
        static_assert(cues::is_safe_to_alias<char      , T>::value, "char should be safe to alias anything");
        static_assert(cues::is_safe_to_alias<char const , T>::value, "char const should be safe to alias anything");

        static_assert(cues::is_safe_to_alias<signed char      , T>::value, "signed char should be safe to alias anything");
        static_assert(cues::is_safe_to_alias<signed char const , T>::value, "signed char const should be safe to alias anything");

        static_assert(cues::is_safe_to_alias<unsigned char      , T>::value, "unsigned char should be safe to alias anything");
        static_assert(cues::is_safe_to_alias<unsigned char const , T>::value, "unsigned char const should be safe to alias anything");

        #if defined __cpp_lib_byte

        static_assert(cues::is_safe_to_alias<std::byte      , T>::value, "std::byte should be safe to alias anything");
        static_assert(cues::is_safe_to_alias<std::byte const , T>::value, "std::byte const should be safe to alias anything");

        #endif

        static_assert(cues::is_safe_to_alias<T , T>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T , T const>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T , T volatile>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T , T volatile const>::value, "a type should be able to alias itself");

        static_assert(cues::is_safe_to_alias<T const, T>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T const, T const>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T const, T volatile>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T const, T const volatile>::value, "a type should be able to alias itself");

        static_assert(cues::is_safe_to_alias<T volatile, T>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T volatile, T const>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T volatile, T volatile>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T volatile, T const volatile>::value, "a type should be able to alias itself");

        static_assert(cues::is_safe_to_alias<T volatile const, T>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T volatile const, T const>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T volatile const, T volatile>::value, "a type should be able to alias itself");
        static_assert(cues::is_safe_to_alias<T volatile const, T const volatile>::value, "a type should be able to alias itself");

        static_assert(!cues::is_safe_to_alias<typename std::add_pointer<T>::type, T>::value, "a pointer to a type should not alias with the type");

        static_assert(std::is_same<T, trivial_aggregate>::value || !cues::is_safe_to_alias<trivial_aggregate, T>::value,
                      "false positive on trivial_aggregate");
        static_assert(std::is_same<T, scoped_only>::value || !cues::is_safe_to_alias<scoped_only, T>::value,
                      "false positive on scoped_only");

    };

    template<typename T>
    struct integral_alias_test : public basic_alias_test<T>
    {
        static_assert(std::is_integral<T>::value, "test error, testing non-integer as integer");

        using signed_type = typename std::make_signed<typename std::remove_cv<T>::type>::type;
        using unsigned_type = typename std::make_unsigned<typename std::remove_cv<T>::type>::type;


        static_assert(cues::is_safe_to_alias<T, signed_type>::value, "integer alias failure signed type");
        static_assert(cues::is_safe_to_alias<T const, signed_type>::value, "integer alias failure signed type");
        static_assert(cues::is_safe_to_alias<T volatile, signed_type>::value, "integer alias failure signed type");
        static_assert(cues::is_safe_to_alias<T volatile const, signed_type>::value, "integer alias failure signed type");

        static_assert(cues::is_safe_to_alias<T, unsigned_type>::value, "integer alias failure unsigned type");
        static_assert(cues::is_safe_to_alias<T const, unsigned_type>::value, "integer alias failure unsigned type");
        static_assert(cues::is_safe_to_alias<T volatile, unsigned_type>::value, "integer alias failure unsigned type");
        static_assert(cues::is_safe_to_alias<T volatile const, unsigned_type>::value, "integer alias failure unsigned type");

    };


    constexpr integral_alias_test<char>                     char_alias_test;
    constexpr integral_alias_test<signed char>             schar_alias_test;
    constexpr integral_alias_test<unsigned char>           uchar_alias_test;
    constexpr integral_alias_test<signed int>               sint_alias_test;
    constexpr integral_alias_test<unsigned int>             uint_alias_test;
    constexpr integral_alias_test<signed long int>         slint_alias_test;
    constexpr integral_alias_test<unsigned long int>       ulint_alias_test;
    constexpr integral_alias_test<signed long long int>   sllint_alias_test;
    constexpr integral_alias_test<unsigned long long int> ullint_alias_test;

    constexpr basic_alias_test<trivial_aggregate>      aggregate_alias_test;
    constexpr basic_alias_test<scoped_only>                scoped_only_test;
    constexpr basic_alias_test<signed long int [8]>        known_array_test;
    constexpr basic_alias_test<trivial_aggregate[]>      unknown_array_test;

    constexpr basic_alias_test<unsigned int *>              puint_alias_test;
    constexpr basic_alias_test<unsigned long int const *> pculint_alias_test;

    static_assert(!cues::is_safe_to_alias<unsigned int, unsigned long long int>::value, "false positive on unsigned");
    static_assert(!cues::is_safe_to_alias<std::pair<unsigned short, signed long>, std::pair<signed short, signed long>>::value,
                  "false positive on pair (signedness)");
    static_assert(!cues::is_safe_to_alias<std::pair<unsigned short, signed long>, std::pair<unsigned short const, signed long>>::value,
                  "false positive on pair (constness)");
    static_assert(cues::is_safe_to_alias<std::pair<unsigned short, signed long> const, std::pair<unsigned short, signed long>>::value,
                  "false negative on pair (constness)");

    static_assert(cues::is_safe_to_alias<unsigned long int const [8], unsigned long int[8]>::value,
                  "false negative on array (constness)");

    // since c++20, one fixed and one unbounded is also permitted
    #if __cplusplus >= 202002L
        static_assert(cues::is_safe_to_alias<signed long int[], signed long int[8]>, "false negative on array (bounds)");
    #endif

    static_assert(cues::is_safe_to_alias<
            std::uint_least16_t multiple_aggregate::*,
            std::uint_least16_t multiple_aggregate::*
        >::value,
        "false negative (pointer to member)"
    );

    static_assert(!cues::is_safe_to_alias<
            std::uint_least16_t trivial_aggregate::*,
            std::uint_least16_t multiple_aggregate::*
        >::value,
        "false positive (pointer to member)"
    );

    static_assert(cues::is_safe_to_alias<
            trivial_aggregate * multiple_aggregate::*,
            trivial_aggregate * multiple_aggregate::*
        >::value,
        "false negative (pointer to member pointer)"
    );


    // container tests
    static_assert(
        !cues::is_container<empty_aligned_to_8>::value,
        "false positive on is_container<empty_aligned_to_8> "
    );

    static_assert(
        !cues::is_associative_container<empty_aligned_to_8>::value,
        "false positive on is_associative_container<empty_aligned_to_8> "
    );

    static_assert(
        !cues::is_container<std::size_t>::value,
        "false positive on is_container<std::size_t> "
    );
    static_assert(
        !cues::is_associative_container<std::size_t>::value,
        "false positive on is_associative_container<std::size_t> "
    );

    static_assert(
        !cues::is_container<std::size_t[]>::value,
        "false positive on is_container<std::size_t[]> "
    );
    static_assert(
        !cues::is_associative_container<std::size_t[]>::value,
        "false positive on is_associative_container<std::size_t[]> "
    );

    static_assert(
        !cues::is_container<std::size_t[8]>::value,
        "false positive on is_container<std::size_t[8]> "
    );
    static_assert(
        !cues::is_associative_container<std::size_t[8]>::value,
        "false positive on is_associative_container<std::size_t[8]> "
    );

    static_assert(
        cues::is_container<std::array<empty_aligned_to_8,8>>::value,
        "false negative on is_container<std::array<empty_aligned_to_8,8>> "
    );
    static_assert(
        !cues::is_associative_container<std::array<empty_aligned_to_8,8>>::value,
        "false positive on is_associative_container<std::array<empty_aligned_to_8,8>> "
    );

    static_assert(
        cues::is_container<std::vector<empty_aligned_to_8>>::value,
        "false negative on is_container<std::vector<empty_aligned_to_8>> "
    );
    static_assert(
        !cues::is_associative_container<std::vector<empty_aligned_to_8>>::value,
        "false positive on is_associative_container<std::vector<empty_aligned_to_8>> "
    );

    static_assert(
        cues::is_container<std::deque<empty_aligned_to_8>>::value,
        "false negative on is_container<std::deque<empty_aligned_to_8>> "
    );
    static_assert(
        !cues::is_associative_container<std::deque<empty_aligned_to_8>>::value,
        "false positive on is_associative_container<std::deque<empty_aligned_to_8>> "
    );

    // std::forward_list is not required to provide the size() function,
    // but the named requirements for Container include size().
    // thus, it does not seem correct to fail the test if std::forward_list
    // does not satisfy is_container, nor to fail if it does not
    // however, it is definitely not an associative container
    static_assert(
        !cues::is_associative_container<std::forward_list<empty_aligned_to_8>>::value,
        "false positive on is_container<std::forward_list<empty_aligned_to_8>> "
    );

    static_assert(
        cues::is_container<std::list<empty_aligned_to_8>>::value,
        "false negative on is_container<std::list<empty_aligned_to_8>> "
    );
    static_assert(
        !cues::is_associative_container<std::list<empty_aligned_to_8>>::value,
        "false positive on is_associative_container<std::list<empty_aligned_to_8>> "
    );

    static_assert(
        cues::is_container<std::set<empty_aligned_to_8>>::value,
        "false negative on is_container<std::set<empty_aligned_to_8>> "
    );
    static_assert(
        cues::is_associative_container<std::set<empty_aligned_to_8>>::value,
        "false negative on is_associative_container<std::set<empty_aligned_to_8>> "
    );

    static_assert(
        cues::is_container<std::multiset<empty_aligned_to_8>>::value,
        "false negative on is_container<std::multiset<empty_aligned_to_8>> "
    );
    static_assert(
        cues::is_associative_container<std::multiset<empty_aligned_to_8>>::value,
        "false negative on is_associative_container<std::multiset<empty_aligned_to_8>> "
    );

    static_assert(
        cues::is_container<std::map<std::size_t, empty_aligned_to_8>>::value,
        "false negative on is_container<std::map<std::size_t, empty_aligned_to_8>> "
    );
    static_assert(
        cues::is_associative_container<std::map<std::size_t, empty_aligned_to_8>>::value,
        "false negative on is_associative_container<std::map<std::size_t, empty_aligned_to_8>> "
    );

    static_assert(
        cues::is_container<std::multimap<std::size_t, empty_aligned_to_8>>::value,
        "false negative on is_container<std::multimap<std::size_t, empty_aligned_to_8>> "
    );
    static_assert(
        cues::is_associative_container<std::multimap<std::size_t, empty_aligned_to_8>>::value,
        "false negative on is_associative_container<std::multimap<std::size_t, empty_aligned_to_8>> "
    );

    static_assert(
        cues::is_container<std::unordered_set<std::size_t>>::value,
        "false negative on is_container<std::unordered_set<std::size_t>> "
    );
    static_assert(
        !cues::is_associative_container<std::unordered_set<std::size_t>>::value,
        "false positive on is_associative_container<std::unordered_set<std::size_t>> "
    );

    static_assert(
        cues::is_container<std::unordered_multiset<std::size_t>>::value,
        "false negative on is_container<std::unordered_multiset<std::size_t>> "
    );
    static_assert(
        !cues::is_associative_container<std::unordered_multiset<std::size_t>>::value,
        "false positive on is_associative_container<std::unordered_multiset<std::size_t>> "
    );

    static_assert(
        cues::is_container<std::unordered_map<std::size_t, std::size_t>>::value,
        "false negative on is_container<std::unordered_map<std::size_t>> "
    );
    static_assert(
        !cues::is_associative_container<std::unordered_map<std::size_t, std::size_t>>::value,
        "false positive on is_associative_container<std::unordered_map<std::size_t>> "
    );

    static_assert(
        cues::is_container<std::unordered_multimap<std::size_t, std::size_t>>::value,
        "false negative on is_container<std::unordered_multimap<std::size_t>> "
    );
    static_assert(
        !cues::is_associative_container<std::unordered_multimap<std::size_t, std::size_t>>::value,
        "false positive on is_associative_container<std::unordered_multimap<std::size_t>> "
    );

    static_assert(
        cues::is_constructible_or_convertible<
            cues::test::move_only,
            cues::test::move_only &&
        >::value,
        "false negative constructing/converting moveonly type"
    );

    static_assert(
        !cues::is_constructible_or_convertible<
            cues::test::move_only,
            int
        >::value,
        "false positive constructing/converting moveonly type"
    );

    static_assert(
        cues::is_constructible_or_convertible<
            signed long,
            unsigned short
        >::value,
        "false negative constructing/converting larger signed from smaller unsigned type"
    );

    static_assert(
        std::is_same<
            typename std::vector<int>::iterator,
            typename cues::iterator_type<std::vector<int> >::type
        >::value,
        "failure on vector iterator"
    );

    static_assert(
        std::is_same<
            typename std::vector<int>::const_iterator,
            typename cues::iterator_type<std::vector<int> const>::type
        >::value,
        "failure on vector iterator"
    );

    static_assert(
        std::is_same<
            typename std::map<unsigned short, signed long>::iterator,
            typename cues::iterator_type<std::map<unsigned short, signed long> >::type
        >::value,
        "failure on map iterator"
    );

    static_assert(
        std::is_same<
            typename std::map<unsigned short, signed long>::const_iterator,
            typename cues::const_iterator_type<std::map<unsigned short, signed long> >::type
        >::value,
        "failure on map const iterator"
    );

    static_assert(
        std::is_same<
            typename std::array<int, 8>::const_iterator,
            typename cues::const_iterator_type<std::array<int, 8> >::type
        >::value,
        "failure on array const iterator"
    );

    static_assert(cues::is_template_of<std::vector, std::vector<int> >::value, "failed to identify vector<int> as vector");
    static_assert(cues::is_template_of<std::map, std::map<unsigned short, signed long> >::value, "failed to identify map<unsigned short, long> as map");
    static_assert(!cues::is_template_of<std::vector, cues::test::move_only>::value, "incorrectly identified non-template type as a vector");
    static_assert(!cues::is_template_of<std::vector, std::deque<int>>::value, "incorrectly identified deque<int> as a vector");

}
}
#endif // TYPE_TRAITS_TEST_HPP_INCLUDED
