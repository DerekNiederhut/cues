#include "pool_list_test.hpp"

#include "cues/version_helpers.hpp"

#if (__cplusplus < 201703L && (!defined ( __cpp_lib_launder) || !defined(__cpp_lib_if_constexpr)))

#pragma message "NOTE: skipping test of cues::pool_list because constexpr if and std::launder are not both available."
namespace cues
{
namespace test
{
    std::size_t check_pool_list_test_runtime_assertions() {return 0;}
}
}
#else

#include "cues/homogenous_pool.hpp"
#include "cues/pool_list.hpp"
#include "cues/type_traits.hpp"
#include "helper_types.hpp"
#include <cassert>

namespace cues
{
namespace test
{

// helper type
template<typename T, typename U = T>
struct checked_splice
{

    using const_iterator = typename T::const_iterator;

    checked_splice(T const & lhs, U const & rhs)
    : original_lhs_size(lhs.size())
    , original_rhs_size(rhs.size())
    , original_allocator_capacity((lhs.get_allocator()).remaining_size())
    {
        assert(((void)"allocators not equal, splice undefined behavior",
                lhs.get_allocator() == rhs.get_allocator()));
    }

    std::size_t operator()(T & lhs, typename T::const_iterator dest,
                           U & rhs, typename U::const_iterator src_begin, typename U::const_iterator src_end) const
    {
        static_assert(
            std::is_same<typename T::allocator_type, typename U::allocator_type>::value,
            "splicing only valid if allocators are the same."
        );

        assert(((void)"allocators must be equal", lhs.get_allocator() == rhs.get_allocator()));

        lhs.splice(dest, rhs, src_begin, src_end);

        assert(((void)"splice changed allocation", lhs.get_allocator().remaining_size() == original_allocator_capacity));

        assert(((void)"splice did not change size symmetrically",
            lhs.size() == original_lhs_size + (original_rhs_size - rhs.size())
        ));

        return static_cast<std::size_t>(
            lhs.get_allocator().remaining_size() != original_allocator_capacity
        )
        +
        static_cast<std::size_t>(
            lhs.size() != original_lhs_size + (original_rhs_size - rhs.size())
        );
    }

    std::size_t get_original_allocator_capacity() const noexcept
    {
        return original_allocator_capacity;
    }

private:
    std::size_t original_lhs_size;
    std::size_t original_rhs_size;
    std::size_t original_allocator_capacity;
};


template<
    typename Test_Element_Type,
    std::size_t Pool_Size,
    std::size_t First_List_Size = Pool_Size/2,
    std::size_t Second_List_Size = Pool_Size - First_List_Size
>
void check_list_errors(std::size_t & cumulative_assertion_failures)
{
    static constexpr std::size_t const pool_size = Pool_Size;
    static constexpr std::size_t const first_list_size  = First_List_Size;
    static constexpr std::size_t const second_list_size  = Second_List_Size;

    using test_element_type = Test_Element_Type;
    static constexpr test_element_type const zero_element = make_value<test_element_type>(0);

    using test_pool_type = cues::homogenous_pool<test_element_type, pool_size>;
    using test_allocator_type = typename test_pool_type::allocator_type;
    test_pool_type test_pool;

    using first_test_pool_list = cues::pool_list<
        test_element_type,
        first_list_size,
        typename cues::select_least_integer_by_digits<
            cues::minimum_bits(first_list_size), cues::integer_selector_signedness::use_unsigned
        >::type,
        test_allocator_type
    >;

    using second_test_pool_list = cues::pool_list<
        test_element_type,
        second_list_size,
        typename cues::select_least_integer_by_digits<
            cues::minimum_bits(second_list_size), cues::integer_selector_signedness::use_unsigned
        >::type,
        test_allocator_type
    >;

    first_test_pool_list  first_list{test_allocator_type{test_pool}};
    second_test_pool_list second_list{test_allocator_type{test_pool}};

    static_assert(cues::provides_allocator<typename first_test_pool_list::node_type>::value,
                      "allocated_unique_ptr defeats its purpose of providing the allocator");
    static_assert(cues::provides_allocator<typename second_test_pool_list::node_type>::value,
                      "allocated_unique_ptr defeats its purpose of providing the allocator");
    static_assert(std::is_same<typename first_test_pool_list::allocator_type, typename second_test_pool_list::allocator_type>::value,
                  "allocators are supposed to be the same for the two test pools.");

    std::size_t const common_list_size = (std::min<std::size_t>)(first_list.max_size(), second_list.max_size());
    std::size_t const half_size = common_list_size/2u;

    assert(((void)"new list not empty",first_list.empty()));
    cumulative_assertion_failures += static_cast<std::size_t>(!first_list.empty());

    assert(((void)"new list size != 0",first_list.size() == 0u));
    cumulative_assertion_failures += static_cast<std::size_t>(first_list.size() != 0u);

    assert(((void)"new list max_size != N",first_list.max_size() == first_list_size));
    cumulative_assertion_failures += static_cast<std::size_t>(first_list.max_size() != first_list_size);

    assert(((void)"new list max_size != N",second_list.max_size() == second_list_size));
    cumulative_assertion_failures += static_cast<std::size_t>(second_list.max_size() != second_list_size);

    assert(((void)"two empty lists do not compare equal",first_list == second_list));
    cumulative_assertion_failures += static_cast<std::size_t>(first_list != second_list);

    try{

        if constexpr (std::is_copy_constructible<test_element_type>::value)
        {
            first_list.push_back(zero_element);
        }
        else
        {
            first_list.push_back(make_value<test_element_type>(0));
        }

        assert(((void)"size != 1 after one push",first_list.size() == 1u));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.size() != 1u);

        assert(((void)"front != back after one push",first_list.back() == first_list.front()));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.back() != first_list.front());

        assert(((void)"back != value pushed back",first_list.back() == zero_element));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.back() != zero_element);

        assert(((void)"front != at(0) after one push",first_list.at(0) == first_list.front()));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.at(0) != first_list.front());

        assert(((void)"at(0) != [0] after one push",first_list.at(0) == first_list[0]));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.at(0) != first_list[0]);

        first_list.pop_back();
        assert(((void)"pops == pushes list not empty",first_list.empty()));
        cumulative_assertion_failures += static_cast<std::size_t>(!first_list.empty());

    }
    catch(...)
    {
        // none of the above operations should fail
        ++cumulative_assertion_failures;
    }

    try {
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.at(0) != zero_element);
        // should have thrown exception
        ++cumulative_assertion_failures;
    }
    catch (std::out_of_range const &)
    {
        ; // correct exception
    }
    catch (...)
    {
        // wrong exception
        ++cumulative_assertion_failures;
    }

    try{
        std::size_t const safe_size = (std::min<std::size_t>)(common_list_size, pool_size/2u);
        for (std::size_t count = 0;
            count < safe_size;
            ++count
        ) {
            // fill with safe_size even numbers in order
            first_list.push_back(make_value<test_element_type>(2u*count));
        }
        assert(((void)"unexpected size after fill",first_list.size() == safe_size));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.size() != safe_size);
        // use iterators to go forward
        for ( auto itr = first_list.begin();
            itr != first_list.end();
            ++itr
        )
        {
            // fill with safe_size odd numbers in order
            second_list.push_back(make_value<test_element_type>(value_or_first(*itr) + 1u));
        }
        assert(((void)"unexpected size after fill",second_list.size() == safe_size));
        cumulative_assertion_failures += static_cast<std::size_t>(second_list.size() != safe_size);
        // verify not equal
        assert(((void)"unequal list compares equal",first_list != second_list));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list == second_list);


        // scope to destruct temporary
        {
            // steal from first list
            first_test_pool_list disposable = std::move(first_list);
            // verify number stolen
            assert(((void)"unexpected size after move",disposable.size() == safe_size));
            cumulative_assertion_failures += static_cast<std::size_t>(disposable.size() != safe_size);

            // reverse iterate, check sorting
            for (auto sitr = second_list.crbegin();
                sitr != second_list.crend();
                ++sitr
            )
            {
                auto const & elem = disposable.at(
                    // end - 1 = last valid
                    (disposable.size() - 1) -
                    // distance from last
                    (sitr - second_list.crbegin())
                );
                assert(((void)"unexpected ordering", *sitr > elem));
                cumulative_assertion_failures += static_cast<std::size_t>(*sitr <= elem);
            }
        }

        if constexpr (std::is_copy_constructible<test_element_type>::value)
        {
            // was moved-from, put into known state via copy assignment
            first_list = second_list;

            assert(((void)"copied list not the same size",first_list.size() == second_list.size()));
            cumulative_assertion_failures += static_cast<std::size_t>(first_list.size() != second_list.size());

            assert(((void)"copied list does not compare equal",first_list == second_list));
            cumulative_assertion_failures += static_cast<std::size_t>(first_list != second_list);

            // clear second list, which had odd numbers, now first list has
            // safe_size odd numbers and second list will be empty
            second_list.clear();
        }
        else
        {
            first_list = std::move(second_list);
            second_list = std::move(second_test_pool_list{test_allocator_type{test_pool}});
        }

        assert(((void)"copied list size changed after clearing source",first_list.size() == safe_size));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.size() != safe_size);

        assert(((void)"not empty after being cleared",second_list.empty()));
        cumulative_assertion_failures += static_cast<std::size_t>(!second_list.empty());

        assert(((void)"non-empty list compares equal to empty list",first_list != second_list));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list == second_list);

        // take every other element from odd number list
        // have common_list_size/2 odd numbers
        for (auto itr = first_list.begin();
            itr != first_list.cend();
            ++itr
        ) {
            if constexpr (std::is_copy_constructible<test_element_type>::value)
            {
                second_list.emplace_back(*itr);
            }
            else
            {
                second_list.emplace_back(std::move(*itr));
            }
            // assigning to the result of erase gives us
            // the iterator to the next element, so the
            // increment skips the *next* element and
            // we get every other one
            itr = first_list.erase(itr);
            // break before incrementing if the increment would be invalid
            if (itr == first_list.cend()) {break;}
        }

        assert(((void)"unexpected element count", safe_size == first_list.size() + second_list.size()));
        cumulative_assertion_failures += static_cast<std::size_t>(safe_size != first_list.size() + second_list.size());

        // insert missing safe_size / 2 odd numbers, in order
        {
            std::uint_least8_t idx = 0;
            // now loop second list and insert from first list
            for (auto itr = second_list.begin();
                itr != second_list.end();
                ++itr
            )
            {
                // taking every other meant that we got 1,5,11,...
                // from 1,3,5,7,... leaving 3,7,13,...
                // insert is supposed to insert before the iterator,
                // so, to get the remaining ones, take from first
                // if first is less than ours
                if (idx < first_list.size() && *itr > first_list[idx])
                {
                    if constexpr(std::is_copy_constructible<test_element_type>::value)
                    {
                        second_list.insert(itr, first_list[idx]);
                    }
                    else
                    {
                        second_list.insert(itr, std::move(first_list[idx]));
                    }
                    ++idx;
                }
            }
            // after the loop, one final check on idx
            for (;idx < first_list.size();++idx)
            {
                if constexpr(std::is_copy_constructible<test_element_type>::value)
                {
                    second_list.insert(second_list.end(), first_list[idx]);
                }
                else
                {
                    second_list.insert(second_list.end(), std::move(first_list[idx]));
                }
            }
        }

        assert(((void)"unexpected element count", safe_size == second_list.size() ));
        cumulative_assertion_failures += static_cast<std::size_t>(safe_size != second_list.size());

        // now erase the rest of first, in backward order
        for (auto itr = first_list.cend();
            itr != first_list.cbegin();
            itr = first_list.erase(--itr)
        ) { }

        assert(((void)"erased list not empty", first_list.empty() ));
        cumulative_assertion_failures += static_cast<std::size_t>(!first_list.empty());

    }
    catch (...)
    {
        // wrong exception
        assert(((void)"wrong exception", false));
        ++cumulative_assertion_failures;
    }

    try
    {
        // use assign to provide values to first_list that should match second_list,
        // which has list_size odd numbers
        // initializer_list cannot be moved from (it refers to const elements)
        // so skip this one if we cannot copy
        if constexpr (std::is_copy_constructible<test_element_type>::value)
        {
            first_list.assign({
                 make_value<test_element_type>( 1),make_value<test_element_type>( 3)
                ,make_value<test_element_type>( 5),make_value<test_element_type>( 7)
                ,make_value<test_element_type>( 9),make_value<test_element_type>(11)
                ,make_value<test_element_type>(13),make_value<test_element_type>(15)
                ,make_value<test_element_type>(17),make_value<test_element_type>(19)
                ,make_value<test_element_type>(21),make_value<test_element_type>(23)
                ,make_value<test_element_type>(25),make_value<test_element_type>(27)
                ,make_value<test_element_type>(29),make_value<test_element_type>(31)
            });
        }

        // trim any extras from the second list
        if (second_list.size() > first_list.size())
        {
            second_list.erase(second_list.cend() - (second_list.size() - first_list.size()) , second_list.cend());
        }

        assert(((void)"error in assign or list insertion", first_list == second_list ));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list != second_list);

        // merge with self should do nothing
        first_list.merge(first_list, std::less<test_element_type>{});
        // verify still equal
        assert(((void)"error in assign or list insertion", first_list == second_list ));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list != second_list);
        {
            // merge with empty list should not change contents
            first_test_pool_list disposable{test_allocator_type{test_pool}};
            first_list.merge(disposable, std::less<test_element_type>{});
            assert(((void)"error in assign or list insertion", first_list == second_list ));
            cumulative_assertion_failures += static_cast<std::size_t>(first_list != second_list);
        }

    }
    catch(std::bad_alloc const &)
    {
        assert(((void)"should have been able to assign", first_list.max_size() < 16u || pool_size < 16u));
        cumulative_assertion_failures += static_cast<std::size_t>(first_list.max_size() >= 16u && pool_size >= 16u);

    }
    catch(...)
    {
        assert(((void)"wrong exception", false));
        ++cumulative_assertion_failures;
    }

    try
    {
        std::size_t const safe_size = (std::min<std::size_t>)(half_size, pool_size/2u);
        std::less<test_element_type> comp {};
        first_list.clear();
        second_list.clear();
        // fill both half way with the same values
        for (std::size_t index = 0;
            index < safe_size;
            ++index
        ) {
            first_list.push_back(make_value<test_element_type>(index));
            second_list.push_back(make_value<test_element_type>(index));
        }

        checked_splice<second_test_pool_list, first_test_pool_list> splice_checker(second_list, first_list);

        //splice from beginning of first to end of second
        cumulative_assertion_failures += splice_checker(
            second_list, second_list.end(), first_list,
            first_list.begin(),
            ((first_list.end() != first_list.begin()) ? first_list.begin() + 1 : first_list.begin() )
        );

        // splice from end of first to beginning of second
        cumulative_assertion_failures += splice_checker(
            second_list, second_list.begin(), first_list,
            (first_list.end() != first_list.begin()) ? --(first_list.end()) : first_list.end(),
            first_list.end()
        );
        // splice remainder of first to middle of second
        cumulative_assertion_failures += splice_checker(
            second_list,
            second_list.begin() + ((second_list.end() - second_list.begin())/2),
            first_list,
            first_list.begin(),
            first_list.end()
        );

        // second list should now be empty
        assert(((void)"splice did not remove from list", first_list.empty()));
        cumulative_assertion_failures += static_cast<std::size_t>(!first_list.empty());

        second_list.sort(comp);
        // the first list was a copy of the second, and we just
        // sorted it in order, so there should be two of each value
        cumulative_assertion_failures += container_sort_errors(
            second_list.cbegin(),
            second_list.cend(),
            comp
        );

        // extract and insert from second_list into first_list
        // this inserts in order, as nodes are consumed from the
        // front of second_list and put into the back of first_list,
        // which is initially empty
        for (auto itr = second_list.cbegin();
            itr != second_list.cend();
            itr = second_list.cbegin()
        ) {
            first_list.insert(first_list.end(), second_list.extract(itr));
        }

        assert(((void)"extract + insert changed allocation", (first_list.get_allocator()).remaining_size() == splice_checker.get_original_allocator_capacity()));
        cumulative_assertion_failures += static_cast<std::size_t>((first_list.get_allocator()).remaining_size()
                                          != splice_checker.get_original_allocator_capacity());

        // should still be sorted with its duplicates
        cumulative_assertion_failures += container_sort_errors(
            first_list.cbegin(),
            first_list.cend(),
            comp
        );

        // half_size was half the size of the smaller list.
        // first_list is currently full and sorted, second is empty.
        // remove the end half of first list
        // while adding elements to the second list, in sorted order,
        // that will be inter-mixed
        for (std::uint_least16_t index = 0;
            index < safe_size;
            ++index
        ) {
            first_list.pop_back();
            second_list.push_back(make_value<test_element_type>(2*index));
        }
        // we added as much as we removed, so the overall allocations should be the same
        assert(((void)"equivalent pop & push changed allocation", (first_list.get_allocator()).remaining_size() == splice_checker.get_original_allocator_capacity()));
        cumulative_assertion_failures += static_cast<std::size_t>((first_list.get_allocator()).remaining_size()
                                          != splice_checker.get_original_allocator_capacity());
        //  this should maintain the sorted order
        first_list.merge(second_list, comp);
        // merge splices elements
        assert(((void)"merge changed allocation", (first_list.get_allocator()).remaining_size() == splice_checker.get_original_allocator_capacity()));
        cumulative_assertion_failures += static_cast<std::size_t>((first_list.get_allocator()).remaining_size()
                                          != splice_checker.get_original_allocator_capacity());
        // verify that the merged list is still in order
        cumulative_assertion_failures += container_sort_errors(
            first_list.cbegin(),
            first_list.cend(),
            comp
        );

        // use reordering erase to remove every other element out of order
        for ( auto itr = first_list.cbegin();
            first_list.cend() - itr > 1;
            // erase will provide the next (unerased) element,
            // so increment again after that leaves an element
            // in place, so that we erase every other
            itr =  first_list.reordering_erase(itr) + 1
        )
        {}
        // now insert some out of order elements.
        // we just cut half, so adding half of that should fit.
        if constexpr (std::is_copy_constructible<test_element_type>::value)
        {
            first_list.insert(
               first_list.cbegin() + (first_list.cend() - first_list.cbegin())/2,
               safe_size/2u,
               make_value<test_element_type>(100u)
            );
        }
        else
        {
            auto itr = first_list.begin() + (first_list.end() - first_list.begin())/2;
            for (std::size_t copies = 0;
                copies < safe_size/2u;
                ++copies)
            {
                itr = first_list.insert(
                    itr,
                    make_value<test_element_type>(100u)
                );
                // it doesn't really matter if we insert identical elements
                // repeatedly before each other or after each other, and after means smaller moves
                ++itr;
            }
        }
        // sort it again
        first_list.sort(comp);
        // verify
        cumulative_assertion_failures += container_sort_errors(
            first_list.cbegin(),
            first_list.cend(),
            comp
        );

    }
    catch(...)
    {

        assert(((void)"should not have thrown an exception here", false));
        ++cumulative_assertion_failures;
    }


    try{
        if constexpr(std::is_copy_constructible<test_element_type>::value)
        {
            first_list.insert(
                first_list.cend(),
                (std::numeric_limits<typename first_test_pool_list::size_type>::max)(),
                zero_element
            );
        }
        else
        {
            // can't copy, so repeat insert temporary
            for (std::size_t inserts = 0;
                inserts < (std::numeric_limits<typename first_test_pool_list::size_type>::max)();
                ++inserts)
            {
                first_list.insert(
                    first_list.cend(),
                    make_value<test_element_type>(100u)
                );
            }
        }
    }
    catch(std::bad_alloc const &)
    {
        if (pool_size < (std::numeric_limits<typename first_test_pool_list::size_type>::max)()
            || first_list.max_size() < (std::numeric_limits<typename first_test_pool_list::size_type>::max)()
        )
        {
            // correct exception
            ;
        }
        else
        {
            assert(((void)"should not have thrown", false));
            ++cumulative_assertion_failures;
        }
    }
    catch(...)
    {
        assert(((void)"wrong exception", false));
        ++cumulative_assertion_failures;
    }

    first_list.clear();

    if constexpr (std::is_copy_constructible<test_element_type>::value)
    {
        try{
            first_list.assign(
                (std::numeric_limits<typename first_test_pool_list::size_type>::max)(),
                zero_element
            );
            first_list.clear();
        }
        catch(std::bad_alloc const &)
        {
            first_list.clear();
            if (pool_size < (std::numeric_limits<typename first_test_pool_list::size_type>::max)()
                || first_list.max_size() < (std::numeric_limits<typename first_test_pool_list::size_type>::max)()
            )
            {
                // correct exception
                ;
            }
            else
            {
                assert(((void)"should not have thrown", false));
                ++cumulative_assertion_failures;
            }
        }
        catch(...)
        {
            assert(((void)"wrong exception", false));
            ++cumulative_assertion_failures;
        }
    }

}


std::size_t check_pool_list_test_runtime_assertions()
{
    std::size_t cumulative_assertion_failures = 0;

    // simple case; plain number, lists are even, equal numbers that fit in pool
    check_list_errors<std::uint_least16_t, 32u>(cumulative_assertion_failures);
    // plain number, lists are odd, unequal numbers that fit in the pool
    check_list_errors<std::uint_least8_t, 64u, 23u>(cumulative_assertion_failures);
    // plain number, lists are odd, minimal size, less than the pool
    check_list_errors<std::uint_least64_t, 5u, 1u, 1u>(cumulative_assertion_failures);
    // plain number, larger than pool, sizes maximum uint_least8_t
    check_list_errors<std::uint_least32_t, 8u, 255u, 6u>(cumulative_assertion_failures);
    // plain number, only one is larger than the pool but also may use different
    // size_type
    check_list_errors<std::uint_least16_t, 17u, 8u, 65535u>(cumulative_assertion_failures);
    // aggregate
    check_list_errors<cues::test::trivial_aggregate, 32u>(cumulative_assertion_failures);
    check_list_errors<cues::test::trivial_aggregate, 3u,1u,1u>(cumulative_assertion_failures);
    check_list_errors<cues::test::trivial_aggregate, 8u, 255u,65535u>(cumulative_assertion_failures);

    // non-copyable
    check_list_errors<std::pair<std::uint_least16_t, cues::test::move_only>, 16u>(cumulative_assertion_failures);

    return cumulative_assertion_failures;
}


}
}
#endif
