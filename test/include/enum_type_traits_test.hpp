#ifndef CUES_TEST_ENUM_TYPE_TRAITS_TEST_HPP_INCLUDED
#define CUES_TEST_ENUM_TYPE_TRAITS_TEST_HPP_INCLUDED

#include <cstdint>
#include <type_traits>
#include "cues/enum_type_traits.hpp"

namespace cues
{
namespace test
{

enum test_enum_empty
{};

// positive, sequential values
enum class test_enum_8 : std::uint_least8_t
{
    test_8_0,
    test_8_1,
    test_8_2,
    test_8_3,
    test_8_4,
    test_8_5,
    test_8_6,
    test_8_7
};

// positive, non-sequential values
enum class test_enum_16 : std::uint_least16_t
{
    test_16_0 = 0x7F00,
    test_16_1 = 0x7F80,
    test_16_2 = 0x7FC0,
    test_16_3 = 0x7FE0,
    test_16_4 = 0x7FF0,
    test_16_5 = 0x7FF8,
    test_16_6 = 0x7FFC,
    test_16_7 = 0x7FFE
};

// positive and negative sparse values in no particular order
enum class test_enum_32 : std::int_least32_t
{
    test_32_0 =  0x7FFFFFFF,
    test_32_1 = -0x7FFFFFFF,
    test_32_2 =  0x0,
    test_32_3 = -0x7FFF0000,
    test_32_4 = -0x0000007F,
    test_32_5 =  0x7FFFFFF0,
    test_32_6 =  0x0000007F,
    test_32_7 =  0x7FFFFF00

};

    // is_integral_or_enum
    static_assert(cues::is_integral_or_enum<signed char>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<unsigned char>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<char>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<short int>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<unsigned short int>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<int>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<unsigned int>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<long int>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<unsigned long int>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<long long int>::value, "Implementation error in is_integral_or_enum.");
    static_assert(cues::is_integral_or_enum<unsigned long long int>::value, "Implementation error in is_integral_or_enum.");

    static_assert(!cues::is_integral_or_enum<float>::value, "Implementation error in is_integral_or_enum.");
    static_assert(!cues::is_integral_or_enum<double>::value, "Implementation error in is_integral_or_enum.");
    static_assert(!cues::is_integral_or_enum<long double>::value, "Implementation error in is_integral_or_enum.");

    static_assert(cues::is_integral_or_enum<cues::test::test_enum_empty>::value, "Assertion violation: integral or enum test on test_enum_empty");
    static_assert(cues::is_integral_or_enum<cues::test::test_enum_8>::value, "Assertion violation: integral or enum test on test_enum_8");
    static_assert(cues::is_integral_or_enum<cues::test::test_enum_16>::value, "Assertion violation: integral or enum test on test_enum_16");
    static_assert(cues::is_integral_or_enum<cues::test::test_enum_32>::value, "Assertion violation: integral or enum test on test_enum_32");


    // is_or_underlying_is_unsigned, is_or_underlying_is_signed
    static_assert(cues::is_or_underlying_is_unsigned<unsigned char>::value
                  && !cues::is_or_underlying_is_signed<unsigned char>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(!cues::is_or_underlying_is_unsigned<signed char>::value
                  && cues::is_or_underlying_is_signed<signed char>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    // char is allowed to be either, but must be only one
    static_assert(cues::is_or_underlying_is_unsigned<char>::value
                   != cues::is_or_underlying_is_signed<char>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(cues::is_or_underlying_is_unsigned<unsigned short int>::value
                  && !cues::is_or_underlying_is_signed<unsigned short int>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(!cues::is_or_underlying_is_unsigned<signed short int>::value
                && cues::is_or_underlying_is_signed<signed short int>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(cues::is_or_underlying_is_unsigned<unsigned int>::value
                && !cues::is_or_underlying_is_signed<unsigned int>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(!cues::is_or_underlying_is_unsigned<signed int>::value
                && cues::is_or_underlying_is_signed<signed int>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(cues::is_or_underlying_is_unsigned<unsigned long int>::value
                && !cues::is_or_underlying_is_signed<unsigned long int>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(!cues::is_or_underlying_is_unsigned<signed long int>::value
                && cues::is_or_underlying_is_signed<signed long int>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(cues::is_or_underlying_is_unsigned<unsigned long long int>::value
                && !cues::is_or_underlying_is_signed<unsigned long long int>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(!cues::is_or_underlying_is_unsigned<signed long long int>::value
                && cues::is_or_underlying_is_signed<signed long long int>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");

    static_assert(!cues::is_or_underlying_is_unsigned<float>::value
                && cues::is_or_underlying_is_signed<float>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(!cues::is_or_underlying_is_unsigned<double>::value
                && cues::is_or_underlying_is_signed<double>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");
    static_assert(!cues::is_or_underlying_is_unsigned<long double>::value
                && cues::is_or_underlying_is_signed<long double>::value,
                  "Implementation error in is_or_underlying_is_unsigned.");

     static_assert(cues::is_or_underlying_is_unsigned<cues::test::test_enum_empty>::value
                   != cues::is_or_underlying_is_signed<cues::test::test_enum_empty>::value,
                  "Assertion violation: is_or_underlying_is_unsigned test on test_enum_empty");
    static_assert(cues::is_or_underlying_is_unsigned<cues::test::test_enum_8>::value
                && !cues::is_or_underlying_is_signed<cues::test::test_enum_8>::value,
                  "Assertion violation: is_or_underlying_is_unsigned test on test_enum_8");
    static_assert(cues::is_or_underlying_is_unsigned<cues::test::test_enum_16>::value
                && !cues::is_or_underlying_is_signed<cues::test::test_enum_16>::value,
                  "Assertion violation: is_or_underlying_is_unsigned test on test_enum_16");
    static_assert(!cues::is_or_underlying_is_unsigned<cues::test::test_enum_32>::value
                && cues::is_or_underlying_is_signed<cues::test::test_enum_32>::value,
                  "Assertion violation: is_or_underlying_is_unsigned test on test_enum_32");

    static_assert(
        std::is_same<typename cues::type_or_underlying<test_enum_8>::type, std::uint_least8_t>::value,
        "error in enum underlying type for uint_least8_t test"
    );

    static_assert(
        std::is_same<typename cues::type_or_underlying<std::uint_least8_t>::type, std::uint_least8_t>::value,
        "error in identity type for uint_least8_t test"
    );

    static_assert(
        std::is_same<typename cues::type_or_underlying<test_enum_16>::type, std::uint_least16_t>::value,
        "error in enum underlying type for uint_least16_t test"
    );

    static_assert(
        std::is_same<typename cues::type_or_underlying<std::uint_least16_t>::type, std::uint_least16_t>::value,
        "error in identity type for uint_least16_t test"
    );

    static_assert(
        std::is_same<typename cues::type_or_underlying<test_enum_32>::type, std::int_least32_t>::value,
        "error in enum underlying type for int_least32_t test"
    );

    static_assert(
        std::is_same<typename cues::type_or_underlying<std::int_least32_t>::type, std::int_least32_t>::value,
        "error in identity type for int_least32_t test"
    );

    static_assert(
        std::is_integral<typename cues::type_or_underlying<test_enum_empty>::type>::value,
        "error in empty enum with unspecified type"
    );

    // scoped enums have int when type is unspecified
    enum class test_scoped_enum
    {};

    static_assert(
        std::is_same<typename cues::type_or_underlying<test_scoped_enum>::type, int>::value,
        "error in enum underlying type for empty scoped enum test"
    );

    static_assert(
        std::is_same<typename cues::type_or_underlying<std::intmax_t>::type, std::intmax_t>::value,
        "error in identity type for intmax_t test"
    );

    class non_enum_class
    {};

    static_assert(
        std::is_same<typename cues::type_or_underlying<non_enum_class>::type, non_enum_class>::value,
        "error in identity type for class test"
    );

}
}
#endif // CUES_TEST_ENUM_TYPE_TRAITS_TEST_HPP_INCLUDED
