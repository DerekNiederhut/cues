#ifndef CUES_TEST_TRIPLE_BUFFER_TEST_HPP_INCLUDED
#define CUES_TEST_TRIPLE_BUFFER_TEST_HPP_INCLUDED

#include <cstddef>

namespace cues
{
namespace test
{

std::size_t check_triple_buffer_test_runtime_assertions();

}
}
#endif // CUES_TEST_TRIPLE_BUFFER_TEST_HPP_INCLUDED
