#ifndef CUES_TEST_PASSKEY_TEST_HPP_INCLUDED
#define CUES_TEST_PASSKEY_TEST_HPP_INCLUDED

#include <type_traits>

#include "cues/passkey.hpp"
#include "cues/functional.hpp"

namespace cues
{
namespace test
{

    class accessor_one;
    class accessor_two;
    class accessor_three;
    class denied_one;

    // for the purposes of the test, the resource objects simply return true,
    // but will trigger compile-time errors if a class without access tries to call them.

    class simple_resource
    {
        public:
            using permission_type = passkey<accessor_one>;
            constexpr bool locked_access_member(permission_type) const {return true;}
    };

    class multiaccess_resource
    {
    public:
        using permission_type = passkey_permission_group<passkey<accessor_one>, passkey<accessor_two>, passkey<accessor_three>>;
        static_assert(is_contained_in<passkey<accessor_one>, passkey<accessor_one>, passkey<accessor_two>, passkey<accessor_three>>::value, "Assertion violation: contained_in fails.");
        static_assert(is_contained_in<passkey<accessor_two>, passkey<accessor_one>, passkey<accessor_two>, passkey<accessor_three>>::value, "Assertion violation: contained_in fails.");
        static_assert(is_contained_in<passkey<accessor_three>, passkey<accessor_one>, passkey<accessor_two>, passkey<accessor_three>>::value, "Assertion violation: contained_in fails.");
        constexpr bool locked_access_member(permission_type) const {return true;}
        constexpr bool locked_only_two_permitted(passkey<accessor_two>) const {return true;}
        constexpr bool locked_only_three_permitted(passkey<accessor_three>) const {return true;}
    };

    template<class RESOURCE, class ACCESSOR>
    struct appropriate_type
    : public std::integral_constant<
        cues::functor_select_mode,
        ((std::is_constructible<typename RESOURCE::permission_type, cues::passkey<ACCESSOR> >::value) ?
        functor_select_mode::select_forward_to_member_fn
        : functor_select_mode::select_default)
     >
    {};

    // helper type alias to reduce repetition
    template<class RESOURCE, class ACCESSOR>
    struct functor_helper
    {

        using type = typename cues::select_default_or_forwarding<
                appropriate_type<RESOURCE, ACCESSOR>::value,
                RESOURCE,
                bool,
                cues::passkey<ACCESSOR>
            >::type;
    };

    class accessor_one
    {
    public:
        // straightforward test: we have access, call it directly
        constexpr bool do_thing_with_simple_resource(simple_resource const & resource) const
        {
            return resource.locked_access_member({});
        }
        // straightforward test: we have access, call it directly
        constexpr bool do_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
             return resource.locked_access_member(passkey<accessor_one>());
        }
        // this one should fail to compile if we try to call it directly.
        // in order to not fail, we switch on whether to call it or return false
        // with a helper functor
        constexpr bool do_two_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
            return (cues::make_functor<
                        typename functor_helper<
                            multiaccess_resource const,
                            accessor_one
                        >::type,
                         decltype (&resource),
                        decltype(&multiaccess_resource::locked_only_two_permitted)
                    >(&resource, &multiaccess_resource::locked_only_two_permitted)
                    )(std::move(passkey<accessor_one>{}));
        }
        // same as above
        constexpr bool do_three_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
            return (cues::make_functor<
                        typename functor_helper<
                            multiaccess_resource const,
                            accessor_one
                        >::type,
                        decltype(&resource),
                        decltype(&multiaccess_resource::locked_only_three_permitted)
                    >(&resource, &multiaccess_resource::locked_only_three_permitted)
                    )(std::move(passkey<accessor_one>{}));
        }
    };

    class accessor_two
    {
        public:
            using simple_functor_type = functor_helper<simple_resource const, accessor_two>::type;
            using multiaccess_functor_type = functor_helper<multiaccess_resource const, accessor_two>::type;

        // should return false
        constexpr bool do_thing_with_simple_resource(simple_resource const & resource) const
        {
            return (cues::make_functor<
                        simple_functor_type,
                        decltype(&resource),
                        decltype(&simple_resource::locked_access_member)
                    >(&resource, &simple_resource::locked_access_member)
                    )(std::move(passkey<accessor_two>{}));
        }
        // direct test of group
        constexpr bool do_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
            return resource.locked_access_member(multiaccess_resource::permission_type(passkey<accessor_two>{}));
        }
        // direct test of narrower permission
        constexpr bool do_two_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
            return resource.locked_only_two_permitted({});
        }
        // indirect test of forbidden permission
        constexpr bool do_three_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
            return (cues::make_functor<
                        multiaccess_functor_type,
                        decltype(&resource),
                        decltype(&multiaccess_resource::locked_only_three_permitted)
                    >(&resource, &multiaccess_resource::locked_only_three_permitted)
                    )(std::move(passkey<accessor_two>{}));
        }
    };

    class accessor_three
    {
        public:
            using simple_functor_type = functor_helper<simple_resource const, accessor_three>::type;
            using multiaccess_functor_type = functor_helper<multiaccess_resource const, accessor_three>::type;

        // indirect test of fail condition again
        constexpr bool do_thing_with_simple_resource(simple_resource const & resource) const
        {
            return (cues::make_functor<
                        simple_functor_type,
                        decltype(&resource),
                        decltype(&simple_resource::locked_access_member)
                    >(&resource, &simple_resource::locked_access_member)
                    )(std::move(passkey<accessor_three>{}));
        }
        // indirect test of success on group
        constexpr bool do_thing_with_multiaccess_resource(multiaccess_resource const  & resource) const
        {
            return (cues::make_functor<
                        multiaccess_functor_type,
                        decltype(&resource),
                        decltype(&multiaccess_resource::locked_access_member)
                    >(&resource, &multiaccess_resource::locked_access_member)
                    )(std::move(passkey<accessor_three>{}));
        }
        // indirect test of failure
        constexpr bool do_two_thing_with_multiaccess_resource(multiaccess_resource const  & resource) const
        {
            return (cues::make_functor<
                        multiaccess_functor_type,
                        decltype(&resource),
                        decltype(&multiaccess_resource::locked_only_two_permitted)
                    >(&resource, &multiaccess_resource::locked_only_two_permitted)
                    )(std::move(passkey<accessor_three>{}));
        }
        // indirect test success on narrow
        constexpr bool do_three_thing_with_multiaccess_resource(multiaccess_resource const  & resource) const
        {
            return (cues::make_functor<
                        multiaccess_functor_type,
                        decltype(&resource),
                        decltype(&multiaccess_resource::locked_only_three_permitted)
                    >(&resource,  &multiaccess_resource::locked_only_three_permitted)
                    )(std::move(passkey<accessor_three>{}));
        }
    };

    class denied_one
    {
    public:
        using simple_functor_type = functor_helper<simple_resource const, denied_one>::type;
        using multiaccess_functor_type = functor_helper<multiaccess_resource const, denied_one>::type;
        constexpr bool do_thing_with_simple_resource(simple_resource const & resource) const
        {
            return (cues::make_functor<
                        simple_functor_type,
                        decltype(&resource),
                        decltype(&simple_resource::locked_access_member)
                    >(&resource,  &simple_resource::locked_access_member))
                    (passkey<denied_one>{});
        }
        constexpr bool do_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
            return (cues::make_functor<
                        multiaccess_functor_type,
                        decltype(&resource),
                        decltype(&multiaccess_resource::locked_access_member)
                    >(&resource,  &multiaccess_resource::locked_access_member))
                    (passkey<denied_one>{});
        }
        constexpr bool do_two_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
            return (cues::make_functor<
                        multiaccess_functor_type,
                        decltype(&resource),
                        decltype(&multiaccess_resource::locked_only_two_permitted)
                    >(&resource &multiaccess_resource::locked_only_two_permitted))
                    (passkey<denied_one>{});
        }
        constexpr bool do_three_thing_with_multiaccess_resource(multiaccess_resource const & resource) const
        {
            return (cues::make_functor<
                        multiaccess_functor_type,
                        decltype(&resource),
                        decltype(&multiaccess_resource::locked_only_three_permitted)
                    >(&resource,  &multiaccess_resource::locked_only_three_permitted))
                    (passkey<denied_one>{});
        }
    };


}
}


#endif // CUES_TEST_PASSKEY_TEST_HPP_INCLUDED
