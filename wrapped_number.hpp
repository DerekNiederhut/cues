#ifndef CUES_WRAPPED_NUMBER_HPP
#define CUES_WRAPPED_NUMBER_HPP

#include <cstddef>
#include <functional>
#include <limits>
#include <type_traits>

#include "cues/type_traits.hpp"      //< type or promoted
#include "cues/enum_type_traits.hpp" //< type_or_underlying

namespace cues {

//! Template for numbers of an arithmetic type which may need to be discriminated, but
//! with minimal overhead.
//! Wraps the number in a new type to differentiate it from other numbers of the same type
//! for clarity of intent and compile-time checking, for example overload resolution.
//! Could also be useful for example to prevent confusing different identity types that
//! use the same numeric type "T" (use different discriminator numbers to do so).
//! for example:
//! using handle_id = cues::wrapped_number<std::size_t,0>;
//! using message_id = cues::wrapped_number<std::size_t,1>;
//! See also the fnv constexpr hash if you want to use strings to generate the discriminator,
//! for example:
//! using handle_id = cues::wrapped_number<std::size_t,
//!     cues::fnv_1a<std::numeric_limits<std::size_t>::digits>(
//!         std::string_view("handle_id"),
//!         cues::fnv1_init<std::numeric_limits<std::size_t>::digits>()
//!     )
//! >;
//! Types can also be used as a discriminator, in case further selection
//! based on type, instead of a fixed value, is necessary.  Default type is
//! void and that would suffice if this feature is not needed.
//! Requires explicit conversion to or from the number, like an enum class,
//! but also supports mathematical operators if the underlying type does,
//! and makes it clear that this is a number and not an enumeration.
//! This gives it potential use in overload selection--that
//! is, func(wrapped_number<int,0>) could not be accidentally selected
//! instead of func(int), and vice versa, without requiring intermediate
//! casts for simple arithmetic and logic operations.
//! Comparison operators and specialization of std::hash
//! provided for standard containers & algorithms.


template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE = void>
class wrapped_number
{
    public:
        using underlying_type = T;

        wrapped_number() = default;


        explicit constexpr wrapped_number(underlying_type const & raw_val) noexcept(std::is_nothrow_copy_constructible<T>::value)
        : value(raw_val)
        {}

        explicit constexpr operator underlying_type () const noexcept
        {
            return value;
        }

        template<typename U = T>
        constexpr
        typename std::enable_if<
            std::numeric_limits<U>::is_specialized,
            typename type_or_promoted<U>::type
        >::type
        operator+() const
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            return +value;
        }

        template<typename U = T>
        constexpr
        typename std::enable_if<
            std::numeric_limits<U>::is_specialized,
            typename type_or_promoted<U>::type
        >::type
        operator-() const
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            return -value;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized
            && std::numeric_limits<V>::is_specialized,
            wrapped_number
        >::type & operator+=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value += static_cast<operating_type>(v);
            return *this;
        }

        template<typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
            std::numeric_limits<U>::is_specialized,
            wrapped_number
        >::type & operator++()
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            return this->operator+=(underlying_type{1});
        }

        template<typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
            std::numeric_limits<U>::is_specialized,
            wrapped_number
        >::type operator++(int)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            wrapped_number temp{*this};
            operator++();
            return temp;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized
            && std::numeric_limits<V>::is_specialized,
            wrapped_number
        >::type & operator-=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value -= static_cast<operating_type>(v);
            return *this;
        }

        template<typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
            std::numeric_limits<U>::is_specialized,
            wrapped_number
        >::type & operator--()
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            return this->operator-=(underlying_type{1});
        }

        template<typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
            std::numeric_limits<U>::is_specialized,
            wrapped_number
        >::type operator--(int)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            wrapped_number temp{*this};
            operator--();
            return temp;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized
            && std::numeric_limits<V>::is_specialized,
            wrapped_number
        >::type & operator*=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value *= static_cast<operating_type>(v);
            return *this;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized
            && std::numeric_limits<V>::is_specialized,
            wrapped_number
        >::type & operator/=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value /= static_cast<operating_type>(v);
            return *this;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized
            && std::numeric_limits<V>::is_specialized,
            wrapped_number
        >::type & operator%=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value %= static_cast<operating_type>(v);
            return *this;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer
            && std::numeric_limits<V>::is_specialized && std::numeric_limits<V>::is_integer,
            wrapped_number
        >::type & operator&=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value &= static_cast<operating_type>(v);
            return *this;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer
            && std::numeric_limits<V>::is_specialized && std::numeric_limits<V>::is_integer,
            wrapped_number
        >::type & operator|=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value |= static_cast<operating_type>(v);
            return *this;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer
            && std::numeric_limits<V>::is_specialized && std::numeric_limits<V>::is_integer,
            wrapped_number
        >::type & operator^=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value ^= static_cast<operating_type>(v);
            return *this;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer
            && std::numeric_limits<V>::is_specialized && std::numeric_limits<V>::is_integer,
            wrapped_number
        >::type & operator<<=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value <<= static_cast<operating_type>(v);
            return *this;
        }

        template<typename V = T, typename U = T>
        CUES_CONSTEXPR_BUT_NOT_CONST
        typename std::enable_if<
               std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer
            && std::numeric_limits<V>::is_specialized && std::numeric_limits<V>::is_integer,
            wrapped_number
        >::type & operator>>=(V const & v)
        {
            static_assert(std::is_same<U, T>::value, "unintended use of SFINAE parameter.");
            using operating_type = typename type_or_underlying<V>::type;
            value >>= static_cast<operating_type>(v);
            return *this;
        }

    private:
        underlying_type value;
};

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
inline constexpr bool operator==(
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & lhs,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & rhs
) noexcept
{
    return static_cast<T>(lhs)
        == static_cast<T>(rhs);
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
inline constexpr bool operator<(
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & lhs,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & rhs
) noexcept
{
    return static_cast<T>(lhs)
        < static_cast<T>(rhs);
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
inline constexpr bool operator>(
    T const & lhs,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & rhs
) noexcept
{
    return rhs < lhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
inline constexpr bool operator<=(
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & lhs,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & rhs
) noexcept
{
    return !(rhs < lhs);
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
inline constexpr bool operator>=(
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & lhs,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & rhs
) noexcept
{
    return !(lhs < rhs);
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
inline constexpr bool operator!=(
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & lhs,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & rhs
) noexcept
{
    return !(lhs == rhs);
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
       std::numeric_limits<T>::is_specialized
    && std::numeric_limits<U>::is_specialized,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator+(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs += rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
    std::numeric_limits<T>::is_specialized,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator-(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs -= rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
    std::numeric_limits<T>::is_specialized,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator*(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs *= rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
    std::numeric_limits<T>::is_specialized,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator/(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs /= rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
    std::numeric_limits<T>::is_specialized,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator%(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs %= rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
inline constexpr
typename std::enable_if<
    std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator~(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> const & num)
{
    return wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{~static_cast<T>(num)};
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
       std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer
    && std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator&(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs &= rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
       std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer
    && std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator|(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs |= rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
       std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer
    && std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator^(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs ^= rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
       std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer
    && std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator<<(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs <<= rhs;
}

template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE, typename U>
inline constexpr
typename std::enable_if<
       std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer
    && std::numeric_limits<U>::is_specialized && std::numeric_limits<U>::is_integer,
    wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>
>::type operator>>(wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lhs, U const & rhs)
{
    return lhs >>= rhs;
}


} // namespace cues

namespace std
{

    template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
    struct hash<cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>>
    // privately inheriting from std::hash<T> instead of having std::hash<T> as a member
    // so that if std::hash<T> has no state, we can benefit from the empty
    // base class optimization
    : private std::hash<T>
    {
        using argument_type = cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>;
        using result_type = std::size_t;

        result_type operator() (argument_type val) const
        {
            // forward to std::hash<T> after using conversion to T
            return std::hash<T>::operator()(static_cast<T>(val));
        }
    };

    template<typename T, std::size_t DISCRIMINATOR_VALUE, class DISCRIMINATOR_TYPE>
    struct numeric_limits<cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>>
    {
        static constexpr bool is_specialized = std::numeric_limits<T>::is_specialized;
        static constexpr bool is_signed = std::numeric_limits<T>::is_signed;
        static constexpr bool is_integer = std::numeric_limits<T>::is_integer;
        static constexpr bool is_exact = std::numeric_limits<T>::is_exact;
        static constexpr bool has_infinity = std::numeric_limits<T>::has_infinity;
        static constexpr bool has_quiet_NaN = std::numeric_limits<T>::has_quiet_NaN;
        static constexpr bool has_signaling_NaN = std::numeric_limits<T>::has_signaling_NaN;
        static constexpr std::float_denorm_style has_denorm = std::numeric_limits<T>::has_denorm;
        static constexpr bool has_denorm_loss = std::numeric_limits<T>::has_denorm_loss;
        static constexpr std::float_round_style round_style = std::numeric_limits<T>::round_style;
        static constexpr bool is_iec559 = std::numeric_limits<T>::is_iec559;
        static constexpr bool is_bounded = std::numeric_limits<T>::is_bounded;
        static constexpr bool is_modulo = std::numeric_limits<T>::is_modulo;

        static constexpr int digits = std::numeric_limits<T>::digits;
        static constexpr int digits10 = std::numeric_limits<T>::digits10;
        static constexpr int max_digits10 = std::numeric_limits<T>::max_digits10;
        static constexpr int radix = std::numeric_limits<T>::radix;

        static constexpr int min_exponent   = std::numeric_limits<T>::min_exponent;
        static constexpr int min_exponent10 = std::numeric_limits<T>::min_exponent10;

        static constexpr int max_exponent = std::numeric_limits<T>::max_exponent;
        static constexpr int max_exponent10 = std::numeric_limits<T>::max_exponent10;

        static constexpr bool traps = std::numeric_limits<T>::traps;
        static constexpr bool tinyness_before = std::numeric_limits<T>::tinyness_before;

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> min() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{(std::numeric_limits<T>::min)()};
        }

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> lowest() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{std::numeric_limits<T>::lowest()};
        }

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> max() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{(std::numeric_limits<T>::max)()};
        }

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> epsilon() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{std::numeric_limits<T>::epsilon()};
        }

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> round_error() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{std::numeric_limits<T>::round_error()};
        }

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> infinity() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{std::numeric_limits<T>::infinity()};
        }

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> quiet_NaN() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{std::numeric_limits<T>::quiet_NaN()};
        }

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> signaling_NaN() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{std::numeric_limits<T>::signaling_NaN()};
        }

        static constexpr cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE> denorm_min() noexcept
        {
            return cues::wrapped_number<T, DISCRIMINATOR_VALUE, DISCRIMINATOR_TYPE>{std::numeric_limits<T>::denorm_min()};
        }
    };
}

#endif // CUES_WRAPPED_NUMBER_HPP
