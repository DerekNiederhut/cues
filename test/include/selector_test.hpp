#ifndef CUES_TEST_SELECTOR_TEST_HPP_INCLUDED
#define CUES_TEST_SELECTOR_TEST_HPP_INCLUDED

#include <cstdint>
#include <type_traits>
#include "cues/variadic_type_traits.hpp"
#include "cues/integer_selector.hpp"
#include "cues/constexpr_math.hpp"

namespace cues
{
namespace test
{


    template<typename fast_unsigned, typename fast_signed, typename least_unsigned, typename least_signed, std::size_t ... BYTE_COUNTS>
    constexpr bool selection_matches()
    {   return (
        cues::is_same_as_all<
            fast_unsigned,
            typename cues::select_integer<     BYTE_COUNTS,cues::integer_selector_signedness::use_unsigned, cues::integer_selector_category::fast>::type ...,
            typename cues::select_fast_integer<BYTE_COUNTS,cues::integer_selector_signedness::use_unsigned>::type ...
        >::value
        &&
        cues::is_same_as_all<
            fast_signed,
            typename cues::select_integer<     BYTE_COUNTS,cues::integer_selector_signedness::use_signed, cues::integer_selector_category::fast>::type ...,
            typename cues::select_fast_integer<BYTE_COUNTS,cues::integer_selector_signedness::use_signed>::type ...
        >::value
        &&
        cues::is_same_as_all<
            least_unsigned,
            typename cues::select_integer<     BYTE_COUNTS,cues::integer_selector_signedness::use_unsigned, cues::integer_selector_category::least>::type...,
            typename cues::select_least_integer<BYTE_COUNTS,cues::integer_selector_signedness::use_unsigned>::type...
        >::value
        &&
        cues::is_same_as_all<
            least_signed,
            typename cues::select_integer<     BYTE_COUNTS,cues::integer_selector_signedness::use_signed, cues::integer_selector_category::least>::type...,
            typename cues::select_least_integer<BYTE_COUNTS,cues::integer_selector_signedness::use_signed>::type...
        >::value
        );
    }

    static_assert(cues::test::selection_matches<std::uint_fast8_t, std::int_fast8_t,std::uint_least8_t, std::int_least8_t, 0,1>(),
                  "Assertion violation: 0 or 1 byte selection");
    static_assert(cues::test::selection_matches<std::uint_fast16_t, std::int_fast16_t,std::uint_least16_t, std::int_least16_t, 2>(),
                  "Assertion violation: 2 byte selection");
    static_assert(cues::test::selection_matches<std::uint_fast32_t, std::int_fast32_t,std::uint_least32_t, std::int_least32_t, 3,4>(),
                  "Assertion violation: 3 or 4 byte selection");
    static_assert(cues::test::selection_matches<std::uint_fast64_t, std::int_fast64_t,std::uint_least64_t, std::int_least64_t, 5,6,7,8>(),
                  "Assertion violation: 5, 6, 7, or 8 byte selection");


    static_assert(
        std::is_same<
            typename cues::select_integer_by_digits<24u, cues::integer_selector_signedness::use_unsigned, cues::integer_selector_category::least>::type,
            std::uint_least32_t
        >::value,
        "Unexpected result for unsigned integer of at least 24 digits not being std::uint_least32_t"
    );

    static_assert(
        std::is_same<
            typename cues::select_integer_by_digits<16u, cues::integer_selector_signedness::use_signed, cues::integer_selector_category::least>::type,
            std::int_least32_t
        >::value,
        "Unexpected result for signed integer of at least 16 digits not being std::int_least32_t"
    );

    static_assert(
        std::is_same<
            typename cues::select_integer_by_digits<31u, cues::integer_selector_signedness::use_signed, cues::integer_selector_category::least>::type,
            std::int_least32_t
        >::value,
        "Unexpected result for signed integer of at least 31 digits not being std::int_least32_t"
    );

    static_assert(
        std::is_same<
            typename cues::select_integer_by_digits<10u, cues::integer_selector_signedness::use_unsigned, cues::integer_selector_category::least>::type,
            std::uint_least16_t
        >::value,
        "Unexpected result for unsigned integer of at least 10 bits not being std::uint_least16_t"
    );

    static_assert(
        std::is_same<
            typename cues::select_integer_by_digits<8u, cues::integer_selector_signedness::use_signed, cues::integer_selector_category::least>::type,
            std::int_least16_t
        >::value,
        "Unexpected result for signed integer of at least 8 digits not being std::int_least16_t"
    );

    static_assert(
        std::is_same<
            typename cues::select_integer_by_digits<7u, cues::integer_selector_signedness::use_signed, cues::integer_selector_category::least>::type,
            std::int_least8_t
        >::value,
        "Unexpected result for signed integer of at least 7 digits not being std::int_least8_t"
    );

    template<std::size_t DIGITS, bool IS_SIGNED>
    class custom_integer_type
    {
    public:
        static constexpr int digits = static_cast<int>(DIGITS);
        static constexpr bool is_signed = IS_SIGNED;

    private:
        // have size start large and get smaller as the number of digits increases, to ensure
        // selector is not still relying on size
        cues::raw_byte_type dummy[8 - ((DIGITS/8u < 8u) ? (DIGITS/8u) : 8u)];
    };

    template<std::size_t DIGITS, bool IS_SIGNED>
    struct custom_integer_limits
    {
        public:
            using my_int = custom_integer_type<DIGITS, IS_SIGNED>;

        static constexpr bool is_specialized = true;
        static constexpr bool is_signed = IS_SIGNED;
        static constexpr bool is_integer = true;
        static constexpr bool is_exact = true;
        static constexpr bool has_infinity = false;
        static constexpr bool has_quiet_NaN = false;
        static constexpr bool has_signaling_NaN = false;
        static constexpr std::float_denorm_style has_denorm = std::denorm_absent;
        static constexpr bool has_denorm_loss = false;
        static constexpr std::float_round_style round_style = std::round_toward_zero;
        static constexpr bool is_iec559 = false;
        static constexpr bool is_bounded = true;
        static constexpr bool is_modulo = !IS_SIGNED;
        static constexpr int digits = DIGITS;
        static constexpr int digits10 = cues::log10(cues::pow(2u,DIGITS) - 1);
        // this is the value used for standard integer types
        static constexpr int max_digits10 = 0;
        static constexpr int radix = 2;
        static constexpr int min_exponent = 0;
        static constexpr int min_exponent10 = 0;
        static constexpr int max_exponent = 0;
        static constexpr int max_exponent10 = 0;

        static constexpr bool traps = true;
        static constexpr bool tinyness_before = false;

        // not meaningful for the dummy test type
        static constexpr my_int min() noexcept {return my_int{};}
        static constexpr my_int lowest() noexcept {return my_int{};}
        static constexpr my_int max() noexcept {return my_int{};}
        static constexpr my_int epsilon() noexcept {return my_int{};}
        static constexpr my_int round_error() noexcept {return my_int{};}
        static constexpr my_int infinity() noexcept {return my_int{};}

        static constexpr my_int quiet_NaN() noexcept {return my_int{};}
        static constexpr my_int signaling_NaN() noexcept {return my_int{};}
        static constexpr my_int denorm_min() noexcept {return my_int{};}

    };

}
}
namespace std
{

template<std::size_t DIGITS, bool IS_SIGNED>
struct numeric_limits<cues::test::custom_integer_type<DIGITS, IS_SIGNED>>
: public cues::test::custom_integer_limits<DIGITS, IS_SIGNED>
{
};

}

namespace cues
{
    namespace test
    {
        template<typename T>
        using matching_least_integer_type = typename select_integer_by_digits<
            std::numeric_limits<T>::digits,
            std::numeric_limits<T>::is_signed ? integer_selector_signedness::use_signed : integer_selector_signedness::use_unsigned,
            integer_selector_category::least
        >::type;



        static_assert(
            std::is_same<
                matching_least_integer_type<cues::test::custom_integer_type<4u, false>>,
                std::uint_least8_t
            >::value,
            "unexpected integer selection"
        );

        static_assert(
            std::is_same<
                matching_least_integer_type<cues::test::custom_integer_type<4u, true>>,
                std::int_least8_t
            >::value,
            "unexpected integer selection"
        );

        static_assert(
            std::is_same<
                matching_least_integer_type<cues::test::custom_integer_type<12u, true>>,
                std::int_least16_t
            >::value,
            "unexpected integer selection"
        );

        static_assert(
            std::is_same<
                matching_least_integer_type<cues::test::custom_integer_type<24u, true>>,
                std::int_least32_t
            >::value,
            "unexpected integer selection"
        );

        static_assert(
            std::is_same<
                matching_least_integer_type<cues::test::custom_integer_type<48u, false>>,
                std::uint_least64_t
            >::value,
            "unexpected integer selection"
        );
    }
}

#endif // CUES_TEST_SELECTOR_TEST_HPP_INCLUDED
