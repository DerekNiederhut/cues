#ifndef CUES_MEMORY_HPP
#define CUES_MEMORY_HPP

#include <type_traits>
#include <memory>
#include <new>

#include "cues/void_t.hpp"
#include "cues/allocator_traits.hpp"

namespace cues {

//! deleter for unique_ptr when an Allocator is used to allocate storage
//! similar to allocator_delete implementation in P0316R0, except:
//! Allocators without state will not save a copy of the allocator, they will
//! default-construct it as needed
//! No specialization for reference types and reference_wrapper;
//! Allocator requirements seem to imply for any sane implementation that
//! allocators are either stateless, or a reference type to necessary state already.
//! similarly, returns a copy of the allocator instead of references
template<class Allocator, bool IS_STATELESS = cues::allocator_traits<Allocator>::is_stateless
>
class allocator_delete
{
    public:
        using allocator_type = typename std::remove_reference<Allocator>::type;
        using value_type = typename std::allocator_traits<Allocator>::value_type;


    allocator_delete(allocator_type a)
    : impl_alloc(std::move(a))
    {}

    //! WARNING: deletion may not be possible if default-constructed
    allocator_delete() = default;


    void operator()(value_type * obj) const
    {
        obj->~value_type();
        impl_alloc.deallocate(obj, 1);
    }

    allocator_type get_allocator() const
    {
        return impl_alloc;
    }

    private:

        allocator_type impl_alloc;
};


template<class Allocator>
class allocator_delete<Allocator, true>
{
     public:
        using allocator_type = Allocator;
        using value_type = typename std::allocator_traits<Allocator>::value_type;

        // included for consistency with stateful allocator specialization
        allocator_delete(allocator_type )
        {}

        allocator_delete() = default;

        void operator()(value_type * obj) const {
            obj->~value_type();
            allocator_type a{};
            a.deallocate(obj, 1);
        }

        allocator_type get_allocator() const
        {
            return allocator_type{};
        }
};

//! scoped as in variable scope like boost::scoped_ptr, not scoped as in
//! scoped_allocator_adapter.
//! Meant as a short-lived helper RAII class for things like creating
//! a unique_ptr with an allocator, after which the unique_ptr can
//! take on responsibility of deallocation.
template<class Allocator>
class scoped_allocation
{
public:
    using allocator_type = Allocator;
    using size_type = typename std::allocator_traits<allocator_type>::size_type;
    using pointer = typename std::allocator_traits<allocator_type>::pointer;

    explicit constexpr scoped_allocation(allocator_type const & a) noexcept(std::is_nothrow_default_constructible<allocator_type>::value)
    : my_alloc(a)
    , alloc_storage(nullptr)
    , alloc_size(0)
    {}

    constexpr scoped_allocation(allocator_type const & a, size_type n)
    : my_alloc(a)
    , alloc_storage(my_alloc.allocate(n))
    , alloc_size(n)
    {
    }

    ~scoped_allocation() noexcept(noexcept(reset()))
    {
        reset();
    }

    constexpr operator bool () const noexcept
    {
        return (alloc_storage != nullptr);
    }

    constexpr bool empty() const noexcept
    {
        return (alloc_storage == nullptr);
    }

    explicit constexpr operator pointer () const noexcept
    {
        return alloc_storage;
    }

    pointer release() noexcept
    {
        pointer temp = alloc_storage;
        alloc_storage = nullptr;
        return temp;
    }

    void reset() noexcept(noexcept(my_alloc.deallocate(nullptr, 0u)))
    {
        if (alloc_storage != nullptr)
        {
            my_alloc.deallocate(alloc_storage, alloc_size);
            alloc_storage = nullptr;
        }
    }

    //! WARNING: this works differently than
    //! unique_ptr::reset, in that it deallocates memory before
    //! assigning new memory.  This has the disadvantage of
    //! being able to end up in a state with no memory,
    //! (aka empty()), but has the advantage of making
    //! more memory available, to make it more likely that
    //! there will be enough memory for the allocation.
    void reset(size_type n)
    {
        // Allocator requirements
        static_assert(noexcept(reset()), "assumption violation: deallocation throws exceptions");
        reset();
        alloc_storage = my_alloc.allocate(n);
        alloc_size = n;
    }


private:

    scoped_allocation(scoped_allocation const &) = delete;
    scoped_allocation(scoped_allocation &&) = delete;

    scoped_allocation & operator=(scoped_allocation const &) = delete;
    scoped_allocation & operator=(scoped_allocation &&) = delete;

    allocator_type my_alloc;
    pointer        alloc_storage;
    size_type      alloc_size;
};

//! similar to implementation of P0316R0, except:
//! uses scoped_allocation object to manage storage instead of unique_ptr and lambdas
//! uses placement new directly instead of using allocator_traits::construct,
//! uses enable_if to make use with arrays invalid
//! which will do one of:
//! call the deprecated allocator.construct, which is deprecated because the point
//!    of having an allocator in the first place is to separate allocation and construction
//! call placement new directly (before c++20)
//! call std::construct_at() (c++20), which by default calls placement new
template<typename T, class Allocator, class ... Args>
typename std::enable_if<
    !std::is_array<T>::value,
    std::unique_ptr<T, allocator_delete<Allocator>>
>::type
allocate_unique(Allocator const & a, Args && ... args)
{
    using local_allocator_type = typename std::allocator_traits<Allocator>::template rebind_alloc<T>;
    using pointer = typename std::allocator_traits<local_allocator_type>::pointer;
    local_allocator_type local_a(a);
    scoped_allocation<local_allocator_type> storage(local_a, 1);
    static_assert(std::is_nothrow_copy_constructible<allocator_delete<Allocator>>::value,
                  "assumption violation: deleter is not nothrow copy constructible");
    allocator_delete<Allocator> deleter{a};
    // if the scoped allocation threw, no memory was allocated, and the exception
    // propagates without anything to clean up
    // if the constructor of T throws, scoped_allocation will deallocate the memory when the stack is unwound
    std::unique_ptr<T, allocator_delete<Allocator>> obj (
         (::new (static_cast<pointer>(storage)) T(std::forward<Args>(args)...)),
         deleter
    );
    // if the constructor did not throw, unique_ptr will clean up the memory, so release
    // the ownership from scoped_allocation
    storage.release();
    return obj;
}

//! thin wrapper around unique_ptr with allocator_deleter that also
//! defines type alias for allocator_type
//! and has a get_allocator() function, so that the allocator can be
//! queried and/or used from the pointer, which in the specific case
//! of allocator_deleter, it always knows
template<typename T, class Allocator>
class allocated_unique_ptr : public std::unique_ptr<T, allocator_delete<Allocator>>
{
    public:

        using implementation_type = std::unique_ptr <T, allocator_delete<Allocator>>;

        using pointer      = typename implementation_type::pointer;
        using element_type = typename implementation_type::element_type;
        using deleter_type = typename implementation_type::deleter_type;


        using allocator_type = Allocator;

        allocated_unique_ptr() = default;

        constexpr allocated_unique_ptr( std::nullptr_t ) noexcept
        : implementation_type(nullptr)
        {}

        allocated_unique_ptr(pointer p, deleter_type d) noexcept
        : implementation_type(p,d)
        {}

        allocator_type get_allocator() const noexcept
        {
            return implementation_type::get_deleter().get_allocator();
        }

        allocated_unique_ptr(allocated_unique_ptr &&) = default;

        allocated_unique_ptr(implementation_type && p) noexcept
        : implementation_type(std::move(p))
        {}

        allocated_unique_ptr& operator=( allocated_unique_ptr && ) = default;

        allocated_unique_ptr& operator=( std::nullptr_t )
        {
            implementation_type::operator=(nullptr);
        }

        allocated_unique_ptr& operator=( implementation_type && p)
        {
            implementation_type::operator=(std::move(p));
        }

        // inheritance already gives implicit conversion of reference
        // but if we need a value, we'll have to move
        operator implementation_type () noexcept
        {
            return implementation_type(std::move(static_cast<implementation_type &>(*this)));
        }

        explicit operator bool() const noexcept
        {
            return implementation_type::get() != nullptr;
        }

        element_type & operator*() const noexcept(noexcept(*std::declval<pointer>()))
        {
            return implementation_type::operator*();
        }

        pointer operator->() const noexcept
        {
            return implementation_type::operator->();
        }
    private:


};

} // namespace cues

#endif // CUES_MEMORY_HPP
