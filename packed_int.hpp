#ifndef CUES_PACKED_INT_HPP
#define CUES_PACKED_INT_HPP

//! packed_unsigned and packed_signed are used to compress integral data into a larger integer; for example,
//! to pack three 5-bit integers into a single 16-bit integer.
//! This can be useful when serializing binary data, to reduce the archive size.
//! It may also be useful when transferring large amounts of data, for example over a
//! network or between CPU and GPU.
//! It may also be useful when the data is used in a large containers, to increase the number of elements
//! that can be kept in the cache at a given time, in particular for structures with linear
//! or nearly linear memory and access patterns, like boost::flat_map, or cues::pool_map, respectively.

//! Unlike bitfields, the order is consistent regardless of compiler, though for binary serialization
//! one would need to account for big-endian vs little-endian integer representation.
//! Additionally unlike bitfields, the entire underlying integer is readily available, and
//! default comparison and relational operators are provided using that integer.

//! The signed and unsigned versions are meant to behave similarly; however, the
//! nature of signed overflow and shifting in c++ means that the signed version
//! has to use multiply, divide, add, and subtract instructions (in source code), where unsigned
//! can get away with shifting, bitwise or, and bitwise and, which more clearly reflect that the
//! values are simply bit-packed into the larger type.
//! Though one hopes an optimizing compiler can use shifting if the hardware implements it properly
//! for signed types and is faster, if this is not the case, the unsigned version may improve performance.

#include <limits>


#include "cues/constexpr_math.hpp"
#include "cues/integer_selector.hpp"

namespace cues {


template<typename UNSIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
class packed_unsigned
{
    public:

        using underlying_type = UNSIGNED_TYPE;
        using narrowed_type = typename cues::select_least_integer_by_digits<
            std::numeric_limits<UNSIGNED_TYPE>::digits/VALUE_COUNT,
            cues::integer_selector_signedness::use_unsigned
        >::type;
        static_assert(
            std::numeric_limits<underlying_type>::is_integer
            // bool is considered an integer, but we want to be able to store at least 2u in each of
            // VALUE_COUNT parts, so we need more digits than that
            && std::numeric_limits<underlying_type>::digits > 2u*VALUE_COUNT
            &&!std::numeric_limits<underlying_type>::is_signed, "unsigned must have large enough underlying unsigned integer type"
        );
        static constexpr underlying_type const field_width   = std::numeric_limits<UNSIGNED_TYPE>::digits/VALUE_COUNT;
        static constexpr underlying_type const mask          = cues::pow(underlying_type{2u}, field_width) - underlying_type{1u};

        static constexpr underlying_type const max_field_val = mask;
        static constexpr underlying_type const min_field_val = 0;

    private:
        static constexpr underlying_type compose(std::uint_least8_t decrement)
        {
            return (decrement == 0) ? mask : ( (mask << (decrement*field_width)) | compose(decrement - 1) );
        }

    public:

        static_assert(VALUE_COUNT > 1u, "If you don't need to pack more than one, just use the regular integral type.");
        static constexpr underlying_type max_underlying_val = compose(underlying_type{VALUE_COUNT-1u});

        constexpr packed_unsigned() noexcept
        : uval(0)
        {}

        explicit constexpr packed_unsigned(underlying_type v) noexcept
        : uval((v > max_underlying_val) ? max_underlying_val : v)
        {}

        explicit constexpr packed_unsigned(narrowed_type const (&v)[VALUE_COUNT] ) noexcept
        : uval(0u)
        {
            for (std::uint_least8_t idx = 0;
            idx < VALUE_COUNT;
            ++idx)
            {
                uval |= ((v[idx] & mask) << (field_width*idx));
            }
        }

        explicit constexpr operator underlying_type() const noexcept
        {
            return uval;
        }

        constexpr narrowed_type operator[] (std::uint_least8_t idx) const noexcept
        {
            assert(((void)"index out of range in packed_unsigned::operator[]", idx < VALUE_COUNT));
            return static_cast<narrowed_type>((uval >> (idx*field_width)) & mask);
        }

        constexpr narrowed_type at (std::uint_least8_t idx) const
        {
            return (idx < VALUE_COUNT) ?
                operator[](idx)
                :
                throw std::out_of_range("invalid index in packed_unsigned::at()");
            ;
        }

    private:
        underlying_type uval;

};

template<typename UNSIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator==( packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> lhs, packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> rhs)
{
    return static_cast<UNSIGNED_TYPE>(lhs) == static_cast<UNSIGNED_TYPE>(rhs);
}

template<typename UNSIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator!=( packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> lhs, packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> rhs)
{
    return !(lhs == rhs);
}

template<typename UNSIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator<( packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> lhs, packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> rhs)
{
    return static_cast<UNSIGNED_TYPE>(lhs) < static_cast<UNSIGNED_TYPE>(rhs);
}

template<typename UNSIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator>( packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> lhs, packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> rhs)
{
    return rhs < lhs;
}

template<typename UNSIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator<=( packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> lhs, packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> rhs)
{
    return !(rhs < lhs);
}

template<typename UNSIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator>=( packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> lhs, packed_unsigned<UNSIGNED_TYPE, VALUE_COUNT> rhs)
{
    return !(lhs < rhs);
}

//! signed implementation uses bitfields for ease of access;
//! because bitfield order is undefined, converting to/from undelrying_type
//! is done explicitly via math (rather than, for example, memcpy)
//! to match the unsigned packed type.
template<typename SIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
class packed_signed
{

    public:

        using underlying_type = SIGNED_TYPE;
        using narrowed_type = typename cues::select_least_integer_by_digits<
            std::numeric_limits<underlying_type>::digits/VALUE_COUNT,
            cues::integer_selector_signedness::use_signed
        >::type;
        static constexpr unsigned int field_width = std::numeric_limits<underlying_type>::digits/VALUE_COUNT;

        static_assert(
            std::numeric_limits<underlying_type>::is_integer
            // bool is considered an integer, but we want to be able to store at least 2u in each of
            // 3 parts, so we need more digits than that
            && std::numeric_limits<underlying_type>::digits > 2*VALUE_COUNT
            && std::numeric_limits<underlying_type>::is_signed, "signed must have underlying signed integer type"
        );

        // although the underlying integer is probably 2's complement, we will be symmetric
        static constexpr narrowed_type max_field_val      = cues::pow(narrowed_type(2), (field_width-1)) - 1;
        static constexpr narrowed_type min_field_val      = -max_field_val;

        static constexpr underlying_type minor_divisor      = cues::pow(underlying_type(2), field_width);
        // 2^(field_width * 2) === (2^field_width)^2
        static_assert(VALUE_COUNT > 1u, "If you don't need to pack more than one, just use the regular integral type.");
        static constexpr underlying_type max_divisor      = cues::pow(minor_divisor, VALUE_COUNT - 1);

        private:
        static constexpr underlying_type compose(std::uint_least8_t decrement)
        {
            return (decrement == 0) ?
            max_field_val
            :
            (max_field_val*cues::pow(minor_divisor, decrement) + compose(decrement - 1) );
        }
        public:

        static constexpr underlying_type max_underlying_val = compose(VALUE_COUNT - 1);
        static constexpr underlying_type min_underlying_val = -max_underlying_val;


        constexpr packed_signed() noexcept
        : uval(0)
        {}

        explicit constexpr packed_signed(underlying_type v) noexcept
        : uval( sanitized_underlying(v))
        {}

        constexpr packed_signed(narrowed_type const (&v)[VALUE_COUNT] ) noexcept
        : uval(0)
        {

            for (std::uint_least8_t idx = 0;
            idx < VALUE_COUNT;
            ++idx)
            {
                // mathematically correct; one hopes the compiler optimizes this
                uval += (sanitized_field(v[idx]) * cues::pow(underlying_type{2}, field_width*idx));
            }
        }

        explicit constexpr operator underlying_type() const noexcept
        {
            return uval;
        }

        constexpr narrowed_type operator[] (std::uint_least8_t idx) const noexcept
        {
            assert(((void)"invalid index in packed_signed::operator[]",idx < VALUE_COUNT));

            // shifting is not as well-defined for signed types,
            // and masking signed with unsigned means converting at least one type.
            // use arithmetic instead; this is unfortunately expensive but perhaps
            // the compiler can figure out what is going on and optimize it to shifts

            // std::div is not constexpr until c++23,
            // at which point division and modulus also specify the sign well,
            // but to work all the way back to c++11, and to have signs that make
            // sense, we'll use division and multiplication, and hope the compiler
            // can optimize if modulus makes more sense.
            #if __cplusplus >= 201402L || ( defined(__cpp_constexpr) && __cpp_constexpr >= 201402L )
            underlying_type remaining_value = uval;
            underlying_type maybe_answer = 0;
            for (std::uint_least8_t alt_idx = 0;
                alt_idx <= idx;
                ++alt_idx)
            {
                static_assert(__cplusplus >= 201103L, "well-specified integer division rounding is required." );
                // normalize handles values out of the smaller range by wrapping;
                // recall the min/max limits use 1 fewer bit than the field width
                // so that the value fits without losing sign data, so modulo the
                // divisor could return a result indicating we have wrapped
                // (eg the highest bits are negative but we are positive, so the
                // modulus gives a larger negative than we could store, because
                // the actual value should be positive)
                maybe_answer = normalize_field(remaining_value % minor_divisor);
                // done in this order as it may affect rounding if the signs differ,
                // since integer division since c++11 rounds toward zero.
                // compiler is welcome to make this smarter if it can.
                remaining_value -= maybe_answer;
                remaining_value /= minor_divisor;
            }
            return maybe_answer;
            #else
            return recursive_calculate(uval, 0, idx);
            #endif

        }

        constexpr narrowed_type at (std::uint_least8_t idx) const
        {
            return (idx < VALUE_COUNT) ?
              operator[](idx)
            : throw std::out_of_range();
        }


    private:

        #if __cplusplus < 201402L && ( !defined(__cpp_constexpr) || __cpp_constexpr < 201402L )
        constexpr underlying_type recursive_calculate(underlying_type remaining_value, std::uint_least8_t idx, std::uint_least8_t limit)
        {

            return (idx >= limit) ?
            (remaining_value % minor_divisor)
            :
            recursive_calculate((remaining_value - normalize_field(remaining_value % minor_divisor))/minor_divisor, idx+1, limit)
            ;

        }
        #endif

        static constexpr underlying_type normalize_field(underlying_type v) noexcept
        {
            return v + ((v < min_field_val) ?
                minor_divisor
                :
                (
                    (v > max_field_val) ?
                    (-minor_divisor)
                    :
                    0
                )
            );
        }

        static constexpr underlying_type sanitized_underlying(underlying_type v) noexcept
        {
            return (v > max_underlying_val) ?
                max_underlying_val
            : ( (v < min_underlying_val) ?
               min_underlying_val
               :
                v
            );
        }

        static constexpr narrowed_type sanitized_field(narrowed_type v) noexcept
        {
            return (v > max_field_val) ?
                max_field_val
            : (
               (v < min_field_val) ?
               min_field_val
               :
                v
            );
        }

        underlying_type uval;

};

template<typename SIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator==( packed_signed<SIGNED_TYPE, VALUE_COUNT> lhs, packed_signed<SIGNED_TYPE, VALUE_COUNT> rhs)
{
    return static_cast<SIGNED_TYPE>(lhs) == static_cast<SIGNED_TYPE>(rhs);
}

template<typename SIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator!=( packed_signed<SIGNED_TYPE, VALUE_COUNT> lhs, packed_signed<SIGNED_TYPE, VALUE_COUNT> rhs)
{
    return !(lhs == rhs);
}

template<typename SIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator<( packed_signed<SIGNED_TYPE, VALUE_COUNT> lhs, packed_signed<SIGNED_TYPE, VALUE_COUNT> rhs)
{
    return static_cast<SIGNED_TYPE>(lhs) < static_cast<SIGNED_TYPE>(rhs);
}

template<typename SIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator>( packed_signed<SIGNED_TYPE, VALUE_COUNT> lhs, packed_signed<SIGNED_TYPE, VALUE_COUNT> rhs)
{
    return rhs < lhs;
}

template<typename SIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator<=( packed_signed<SIGNED_TYPE, VALUE_COUNT> lhs, packed_signed<SIGNED_TYPE, VALUE_COUNT> rhs)
{
    return !(rhs < lhs);
}

template<typename SIGNED_TYPE, std::uint_least8_t VALUE_COUNT>
inline constexpr bool operator>=( packed_signed<SIGNED_TYPE, VALUE_COUNT> lhs, packed_signed<SIGNED_TYPE, VALUE_COUNT> rhs)
{
    return !(lhs < rhs);
}



} // namespace cues

#endif // CUES_PACKED_INT_HPP
