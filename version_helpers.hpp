#ifndef CUES_VERSION_HELPERS_HPP_INCLUDED
#define CUES_VERSION_HELPERS_HPP_INCLUDED

#if __cplusplus >= 202002L || ( defined( __has_include ) && __has_include(<version>) )
    #define CUES_HAS_VERSION_HEADER 1
    #include <version>
#else
    #define CUES_HAS_VERSION_HEADER 0
    #include <ciso646>
#endif

#if __cplusplus < 201103L
    #error "CUES does not support C++ prior to C++11"
#endif

#if __cplusplus >= 201402L || defined (__cpp_constexpr) && __cpp_constexpr >= 201304L
    #define CUES_CONSTEXPR_BUT_NOT_CONST constexpr
    #define CUES_HAS_NON_CONST_CONSTEXPR
#else
    #define CUES_CONSTEXPR_BUT_NOT_CONST
#endif

#if __cplusplus >= 201703L
    #define CUES_CONSTEXPR_AFTER_CPP17 constexpr
#else
    #define CUES_CONSTEXPR_AFTER_CPP17
#endif

#if __cplusplus >= 201402L || (defined __cpp_constexpr) && __cpp_constexpr >= 201304L
    #define CUES_CONSTEXPR_AFTER_CPP14 constexpr
#else
    #define CUES_CONSTEXPR_AFTER_CPP14
#endif

#if __cplusplus >= 201703L || defined ( __cpp_if_constexpr ) && __cpp_if_constexpr >=  201606L
    #define CUES_CONSTEXPR_OR_RUNTIME_IF constexpr
#else
    #define CUES_CONSTEXPR_OR_RUNTIME_IF
#endif

//c++17 standard
#if defined __has_cpp_attribute
    #if __has_cpp_attribute(fallthrough)
        #define CUES_FALLTHROUGH [[fallthrough]]
    #endif

    #if __has_cpp_attribute(nodiscard)
        #define CUES_NODISCARD [[nodiscard]]
    #endif

    #if __has_cpp_attribute(deprecated)
        #define CUES_DEPRECATED_MSG(msg) [[deprecated(msg)]]
        #define CUES_DEPRECATED [[deprecated]]
    #endif

#endif
//c++11 compiler extensions
#if __cplusplus >= 201103L
    #if !( defined CUES_FALLTHROUGH )
        #if defined __GNUC__
            #define CUES_FALLTHROUGH [[gnu::fallthrough]]
        #elif defined __clang__
            #define CUES_FALLTHROUGH [[clang::fallthrough]]
        #endif
    #endif
    #if !( defined CUES_NODISCARD )
         #if defined __GNUC__
            #define CUES_NODISCARD [[gnu::warn_unused_result]]
        #elif defined __clang__
            #define CUES_NODISCARD [[clang::warn_unused_result]]
        #endif
    #endif
    #if !(defined CUES_DEPRECATED)
        #if defined __GNUC__
            #define CUES_DEPRECATED [[gnu::deprecated]]
            #define CUES_DEPRECATED_MSG(msg) [[gnu::deprecated(msg)]]
        #endif
    #endif
#endif
// pre-c++11 non-portable
#if defined __has_attribute
    #if !( defined CUES_FALLTHROUGH ) && __has_attribute(fallthrough)
        #define CUES_FALLTHROUGH __attribute__ ((fallthrough))
    #endif
    #if !(defined CUES_NODISCARD) && __has_attribute(warn_unused_result)
        #define CUES_NODISCARD __attribute__ ((warn_unused_result))
    #endif

    #if !(defined CUES_DEPRECATED) && __has_attribute(deprecated)
        #define CUES_DEPRECATED_MSG(msg) __attribute__ ((deprecated(msg)))
        #define CUES_DEPRECATED __attribute__ ((deprecated))
    #endif
#endif


// define it so we can use it even if it is empty;
#ifndef CUES_FALLTHROUGH
    #define CUES_FALLTHROUGH
#endif

#ifndef CUES_NODISCARD
    #define CUES_NODISCARD
#endif

#ifndef CUES_DEPRECATED
    #define CUES_DEPRECATED
    #define CUES_DEPRECATED_MSG(msg)
#endif

// std::reference_wrapper cannot be used with incomplete types prior to c++20
// boost::reference_wrapper can
#if __cplusplus >= 202002L
    #define CUES_REFERENCE_WRAPPER_INCLUDE <functional>
    #define CUES_REFERENCE_WRAPPER_NAMESPACE std
#else
    #define CUES_REFERENCE_WRAPPER_INCLUDE <boost/core/ref.hpp>
    #define CUES_REFERENCE_WRAPPER_NAMESPACE boost
#endif

// variant was required in c++17 but could have been provided earlier
#if  __cplusplus >= 201703L || defined __cpp_lib_variant
    #define CUES_VARIANT_INCLUDE <variant>
    #define CUES_VARIANT_NAMESPACE std

#else
    #define CUES_VARIANT_INCLUDE <boost/variant.hpp>
    #define CUES_VARIANT_NAMESPACE boost
#endif

#if ( defined( __cpp_lib_optional ) || defined( __cpp_lib_any ) || defined ( __cpp_lib_variant ) || __cplusplus > 201603L )
    #define CUES_HAS_IN_PLACE_T 1
#else
    #define CUES_HAS_IN_PLACE_T 0
#endif

#if __cplusplus >= 202002L
    #define CUES_ATOMIC_VAR_INIT(x) (x)
#else
    #define CUES_ATOMIC_VAR_INIT(x) ATOMIC_VAR_INIT(x)
#endif

#if __cplusplus > 201703L || ( defined __cpp_lib_byte && __cpp_lib_byte >= 201603L )
    #include <cstddef>
#endif

#if defined (__cpp_lib_string_view) || __cplusplus >= 201703L
    #define CUES_HAS_STRING_VIEW
#endif

namespace cues
{
    using raw_byte_type =
    #if __cplusplus >= 201703L || ( defined __cpp_lib_byte && __cpp_lib_byte >= 201603L )
        std::byte
    #else
        unsigned char
    #endif
    ;

    using utf8_char_type =
    #if __cplusplus >= 202002L || ( defined __cpp_char8_t )
        char8_t
    #else
        char
    #endif
    ;
}

#if __cplusplus >= 202002L || defined(__cpp_lib_format) || ( defined( __has_include ) && __has_include(<format>) )
    #define CUES_FORMAT_INCLUDE <format>
    #define CUES_FORMAT_NAMESPACE std
#else
    #define FMT_HEADER_ONLY
    #define CUES_FORMAT_INCLUDE <fmt/format.h>
    #define CUES_FORMAT_NAMESPACE fmt

#endif

#endif // CUES_VERSION_HELPERS_HPP_INCLUDED
