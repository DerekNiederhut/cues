#ifndef CUES_SEQUENCE_HPP_INCLUDED
#define CUES_SEQUENCE_HPP_INCLUDED

#include <cstddef>
#include <type_traits>

namespace cues
{
//! like std::get(std::array) but for parameter pack
//! requires pack size > N, just like array operator[]
template<std::size_t N, typename T, T... VALS>
struct nth_from_sequence;

// specialization when there is at least one element in the pack
// recursively calls with reduced index on pack with first element removed
template<std::size_t N, typename T, T FIRST_VAL, T... VALS>
struct nth_from_sequence<N, T, FIRST_VAL, VALS...>
{
    static constexpr T value = nth_from_sequence<N-1, T, VALS...>::value;
};

// specialization for when N is 0 and there is at least one element still in the pack
template<typename T, T FIRST_VAL, T... VALS>
struct nth_from_sequence<0, T, FIRST_VAL, VALS...>
{
    static constexpr T value = FIRST_VAL;
};

//! returns the sum of a non-type parameter pack
//! can specify a different return type than the input type
//! zero-length case is additive identity, stops recursion
template<typename T, typename U, U... ADDENDS>
struct sum_of_sequence
{
    static constexpr T value = static_cast<T>(0);
};

template<typename T, typename U, U first, U... ADDENDS>
struct sum_of_sequence<T, U, first, ADDENDS...>
{
    static constexpr T value = static_cast<T>(first) + sum_of_sequence<T, U, ADDENDS...>::value;
};

//! returns product of a non-type parameter pack
//! zero-length case uses convention of multiplicative identity (1), stops recursion
//! note that this also means single-element sequence has product equal to itself
template<typename T, typename U, U... FACTORS>
struct product_of_sequence
{
    static constexpr T value = static_cast<T>(1);
};

template<typename T, typename U, U first, U... FACTORS>
struct product_of_sequence<T, U, first, FACTORS...>
{
    static constexpr T value = static_cast<T>(first) * product_of_sequence<T, U, FACTORS...>::value;
};

//! sequence template
//! like std::integer_sequence, but:
//! accepts more types (in c++20 and later),
//! provides class-scope constexpr functions for sequence sum, product, and getting a value by index,
//! and provides type aliases for manually appending/prepending values.
//! primary template will take 0-length sequences
template<typename T, T... EXISTING_VALUES>
struct sequence
{
    using value_type = T;
    template<T... APPEND_VALUE>
    using append_type    = sequence<T, EXISTING_VALUES..., APPEND_VALUE...>;

    template<T... PREPEND_VALUE>
    using prepend_type   = sequence<T, PREPEND_VALUE..., EXISTING_VALUES...>;

    static constexpr std::size_t size() noexcept { return sizeof...(EXISTING_VALUES); }

    template<std::size_t N>
    static constexpr T nth() noexcept
    {
        static_assert(N < size(), "index out of bounds.");
        return nth_from_sequence<N, T, EXISTING_VALUES...>::value;
    }

    static constexpr value_type sum() noexcept
    {
        return sum_of_sequence<value_type, value_type, EXISTING_VALUES...>::value;
    }

    static constexpr value_type product() noexcept
    {
        return product_of_sequence<value_type, value_type, EXISTING_VALUES...>::value;
    }

};

//! specialization for sequence length >= 1
template<typename T, T FIRST, T... LAST>
struct sequence<T, FIRST, LAST...>
{
    using value_type = T;
    template<T... APPEND_VALUE>
    using append_type    = sequence<T, FIRST, LAST..., APPEND_VALUE...>;

    template<T... PREPEND_VALUE>
    using prepend_type   = sequence<T, PREPEND_VALUE..., FIRST, LAST...>;

    static constexpr std::size_t size() noexcept { return 1 + sizeof...(LAST); }

    template<std::size_t N>
    static constexpr T nth() noexcept
    {
        static_assert(N < size(), "index out of bounds.");
        return nth_from_sequence<N, T, FIRST, LAST...>::value;
    }

    static constexpr value_type sum() noexcept
    {
        return sum_of_sequence<value_type, value_type, FIRST, LAST...>::value;
    }

    static constexpr value_type product() noexcept
    {
        return product_of_sequence<value_type, value_type, FIRST, LAST...>::value;
    }
};


}
#endif // CUES_SEQUENCE_HPP_INCLUDED
