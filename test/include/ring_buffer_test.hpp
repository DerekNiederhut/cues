#ifndef CUES_TEST_RING_BUFFER_TEST_HPP_INCLUDED
#define CUES_TEST_RING_BUFFER_TEST_HPP_INCLUDED

#include <cstddef>


namespace cues
{
namespace test
{

std::size_t check_ring_buffer_test_runtime_assertions();




}
}


#endif // CUES_TEST_RING_BUFFER_TEST_HPP_INCLUDED
