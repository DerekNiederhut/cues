#ifndef CUES_NON_OWNING_PTR_HPP_INCLUDED
#define CUES_NON_OWNING_PTR_HPP_INCLUDED

#pragma once

namespace cues
{
    // the C++ standard has been considering an exempt_ptr or observer_ptr
    // since the 2011 standard, but has failed to accept one.  Their current
    // suggested implementation adds the interface of other smart pointers
    // (for example, reset()) but for documentation-only, this simpler
    // implementation will suffice and means I didn't waste time on something
    // that becomes obsolete if the standard decides to add it.
    template<typename T>
    using non_owning_ptr = T*;


}


#endif // CUES_NON_OWNING_PTR_HPP_INCLUDED
