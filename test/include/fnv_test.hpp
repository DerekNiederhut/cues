#ifndef CUES_TEST_FNV_TEST_HPP
#define CUES_TEST_FNV_TEST_HPP

/* LICENSE VARIATION
 * regardless of the license for other files within the cues library,
 * this file is not copyright and is in the public domain, in keeping
 * with the wishes of the authors of source on which it is based.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "cues/functional_pd.hpp"
#include <string_view>
#include <array>


namespace cues {
namespace test {

// test case for plain char array & pointers initialized via string literal
static constexpr char const test_case_55[] = "chongo was here!\n";

// test for raw array of signed char, including null
static constexpr signed char const test_case_23[] = {
    'f','o','o','b','a','r','\0'
};

// test case for std::array (iterators are pointers), inlcuding null
static constexpr std::array<unsigned char const,4> test_case_20 = {
    {'f','o','o','\0'}
};

static constexpr unsigned char const test_case_20_before_constexpr_array[4] = {
    'f','o','o','\0'
};

// test case for array of cues::raw_byte_type
static constexpr cues::raw_byte_type const test_case_29[] = {
    static_cast<cues::raw_byte_type>('c'),
    static_cast<cues::raw_byte_type>('h'),
    static_cast<cues::raw_byte_type>('o'),
    static_cast<cues::raw_byte_type>('n'),
    static_cast<cues::raw_byte_type>('g'),
    static_cast<cues::raw_byte_type>('o'),
    static_cast<cues::raw_byte_type>(' ')
};



// values for test cases are a subset of those provided by the authors of the C implementation,
// available (at time of writing) at: http://www.isthe.com/chongo/src/fnv/test_fnv.c

#if defined ( __cpp_lib_constexpr_string_view)
static_assert(cues::fnv_1a_consteval<32>(std::string_view(""),      cues::fnv1_init<32>()) == 0x811c9dc5UL, "failure on 32-bit test case 0.");
static_assert(cues::fnv_1a_consteval<32>(std::string_view("f"),     cues::fnv1_init<32>()) == 0xe30c2799UL, "failure on 32-bit test case 6.");
static_assert(cues::fnv_1a_consteval<32>(std::string_view("foo"),   cues::fnv1_init<32>()) == 0xa9f37ed7UL, "failure on 32-bit test case 8.");
static_assert(cues::fnv_1a_consteval<32>(std::string_view("foobar"),cues::fnv1_init<32>()) == 0xbf9cf968UL, "failure on 32-bit test case 11.");
#else
static_assert(cues::fnv_1a_consteval<32>(&(test_case_20[0]), &(test_case_20[0]), cues::fnv1_init<32>()) == 0x811c9dc5UL, "failure on 32-bit test case 0.");
static_assert(cues::fnv_1a_consteval<32>(&(test_case_23[0]), &(test_case_23[1]), cues::fnv1_init<32>()) == 0xe30c2799UL, "failure on 32-bit test case 6.");
static_assert(cues::fnv_1a_consteval<32>(&(test_case_20[0]), &(test_case_20[3]), cues::fnv1_init<32>()) == 0xa9f37ed7UL, "failure on 32-bit test case 8.");
static_assert(cues::fnv_1a_consteval<32>(&(test_case_23[0]), &(test_case_23[6]), cues::fnv1_init<32>()) == 0xbf9cf968UL, "failure on 32-bit test case 11.");
#endif
static_assert(cues::fnv_1a_consteval<32>(
#if defined ( __cpp_lib_array_constexpr )
    test_case_20.cbegin(),
    test_case_20.cend(),
#else
    &(test_case_20_before_constexpr_array[0]),
    &(test_case_20_before_constexpr_array[sizeof(test_case_20_before_constexpr_array)/sizeof(test_case_20_before_constexpr_array[0])]),
#endif
    cues::fnv1_init<32>()) == 0x6150ac75UL, "failure on 32-bit test case 20.");
#if defined( __cpp_lib_constexpr_iterator)
static_assert(cues::fnv_1a_consteval<32>(
    std::begin(test_case_23),
    std::end(test_case_23),
    cues::fnv1_init<32>()) == 0x0c1c9eb8UL, "failure on 32-bit test case 23.");
static_assert(cues::fnv_1a_consteval<32>(
    std::begin(test_case_55),
    std::end(test_case_55),
    cues::fnv1_init<32>()) == 0x8227df4fUL, "failure on 32-bit test case 55.");
static_assert(cues::fnv_1a_consteval<32>(
    std::begin(test_case_29),
    std::end(test_case_29),
    cues::fnv1_init<32>()) == 0x6bdd5c67UL, "failure on 32-bit test case 29.");
#else
static_assert(cues::fnv_1a_consteval<32>(
    &(test_case_23[0]),
    &(test_case_23[sizeof(test_case_23)/sizeof(test_case_23[0])]),
    cues::fnv1_init<32>()) == 0x0c1c9eb8UL, "failure on 32-bit test case 23.");
static_assert(cues::fnv_1a_consteval<32>(
    &(test_case_55[0]),
    &(test_case_55[sizeof(test_case_55)/sizeof(test_case_55[0])]),
    cues::fnv1_init<32>()) == 0x8227df4fUL, "failure on 32-bit test case 55.");
static_assert(cues::fnv_1a_consteval<32>(
    &(test_case_29[0]),
    &(test_case_29[sizeof(test_case_29)/sizeof(test_case_29[0])]),
    cues::fnv1_init<32>()) == 0x6bdd5c67UL, "failure on 32-bit test case 29.");
#endif

#if defined ( __cpp_lib_constexpr_string_view)
static_assert(cues::fnv_1a_consteval<64>(std::string_view(""),      cues::fnv1_init<64>()) == 0xcbf29ce484222325ULL, "failure on 64-bit test case 0.");
static_assert(cues::fnv_1a_consteval<64>(std::string_view("f"),     cues::fnv1_init<64>()) == 0xaf63db4c8601ead9ULL, "failure on 64-bit test case 6.");
static_assert(cues::fnv_1a_consteval<64>(std::string_view("foo"),   cues::fnv1_init<64>()) == 0xdcb27518fed9d577ULL, "failure on 64-bit test case 8.");
static_assert(cues::fnv_1a_consteval<64>(std::string_view("foobar"),cues::fnv1_init<64>()) == 0x85944171f73967e8ULL, "failure on 64-bit test case 11.");
#else
static_assert(cues::fnv_1a_consteval<64>(&(test_case_20[0]), &(test_case_20[0]), cues::fnv1_init<64>()) == 0xcbf29ce484222325ULL, "failure on 64-bit test case 0.");
static_assert(cues::fnv_1a_consteval<64>(&(test_case_23[0]), &(test_case_23[1]), cues::fnv1_init<64>()) == 0xaf63db4c8601ead9ULL, "failure on 64-bit test case 6.");
static_assert(cues::fnv_1a_consteval<64>(&(test_case_20[0]), &(test_case_20[3]), cues::fnv1_init<64>()) == 0xdcb27518fed9d577ULL, "failure on 64-bit test case 8.");
static_assert(cues::fnv_1a_consteval<64>(&(test_case_23[0]), &(test_case_23[6]), cues::fnv1_init<64>()) == 0x85944171f73967e8ULL, "failure on 64-bit test case 11.");
#endif

static_assert(cues::fnv_1a_consteval<64>(
#if defined ( __cpp_lib_array_constexpr )
    test_case_20.cbegin(),
    test_case_20.cend(),
#else
    &(test_case_20_before_constexpr_array[0]),
    &(test_case_20_before_constexpr_array[sizeof(test_case_20_before_constexpr_array)/sizeof(test_case_20_before_constexpr_array[0])]),
#endif
    cues::fnv1_init<64>()) == 0xdd1270790c25b935ULL, "failure on 64-bit test case 20.");

#if defined( __cpp_lib_constexpr_iterator)
static_assert(cues::fnv_1a_consteval<64>(
    std::begin(test_case_23),
    std::end(test_case_23),
    cues::fnv1_init<64>()) == 0x34531ca7168b8f38ULL, "failure on 64-bit test case 23.");
static_assert(cues::fnv_1a_consteval<64>(
    std::begin(test_case_55),
    std::end(test_case_55),
    cues::fnv1_init<64>()) == 0xc33bce57bef63eafULL, "failure on 64-bit test case 55.");
static_assert(cues::fnv_1a_consteval<64>(
    std::begin(test_case_29),
    std::end(test_case_29),
    cues::fnv1_init<64>()) == 0xf35a83c10e4f1f87ULL, "failure on 64-bit test case 29.");
#else
    static_assert(cues::fnv_1a_consteval<64>(
    &(test_case_23[0]),
    &(test_case_23[sizeof(test_case_23)/sizeof(test_case_23[0])]),
    cues::fnv1_init<64>()) == 0x34531ca7168b8f38ULL, "failure on 64-bit test case 23.");
static_assert(cues::fnv_1a_consteval<64>(
    &(test_case_55[0]),
    &(test_case_55[sizeof(test_case_55)/sizeof(test_case_55[0])]),
    cues::fnv1_init<64>()) == 0xc33bce57bef63eafULL, "failure on 64-bit test case 55.");
static_assert(cues::fnv_1a_consteval<64>(
    &(test_case_29[0]),
    &(test_case_29[sizeof(test_case_29)/sizeof(test_case_29[0])]),
    cues::fnv1_init<64>()) == 0xf35a83c10e4f1f87ULL, "failure on 64-bit test case 29.");
#endif

} // namespace test
} // namespace cues

#endif // CUES_TEST_FNV_TEST_HPP
