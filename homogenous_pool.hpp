#ifndef CUES_HOMOGENOUS_POOL_HPP
#define CUES_HOMOGENOUS_POOL_HPP

#if (__cplusplus < 201703L && !defined ( __cpp_lib_launder) )
    #error "cues::homogenous_pool requires std::launder, which is not availabe."
#endif

#include <cstddef>      // size_t, std::byte
#include <cassert>
#include <limits>       // for static assertions

#include <type_traits>  // false_type, true_type, etc.
#include <new>          // std::bad_alloc, placement new
#include <algorithm>    // std::copy_n
#include <memory>       // std::align

#include "cues/non_owning_ptr.hpp"
#include "cues/integer_selector.hpp"
#include "cues/constexpr_math.hpp"  // minimum bits
#include "cues/version_helpers.hpp" // includes <version> or <ciso646>
#include "cues/allocator_traits.hpp"

namespace cues {

//! Pool that uses pre-allocated space to provide fixed-size chunks
//! Intended for use cases where memory is typically allocated and
//! deallocated one chunk at a time and tailored for that behavior.
//! Should also be reasonable to allocate multiple chunks at once
//! if deallocation is consistently last-in, first-out and the number
//! of chunks is not large.

//! Best-case allocation of n blocks from a container size N is O(n),
//! in particular meaning that when n==1 allocation is O(1).
//! worst-case allocation of n > 1 blocks from a container size N is O(N),
//! though this does not apply to n == 1, which is always O(1).
//! This is managed by maintaining a stack of free chunks, so that
//! this remains true for any amount and any order of single-block
//! allocations and deallocations.
//! More specifically, for allocations of multiple blocks at once,
//! there is an additional minimum O(n) overhead to search for a large enough
//! contiguous block; if the top n indices of the free stack do not contain a
//! large enough chunk, this could be as much as O(N) and may even result in
//! false negatives if many different block sizes are allocated and deallocated
//! out of order.  The stack then must be
//! adjusted for the removal, which is worst-case O(N-n)
//! Space overhead (on top of N*sizeof(T) bytes for the data itself)
//! is (N+1)*sizeof(size_type).
//! The size_type can be chosen to minimize the space overhead of the free
//! stack, and will by default pick the smallest unsigned integer that can
//! store N (for example an unsigned char for N < 256).

//! WARNING: an individual pool or allocator object is not thread-safe, though
//! different threads could each have their own pool safely, or wrap the
//! pool with some kind of lock guard.  Be cautious doing this when the
//! Allocator requirements are needed, as deallocation may not throw exceptions
//! and poses a risk of busy-waiting on a lock.

//! NOTE: the pool object itself contains the memory for the allocation.  If
//! you want this memory on the heap, create the entire object that way, for
//! example via std::make_unique<>.

//! The pool is more like the "regions" described by
//! Berger, Zorn, & McKinley in "Reconsidering Custom Memory Allocation"
//! https://people.cs.umass.edu/~emery/pubs/berger-oopsla2002.pdf
//! than their "reaps"; it contains a region of space and does not return
//! that space to the system until all objects are deallocated.
//! Their "reaps" may be more beneficial when consistent timing is not important,
//! or when frequently allocating more than one block at a time,
//! but this pool has the advantage of being more deterministic in time for
//! allocations of a single element at a time, as it will never ask the system
//! for more memory nor release memory outside of construction and destruction,
//! and single allocations and deallocations always finish in O(1) time.


//! The pool is similar to Paul Glinkel's TFreeList class, from
//! "Game Programming Gems 4", Charles River Media, 2004
//! Unlike that approach, this pool:
//! 1) always contains a fixed amount of storage, which means:
//!    A) If used as an automatic or static storage duration variable, it does
//!       not need to touch the heap at all, and can be used for embedded programs
//!       that do not reserve any RAM for heap storage.
//!    B) Data locality may be improved for the stored objects and metadata, which
//!       may improve speed in some applications
//!    C) In the case where a separate free stack is necessary, separate size and
//!       stack top variables are not necessary as stack top is always
//!       N - 1 - size and can quickly be calculated when needed; in the case
//!       where the linked list is used, it uses the same storage as the allocation
//!       memory.
//!    D) It does not need a variable to track whether it should delete the memory
//!       memory region on destruction; it will always be released on destruction
//! 2) uses indices into the array instead of pointers, which will,
//!    for types using using size_type such that sizeof(size_type) < sizeof(T *),
//!    reduce memory usage.
//! 3) provides for allocation/deallocation of more than one block at a time,
//!    so that allocation of an array of blocks is possible if there is room.
//!    Multi-block allocation cannot make the same complexity guarantee as
//!    allocating or deallocating a single block at a time, but is part of the
//!    Allocator requirements for use with standard containers.
//! 4) provides for aligning the start of the data array more strictly than that of
//!    the stored type T.
//! 5) the type for representing the size is unsigned, to allow twice as many elements
//!    as the corresponding size_type, and can be user-specified, allowing
//!    for allocating more blocks than could be represented by a plain integer
//!    (for example if std::size_t happens to be equivalent to unsigned long long)
//! 6) provides two allocation methods, one that returns nullptr on failure, for
//!    applications where exceptions are not acceptable, and one that throws
//!    std::bad_alloc on failure, to match the Allocator requirements for the
//!    standard library, instead of an assert check followed by returning an
//!    invalid, but non-null, pointer on failure.
//! 7) provides a mechanism to allocate on a particular alignment boundary,
//!    to support rebinding an allocator to allocate a different type; this
//!    is required for standard containers that have to allocate for example
//!    some node type other than T, but is likely to result in worse
//!    performance than use cases that only need to allocate T.

//! This pool also shares some similarities with boost::pool::object_pool (and thus
//! boost::pool::pool and boost::pool::simple_segregated_storate), though:
//! 1) although this pool still allows clients to put it where they want in memory
//!    (automatic variable, std::make_unique<>, cues::make_placement, etc), the
//!    object contains the fixed amount of memory it uses (and will never add more)
//!    instead of referring to memory outside the object, so:
//!    A) there is no need to template the pool on allocator type
//!    B) the pool object is always destroyed at the same time as the memory
//!    C) because the size is fixed, the object never spends any time trying to
//!       allocate more space for itself without the client knowing, providing
//!       more predictable timing in the case of allocating a single block
//!       at a time, at the expense of failing once the internal space is
//!       exhausted, until some is deallocated.
//! 2) This pool follows the interface of the c++ Allocator requirements rather
//!    than the C malloc/free functions, in particular meaning that it leaves the
//!    size of an individual allocation to be remembered by clients, which, combined
//!    with using a minimum unsigned integer as an index type instead of storing
//!    pointers, may reduce space overhead for types where the size or alignment
//!    of the type is smaller than a pointer plus what is most likely a std::size_t
//!    (boost uses the provided allocator's size_type which for the default allocators
//!    will be std::size_t), in particular for applications where the client would
//!    already know that information anyway (say, a container that tracks its
//!    allocations, or a smart pointer) and making the implementation of the
//!    allocator object simple.
//!    Applications which do not want the burden of storing the size
//!    may benefit from using the boost pools instead, or could wrap
//!    this pool object with one that maps indices to sizes.
//!    Another consequence is that this pool's worst-case performance
//!    (for allocations of n > 1 objects at a time not done in LIFO order)
//!    will be significantly worse--O(N), where N is total blocks in the
//!    pool--than boost, and even allocations of arrays that are in LIFO
//!    order are still expected to be worse for large n, though for small n,
//!    for example std::pair<U, T> for an associative container where U is
//!    similar in size to T, this may not be critical.  Applications allocating
//!    much larger blocks may prefer the additional space overhead of the size
//!    tracking to improve allocation/deallocation speed.
//! 3) Allocators never use a singleton pool, but always point to the individual
//!    pool object in use.  This makes it straightforward to have multiple pools
//!    with possibly multiple allocators to each.
//! 4) This pool makes no attempt to make allocations thread-safe on a particular
//!    pool object, where the boost pools do.
//!    For applications not sharing allocations among threads, this may improve
//!    performance, though for applications where thread-safety is required,
//!    using boost may be preferable.
//! 5) It does not include support for constructing and destructing the objects,
//!    (optional in the standard Allocator requirements, deprecated in c++17
//!    and removed in c++20 for std::allocator),
//!    instead focusing on the single responsibility of allocating memory, and
//!    leaving object construction/destruction to for example containers making
//!    use of the allocator, a unique_ptr with a custom deleter, etc.
//! 6) the use of

template<
    class T,
    std::size_t N,
    typename SIZE_TYPE,
    std::size_t DATA_ALIGN,
    class U
> class homogenous_allocator;

template<
    class T,
    std::size_t N,
    typename SIZE_TYPE,
    std::size_t DATA_ALIGN,
    class U
> class homogenous_deleter;

template<
    class T,
    std::size_t N,
    typename SIZE_TYPE = typename cues::select_least_integer_by_digits<
          cues::minimum_bits(N)
        , cues::integer_selector_signedness::use_unsigned
    >::type,
    std::size_t DATA_ALIGN = alignof(T) >= alignof(SIZE_TYPE) ? alignof(T) : alignof(SIZE_TYPE)
>
class homogenous_pool
{
    public:

        static_assert(N <= (std::numeric_limits<SIZE_TYPE>::max)(), "assumption violation: size_type is inadequate for storing N.");
        static_assert(N > 0, "assumption violation: pool should have space for more than zero elements.");
        static_assert(std::numeric_limits<SIZE_TYPE>::is_integer && !std::numeric_limits<SIZE_TYPE>::is_signed,
                       "assumption violation: size_type is not an unsigned integer type.");

        static_assert(alignof(T) <= DATA_ALIGN && alignof(SIZE_TYPE) <= DATA_ALIGN,
                      "assumption violation: provided alignment is too strict.");

        using pointer       = T *;
        using const_pointer = T const *;
        using value_type    = T;
        using size_type     = SIZE_TYPE;
        // strictly speaking, difference type is supposed to be able to represent the subtraction
        // of any two size_type values.  in cases where size_type is as large as uintmax_t, and
        // N == std::numeric_limits<uintmax_t>::max(), this may not be possible,
        // as umax and 0 - umax may not be representable in any signed type.
        // otherwise cues::integer_selector will attempt to pick the smallest signed integer
        // that can represent +/-N, which is the largest value it expects to have to represent.

        using difference_type = typename cues::select_least_integer_by_digits<
            cues::minimum_bits(N)
            , cues::integer_selector_signedness::use_signed
        >::type ;


        static constexpr typename cues::select_least_integer_by_digits<
            cues::minimum_bits(cues::max(sizeof(value_type), sizeof(size_type)))
            , cues::integer_selector_signedness::use_unsigned
        >::type const
        block_size = cues::max(cues::max(alignof(value_type),sizeof(value_type)), cues::max(alignof(size_type),sizeof(size_type)));

        // allocators use size_type to refer to count of objects of size sizeof(T),
        // but sometimes we need to work in bytes
        using byte_size_type = typename cues::select_least_integer_by_digits<
              cues::minimum_bits(N*block_size)
            , cues::integer_selector_signedness::use_unsigned
        >::type;

        // in particular the "native" (not rebound to another type) allocator type
        using allocator_type = homogenous_allocator<T, N, SIZE_TYPE, DATA_ALIGN, T>;

        //! constructor takes O(N); prefer to establish these objects at program start
        //! and then pass by reference instead of constructing them on-the-fly, unless
        //! N is small.
        // currently placement new cannot be used in constexpr contexts, or else this could
        // potentially be cosntexpr
        homogenous_pool() noexcept
        : current_size{0}
        , free_list_start{0}
        {
            for (size_type idx = 0;
                idx < N;
                ++idx
            ){
                new (index_to_pv(idx)) size_type{static_cast<size_type>(idx+1u)};
            }
        }

        // bulk copying of the memory would only make sense if there were no
        // allocations; who would know to deallocate from the copy?
        // if there are no allocations, there is no need to copy anyway,
        // so just delete the copy.  move has similar issues.
        homogenous_pool(homogenous_pool const &) = delete;
        homogenous_pool & operator=(homogenous_pool const &) = delete;
        homogenous_pool(homogenous_pool &&) = delete;
        homogenous_pool & operator=(homogenous_pool &&) = delete;

        //! for n == 1, with alignment == alignof(T),
        //! this is worst case O(1), though it may fail if there is no room.
        //! for n > 1, it is best case O(n + a) --where "a" is zero for
        //! alignment <= alignof(T), and alignment - alignof(T) otherwise--
        //! if the first free space has room for n objects
        //! of T with the alignment requested, but worst case it is
        //! O(N) and may still fail after that if it cannot find room.
        //! For n > 1 (or alignment > alignof(T)) it will also try to sort
        //! blocks that are both adjacent in memory and to the existing search
        //! range in the linked list, but does not reconsider previously
        //! visited nodes, so it can avoid worse time complexity, and thus may
        //! still fail if adjacent blocks are many nodes away on the free list.
        //! Users can call sort_free_list(), which is O((N-n)^2), to sort all adjacent space
        //! regardless of its position on the free list if they are willing to pay that cost.
        //! Because this only allocates space, and only does so if it can succeed, a failure
        //! shall not affect the capacity, and shall leave the pool in a usable state.
        //! Called try_allocate instead of allocate because it does not quite match the
        //! interface for the Allocator concept in that it returns nullptr on failure instead
        //! of throwing std::bad_alloc, which allows it to be noexcept
        #if __cplusplus >= 201402L || (defined __cpp_constexpr) && __cpp_constexpr >= 201304L
        constexpr
        #endif
        pointer try_allocate(size_type n, size_type alignment) noexcept
        {
            assert(this->current_size <= N); //< verify no bug in this object

            // we asked for n Ts, and a block may be sizeof(T) or it may be sizeof(S)
            // so adjust n to be the number of blocks
            n = blocks_allocated_per_Ts_allocated(n);
            // reserve a minimum of one block because subsequent allocations
            // need unique addresses.
            if (n == 0) {n = 1;}

            // if there couldn't possibly be enough room, don't bother looking
            if ((this->free_list_start == N) || (n > (N - this->current_size)))
            {
                return nullptr;
            }

            void * candidate_ptr             = index_to_pv(free_list_start);
            size_type previous_last          = free_list_start;
            size_type search_range_begin     = free_list_start;
            size_type search_range_end       = free_list_start + 1;
            size_type next                   = *index_to_pcnext(free_list_start);

            // for n == 1, we may already be done even for stricter alignment
            // if the first free spot happens to be aligned.  for n > 1 we don't
            // have enough space and the loop will check alignment.
            if (alignment > alignof(T) && n <=1)
            {
                std::size_t temp_size = block_size;
                candidate_ptr = std::align(alignment, n*block_size, candidate_ptr, temp_size);
            }

            // we're looking for contiguous blocks that add up to n blocks
            // for n <= 1, we don't need to find contiguous elements and the whole loop can be skipped
            // unless the alignment requirement is stricter
            for (;
                // continue while we haven't found enough blocks
                // or we have, but we haven't reached the requested alignment yet
                ((search_range_end - search_range_begin < n) || (candidate_ptr == nullptr))
                // and while there could still be enough blocks to find
                && (next < N);
                next = *index_to_pcnext(search_range_end-1u)
            ) {
                // check contiguity
                if  // increasing case: end is already not inclusive,
                     // so being larger than end means not contiguous
                     (next == search_range_end )
                {
                    // increasing by 1 is already in order, no need to coalesce
                    search_range_end = next+1u;
                }
                else if ((next < search_range_begin) && (next == search_range_begin - 1u))
                {
                    // cache next's next
                    size_type next_next = *index_to_pcnext(next);
                    // change next to point to search_begin instead of
                    // next next
                    *index_to_pnext(next) = search_range_begin;
                    // change the end of the search range to point to
                    // next's former next
                    *index_to_pnext(search_range_end - 1u) = next_next;

                    // we need to update the previous element as well, or else
                    // the free list start.
                    if (previous_last == free_list_start)
                    {
                        // we were starting after the earlier element, now start with it
                        free_list_start = next;
                    }
                    else
                    {
                        // the previous last was starting with an element after this one,
                        // now have it point to the earlier one
                        *index_to_pnext(previous_last) = next;
                    }
                }
                else // not contiguous any more, start looking again at next block
                {
                    // save the last index in the previous range
                    previous_last = (search_range_end == search_range_begin) ?
                        search_range_begin
                        :
                        search_range_end - 1u
                    ;
                    // start checking at the new beginning
                    search_range_begin = next;
                    search_range_end   = next+1u;
                } // end contiguity check

                 // if loop is about to finish
                 // (that is, we're now at (end - begin) >= n)
                 // check alignment
                 if (search_range_end - search_range_begin >= n)
                 {
                    candidate_ptr = index_to_pv(search_range_begin);
                    if (alignment > alignof(T))
                    {
                        std::size_t temp_size = (search_range_end - search_range_begin)*block_size;
                        candidate_ptr = std::align(alignment, n*sizeof(T), candidate_ptr, temp_size);
                    } // end alignment adjustment
                 } // end update candidate pointer
            } // end looped search for aligned contiguous region

            if (   (search_range_end - search_range_begin < n)
                || (nullptr == candidate_ptr)
            )
            {
                return nullptr;
            }

            // if the aligned space we found doesn't start at the
            // beginning of the contiguous block we found, store
            // that earlier region as the last
            size_type allocation_begin = pcv_to_index(candidate_ptr);
            // set allocation_end to be the required block count after begin
            size_type allocation_end = allocation_begin + n;

            // if there is space at the end of the allocation range,
            // have that space be next instead of the space after it,
            // to which it should already be pointing due to sorting
            if (allocation_end < search_range_end)
            {
                // cache the first index after the allocation range
                // as the next free block
                next = *index_to_pcnext(allocation_end - 1);
            }

            // sorting as we go means that if free list start
            // was within allocation_begin, search_range_end,
            // it is now at allocation_begin, and can be changed
            // to account for this allocation by setting it to
            // next, which points to the next block after the allocation
            // (which may or may not also be after the search range)
            if (free_list_start == allocation_begin)
            {
                free_list_start = next;
            }
            // free_list_start could also be == search_range_begin,
            // in which case we want the run from search_range_begin
            // to allocation_begin to point to next.
            else if (free_list_start == search_range_begin)
            {
                assert(allocation_begin > search_range_begin);
                *index_to_pnext( allocation_begin - 1u) = next;
            }
            // finally, it is possible that free list start was not
            // within the search range at all, in which case we want
            // the previous last block to point to next
            else
            {
                *index_to_pnext(previous_last) = next;
            }

            // update size
            // using end-begin here in case n == 0 and we allocated a block anyway.
            current_size += (allocation_end - allocation_begin);

            // if we have if constexpr, conditionally remove the loop if the type is trivially destructible;
            // standard permits ending lifetime by re-using storage if the destructor is trivial

            if CUES_CONSTEXPR_OR_RUNTIME_IF
            (!std::is_trivially_destructible<size_type>::value)
            {
                // destruct the index types so the memory can be used for something else if necessary
                // most likely, size_type is trivial so the loop will be skipped
                for (size_type idx = allocation_begin;
                    idx < allocation_end;
                    index_to_pnext(idx++)->size_type::~size_type()
                )
                {}
            }

            return std::launder(static_cast<pointer>(candidate_ptr));

        }

        //! as with try_allocate, but will throw if try_allocate would return nullptr
        //! to match the named requirements for Allocator
        #if __cplusplus >= 201402L || (defined __cpp_constexpr) && __cpp_constexpr >= 201304L
        constexpr
        #endif
        pointer allocate(size_type n, size_type alignment)
        {
            pointer maybe_allocated = this->try_allocate(n, alignment);
            if (maybe_allocated == nullptr)
            {
                throw std::bad_alloc();
            }
            return maybe_allocated;
        }

        //! PRECONDITION: as with the Allocator Concept, this function may only
        //! be called with a pointer returned by allocate (or try_allocate),
        //! passing the size passed to that call of allocate (or try_allocate)
        //! and may only be called once for each such returned pointer.
        //! provided the preconditions are met,
        //! deallocation is always O(n) (and thus O(1) for n==1)

        #if __cplusplus >= 201402L || (defined __cpp_constexpr) && __cpp_constexpr >= 201304L
        constexpr
        #endif
        void deallocate(pointer ptr, size_type n) noexcept
        {

            assert(this->current_size <= N);
            // we are deallocating n Ts, but how many blocks is that?

            // permit passing nullptr like with operator delete
            if (ptr == nullptr ) {return;}

            // deallocate as many as we allocated
            n = blocks_allocated_per_Ts_allocated(n);
            assert(n <= this->current_size);

            // allocate always reserves a block even if called with 0, so
            // if we get zero, set it to one
            if (n == 0) {n = 1;}

            size_type next = this->free_list_start;
            size_type const starting_index = pcv_to_index(ptr);

            // put indices back in place
            for (size_type working_index = starting_index, end_index = starting_index + n;
                working_index < end_index;
                (new (index_to_pv(--end_index)) size_type{next}), (next = end_index)
            )
            {}
            // update the beginning to point to the first one
            this->free_list_start = starting_index;
            // update the size
            this->current_size -= n;
        }

        //! absolute maximum possible value that could hypothetically be requested
        // you could, strictly speaking, ask for all N blocks at once, which,
        // in the case of larger size_type, mean allocating more Ts than there
        // are corresponding entries in the free list
        constexpr size_type max_size() const noexcept
        {
            return possible_T_allocations_per_blocks_allocated(N);
        }

        //! for sizeof(T) >= sizeof(size_type), this is the same
        //! as max_size().  If sizeof(T) < sizeof(size_type), this
        //! represents the maximum number of single-T allocations,
        //! where max_size() would be the maximum Ts you could get
        //! if you allocated the entire memory space all at once
        constexpr size_type max_blocks() const noexcept
        {
            return N;
        }

        //! Describes how many allocations could still be made
        //! if a single T is allocated each time.
        //! For sizeof(T) >= sizeof(size_type),
        //! this is the same as remaining_capacity, which
        //! also takes into account the possibility of allocating
        //! more than one T per block
        constexpr size_type remaining_blocks() const noexcept
        {
            return N - this->current_size;
        }

        //! returns the maximum number of Ts that could currently be allocated.
        //! calls to allocate for this many slots may not necessarily succeed.
        //! The remaining memory may not be contiguous, which could mean large
        //! arrays cannot be allocated all at once.
        constexpr size_type remaining_capacity() const noexcept
        {
            return possible_T_allocations_per_blocks_allocated(remaining_blocks());
        }

        //! access to (const) data array.
        //! can permit outside inspection of the raw data
        constexpr cues::raw_byte_type const * data() const noexcept
        {
            return &(this->raw_memory[0]);
        }

        //! Re-sorts the free list, making free blocks available as contiguous where possible.
        //! Not necessary for applications where one block is allocated at a time,
        //! but may improve subsequent allocation speed or chance of success
        //! for allocations of n > 1, at the cost of up-front time;
        //! for a pool of block size N and used size n, this time is
        //! O((N-n)^2) in most cases (for n == N, a full pool, it reduces to
        //!      O(1) and does nothing),
        //! O(N) for a completely empty pool (n == 0), where it simply rewrites
        //!      the free-list in order instead of sorting
        //! approaches O(N-n) if the free list is already sorted (for example
        //!      due to LIFO allocation/deallocation ordering).
        //! Note that the pool will try to sort in-place when searching for more than one
        //! contiguous block, but does so in a more limited fashion (only sorts free blocks
        //! that are adjacent both in memory and in the linked list order) in order to preserve
        //! its complexity guarantee; this can cause it to miss free blocks that are adjacent in
        //! memory but not adjacent in the free list (and possibly fail allocation),
        //! which this method will be able to sort at the expense of taking up to O((N-n)^2) time.
        #if __cplusplus >= 201402L || (defined __cpp_constexpr) && __cpp_constexpr >= 201304L
        constexpr
        #endif
        void sort_free_list() noexcept
        {

            // if the whole area is free, n == N, don't bother sorting existing data
            // with an O(n^2) algorithm (which would be O(N^2) now)
            // just rewrite the indices in order, takes O(N)
            if (current_size == 0) {
                for (size_type idx = 0; idx < N; ++ idx)
                {
                    *std::launder(static_cast<size_type *>(index_to_pv(idx))) = idx + 1u;
                }
                return;
            }

            // if the free list is empty, there is nothing to sort
            if (this->free_list_start == N) {return;}

            // otherwise free list is more than 0 items.
            // insertion sort can sort it in-place
            // start our new list with the first element in the old list
            size_type new_begin = this->free_list_start;
            // remove the first element from the old list
            this->free_list_start = *std::launder(static_cast<size_type const *>(index_to_pcv(this->free_list_start)));
            // end the new list after the first element
            *std::launder(static_cast<size_type *>(index_to_pv(new_begin))) = N;

            // now handle the remaining elements
            for(size_type current = this->free_list_start, last = new_begin;
                current < N;
                // remove from old list
                (this->free_list_start = *std::launder(static_cast<size_type const *>(index_to_pcv(this->free_list_start))))
                 // consider new beginning of old list
                , (current = this->free_list_start)
            )
            {
                // are we inserting at the end ?
                // checking that first because the class is tailored first for
                // single element allocation, and LIFO after that.
                // for LIFO allocation/deallocation, the free elements are in
                // ascending order, and thus each would be higher than the last
                // this rewards clients that use the class for its intended application
                // at the expense of those who might be better off with another solution
                // anyway.
                if (current >= last)
                {
                    // put end of list after current
                    *std::launder(static_cast<size_type *>(index_to_pv(current))) = N;
                    // put current after last
                    *std::launder(static_cast<size_type *>(index_to_pv(last))) = current;
                    // update last
                    last = current;
                }
                // different handling due to begin being separate index
                else  if (current < new_begin)
                {
                    // put new_begin after current
                    *std::launder(static_cast<size_type *>(index_to_pv(current))) = new_begin;
                    // put current at the beginning
                    new_begin = current;
                }
                // somewhere in the middle, search for insertion position
                else
                {
                    size_type candidate = new_begin;
                    size_type lbound = *std::launder(static_cast<size_type const *>(index_to_pcv(candidate)));
                    for ( ;
                        (current < lbound) && (lbound < N);
                          (candidate = lbound)
                        , (lbound = *std::launder(static_cast<size_type const *>(index_to_pcv(lbound))))
                    )
                    {}
                    // when loop finished, current was less than candidate but >= lbound
                    // so put current between candidate and lbound
                    // lbound after current
                    *std::launder(static_cast<size_type *>(index_to_pv(current)))   = lbound;
                    // current after candidate
                    *std::launder(static_cast<size_type *>(index_to_pv(candidate))) = current;

                }
            }
            // point the free list to begin with the first element in the sorted list
            this->free_list_start = new_begin;
        }

    private:

        static constexpr bool value_needs_more_than_size =
            cues::max(alignof(value_type),sizeof(value_type)) >=
            cues::max(alignof(size_type),sizeof(size_type))
        ;

        static constexpr byte_size_type const value_size_or_one =
        value_needs_more_than_size
        ? 1
        : cues::max(alignof(value_type),sizeof(value_type));

        static constexpr byte_size_type const size_size_or_one =
        value_needs_more_than_size ? 1 : cues::max(alignof(size_type),sizeof(size_type));

        static constexpr byte_size_type blocks_allocated_per_Ts_allocated(size_type n)
        {
            // constexpr if would work but requires c++17, otherwise
            // switch in a return statement with the conditional operator
            #if __cplusplus >= 201703L || ( defined (__cpp_if_constexpr) && __cpp_if_constexpr >= 201606L )
            if constexpr (value_size_or_one == 1 && size_size_or_one == 1 )
            {
                return
            #else
            //  since the condition is constexpr, hopefully the compiler will
            // optimize it out on its own
            return (value_size_or_one == 1 && size_size_or_one == 1 ) ?
            #endif
                ((n == 0) ? 1 : n)
            #if __cplusplus >= 201703L || ( defined (__cpp_if_constexpr) && __cpp_if_constexpr >= 201606L )
                ;
            }
            else
            {
                return
            #else
            :
            #endif
                // don't truncate, we need the whole block of the remainder
                cues::div_roundup<byte_size_type>::calculate(static_cast<byte_size_type>(n*value_size_or_one), size_size_or_one);
            #if __cplusplus >= 201703L || ( defined (__cpp_if_constexpr) && __cpp_if_constexpr >= 201606L )
            }
            #endif

        }

        static constexpr byte_size_type possible_T_allocations_per_blocks_allocated(size_type n)
        {
            #if __cplusplus >= 201703L || ( defined (__cpp_if_constexpr) && __cpp_if_constexpr >= 201606L )
            if constexpr (value_size_or_one == 1 && size_size_or_one == 1 )
            {
                return
            #else
            //  since the condition is constexpr, hopefully the compiler will
            // optimize it out on its own
            return (value_size_or_one == 1 && size_size_or_one == 1 ) ?
            #endif
                n
            #if __cplusplus >= 201703L || ( defined (__cpp_if_constexpr) && __cpp_if_constexpr >= 201606L )
                ;
            }
            else
            {
                return
            #else
            :
            #endif
                // truncate, we can't allocate an extra T without enough space
                static_cast<byte_size_type>((n*size_size_or_one) / value_size_or_one);
            #if __cplusplus >= 201703L || ( defined (__cpp_if_constexpr) && __cpp_if_constexpr >= 201606L )
            }
            #endif
        }

        constexpr void const * index_to_pcv(size_type idx) const
        {
            return static_cast<void const *>(&(this->raw_memory[idx*block_size]));
        }

        constexpr void * index_to_pv(size_type idx)
        {
            return const_cast<void *>(index_to_pcv(idx));
        }

        constexpr size_type pcv_to_index(void const * ptr) const
        {
            return static_cast<size_type>((static_cast<cues::raw_byte_type const *>(ptr) - &(this->raw_memory[0]))/block_size);
        }

        constexpr size_type const * index_to_pcnext(size_type index) const
        {
            return std::launder(static_cast<size_type const *>(index_to_pcv(index)));
        }

        constexpr size_type * index_to_pnext(size_type index)
        {
            return const_cast<size_type *>(const_cast<homogenous_pool const *>(this)->index_to_pcnext(index));
        }

        alignas (DATA_ALIGN) cues::raw_byte_type raw_memory[N*block_size];
        size_type current_size; //< related to top of the stack as top = N - 1 - current_size
        size_type free_list_start;

};


//! allocator type that refers to a pool to handle the actual allocation,
//! but which satisfies the Allocator requirements in particular being
//! copyable, which the pool itself cannot do.
//! WARNING: does not use reference counting to maintain lifetime of pool;
//! it is the application's responsibility to ensure that the pool lives
//! at least as long as the allocator
template<
    class T,
    std::size_t N,
    typename SIZE_TYPE = typename cues::select_least_integer_by_digits<
          cues::minimum_bits(N)
        , cues::integer_selector_signedness::use_unsigned
    >::type,
    std::size_t DATA_ALIGN = alignof(T),
    class U = T
> class homogenous_allocator
{

public:
    using pool_type = homogenous_pool<T,N,SIZE_TYPE, DATA_ALIGN>;

    using pointer         = U *;
    using const_pointer   = U const *;
    using value_type      = U;
    using size_type       = typename pool_type::size_type;
    using difference_type = typename pool_type::difference_type;

    // this allocator has state, so:
    using is_always_equal = std::false_type;
    using propagate_on_container_copy_assignment = std::true_type;
    using propagate_on_container_move_assignment = std::true_type;
    using propagate_on_container_swap            = std::true_type;

    using allocator_category = cues::contiguous_allocator_tag;

    using deleter_type = homogenous_deleter<T, N, SIZE_TYPE, DATA_ALIGN, U>;

    template<typename V>
    struct rebind
    {
        using other = homogenous_allocator<T, N, SIZE_TYPE, DATA_ALIGN, V>;
    };

    constexpr homogenous_allocator() noexcept
    : my_pool(nullptr)
    {}

    constexpr homogenous_allocator(pool_type * pl) noexcept
    : my_pool(pl)
    {}

    constexpr homogenous_allocator(pool_type & pl) noexcept
    : my_pool(&pl)
    {}

    homogenous_allocator(homogenous_allocator const &) = default;
    homogenous_allocator & operator=(homogenous_allocator const &) = default;

    template<typename V>
    constexpr homogenous_allocator(homogenous_allocator<T, N, SIZE_TYPE, DATA_ALIGN, V> const & other) noexcept
    : my_pool(other.get_pool())
    {
    }

    template<typename V>
    constexpr homogenous_allocator & operator=(homogenous_allocator<T, N, SIZE_TYPE, DATA_ALIGN, V> const & other) noexcept
    {
        this->my_pool = other.get_pool();
    }

    // helper function for containers
    constexpr homogenous_allocator select_on_container_copy_construction() const
    {
        return homogenous_allocator{*this};
    }

    constexpr pointer allocate(size_type n) const
    {
        return (nullptr == my_pool) ?
            throw std::bad_alloc()
        :
            std::launder(static_cast<pointer>(static_cast<void *>(my_pool->allocate(blocks_T_per_U(n), alignof(U)))));
    }

    #if __cplusplus >= 201402L || (defined __cpp_constexpr) && __cpp_constexpr >= 201304L
        constexpr
    #endif
    void deallocate(pointer ptr, size_type n) const noexcept
    {
        if (nullptr != my_pool)
        {
            my_pool->deallocate(static_cast<typename pool_type::pointer>(static_cast<void *>(ptr)), blocks_T_per_U(n));
        }
    }

    #if __cplusplus >= 201402L || (defined __cpp_constexpr) && __cpp_constexpr >= 201304L
        constexpr
    #endif
    size_type max_size() const noexcept
    {
        return (nullptr == my_pool) ? 0 : my_pool->max_size()/blocks_T_per_U(1);
    }

    friend constexpr bool operator== (homogenous_allocator const & lhs, homogenous_allocator const & rhs) noexcept
    {
        return lhs.my_pool
            == rhs.my_pool
        ;
    }

    friend constexpr bool operator!= (homogenous_allocator const & lhs, homogenous_allocator const & rhs) noexcept
    {
        return !(lhs == rhs);
    }

    constexpr size_type remaining_size() const
    {
        return (nullptr == my_pool) ? 0 : my_pool->remaining_capacity();
    }

    constexpr std::size_t storage_size() const
    {
        return (nullptr == my_pool) ? 0 : (my_pool->max_blocks() * pool_type::block_size);
    }

    constexpr cues::raw_byte_type const * data() const
    {
        return (nullptr == my_pool) ? nullptr : my_pool->data();
    }

    constexpr std::size_t block_size() const
    {
        return pool_type::block_size;
    }

    constexpr pool_type * get_pool() const
    {
        return this->my_pool;
    }

private:

    // round up to the number of blocks needed to store a U
    // The alignment of U will be passed to the pool separately.
    static constexpr std::size_t blocks_T_per_U(size_type blocks_U)
    {
        return cues::div_roundup<std::size_t>::calculate(
            blocks_U*cues::max(alignof(U),sizeof(U)),
            cues::max(alignof(typename pool_type::value_type),sizeof(typename pool_type::value_type))
        );
    }


    cues::non_owning_ptr<pool_type> my_pool;
};


} // namespace cues

#endif // CUES_HOMOGENOUS_POOL_HPP
