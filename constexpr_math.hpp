#ifndef CUES_CONSTEXPR_MATH_HPP
#define CUES_CONSTEXPR_MATH_HPP

#include <type_traits>
#include <limits>

#if __cplusplus >= 202002L || defined __cpp_lib_int_pow2 || defined __cpp_lib_bitops

#include <bit>
#else
#include <bitset>
#endif

#if __cplusplus >= 201402L || defined ( __cpp_lib_constexpr_algorithms )
#include <algorithm>
#endif

#include "cues/type_traits.hpp"
#include "cues/version_helpers.hpp"

namespace cues {

//! unlike std::pow(val, 2), this is a constexpr function
//! it only handles one exponent, but does not place restrictions
//! on the type and does not require recursion nor conditionals
//! PRECONDITION: the type must be large enough to hold the result
template<typename T>
constexpr T square(T val)
{
    return val*val;
}


//! unlike std::pow(val, power), this is a constexpr function
//! however, it only handles non-negative integral powers
//! PRECONDITION: the type you pass must be large enough to hold
//! the result.  Cast to a wider type if necessary before calling.
template<typename T>
constexpr T pow(T val, unsigned int power)
{
    return (power == static_cast<T>(0)) ?
        static_cast<T>(1) //< anything to the zeroth is one
        :
        (
            (power == static_cast<T>(1)) ?
            val //< anything to the 1st is itself
            :
            // power >= 2, cut as close to in half as we can and recurse
            pow(val, power/2u) * pow(val, power - power/2u)
        )
    ;
}

template<unsigned int NUM_BITS>
constexpr typename select_least_integer_by_digits<NUM_BITS, integer_selector_signedness::use_unsigned>::type bitmask() noexcept
{
    using return_type = typename select_least_integer_by_digits<NUM_BITS, integer_selector_signedness::use_unsigned>::type;
    static_assert(NUM_BITS <= std::numeric_limits<return_type>::digits,"ASSERTION VIOLATION: asked for too large a mask.");
    return cues::pow(static_cast<return_type>(2), NUM_BITS) - 1;
}

#if __cplusplus >= 202002L || defined __cpp_lib_bitops

template<typename INTEGER_TYPE>
constexpr std::size_t popcount(INTEGER_TYPE value) noexcept
{
    return std::popcount(value);
}
//! not constexpr, but provided alongside it for locality
//! note that when std::popcount is available,
//! popcount and fast_popcount are both just std::popcount
template<typename INTEGER_TYPE>
std::size_t fast_popcount(INTEGER_TYPE value) noexcept
{
    return std::popcount(value);
}

#else
//! constexpr with no std::popcount()
//! WARNING: may be especially slow at runtime due to recursion,
//! if compiler does not optimize tail-recursive calls as loop
template<typename INTEGER_TYPE>
constexpr std::size_t popcount(INTEGER_TYPE value) noexcept
{
    static_assert(
              std::numeric_limits<INTEGER_TYPE>::is_specialized
          && std::numeric_limits<INTEGER_TYPE>::is_integer
          && !std::numeric_limits<INTEGER_TYPE>::is_signed,
        "popcount implementation requires unsigned integer"
    );
    // from: https://graphics.stanford.edu/%7Eseander/bithacks.html#CountBitsSetKernighan
    // converted from loop to tai-recursive call
    return (value == 0u) ? 0u : 1u + popcount(value & (value - 1u));
}

//! not constexpr, but provided alongside it for locality
//! may use popcount hardware instruction "under the hood" if
//! standard library and compiler support it, otherwise should use
//! an implementation expected to be fast at runtime
template<typename INTEGER_TYPE>
std::size_t fast_popcount(INTEGER_TYPE value) noexcept
{
    return (std::bitset<std::numeric_limits<INTEGER_TYPE>::digits>(value)).count();
}
#endif // __cplusplus

// use standard library if available and possible
#if __cplusplus >= 202002L || defined __cpp_lib_int_pow2
template<typename T, typename std::enable_if<
    std::is_fundamental<T>::value &&
    std::is_unsigned<T>::value &&
    !std::is_same<bool, T>::value
, bool>::type = true>
constexpr bool is_pow2(T val)
{
    return std::has_single_bit(val);
}
#endif

// differs from https://graphics.stanford.edu/~seander/bithacks.html#DetermineIfPowerOf2
// in that it is templated on type, so it will work for types larger than unsigned int,
// including custom types with std::numeric_limits<> specialized and which provide
// arithmetic and bit operators
template<typename T, typename std::enable_if<
#if __cplusplus >= 202002L || defined __cpp_lib_int_pow2
    !std::is_fundamental<T>::value &&
#endif
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type = false>
constexpr bool is_pow2(T val)
{
    // an exact power of two in an unsigned integer has exactly one bit
    // not equal to 0 (except for the value 0 itself).
    // so, after checking != 0, subtracting one from the value
    // gives a mask that can verify no other non-zero bits;
    // for example:
    // 2^8   = 0b10000000
    // 2^8-1 = 0b01111111
    // but for other values, the bit pattern differs, for example
    //         0b01010100
    // and     0b01010011
    // which results in a non-zero mask
    return (val != T{0}) && ((val & (val - T{1})) == T{0});
}


//! PRECONDITION: zero input will give the maximum negative
//! value of the return type, which is
//! obviously not negative infinity.  Either check for 0 before
//! passing, or assume any returned negative value means -infinity
//! NOTE: rounds down.

#if __cplusplus >= 202002L || defined __cpp_lib_bitops

template<typename T, typename std::enable_if<
    std::is_fundamental<T>::value &&
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type = true>
constexpr int log2(T val)
{
    int leading_zeros = std::countl_zero(val);
    return std::numeric_limits<T>::digits == leading_zeros ?
        // leading zeros equal to digits for unsigned means all bits are zero, means value is zero
        (std::numeric_limits<T>::min)()
        : // the position of the first 1 is proportional to the log, but
          // recall that log(1) == 0, so we need one less than that,
          // and the subtraction does not overflow because we already checked the case
          // that digits == leading_zeros
        std::numeric_limits<T>::digits - leading_zeros - 1;
}
#endif


#if __cplusplus >= 201703L || defined __cpp_if_constexpr
// similar to https://graphics.stanford.edu/~seander/bithacks.html#IntegerLog
// but:
// 1) is templated on integer type and uses if constexpr to remove unnecessary parts
// 2) works on larger types (64+ bits), including custom types for which std::numeric_limits<>
//    is specialized and which provide comparison and shift operators
// 3) log(0) returns the minimum value of the (signed) return type instead of 0
template<typename T, typename std::enable_if<
#if __cplusplus >= 202002L || defined __cpp_lib_bitops
    !std::is_fundamental<T>::value &&
#endif
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type = false>
constexpr int log2(T val)
{
    using return_type = int;
    using pow2_type = unsigned int;
    static_assert(std::numeric_limits<T>::radix == 2, "cues::log2 only works for integers with radix 2");
    // a simpler implementation is ((val > 1) ? (1 + log2(val >> 1)) : 0
    // but that could result in more recursion depth (especially bad at runtime)
    // and leaves log2(0) == log2(1);
    // recursion depth is not critical at compile time, but if this evaluates at runtime,
    // this implementation likely helps to reduce the recursion.
    // it also differentiates log2(0) from log2(1) without resorting to exceptions.
    // branching is also reduced by making use of cast from boolean to integer

    // using if constexpr to avoid warnings about shifting more bits than there are
    // in the type, and also to eliminate impossible checks without dumping that
    // on the optimizer
    if constexpr ((std::numeric_limits<T>::max)() > 18446744073709551615ull)
    {
        // recursion here, we checked that <T>::max() is larger than 64 bits but we are only
        // guaranteed 64 bit literals
        if (val > 18446744073709551615ull)
        {
            return 64 + log2<T>(val >> 64u);
        }
    }
    return_type working_result = 0;
    pow2_type current_pow2 = 0;

    if constexpr ((std::numeric_limits<T>::max)() > 4294967295ul)
    {
        // 2^5 == 32
        current_pow2 = static_cast<pow2_type>(val > 4294967295ul) << 5;

        working_result += current_pow2;
        val >>= current_pow2;
    }

    if constexpr ((std::numeric_limits<T>::max)() > 65535u)
    {
        current_pow2 = static_cast<pow2_type>(val > 65535u) << 4;
        working_result += current_pow2;
        val >>= current_pow2;
    }

    if constexpr ((std::numeric_limits<T>::max)() > 255u)
    {
        current_pow2 = static_cast<pow2_type>(val > 255u) << 3;
        working_result += current_pow2;
        val >>= current_pow2;
    }

    // no type is smaller than 8 bits so we don't need the compile-time switches any more
    current_pow2 = static_cast<pow2_type>(val > 15u) << 2;

    working_result += current_pow2;
    val >>= current_pow2;

    current_pow2 = static_cast<pow2_type>(val > 3u) << 1;

    working_result += current_pow2;
    val >>= current_pow2;

    working_result += static_cast<return_type>(val > 1u);

    // down to val = [0..3) here, l2 is correct for anything except 0
    return (val == 0) ? (std::numeric_limits<return_type>::min)() : working_result;

}

#else

namespace detail
{
    template<typename T>
    constexpr int log2_recurser(T val)
    {
        static_assert(std::numeric_limits<T>::is_integer
            && !std::numeric_limits<T>::is_signed
            && !std::is_same<bool, T>::value
            && std::numeric_limits<T>::radix == 2
            , "log2_recurser called from context that violates its assumptions."
        );
        return static_cast<int>((val > 255) ?
            (8 + log2_recurser(static_cast<T>(val >> 8u)))
            :
            ((val > 15) ?
                (4 + log2_recurser(static_cast<T>(val >> 4u)))
                :
                ((val > 3) ?
                    (2 + log2_recurser(static_cast<T>(val >> 2u)))
                    :
                    ((val > 1) ?
                        1 // 2 or 3
                        :
                        0 // 1 or 0
                    )
                )
            )
        );
    }
}

// as above, but recurse instead of if constexpr.
// slower at runtime, but only necessary if you don't have c++17 yet
// and works all the way back to c++11
template<typename T, typename std::enable_if<
#if __cplusplus >= 202002L || defined __cpp_lib_bitops
    !std::is_fundamental<T>::value &&
#endif
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type = false>
constexpr int log2(T val)
{
    using return_type = int;
    static_assert(std::numeric_limits<T>::radix == 2, "cues::log2 only works for integers with radix 2");
    return ((val == 0) ?
        (std::numeric_limits<return_type>::min)()
        :
        cues::detail::log2_recurser(val)
    );

}
#endif

#if __cplusplus >= 202002L || defined __cpp_lib_bitops
template<typename T, typename std::enable_if<
    std::is_fundamental<T>::value &&
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type = true>
constexpr unsigned int count_trailing_zeros(T val)
{
    return std::countr_zero(val);
}
#endif

#if __cplusplus >= 201703L || defined __cpp_if_constexpr
template<typename T, typename std::enable_if<
    // use c++20 for fundamental types if it exists
    #if __cplusplus >= 202002L || defined __cpp_lib_bitops
    !std::is_fundamental<T>::value &&
    #endif
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type = false>
constexpr unsigned int count_trailing_zeros(T val)
{
    using return_type = unsigned int;

    // hopefully using if constexpr to avoid warnings about shifting more bits than there are
    // in the type, and also to eliminate impossible checks without dumping that
    // on the optimizer

    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() > (std::numeric_limits<uint_least64_t>::max)())
    {
        // recursion here, we checked that <T>::max() is larger than 64 bits but we are only
        // guaranteed 64 bit literals
        if (0 == (val & 0xFFFFFFFFFFFFFFFFull))
        {
            return 64 + count_trailing_zeros(static_cast<T>(val >> 64u));
        }
    }

    return_type cumulative_result = 0;
    return_type current_result = 0;

    if constexpr ((std::numeric_limits<T>::max)() > 4294967295ul)
    {
        current_result = ((T{cues::bitmask<32>()} & val) == 0) ? 32 : 0;

        cumulative_result += current_result;
        val >>= current_result;
    }

    if constexpr ((std::numeric_limits<T>::max)() > 65535u)
    {
        current_result = ((T{cues::bitmask<16>()} & val) == 0) ? 16 : 0;

        cumulative_result += current_result;
        val >>= current_result;
    }

    if constexpr ((std::numeric_limits<T>::max)() > 255u)
    {
        current_result = ((T{cues::bitmask<8>()} & val) == 0) ? 8 : 0;

        cumulative_result += current_result;
        val >>= current_result;
    }

    current_result = ((T{cues::bitmask<4>()} & val) == 0) ? 4 : 0;

    cumulative_result += current_result;
    val >>= current_result;

    current_result = ((T{cues::bitmask<2>()} & val) == 0) ? 2 : 0;

    cumulative_result += current_result;
    val >>= current_result;

    current_result = ((T{cues::bitmask<1>()} & val) == 0) ? 1 : 0;

    cumulative_result += current_result;
    val >>= current_result;

    cumulative_result += static_cast<unsigned int>((T{cues::bitmask<1>()} & val) == 0);

    return cumulative_result;
}
#else
// if we don't have constexpr if, do it recursively
namespace detail
{
    template<typename T, unsigned int REMAINING_BITS>
    constexpr unsigned int count_trailing_zeros_recurser(T val)
    {
        static_assert(std::numeric_limits<T>::is_integer
            && !std::numeric_limits<T>::is_signed
            && !std::is_same<bool, T>::value
            && std::numeric_limits<T>::radix == 2
            , "count_trailing_zeros_recurser called from context that violates its assumptions."
        );

        return (REMAINING_BITS == 1u) ?
            static_cast<T>((T{1u} & val) == 0)
            :
            (
                ((T{cues::bitmask<REMAINING_BITS/2u>()} & val) == 0) ?
                // it is unlikely any fundamental unsigned type has an odd number of bits, but calculate remaining
                // bits this way anyway because a user-defined unsigned type might
                (REMAINING_BITS/2u + count_trailing_zeros_recurser<T, REMAINING_BITS - REMAINING_BITS/2u>(val >> REMAINING_BITS/2u))
                :
                count_trailing_zeros_recurser<T, REMAINING_BITS - REMAINING_BITS/2u>(val)
            )
        ;
    }
}
template<typename T, typename std::enable_if<
    // use c++20 for fundamental types if it exists
    #if __cplusplus >= 202002L || defined __cpp_lib_bitops
    !std::is_fundamental<T>::value &&
    #endif
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type = false>
constexpr unsigned int count_trailing_zeros(T val)
{
    // don't binary search if the least bit is 1
    return static_cast<T>((T{1u} & val) != 0) ?
    0u
    :
    cues::detail::count_trailing_zeros_recurser<T, std::numeric_limits<T>::digits>(val);
}
#endif


//! Returns the minimum number of bits to represent an unsigned integer value.
#if __cplusplus >= 202002L || defined __cpp_lib_int_pow2
template<typename T, typename std::enable_if<
    std::is_fundamental<T>::value &&
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type=true>
constexpr T minimum_bits(T val)
{
    return std::bit_width(val);
}
#endif
template<typename T, typename std::enable_if<
#if __cplusplus >= 202002L || defined __cpp_lib_int_pow2
    !std::is_fundamental<T>::value &&
#endif
    std::numeric_limits<T>::is_integer &&
    !std::numeric_limits<T>::is_signed &&
    !std::is_same<bool, T>::value
, bool>::type=false>
constexpr T minimum_bits(T val)
{
    return (val == 0) ? static_cast<T>(0) : static_cast<T>(log2<T>(val) + 1);
}


//! unlike std::log10, this is a constexpr function,
//! however, it is only defined for unsigned integers.
//! PRECONDITION: zero input will give the maximum negative
//! value of the signed type corresponding to T, which is
//! obviously not negative infinity.  Either check for 0 before
//! passing, or assume any returned negative value means -infinity
//! NOTE: rounds down.
// alternate implementations could calculate log2 and then
// divide by log2(10) (or rather, multiply and divide by
// a ratio which approximates log2(10),
// which may be faster at runtime if log2 is optimized
// to use shifts instead of division.
// But log2 already incurs a loss of information in the integer rounding
// of its result, and multiplying this loss out by the factors can cause
// incorrect results; this can be addressed for example by using additional
// memory for a lookup table.
// This differs from the comparison method presented at
// https://graphics.stanford.edu/~seander/bithacks.html#IntegerLog10
// in that:
// 1) it works for types larger than 32 bits (values larger than 10^9), including custom types
//    that specialize std::numeric_limits<> and provide arithmetic operators
// 2) it uses division and recursion for large values to reduce comparisons for
//    types
// 3) it is templated on type and uses if constexpr to conditionally remove
//    checks that are outside the limits of the type; if you pass a uint8_t,
//    you will get worst case 3 comparisons instead of 9.

// better with constexpr if, but still satisfies constexpr function requirements after c++14
// if that were all, we could also check __cpp_lib_cosntexpr, but we also need
// digit separators from c++14
#if __cplusplus >= 201402L
template<typename T, typename std::enable_if<
    std::numeric_limits<T>::is_integer
    && !std::numeric_limits<T>::is_signed
    && !std::is_same<bool, T>::value
, bool>::type = true>
constexpr int log10(T val)
{
    using return_type = int;

    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() >= 1'000'000'000'000'000'000ull)
    {
        if (val >= 1'000'000'000'000'000'000ull)
        {
            return static_cast<return_type>(18 + log10<T>(val / 1'000'000'000'000'000'000ull));
        }
    }
    // gcc complains about missing single quote under c++11 even when the preprocessor
    // should have eliminated all this code
    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() >= 1'000'000'000ull) // '
    {
        if (val >= 1'000'000'000ull) // '
        {
            return static_cast<return_type>(9 + log10<T>(val / 1'000'000'000ull)); // '
        }
    }

    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() >= 100'000'000ull)
    {
        if (val >= 100'000'000ull)
        {
            return 8;
        }
    }

    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() >= 10'000'000ull)
    {
        if (val >= 10'000'000ull)
        {
            return 7;
        }
    }

    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() >= 1'000'000ul)
    {
        if (val >= 1'000'000ull)
        {
            return 6;
        }
    }

    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() >= 100'000ul) // '
    {
        if (val >= 100'000ull) // '
        {
            return 5;
        }
    }

    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() >= 10'000u) // '
    {
        if (val >= 10'000u) // '
        {
            return 4;
        }
    }

    if CUES_CONSTEXPR_OR_RUNTIME_IF ((std::numeric_limits<T>::max)() >= 1000u)
    {
        if (val >= 1000u)
        {
            return 3;
        }
    }

    return (val >= 100u) ?
        2
        :
        ( (val >= 10) ?
           1
           :
            ( (val >= 1) ?
            0
            :
            (std::numeric_limits<return_type>::min)()
            )
        )
    ;

}
#else

template<typename T, typename std::enable_if<
    std::numeric_limits<T>::is_integer
    && !std::numeric_limits<T>::is_signed
    && !std::is_same<bool, T>::value
, bool>::type = true>
constexpr int log10(T val)
{
    return (val >= 1000000000000000000ull) ?
        (18 + log10<T>(val / 1000000000000000000ull))
        :
        (val >= 1000000000ull) ?
        (9 + log10<T>(val / 1000000000ull))
        :
        (val >= 100000000ul) ?
        8
        :
        (val >= 10000000ul) ?
        7
        :
        (val >= 1000000ul) ?
        6
        :
        (val >= 100000ul) ?
        5
        :
        (val >= 10000u) ?
        4
        :
        (val >= 1000u) ?
        3
        :
        (val >= 100) ?
        2
        :
        (val >= 10) ?
        1
        :
        0
    ;
}

#endif

//! unlike std::abs():
//! this version is constexpr
//! this version will promote to an unsigned type based on the reported number of digits
//! in std::numeric_limits<>, and thus for fundamental integer types, should never
//! result in undefined behavior, as each signed fundamental type has a corresponding
//! unsigned type that can represent all of its positive values and more, with room
//! also for the -min that would not fit into the 2's-complement signed type.
//! Should also work for custom types that specialize std::numeric_limits<> and have
//! digits <= std::uintmax_t
//! also can be called with unsigned types
template<typename T, typename std::enable_if<std::numeric_limits<T>::is_integer, bool>::type = true >
constexpr typename cues::make_unsigned<T>::type abs(T v)
{
    using unsigned_type = typename cues::make_unsigned<T>::type;
    return (v < 0 ) ?
        // not -1*negative, nor 0 - negative, because that can overflow for 2's complement min,
        // and signed overflow is undefined behavior.
        // Instead, use the well-defined rules for signed->unsigned conversion, which
        // result in the unsigned value modulo 2^N, which, for negative values,
        // is max + 1 + signed negative, or max + 1 - abs(signed)
        // recover abs(signed) as (max - (max + 1 - abs(signed))) + 1
        // = max - max - 1 + abs(signed) + 1
        // = abs(signed),but grouped to avoid overflow
        ((std::numeric_limits<unsigned_type>::max)() - static_cast<unsigned_type>(v)) + 1
        :
        static_cast<unsigned_type>(v)
    ;
}

#if __cplusplus >= 201402L || defined ( __cpp_lib_constexpr_algorithms )
template< class T >
constexpr T max(T const & lhs, T const & rhs)
{
    return (std::max)(lhs,rhs);
}
#else
template< class T >
constexpr T max(T const & lhs, T const & rhs)
{
    return (lhs < rhs) ? rhs : lhs;
}
#endif

enum class number_base : std::uint_least8_t
{
    binary      = 1,
    octal       = 3,
    hexadecimal = 4,
    decimal     = 0
};

//! minimum_display_digits is the minimum number of numeric digits required to display
//! an integer value.  For the value of std::numeric_limits<T>::max(), this would be
//! numeric_limits<T>::digits in binary or numeric_limits<T>::digits10+1 in decimal,
//! but these functions work for values other than the type limits, and include support
//! for octal and hexadecimal.
//! minimum_display_digits only covers the digit portion;
//! it includes neither base prefix nor sign.

template<cues::number_base num_base>
struct minimum_display_digits
{
    template<typename T>
    static constexpr unsigned int calculate(T val)
    {
        // log2 returns negative value for 0 input, but
        // 0 is a single digit in any number system
        // octal and hex can divide log2(value) by log2(base) to get the answer
        return (val == 0) ? 1 : cues::log2(cues::abs(val))/static_cast<T>(num_base) + 1;
    }

    template<typename T>
    constexpr unsigned int operator()( T val) const
    {
        return calculate(val);
    }
};

// binary does not need to divide and can wrap existing function except at 0
template<>
struct minimum_display_digits<number_base::binary>
{
    template<typename T>
    static constexpr unsigned int calculate(T val)
    {
        // minimum bits returns 0 for 0, but 0 takes up a display character
        // for negative values, take the absolute value of the type into
        // the corresponding unsigned type and use that (minimum_bits
        // is only defined for unsigned types)
        return (val == 0) ? 1 : minimum_bits(cues::abs(val));
    }

    template<typename T>
    constexpr unsigned int operator()( T val) const
    {
        return calculate(val);
    }
};

// decimal relies on log10 instead of log2
template<>
struct minimum_display_digits<number_base::decimal>
{
    template<typename T>
    static constexpr unsigned int calculate(T val)
    {
        // log10(10) for example would say 1 digit but that takes two
        // similarly for log10(99)
        // log10(100) is 2 but takes 3
        // "0" is still displayed as one digit
        return (val == 0) ? 1 : cues::log10(cues::abs(val)) + 1;
    }

    template<typename T>
    constexpr unsigned int operator()( T val) const
    {
        return calculate(val);
    }

};


//! default argument makes a loose guess at whether to use modulo
//! (better if algorithm or hardware can do division and modulo as
//! one operation), but you can specify either if profiling indicates the
//! guess is wrong.
template<typename T, bool use_modulo = (sizeof(int) > 2), class Enable=void>
struct div_roundup;


//! warning: will correctly handle numerator == 0,
//! but will pass denominator == 0 to operator/,
//! resulting in undefined behavior.
//! Version for CPUs expected to either have hardware division
//! or a software algorithm that can provide quotient and remainder
//! simultaneously.  There is no way to test that functionality
//! for certain, but an integer > 2 bytes is likely to include
//! higher-end chips that put more resources into CPU instructions,
//! math libraries, and/or compiler development
template<typename T>
struct div_roundup<T, true, typename std::enable_if<
   std::numeric_limits<T>::is_integer
&& !std::numeric_limits<T>::is_signed
>::type>
{
    constexpr T operator()(T const & num, T const & denom) const
    {
        return calculate(num, denom);
    }

    static constexpr T calculate(T const & num, T const & denom)
    {
        // on CPUs with sizeof(int) > 2, most likely the division
        // algorithm or hardware will return both division and modulus,
        // and the compiler can optimize the two into a single division call,
        // then compare modulus to zero and add.
        return num / denom + static_cast<T>(num % denom != 0);
    }
};

//! alternate version for sizeof(int) < 2,
//! where the division is probably not done in hardware
//! and the inexpensive chips may not have as much effort
//! put into CPU, library algorithms, or compiler.
//! uses one division with addition and subtraction
//! instead of division and modulo
template<typename T>
struct div_roundup<T, false,
typename std::enable_if<
   std::numeric_limits<T>::is_integer
&& !std::numeric_limits<T>::is_signed
>::type>
{
    constexpr T operator()(T const & num, T const & denom) const
    {
        return calculate(num, denom);
    }

    static constexpr T calculate(T const & num, T const & denom)
    {
        // note that by definition the division
        // numerator / denominator gives us a quotient and remainder,
        // where quotient*denominator + remainder = numerator
        // (assuming denominator != 0)
        // or, using quotient "q" and remainder "r":
        // q*denom +r == num
        // q*denom == num - r
        // q == (num - r)/denom
        // or, for rounding up:
        // q + 1 == (num - r)/denom + 1
        // now, we only want q+1 if r != 0, otherwise we want q
        // so, when r == 0, we want:
        // q == (num - 0)/denom == num/denom
        // note that because integer division truncates, the quotient
        // is reduced when the numerator drops below a multiple of the
        // quotient, and if r == 0, the numerator was an exact
        // multiple of the quotient, so when r == 0,
        // q - 1 == (num - 1)/denom
        // q == (num - 1)/denom + 1
        // When r != 0, we want to find q + 1, so:
        // q + 1 == (num - r)/denom + 1, r within [1,denom)
        // the maximum value, subtracting the smallest amount in the range
        // q + 1 <= (num -1)/denom + 1
        // we could further check the minimum value, by substituting
        // (denom -1) for r, but we know that the point of truncation
        // is exactly r away, so subtracting anything less than r will
        // not change the truncated result.
        // so, whether we have r == 0 or r!= 0, the answer
        // can be found using (num - 1)/denom + 1

        // with one extra zero-check, this avoids the overflow
        // that could be possible with
        // [numerator + (denominator-1)]/denominator
        // which is another way to intuitively look at
        // the truncation of integer division allowing adding
        // any amount that won't change the quotient

        return (num == 0) ? //< don't subtract from unsigned 0
            0
            :
            (num - 1u)/denom + 1u
        ;


    }

};

template<typename T, typename U>
constexpr typename std::conditional<std::numeric_limits<T>::digits < std::numeric_limits<U>::digits, U, T>::type
divide_rounding_up(T const & lhs, U const & rhs)
{
    using larger_type = typename std::conditional<std::numeric_limits<T>::digits < std::numeric_limits<U>::digits, U, T>::type;
    return div_roundup<larger_type>::calculate(lhs, rhs);
}

//! like std::isnan, but should work for custom types that expose
//! NaN functionality via std::numeric_limits and having operator== return false
//! for NaN

template<typename T>
constexpr typename std::enable_if<
    std::numeric_limits<T>::has_quiet_NaN
    || std::numeric_limits<T>::has_signaling_NaN,
bool>::type is_nan(T const & v) noexcept
{
    static_assert(std::numeric_limits<T>::is_specialized, "cues::is_nan<T> requires specialized std::numeric_limits for T");
    return
    // although std::isnan is recommended for floating types,
    // it is generally not legal to put things in namespace std
    // other than std::numeric_limits specializations;
    // whereas operator== can be
    // overloaded to support NaN for custom types in the relevant namespace
    !(v == v);
}

template<typename T>
constexpr typename std::enable_if<
    !std::numeric_limits<T>::has_quiet_NaN
    && !std::numeric_limits<T>::has_signaling_NaN,
bool>::type is_nan(T const & ) noexcept
{
    return false;
}

template<typename T>
constexpr typename std::enable_if<
    std::numeric_limits<T>::has_infinity
    && std::numeric_limits<T>::is_signed,
bool>::type is_inf(T const & v) noexcept
{
    static_assert(std::numeric_limits<T>::is_specialized, "cues::is_inf<T> requires specialized std::numeric_limits for T");
    return
        (v ==  std::numeric_limits<T>::infinity())
        ||
        (v == -std::numeric_limits<T>::infinity())
    ;
}

template<typename T>
constexpr typename std::enable_if<
    std::numeric_limits<T>::has_infinity
    && !std::numeric_limits<T>::is_signed,
bool>::type is_inf(T const & v) noexcept
{
    static_assert(std::numeric_limits<T>::is_specialized, "cues::is_inf<T> requires specialized std::numeric_limits for T");
    return (v ==  std::numeric_limits<T>::infinity());
}

template<typename T>
constexpr typename std::enable_if<
    !std::numeric_limits<T>::has_infinity,
bool>::type is_inf(T const & ) noexcept
{
    return false;
}


} // namespace cues

#endif // CUES_CONSTEXPR_MATH_HPP
