#include "homogenous_pool_test.hpp"

#include "cues/version_helpers.hpp"

#if (__cplusplus < 201703L && !defined ( __cpp_lib_launder) )
    #pragma message "NOTE: skipping test of cues::homogenous_pool because it requires std::launder, which is not available."
    namespace cues
    {
    namespace test
    {
         std::size_t check_homogenous_pool_test_runtime_assertions() {return 0;}
    }
    }
#else


#include <cstdint>
#include <utility>
#include "cues/homogenous_pool.hpp"
#include <cstdint>
#include "helper_types.hpp"

namespace cues
{
namespace test
{

    template<typename POOL_TYPE, typename OBJECT_TYPE = typename POOL_TYPE::value_type>
    constexpr std::size_t block_size_to_object_count(std::size_t block_count)
    {
        return block_count * POOL_TYPE::block_size / sizeof(OBJECT_TYPE);
    }

    template<typename POOL_TYPE, typename OBJECT_TYPE = typename POOL_TYPE::value_type>
    constexpr std::size_t object_count_to_block_size(std::size_t object_count)
    {
        return cues::divide_rounding_up(object_count * cues::max(alignof(OBJECT_TYPE),sizeof(OBJECT_TYPE)), POOL_TYPE::block_size);
    }


    template<typename NATIVE_TYPE, std::size_t POOL_SIZE, typename FOREIGN_TYPE, typename SIZE_TYPE>
    std::size_t hpt_common_runtime_assertions(std::size_t volatile & running_violations)
    {
        using pool_type = cues::homogenous_pool<NATIVE_TYPE,POOL_SIZE,SIZE_TYPE>;
        std::size_t volatile net_allocated_blocks = 0;
        pool_type pool_under_test{};

        running_violations += pool_under_test.max_blocks() != POOL_SIZE;
        // verify max_size and block size matches
        running_violations += pool_under_test.max_size() != block_size_to_object_count<pool_type>(pool_under_test.max_blocks());


        using native_allocator_type = typename pool_type::allocator_type;

        native_allocator_type native_allocator{pool_under_test};

        // the allocator should have the same max_size as the pool
        running_violations += native_allocator.max_size() != pool_under_test.max_size();
        // creating the allocator should not have used any blocks
        running_violations += pool_under_test.remaining_blocks() != POOL_SIZE;

        typename native_allocator_type::pointer allocated_pointers[8] = {nullptr};
        // allocate first number of Ts through the allocator
        static constexpr std::size_t const first_allocation_object_count = 1;
        static constexpr std::size_t const first_allocation_block_size =
            object_count_to_block_size<pool_type>(first_allocation_object_count)
        ;
        // confirm one T can be allocated
        try{
            allocated_pointers[0] = native_allocator.allocate(first_allocation_object_count);
            net_allocated_blocks += first_allocation_block_size;
        }
        catch(...) // this should always succeed, any error is a violation
        {
            ++running_violations;
        }
        // verify pool has reduced capacity
        running_violations += pool_under_test.remaining_blocks() != POOL_SIZE - net_allocated_blocks;
        // verify pool still has same largest possible size
        running_violations += pool_under_test.max_blocks() != POOL_SIZE;


        // allocate second number of Ts through the allocator, if there is room
        static constexpr std::size_t const second_allocation_block_size = 4;
        static constexpr std::size_t const second_allocation_object_count =
            block_size_to_object_count<pool_type>(second_allocation_block_size)
        ;

            try{
                // use a copy to allocate, copies of the allocator should be able to
                // manage the same storage
                native_allocator_type copied_allocator{native_allocator};

                allocated_pointers[1] = copied_allocator.allocate(second_allocation_object_count);
                net_allocated_blocks += second_allocation_block_size;
                // if there was not room, allocation should have failed
                if constexpr (POOL_SIZE < second_allocation_block_size + first_allocation_block_size)
                {
                    ++running_violations;
                }
                running_violations += pool_under_test.remaining_blocks() != POOL_SIZE - net_allocated_blocks;

            }
            catch(std::bad_alloc const &)
            {
                // this should have succeeded if there was room
                if constexpr (POOL_SIZE >= second_allocation_block_size + first_allocation_block_size)
                {
                    ++running_violations;
                }
            }
            catch(...)
            {
                ++running_violations;
            }

        // deallocate oldest
        {
            // test deallocating from a copy
            native_allocator_type copied_allocator{native_allocator};
            copied_allocator.deallocate(allocated_pointers[0], first_allocation_object_count);
        }
        allocated_pointers[0] = nullptr;
        net_allocated_blocks -= first_allocation_block_size;
        // verify increased capacity
        running_violations += pool_under_test.remaining_blocks() != POOL_SIZE - net_allocated_blocks;

        // anonymous scope to drop the two size_t
        {
        std::size_t const oversized_allocation_blocks = POOL_SIZE + 1 - net_allocated_blocks;
        std::size_t const oversized_allocation_object_count =
            block_size_to_object_count<pool_type>(oversized_allocation_blocks)
        ;
        // verify that allocating too large a block fails with std::bad_alloc
        try {
            // one more than the amount of space still available has to fail regardless of fragmentation
            allocated_pointers[2] = native_allocator.allocate(oversized_allocation_object_count);
            // the above line should throw an exception, but if it doesn't, deallocate
            // so the rest of the test can proceed
             native_allocator.deallocate(allocated_pointers[2], oversized_allocation_object_count);
            // should have thrown
            ++running_violations;
        }
        catch(std::bad_alloc const &)
        {
            // this is what is supposed to happen
            // verify that a failed allocation did not change the capacity
            running_violations += pool_under_test.remaining_blocks() != POOL_SIZE - net_allocated_blocks;
        }
        catch(...)
        {
            // wrong exception
            ++running_violations;
        }
        }

        // attempt to allocate the largest remaining contiguous chunk
        // the first allocation might not be large enough, so subtract
        // it from the free space, but allocate that much
        static constexpr std::size_t const third_allocation_block_size =
        // greater here instead of greater or equal because allocators are required to return
        // unique addresses even for zero byte allocations, which means zero has to "reserve"
        // at least one.
            (POOL_SIZE > second_allocation_block_size + first_allocation_block_size) ?
               (pool_under_test.max_blocks() - (first_allocation_block_size + second_allocation_block_size))
               :
               1
        ;

        static constexpr std::size_t const third_allocation_object_count =
            (POOL_SIZE > second_allocation_block_size + first_allocation_block_size) ?
            block_size_to_object_count<pool_type>(third_allocation_block_size)
            :
            0
        ;

        try {
            allocated_pointers[2] = native_allocator.allocate(third_allocation_object_count);
            net_allocated_blocks += third_allocation_block_size;

            running_violations += pool_under_test.remaining_blocks() !=
            ((third_allocation_object_count == 0) ?
             0
             :
             first_allocation_block_size
            );
        }
        catch(std::bad_alloc const &)
        {
            if constexpr (POOL_SIZE > second_allocation_block_size + first_allocation_block_size)
            {
                ++running_violations;
            }
        }
        catch(...)
        {
            ++running_violations;
        }

        // reallocate the last remaining (and also first) chunk
        try {
            allocated_pointers[0] = native_allocator.allocate(first_allocation_object_count);
            net_allocated_blocks += first_allocation_block_size;
            running_violations += pool_under_test.remaining_blocks() != 0;
            running_violations += pool_under_test.max_blocks() != net_allocated_blocks;
        }
        // if the pool size was == 1, the previous step allocated
        // the last one chunk, and this one is permitted to fail
        catch(std::bad_alloc const &)
        {
            if constexpr (third_allocation_object_count != 0)
            {
                ++running_violations;
            }
        }
        catch(...)
        {
            ++running_violations;
        }

        // pool should now be exactly fully allocated, verify that allocating only one object fails now
        // (not testing zero)

        try {
            allocated_pointers[3] = native_allocator.allocate(1);
            native_allocator.deallocate(allocated_pointers[3], 1);
            allocated_pointers[3] = nullptr;
            ++running_violations ;
        }
        catch(std::bad_alloc const &)
        {
            // verify that a failed allocation did not change the capacity
            running_violations += pool_under_test.remaining_blocks() != POOL_SIZE - net_allocated_blocks;
        }
        catch(...)
        {
            ++running_violations ;
        }

        // allocations in chronological order:    maybe [1], [2], [0] ( all succeeded except the one known to be too large)
        //                                     or maybe [1], [2] for small pool sizes (1 succeeded, 2 allocated what was 0, 0 failed)
        //                                     or maybe [2] for very small pool sizes (1 failed, 2 allocated what was 0, 0 failed)
        // deallocate [2] if it exists
        assert(nullptr != allocated_pointers[2]);

        native_allocator.deallocate(allocated_pointers[2], third_allocation_object_count);
        allocated_pointers[2] = nullptr;
        net_allocated_blocks -= third_allocation_block_size;

        // leave some in if we can, but make room if the pool is small
        if constexpr ((POOL_SIZE - 5u) * pool_type::block_size <= 3*sizeof(FOREIGN_TYPE))
        {
            if (nullptr != allocated_pointers[1])
            {
                native_allocator.deallocate(allocated_pointers[1],second_allocation_object_count);
                allocated_pointers[1] = nullptr;
                net_allocated_blocks -= second_allocation_block_size;
            }
            if (nullptr != allocated_pointers[0])
            {
                native_allocator.deallocate(allocated_pointers[0],first_allocation_object_count);
                allocated_pointers[0] = nullptr;
                net_allocated_blocks -= first_allocation_block_size;
            }

            pool_under_test.sort_free_list();
        }

        running_violations += pool_under_test.remaining_blocks() != POOL_SIZE - net_allocated_blocks;

        // rebinding test
        {
            using foreign_allocator_type = typename native_allocator_type::template rebind<FOREIGN_TYPE>::other;
            using foreign_pointer_type   = typename foreign_allocator_type::pointer;
            foreign_pointer_type foreign_ptr_1 = nullptr;
            foreign_pointer_type foreign_ptr_2 = nullptr;

            foreign_allocator_type foreign_allocator(native_allocator);

            // verify that simply creating another allocator has not changed the capacity
            running_violations += pool_under_test.remaining_blocks() != POOL_SIZE - net_allocated_blocks;

            static constexpr std::size_t const first_foreign_allocation_object_count = 1;
            static constexpr std::size_t const first_foreign_allocation_block_count =
                object_count_to_block_size<typename foreign_allocator_type::pool_type, typename foreign_allocator_type::value_type>(
                    first_foreign_allocation_object_count
            );

            try
            {
                // allocate
                foreign_ptr_1 = foreign_allocator.allocate(first_foreign_allocation_object_count);
                net_allocated_blocks += first_foreign_allocation_block_count;

                if constexpr (
                    // do we know that there isn't enough memory?
                    (POOL_SIZE*pool_type::block_size <
                        cues::max(
                            alignof(typename foreign_allocator_type::value_type),
                            sizeof(typename foreign_allocator_type::value_type)
                        )
                        *first_foreign_allocation_object_count
                    )
                )
                {
                    ++running_violations;
                }

            }
            catch(std::bad_alloc const &)
            {
                if constexpr(
                    (POOL_SIZE)*pool_type::block_size >
                    first_foreign_allocation_object_count*cues::max(alignof(FOREIGN_TYPE),sizeof(FOREIGN_TYPE))
                    // also needs to be properly aligned
                    + (
                        (alignof(FOREIGN_TYPE) > alignof(typename pool_type::value_type)) ?
                        (alignof(FOREIGN_TYPE) - alignof(typename pool_type::value_type))
                        :
                        0
                    )
                )
                {
                    ++running_violations;
                }
                else
                {
                    ;
                }
            }
            catch(...)
            {
                ++running_violations;
            }

            // verify that capacity has either decreased appropriately or remained the same
            running_violations += pool_under_test.remaining_blocks() != (
                  POOL_SIZE
                - net_allocated_blocks
            );

            static constexpr std::size_t const second_foreign_allocation_object_count = 2;
            static constexpr std::size_t const second_foreign_allocation_block_count =
                object_count_to_block_size<typename foreign_allocator_type::pool_type, typename foreign_allocator_type::value_type>(
                    second_foreign_allocation_object_count
            );

            try
            {
                // allocate contiguous block of larger type
                foreign_ptr_2 = foreign_allocator.allocate(second_foreign_allocation_object_count);
                net_allocated_blocks += second_foreign_allocation_block_count;
                // can we prove it should have failed?
                if constexpr (
                    // do we know that we shouldn't have had enough blocks?
                    (POOL_SIZE < 2)
                    // do we know that there isn't enough memory?
                    || (POOL_SIZE*pool_type::block_size <
                        cues::max(
                            alignof(typename foreign_allocator_type::value_type),
                            sizeof(typename foreign_allocator_type::value_type)
                        )
                        *(second_foreign_allocation_object_count + first_foreign_allocation_object_count)
                    )
                )
                {
                    ++running_violations;
                }
            }
            catch(std::bad_alloc const &)
            {
                // if we know that should have had enough blocks
                if constexpr(
                    POOL_SIZE >= second_foreign_allocation_block_count + first_foreign_allocation_block_count
                )
                {
                    ++running_violations;
                }
            }
            catch(...)
            {
                ++running_violations;
            }

            running_violations += pool_under_test.remaining_blocks() != (
                  POOL_SIZE
                - net_allocated_blocks
            );

            if (nullptr != foreign_ptr_1)
            {
                foreign_allocator.deallocate(foreign_ptr_1, first_foreign_allocation_object_count);
                net_allocated_blocks -= first_foreign_allocation_block_count;

                running_violations += pool_under_test.remaining_blocks() != (POOL_SIZE - net_allocated_blocks);
            }

            if (nullptr != foreign_ptr_2)
            {
                foreign_allocator.deallocate(foreign_ptr_2, second_foreign_allocation_object_count);
                net_allocated_blocks -= second_foreign_allocation_block_count;

                running_violations += pool_under_test.remaining_blocks() != (POOL_SIZE - net_allocated_blocks);
            }

        }

        return running_violations;
    }

    struct double_scoped_only
    {
        scoped_only first;
        scoped_only second;
    };

    static_assert(sizeof(double_scoped_only) == 2*sizeof(scoped_only), "unexpected size error");


    template<std::size_t N>
    std::size_t check_common_homogenous_pool_assertions()
    {
        std::size_t volatile running_assertions = 0;
        // easiest case:
        // test with
        // sizeof(foreign_type) >  sizeof(size_t) == sizeof(value_type)
        // alignof foreign_type == alignof value_type ==m alignof size_type
        // sizeof uint_least8 should always be 1, scoped is empty and thus should also always have size 1 (to enforce unique address)
        // sizeof double must then be two.
        hpt_common_runtime_assertions<
            cues::test::scoped_only,
            N,
            double_scoped_only,
            std::uint_least8_t
        >(running_assertions);

        // test with
        //                         sizeof(size_t) >= sizeof(value_type)
        // alignof(foreign_type) == alignof(value_type) <= alignof(size_type)
        // scoped_only is size == 1, which means double_scoped_only has size of 2
        // unsigned int could be any size >= 1, depending on how many bits are in a char
        // so sizeof(size_t) could be <, ==, or > than sizeof (foreign_type)
        hpt_common_runtime_assertions<
            cues::test::scoped_only,
            N,
            double_scoped_only,
            unsigned int
        >(running_assertions);

        // test with
        // probably sizeof(foreign_type) <  sizeof(size_t) >  sizeof(value_type)
        // could be == and ==, respectively, if the smallest addressable type is 64 or more bits wide,
        // or == and >, respectively, if the smallest addressable type is 32 or more bits wide
        // probalby alignof(value_type) == alignof(foreign_type ) < alignof(size_type)
        // could be == alignof(size_type) if char and uint_least64_t have the same alignment
        hpt_common_runtime_assertions<
            cues::test::scoped_only,
            N,
            double_scoped_only,
            std::uint_least64_t
        >(running_assertions);

        // test with
        // sizeof(foreign_type) >  sizeof(size_t) <= sizeof(value_type)
        // alignof(foreign_type) == alignof(value_type) >= alignof(size_type)
        hpt_common_runtime_assertions<
            std::uint_least16_t,
            N,
            std::pair<std::uint_least16_t, std::uint_least16_t>,
            std::uint_least8_t
        >(running_assertions);

        // test with
        // (probably) sizeof(foreign_type) <  sizeof(size_t) >= sizeof(value_type)
        // could also be foreign_type >= sizeof(size_type) since least16 could be 32 or 64 bits.
        // (probably) alignof(value_type) < alignof(size_type),
        // though they could have equal alignments
        hpt_common_runtime_assertions<
            std::uint_least16_t,
            N,
            std::pair<std::uint_least16_t, std::uint_least16_t>,
            std::uint_least64_t
        >(running_assertions);

        // test with
        // sizeof(foreign_type) >  sizeof(size_t) < sizeof(value_type)
        // (probably) alignof(value_type) == alignof(size_type) < alignof(foreign_type)
        hpt_common_runtime_assertions<
            std::pair<std::uint_least8_t,std::uint_least8_t>,
            N,
            std::pair<std::pair<std::uint_least8_t,std::uint_least8_t>, std::uint_least64_t>,
            std::uint_least8_t
        >(running_assertions);

        // test with (probably) sizeof(value_type) > sizeof(size_type)
        // (probalby) alignof(value_type) == alignof(foreign_type) > alignof(size_type)
        hpt_common_runtime_assertions<
            std::int_least64_t,
            N,
            std::pair<std::uint_least32_t, std::int_least64_t>,
            std::uint_least8_t
        >(running_assertions);

        // test with
        // (probably) sizeof(foreign_type) >  sizeof(size_t) > sizeof(value_type)
        // least8 is permitted to be 64 bits but is not usually seen in implementations
        // (probably) alignof(value_type) < alignof (size_type) == alignof(foreign_type)
        hpt_common_runtime_assertions<
            std::pair<std::uint_least8_t,std::uint_least8_t>,
            N,
            std::pair<std::pair<std::uint_least8_t,std::uint_least8_t>, std::uint_least64_t>,
            std::uint_least64_t
        >(running_assertions);

        // test with
        // sizeof(size_type) == sizeof(value_type) == sizeof(foreign_type)
        // alignof(size_type) < alignof(value_type) < alignof(foreign_type)
        hpt_common_runtime_assertions<
            empty_aligned_to_8,
            N,
            empty_aligned_to_16,
            std::uint_least8_t
        >(running_assertions);

        // test with
        // sizeof(size_type) == sizeof(value_type) == sizeof(foreign_type)
        // alignof(size_type) < alignof(foreign_type) < alignof(value_type)
        hpt_common_runtime_assertions<
            empty_aligned_to_16,
            N,
            empty_aligned_to_8,
            std::uint_least8_t
        >(running_assertions);



        return running_assertions;
    }


    std::size_t check_homogenous_pool_test_runtime_assertions()
    {
        // test even, power of 2-sized pool, most likely to succeed
        static constexpr std::size_t const pool_test_size_a = 16u;
        // test odd-sized pool
        static constexpr std::size_t const pool_test_size_b = 13u;
        // test even, not power of two
        static constexpr std::size_t const pool_test_size_c = 22u;
        //
        std::size_t volatile running_assertions = 0;

        // test power of two-sized pool
        running_assertions += check_common_homogenous_pool_assertions<pool_test_size_a>();

        // test even, not power-of-two pool
        running_assertions += check_common_homogenous_pool_assertions<pool_test_size_c>();

        // test odd size pool
        running_assertions += check_common_homogenous_pool_assertions<pool_test_size_b>();

        // test with pool size of 1, likely to have weird edge cases
        running_assertions += check_common_homogenous_pool_assertions<1>();

        return running_assertions;
    }

}
}
#endif
