#ifndef CUES_CHRONO_HPP_INCLUDED
#define CUES_CHRONO_HPP_INCLUDED

#include <chrono>
#include <cstdint>

namespace cues
{
namespace chrono
{

#if __cplusplus <= 201703L
    using years     = std::chrono::duration< std::int_fast32_t, std::ratio<31556952>>;
    using months    = std::chrono::duration< std::int_fast32_t, std::ratio<2629746>>;
    using weeks     = std::chrono::duration< std::int_fast32_t, std::ratio<604800>>;
    using days      = std::chrono::duration< std::int_fast32_t, std::ratio<86400>>;

#else
    using years     = std::chrono::years;
    using months    = std::chrono::months;
    using weeks     = std::chrono::weeks;
    using days      = std::chrono::days;

#endif

}
}
#endif // CUES_CHRONO_HPP_INCLUDED
