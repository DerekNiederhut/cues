#ifndef CUES_TRIPLE_BUFFER_HPP
#define CUES_TRIPLE_BUFFER_HPP

//! single-producer, single-consumer triple buffer.
//! Provides consumer with the most recently published data
//! (that is, data may be discarded if not consumed fast enough).
//! Producer can always publish and is never blocked,
//! but may overwrite old data.
//! Consumer can always read data that has been published
//! and not consumed, but may need to wait on producer
//! if there is no published data.

//! uses optional type along with atomic access on the intermediate
//! index to indicate published status.  Using an RAII-like class
//! rather than a raw byte array with placement new allows this
//! class to be significantly simpler.

//! cues::optional is used instead of std::optional because cues::optional
//! can contain native arrays (as opposed to needing to put them in std::array).

//! This class does not inherently need exceptions if used carefully;
//! cues::optional will throw if the preconditions are not respected,
//! and the constructor of the contained "T" class could throw, but
//! if the preconditions are respected and a T with non-throwing constructors
//! is used, the compiler could be instructed to disable exceptions.

#if (__cplusplus < 201703L && (!defined ( __cpp_lib_launder) || !defined(__cpp_lib_if_constexpr) || !defined(__cpp_lib_optional)))
    #error "cues::triple_buffer requires std::launder, std::in_place_t, and constexpr if."
#endif

#include <cstdint>
#include <cstddef>
#include <utility>
#include <atomic>
#include <cassert>
#include <type_traits>

#include "cues/optional.hpp"
#include "cues/access_token.hpp"

namespace cues {


template<typename T,
    std::size_t MEM_ALIGN = alignof(T),
    std::size_t INDEX_ALIGN = alignof(std::uint_least8_t)
>
class triple_buffer
{
    public:

        using value_type      = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
        using size_type       = std::uint_least8_t;
        using difference_type = std::int_least8_t;
        using reference       = typename cues::optional<value_type>::reference;
        using const_reference = typename cues::optional<value_type>::const_reference;


        using const_access_token = access_token_impl<triple_buffer, value_type, true>;
        using access_token       = access_token_impl<triple_buffer, value_type, false>;

        //! default constructor is not atomic and should be completed before
        //! asynchronous use.
        constexpr triple_buffer() noexcept
        : intermediate_idx{0}
        , read_idx{1}
        , write_idx{2}
        {}

        //! PRECONDITION:  called only from producer context
        //! POSTCONDITION: data at publishing index is constructed but not yet published.
        //! returns an access token to the object so it can be further modified before publishing
        //! if necessary.  Do not continue to use the token or the underlying pointer after calling
        //! commit or rollback.
        //! Do not move token across context boundaries.
        template<typename ... Args, typename std::enable_if<std::is_constructible<value_type, Args &&...>::value, bool >::type = true>
        access_token construct_data(Args && ... args) noexcept(std::is_nothrow_constructible<value_type, Args...>::value)
        {
            memory[write_idx].emplace(std::forward<Args>(args)...);
            return access_token(pointer_at_index(write_idx));
        }

        //! PRECONDITION:  same as construct_data(Args ...)
        //! POSTCONDITION: same as construct_data(Args ...)
        //! overload for initializer list
        template<typename U, typename ... Args,
            typename std::enable_if<
                (!std::is_constructible<value_type, U &&>::value || sizeof...(Args) > 0)
                && std::is_constructible<value_type, std::initializer_list<U>, Args...>::value, bool
            >::type = true>
        access_token construct_data(std::initializer_list<U> il, Args && ... args)
        noexcept(std::is_nothrow_constructible<value_type, std::initializer_list<U>, Args...>::value)
        {
            memory[write_idx].emplace(il, std::forward<Args>(args)...);
            return access_token(pointer_at_index(write_idx));
        }

        //! PRECONDITION:  called only from producer context,
        //!                pass only what was returned by the last call to construct_data
        //!                and do not use the token or the pointer after that.
        //! POSTCONDITION: the provided data is published and has lifetime until:
        //!                1) a call to consume() takes ownership of it
        //!                2) a subsequent call to produce() overwrites it
        //!                3) the destructor is called.
        void commit(access_token && token) noexcept
        {
            assert((token.data == pointer_at_index(write_idx)
                   && "publishing data that was not constructed"
            ));
            write_idx = std::atomic_exchange_explicit(&intermediate_idx, write_idx, std::memory_order_acq_rel);
        }

        //! PRECONDITION: same as commit
        //! POSTCONDITION: object is destructed  instead of published and its lifetime ends
        //! used to destruct data constructed for an access token
        void roll_back(access_token && token) noexcept
        {
            assert((token.data == pointer_at_index(write_idx)
                   && "rolling back data that was not constructed"
            ));
            memory[write_idx].reset();
        }

        //! PRECONDITION:  same as construct_data
        //! POSTCONDITION: same as commit
        //! convenience function to chain the two together immediately if you don't need
        //! to access the object between construction and publishing.
        template<typename... Args, typename std::enable_if<std::is_constructible<value_type, Args...>::value, bool >::type = true>
        void emplace_back(Args && ... args) noexcept(std::is_nothrow_constructible<value_type, Args...>::value)
        {
            commit(construct_data(std::forward<Args>(args)...));
        }

        template<typename U, typename ... Args,
            typename std::enable_if<
                // disable if overload taking Args && ... with sizeof==1 would be a better match,
                // so this overload will lose for rvalue references (where initializer list needs a pointer)
                // but could construct value_type without forming an initializer_list
                (!std::is_constructible<value_type, U &&>::value || sizeof...(Args) > 0)
                && std::is_constructible<value_type, std::initializer_list<U>, Args ...>::value
                , bool
            >::type = true
        >
        void emplace_back(std::initializer_list<U> il, Args && ... args) noexcept(std::is_nothrow_constructible<value_type, std::initializer_list<U>, Args...>::value)
        {
            commit(construct_data(il, std::forward<Args>(args)...));
        }

        //! PRECONDITION:  called only from consumer context
        //! POSTCONDITION: no data is modified.
        bool has_unconsumed_data() const noexcept
        {
            if (memory[read_idx].has_value())
            {
                return true;
            }
            // if there isn't any in the current read index, swap once
            // to see if there is some waiting.
            read_idx = std::atomic_exchange_explicit(&intermediate_idx, read_idx, std::memory_order_acq_rel);
            return memory[read_idx].has_value();

        }

        //! PRECONDITION: called only from consumer context.
        //! producer can always write but never reads, so it doesn't need
        //! to be aware of whether or not the buffer is "empty".
        //! NOTE: this also means that any data "owned" by the producer
        //! (stale data not yet overwritten, or written data not yet published)
        //! is not counted.
        //! POSTCONDITION: no data is modified.
        bool empty() const noexcept {return !has_unconsumed_data();}

        //! PRECONDITION: called only from consumer context, has_unconsumed_data() == true
        //! (or, equivalently, !empty)
        //! POSTCONDITION: if binding to another reference, do not continue to use after
        //! any call to mark_consumed or consume (a copy can be made instead)
        const_reference front() const noexcept(false) //< value() can throw
        {
            return memory[read_idx].value();
        }

        //! will work if has_unconsumed_data() == false, but will not be dereferenceable
        //! POSTCONDITION: do not use the token after any call to mark_consumed(),
        //! consume(), or even has_unconsumed_data() if the token does not have data
        //! (has_unconsumed_data can update the read index if there isn't data in it)
        const_access_token peek_front() const noexcept
        {
            return const_access_token(const_pointer_at_index(read_idx));
        }

        //! PRECONDITION:  called only from consumer context
        //! POSTCONDITION: data at this index is destructed if it exists,
        //! set as no longer published,
        //! ( has_unconsumed_data() could return true on the next call
        //! even if the producer thread has not run in the interim).
        void pop_front() noexcept(std::is_nothrow_destructible<value_type>::value)
        {
            memory[read_idx].reset();
        }

        //! PRECONDITION: as with pop_front()
        //! POSTCONDITION: as with pop_front()
        //! adds assert to pop_front()
        //! to verify that the data being consumed is the data that was read.
        void mark_consumed(const_access_token && token) noexcept(std::is_nothrow_destructible<value_type>::value)
        {
            assert((
                   (void)"marking incorrect data as consumed",
                   (token.data == const_pointer_at_index(read_idx))
            ));
            pop_front();
        }

        //! PRECONDITION: called only from consumer context AND if
        //!               ( has_unconsumed_data() is true, OR,
        //!                 equivalently, peek() returned non-nullptr)
        //! POSTCONDITION: as with calling pop_front().
        //! NOTE: if you don't need a copy of the data, using
        //!       peek() and pop_front() may be more efficient.


        void consume(value_type & into_here )
        noexcept(std::is_nothrow_move_assignable<value_type>::value)
        {
            assert((
                    (void)"violation of precondition in triple_buffer::consume(VT &)", has_unconsumed_data()
            ));
            // move-assign
            into_here = std::move(*(memory[read_idx]));
            // consume it
            pop_front();
            // return
            return;
        }


        //! overload of consume that constructs a temporary for you
        //! and moves it to the return value
        template<typename U = value_type,
            class Enable = typename std::enable_if<
                   !std::is_array<U>::value
                && std::is_move_constructible<U>::value
            >::type
        >
        U consume()  noexcept(std::is_nothrow_move_assignable<value_type>::value
                                          && std::is_nothrow_move_constructible<value_type>::value)
        {
            assert((
                    (void)"violation of precondition in triple_buffer::consume()", has_unconsumed_data()
            ));
            // not simply calling consume(value_type {}) here because that would require
            // a public default constructor
            U temp{std::move(*(memory[read_idx]))};
            pop_front();
            // not moving here because compiler can already select move constructor
            // or can elide the copy
            return temp;
        }


    private:

        using private_index_type = size_type;
        using shared_index_type  = std::atomic<private_index_type>;

        cues::raw_byte_type const * const_pointer_at_index(private_index_type idx) const noexcept
        {
            if constexpr (std::is_array<value_type>::value)
            {
                return static_cast<cues::raw_byte_type const *>((*memory[idx]).cbegin());
            }
            else
            {
                return reinterpret_cast<cues::raw_byte_type const *>(&(*(memory[idx])));
            }
        }

        cues::raw_byte_type * pointer_at_index(private_index_type idx) noexcept
        {
            return const_cast<cues::raw_byte_type *>(const_pointer_at_index(idx));
        }


        static constexpr size_t private_index_align = ( INDEX_ALIGN > alignof(private_index_type))
                                                      ? INDEX_ALIGN : alignof(private_index_type);
        static constexpr size_t shared_index_align  = ( INDEX_ALIGN > alignof(shared_index_type))
                                                      ? INDEX_ALIGN : alignof(shared_index_type);
        static constexpr size_t data_align          = ( MEM_ALIGN   > alignof(cues::optional<value_type>))
                                                      ? MEM_ALIGN   : alignof(cues::optional<value_type>);

        using stored_type        = cues::optional<value_type, data_align>;

        alignas(data_align)          stored_type                memory[3];
        alignas(shared_index_align)  shared_index_type  mutable intermediate_idx;
        alignas(private_index_align) private_index_type mutable read_idx;
        alignas(private_index_align) private_index_type         write_idx;


};



} // namespace cues

#endif // CUES_TRIPLE_BUFFER_HPP
