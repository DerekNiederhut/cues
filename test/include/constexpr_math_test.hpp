#ifndef CUES_TEST_CONSTEXPR_MATH_TEST_HPP_INCLUDED
#define CUES_TEST_CONSTEXPR_MATH_TEST_HPP_INCLUDED

#include <cstdint>
#include <limits>

#include "cues/constexpr_math.hpp"

namespace cues
{
namespace test
{

static_assert(cues::is_pow2(   01u), "Failed to identify   1 as a power of 2 (2^0)");
static_assert(cues::is_pow2(  0x2u), "Failed to identify   2 as a power of 2 (2^1)");
static_assert(cues::is_pow2(  static_cast<unsigned char   >(4)), "Failed to identify   4 as a power of 2 (2^2)");
static_assert(cues::is_pow2(  static_cast<unsigned short>(8)), "Failed to identify   8 as a power of 2 (2^3)");
static_assert(cues::is_pow2( static_cast<unsigned int   >(16)), "Failed to identify  16 as a power of 2 (2^4)");
static_assert(cues::is_pow2( static_cast<unsigned long >(32)), "Failed to identify  32 as a power of 2 (2^5)");
static_assert(cues::is_pow2( 64ul), "Failed to identify  64 as a power of 2 (2^6)");
static_assert(cues::is_pow2(static_cast<unsigned long long>(128)), "Failed to identify 128 as a power of 2 (2^7)");

static_assert(cues::is_pow2(32768ul), "Failed to identify 65536 as a power of 2 (2^15)");
static_assert(cues::is_pow2(65536ull), "Failed to identify 65536 as a power of 2 (2^16)");
static_assert(cues::is_pow2(2147483648ul), "Failed to identify 2147483648 as a power of 2 (2^31)");
static_assert(cues::is_pow2(4294967296ull), "Failed to identify 4294967296 as a power of 2 (2^32)");
static_assert(cues::is_pow2(9223372036854775808ull), "Failed to identify 18446744073709551616 as a power of 2 (2^64)");

static_assert(cues::log2( 01u)  == 0, "Failed to calculate log2(1)");
static_assert(cues::log2(0x2u)  == 1, "Failed to calculate log2(2)");
static_assert(cues::log2(  3u)  == 1, "Failed to calculate log2(3)");
static_assert(cues::log2(  4u)  == 2, "Failed to calculate log2(4)");
static_assert(cues::log2(  5u)  == 2, "Failed to calculate log2(5)");
static_assert(cues::log2(  6u)  == 2, "Failed to calculate log2(6)");
static_assert(cues::log2(  7u)  == 2, "Failed to calculate log2(7)");
static_assert(cues::log2(static_cast<unsigned char>(  8)) == 3, "Failed to calculate log2(8)");
static_assert(cues::log2(static_cast<unsigned char>( 10)) == 3, "Failed to calculate log2(10)");
static_assert(cues::log2(static_cast<unsigned short>(16)) == 4, "Failed to calculate log2(16)");
static_assert(cues::log2(static_cast<unsigned long>( 32)) == 5, "Failed to calculate log2(32)");
static_assert(cues::log2(static_cast<unsigned long long>(64)) == 6, "Failed to calculate log2(64)");
static_assert(cues::log2(                100u  ) ==  6, "Failed to calculate log2(100)");
static_assert(cues::log2(                255u  ) ==  7, "Failed to calculate log2(255)");
static_assert(cues::log2(                256u  ) ==  8, "Failed to calculate log2(256)");
static_assert(cues::log2(               1000u  ) ==  9, "Failed to calculate log2(1000)");
static_assert(cues::log2(              10000u  ) == 13, "Failed to calculate log2(10000)");
static_assert(cues::log2(              32768u  ) == 15, "Failed to calculate log2(2^15)");
static_assert(cues::log2(              65535ul ) == 15, "Failed to calculate log2(2^16 - 1)");
static_assert(cues::log2(              65536ul ) == 16, "Failed to calculate log2(2^16)");
static_assert(cues::log2(             100000ul ) == 16, "Failed to calculate log2(100000)");
static_assert(cues::log2(            1000000ul ) == 19, "Failed to calculate log2(10^6)");
static_assert(cues::log2(           10000000ul ) == 23, "Failed to calculate log2(10^7)");
static_assert(cues::log2(          100000000ul ) == 26, "Failed to calculate log2(10^8)");
static_assert(cues::log2(         1000000000ul ) == 29, "Failed to calculate log2(10^9)");
static_assert(cues::log2(         2147483648ul ) == 31, "Failed to calculate log2(2^31)");
static_assert(cues::log2(         4294967296ull) == 32, "Failed to calculate log2(2^32)");
static_assert(cues::log2(        10000000000ull) == 33, "Failed to calculate log2(10^10)");
static_assert(cues::log2(       100000000000ull) == 36, "Failed to calculate log2(10^11)");
static_assert(cues::log2(      1000000000000ull) == 39, "Failed to calculate log2(10^12)");
static_assert(cues::log2(     10000000000000ull) == 43, "Failed to calculate log2(10^13)");
static_assert(cues::log2(    100000000000000ull) == 46, "Failed to calculate log2(10^14)");
static_assert(cues::log2(   1000000000000000ull) == 49, "Failed to calculate log2(10^15)");
static_assert(cues::log2(  10000000000000000ull) == 53, "Failed to calculate log2(10^16)");
static_assert(cues::log2( 100000000000000000ull) == 56, "Failed to calculate log2(10^17)");
static_assert(cues::log2(1000000000000000000ull) == 59, "Failed to calculate log2(10^18)");
static_assert(cues::log2(9223372036854775808ull) == 63, "Failed to calculate log2(2^63)");
static_assert(cues::log2(18446744073709551615ull) == 63, "Failed to calculate log2(2^64)-1");

// get the maximum value of the largest unsigned type,
// take the log,
// shift the number 1 (cast to uintmax-t to avoid overflow) left by that value
// now we have the largest exact power of 2 on this platform;
// assert that it is a power of 2
static_assert(cues::is_pow2(static_cast<std::uintmax_t>(1) << cues::log2((std::numeric_limits<std::uintmax_t>::max)())), "Failed to identify largest power of 2 as a power of 2");

static_assert(cues::minimum_bits(0u) == 0, "minimum bits does not return same value for zero as std::bit_width");
static_assert(cues::minimum_bits(1u) == 1, "minimum bits failure to identify number of bits needed to store value of 1");
static_assert(cues::minimum_bits(2u) == 2, "minimum bits failure to identify number of bits needed to store value of 2");
static_assert(cues::minimum_bits(3u) == 2, "minimum bits failure to identify number of bits needed to store value of 3");
static_assert(cues::minimum_bits(4u) == 3, "minimum bits failure to identify number of bits needed to store value of 4");
static_assert(cues::minimum_bits(5u) == 3, "minimum bits failure to identify number of bits needed to store value of 5");
static_assert(cues::minimum_bits(6u) == 3, "minimum bits failure to identify number of bits needed to store value of 6");
static_assert(cues::minimum_bits(7u) == 3, "minimum bits failure to identify number of bits needed to store value of 7");
static_assert(cues::minimum_bits(8u) == 4, "minimum bits failure to identify number of bits needed to store value of 8");
static_assert(cues::minimum_bits(127u) == 7, "minimum bits failure to identify number of bits needed to store value of 127");
static_assert(cues::minimum_bits(255u) == 8, "minimum bits failure to identify number of bits needed to store value of 255");
static_assert(cues::minimum_bits(256u) == 9, "minimum bits failure to identify number of bits needed to store value of 256");
static_assert(cues::minimum_bits(32767u) == 15, "minimum bits failure to identify number of bits needed to store value of 32767");
static_assert(cues::minimum_bits(32768u) == 16, "minimum bits failure to identify number of bits needed to store value of 32767");
static_assert(cues::minimum_bits(65535u) == 16, "minimum bits failure to identify number of bits needed to store value of 65535");
static_assert(cues::minimum_bits(65536u) == 17, "minimum bits failure to identify number of bits needed to store value of 65535");
static_assert(cues::minimum_bits(2147483647ul) == 31, "minimum bits failure to identify number of bits needed to store value of 2147483647");
static_assert(cues::minimum_bits(2147483648ul) == 32, "minimum bits failure to identify number of bits needed to store value of 2147483648");
static_assert(cues::minimum_bits(9223372036854775807ull) == 63, "minimum bits failure to identify number of bits needed to store value of 9223372036854775807");

static_assert(cues::minimum_bits(1ull << 52u) == 53, "minimum bits failure to identify number of bits needed to store value of 2^52");

static_assert(cues::log10(static_cast<std::uint_least8_t>(1u))    == 0, "failed to calculate log10(1)");
static_assert(cues::log10(2u)    == 0, "failed to calculate log10(2)");
static_assert(cues::log10(9u)    == 0, "failed to calculate log10(9)");
static_assert(cues::log10(10u)   == 1, "failed to calculate log10(10)");
static_assert(cues::log10(11u)   == 1, "failed to calculate log10(11)");
static_assert(cues::log10(50u)   == 1, "failed to calculate log10(50)");
static_assert(cues::log10(99u)   == 1, "failed to calculate log10(99)");
static_assert(cues::log10(100u)  == 2, "failed to calculate log10(100)");
static_assert(cues::log10(101u)  == 2, "failed to calculate log10(101)");
static_assert(cues::log10(static_cast<std::uint_least8_t>(127u))  == 2, "failed to calculate log10(127)");
static_assert(cues::log10(static_cast<std::uint_least8_t>(255u))  == 2, "failed to calculate log10(255)");
static_assert(cues::log10(999u)  == 2, "failed to calculate log10(999)");
static_assert(cues::log10(1000u) == 3, "failed to calculate log10(1000)");
static_assert(cues::log10(5000u) == 3, "failed to calculate log10(5000)");
static_assert(cues::log10(9999u) == 3, "failed to calculate log10(9999)");
static_assert(cues::log10(10000u) == 4, "failed to calculate log10(10000)");
static_assert(cues::log10(static_cast<std::uint_least16_t>(32767u)) == 4, "failed to calculate log10(32767)");
static_assert(cues::log10(static_cast<std::uint_least16_t>(65535u)) == 4, "failed to calculate log10(65535)");
static_assert(cues::log10(99999ul) == 4, "failed to calculate log10(99999)");
static_assert(cues::log10(100000ul) == 5, "failed to calculate log10(100000)");
static_assert(cues::log10(1000000ul) == 6, "failed to calculate log10(1000000)");
static_assert(cues::log10(5000000ul) == 6, "failed to calculate log10(5000000)");
static_assert(cues::log10(10000000ul) == 7, "failed to calculate log10(10000000)");
static_assert(cues::log10(10000001ul) == 7, "failed to calculate log10(10000001)");
static_assert(cues::log10(99999999ul) == 7, "failed to calculate log10(99999999)");
static_assert(cues::log10(100000000ul) == 8, "failed to calculate log10(100000000)");
static_assert(cues::log10(1000000000ul) == 9, "failed to calculate log10(1000000000)");
static_assert(cues::log10(static_cast<std::uint_least32_t>(2147483647ul)) == 9, "failed to calculate log10(2147483647)");
static_assert(cues::log10(static_cast<std::uint_least32_t>(4294967295ul)) == 9, "failed to calculate log10(4294967295)");
static_assert(cues::log10(10000000000ull) == 10, "failed to calculate log10(10000000000)");
static_assert(cues::log10(100000000000ull) == 11, "failed to calculate log10(100000000000)");
static_assert(cues::log10(10000000000000000ull) == 16, "failed to calculate log10(10000000000000000)");
static_assert(cues::log10(10000000000000000000ull) == 19, "failed to calculate log10(10000000000000000000)");
static_assert(cues::log10(static_cast<std::uint_least64_t>(18446744073709551615ull)) == 19, "failed to calculate log10(18446744073709551615)");

static_assert(cues::pow(-2,0u) == 1, "failed to calculate (-2)^0");
static_assert(cues::pow(2u,0u) == 1, "failed to calculate 2^0");
static_assert(cues::pow(10u,0u) == 1, "failed to calculate 10^0");
static_assert(cues::pow(static_cast<std::int_least8_t>(-128),0u) == 1, "failed to calculate (-128)^0");
static_assert(cues::pow(static_cast<std::uint_least8_t>(255u),0u) == 1, "failed to calculate 255^0");
static_assert(cues::pow(static_cast<std::int_least16_t>(-32768),0u) == 1, "failed to calculate (-32768)^0");
static_assert(cues::pow(static_cast<std::uint_least16_t>(65535u),0u) == 1, "failed to calculate 65535^0");
static_assert(cues::pow(static_cast<std::int_least32_t>(-2147483648l),0u) == 1, "failed to calculate (-2147483648l)^0");
static_assert(cues::pow(static_cast<std::uint_least32_t>(294967295ul),0u) == 1, "failed to calculate 4294967295^0");
static_assert(cues::pow(static_cast<std::int_least64_t>(-9223372036854775807ll),0u) == 1, "failed to calculate (-9223372036854775807ll)^0");
static_assert(cues::pow(static_cast<std::uint_least64_t>(18446744073709551615ull),0u) == 1, "failed to calculate 18446744073709551615^0");

static_assert(cues::pow(-2,1u) == -2, "failed to calculate (-2)^1");
static_assert(cues::pow(2u,1u) == 2, "failed to calculate 2^1");
static_assert(cues::pow(10u,1u) == 10, "failed to calculate 10^1");
static_assert(cues::pow(static_cast<std::int_least8_t>(-128),1u) == -128, "failed to calculate (-128)^1");
static_assert(cues::pow(static_cast<std::uint_least8_t>(255u),1u) == 255, "failed to calculate 255^1");
static_assert(cues::pow(static_cast<std::int_least16_t>(-32768),1u) == -32768, "failed to calculate (-32768)^1");
static_assert(cues::pow(static_cast<std::uint_least16_t>(65535u),1u) == 65535u, "failed to calculate 65535^1");
static_assert(cues::pow(static_cast<std::int_least32_t>(-2147483648l),1u) == -2147483648l, "failed to calculate (-2147483648l)^1");
static_assert(cues::pow(static_cast<std::uint_least32_t>(294967295ul),1u) == 294967295ul, "failed to calculate 4294967295^1");
static_assert(cues::pow(static_cast<std::int_least64_t>(-9223372036854775807ll),1u) == -9223372036854775807ll, "failed to calculate (-9223372036854775807ll)^1");
static_assert(cues::pow(static_cast<std::uint_least64_t>(18446744073709551615ull),1u) == 18446744073709551615ull, "failed to calculate 18446744073709551615^1");

static_assert(cues::pow(static_cast<std::uint_least64_t>(2),63u) == 9223372036854775808ull, "failed to calculate 2^63");
static_assert(cues::pow(static_cast<std::int_least64_t>(-2),62u) == 4611686018427387904ll, "failed to calculate (-2)^62");
static_assert(cues::pow(static_cast<std::int_least64_t>(-2),61u) == -2305843009213693952ll, "failed to calculate (-2)^61");
static_assert(cues::pow(static_cast<std::uint_least64_t>(2),32u) == 4294967296ull, "failed to calculate 2^32");
static_assert(cues::pow(static_cast<std::int_least64_t>(-2),32u) == 4294967296ll, "failed to calculate (-2)^32");
static_assert(cues::pow(static_cast<std::int_least64_t>(-2),31u) == -2147483648ll, "failed to calculate (-2)^31");
static_assert(cues::pow(static_cast<std::uint_least32_t>(2),31u) ==  2147483648ul, "failed to calculate 2^31");
static_assert(cues::pow(static_cast<std::int_least32_t>(-2),30u) ==  1073741824l, "failed to calculate (-2)^30");
static_assert(cues::pow(static_cast<std::int_least32_t>(-2),29u) ==  -536870912l, "failed to calculate (-2)^29");
static_assert(cues::pow(static_cast<std::uint_least32_t>(2),16u) ==  65536ul, "failed to calculate 2^16");
static_assert(cues::pow(static_cast<std::int_least32_t>(-2),16u) ==  65536l, "failed to calculate (-2)^16");
static_assert(cues::pow(static_cast<std::uint_least16_t>(2),15u) ==  32768u, "failed to calculate 2^15");
static_assert(cues::pow(static_cast<std::int_least32_t>(-2),15u) ==  -32768l, "failed to calculate (-2)^15");
static_assert(cues::pow(static_cast<std::int_least16_t>(-2),14u) ==  16384, "failed to calculate (-2)^14");
static_assert(cues::pow(static_cast<std::uint_least16_t>(2),8u) ==  256u, "failed to calculate 2^8");
static_assert(cues::pow(static_cast<std::int_least16_t>(-2),8u) ==  256, "failed to calculate (-2)^8");
static_assert(cues::pow(static_cast<std::uint_least8_t>(2),7u) ==  128u, "failed to calculate 2^7");
static_assert(cues::pow(static_cast<std::int_least16_t>(-2),7u) ==  -128, "failed to calculate (-2)^7");
static_assert(cues::pow(static_cast<std::int_least8_t>(-2),6u) ==  64, "failed to calculate (-2)^6");
static_assert(cues::pow(static_cast<std::int_least8_t>(-2),5u) ==  -32, "failed to calculate (-2)^5");
static_assert(cues::pow(static_cast<std::uint_least8_t>(2),2u) ==  4u, "failed to calculate 2^2");
static_assert(cues::pow(static_cast<std::int_least8_t>(-2),2u) ==  4u, "failed to calculate (-2)^2");

static_assert(cues::pow(static_cast<std::uint_least64_t>(4294967295ul), 2) == 18446744065119617025ull, "failed to calculate 4294967295^2");
static_assert(cues::pow(static_cast<std::int_least64_t>(2147483647ll), 2) == 4611686014132420609ll, "failed to calculate 2147483647^2");
static_assert(cues::pow(static_cast<std::uint_least32_t>(65535ul), 2) == 4294836225ul, "failed to calculate 65535^2");
static_assert(cues::pow(static_cast<std::int_least32_t>(32767l), 2) == 1073676289l, "failed to calculate 32767^2");
static_assert(cues::pow(static_cast<std::uint_least16_t>(255), 2) == 65025u, "failed to calculate 255^2");
static_assert(cues::pow(static_cast<std::int_least16_t>(127), 2) == 16129, "failed to calculate 127^2");
static_assert(cues::pow(static_cast<std::uint_least8_t>(15), 2) == 225, "failed to calculate 15^2");
static_assert(cues::pow(static_cast<std::int_least8_t>(11), 2) == 121, "failed to calculate 11^2");

static_assert(cues::pow(static_cast<std::uint_least64_t>(10), 19) == 10000000000000000000ull, "failed to calculate 10^19");
static_assert(cues::pow(static_cast<std::int_least64_t>(10), 18) == 1000000000000000000ll, "failed to calculate 10^18");
static_assert(cues::pow(static_cast<std::uint_least32_t>(10), 9) == 1000000000ul, "failed to calculate 10^9");
static_assert(cues::pow(static_cast<std::int_least32_t>(10), 9) == 1000000000l, "failed to calculate 10^9");
static_assert(cues::pow(static_cast<std::uint_least16_t>(10), 4) == 10000u, "failed to calculate 10^4");
static_assert(cues::pow(static_cast<std::int_least16_t>(10), 4) == 10000, "failed to calculate 10^4");
static_assert(cues::pow(static_cast<std::uint_least8_t>(10), 2) == 100u, "failed to calculate 10^2");
static_assert(cues::pow(static_cast<std::int_least8_t>(10), 2) == 100, "failed to calculate 10^2");

static_assert(cues::pow(static_cast<std::int_least32_t>(3), 11) == 177147l, "failed to calculate 3^11");
static_assert(cues::pow(static_cast<std::int_least16_t>(11), 3) == 1331, "failed to calculate 11^3");

static_assert(cues::pow(static_cast<std::int_least32_t>(-5), 7) == -78125l, "failed to calculate (-5)^7");
static_assert(cues::pow(static_cast<std::int_least16_t>(-7), 5) == -16807, "failed to calculate (-7)^5");


// not using numeric_limits here because the assumption in the
// static_assert applies to fundamental integer types, but not
// necessarily custom integer-like obejcts
template<class T, typename std::enable_if<std::is_integral<T>::value, bool>::type = true>
struct verify_abs_type
{

    static_assert(cues::abs(T{0}) == 0u, "failed abs(0)");
    static_assert(cues::abs(T{1}) == 1u, "failed abs(1)");

    static_assert(cues::abs((std::numeric_limits<T>::max)()) == (std::numeric_limits<T>::max)(), "failed abs(max)");

    static_assert(
        (!std::numeric_limits<T>::is_signed)
        ||
        (cues::abs(static_cast<T>(-1)) == 1u),
        "failed abs(-1)"
    );

    static_assert(
        (!std::numeric_limits<T>::is_signed)
        ||
        (cues::abs(static_cast<T>(-2)) == 2u),
        "failed abs(-2)"
    );

    // -127 fits in a signed 8 whether 1's complement, 2's complement,
    // or sign-magnitude are used, and 127u fits in signed and unsigned 8;
    // 127 is also prime.
    static_assert(
        (!std::numeric_limits<T>::is_signed)
        ||
        (cues::abs(static_cast<T>(-127)) == 127u),
        "failed abs(-127)"
    );

    static_assert(
      #if __cplusplus < 202002L
      // abs(min) == max for sign-magnitude and 1's complement
      cues::abs((std::numeric_limits<T>::min)())
      == (std::numeric_limits<T>::is_signed ? (std::numeric_limits<T>::max)() : 0 )
      ||
      #endif // __cplusplus
      // after c++ 20, only 2's complement is permitted by the standard
      // in which case abs(min) should be max + 1, but
      // the type of max is not large enough to hold max + 1,
      // so subtract it from abs(min) instead
      (cues::abs((std::numeric_limits<T>::min)())
      - 1u)
      == (std::numeric_limits<T>::is_signed ? (std::numeric_limits<T>::max)() : 0),
      "failed abs of min"
    );
};

constexpr verify_abs_type<std::int_least8_t>  tabs_s08;
constexpr verify_abs_type<std::uint_least8_t> tabs_u08;
constexpr verify_abs_type<std::int_least16_t>  tabs_s16;
constexpr verify_abs_type<std::uint_least16_t> tabs_u16;
constexpr verify_abs_type<std::int_least32_t>  tabs_s32;
constexpr verify_abs_type<std::uint_least32_t> tabs_u32;
constexpr verify_abs_type<std::int_least64_t>  tabs_s64;
constexpr verify_abs_type<std::uint_least64_t> tabs_u64;
constexpr verify_abs_type<std::intmax_t>       tabs_smax;
constexpr verify_abs_type<std::uintmax_t>      tabs_umax;

    template<typename T>
    struct bitmask_test_by_type
    {
        static_assert(
            cues::bitmask<std::numeric_limits<T>::digits>()
            == (std::numeric_limits<T>::max)()
            , "mask of full type width not the maximum positive value"
        );
    };



    constexpr bitmask_test_by_type<std::uint_least8_t> mask_u8;
    constexpr bitmask_test_by_type<std::uint_least16_t> mask_u16;
    constexpr bitmask_test_by_type<std::uint_least32_t> mask_u32;
    constexpr bitmask_test_by_type<std::uint_least64_t> mask_u64;
    constexpr bitmask_test_by_type<std::uintmax_t> mask_umax;
    constexpr bitmask_test_by_type<std::int_least8_t> mask_s8;
    constexpr bitmask_test_by_type<std::int_least16_t> mask_s16;
    constexpr bitmask_test_by_type<std::int_least32_t> mask_s32;
    constexpr bitmask_test_by_type<std::int_least64_t> mask_s64;
    constexpr bitmask_test_by_type<std::intmax_t> mask_smax;

    static_assert(cues::bitmask<0>() == 0, "zero should have empty bitmask");
    static_assert(cues::bitmask<1>() == 1, "single bit bitmask should be 1");
    static_assert(cues::bitmask<7>() == 0x7F, "seven bits of mask should be 0x7F");
    static_assert(cues::bitmask<8>() == 0xFF, "single byte bitmask should be 0xFF");
    static_assert(cues::bitmask<16>() == 0xFFFFu, "two byte bitmask should be 0xFFFF");
    static_assert(cues::bitmask<24>() == 0xFFFFFFul, "three byte bitmask should be 0xFFFFFF");


    static_assert(cues::count_trailing_zeros(static_cast<std::uint_least8_t>(0))
                  == std::numeric_limits<std::uint_least8_t>::digits
                  , "failed trailing zeros for 0 as uint_least8_t");
    static_assert(cues::count_trailing_zeros(static_cast<std::uint_least16_t>(0))
                  == std::numeric_limits<std::uint_least16_t>::digits
                  , "failed trailing zeros for 0 as uint_least16_t");
    static_assert(cues::count_trailing_zeros(static_cast<std::uint_least32_t>(0))
                  == std::numeric_limits<std::uint_least32_t>::digits
                  , "failed trailing zeros for 0 as uint_least32_t");
    static_assert(cues::count_trailing_zeros(static_cast<std::uint_least64_t>(0))
                  == std::numeric_limits<std::uint_least64_t>::digits
                  , "failed trailing zeros for 0 as uint_least64_t");
    static_assert(cues::count_trailing_zeros(static_cast<std::uintmax_t>(0))
                  == std::numeric_limits<std::uintmax_t>::digits
                  , "failed trailing zeros for 0 as uintmax_t");

    template<typename T>
    struct check_trailing_zero_composition
    {
        static_assert(!std::numeric_limits<T>::is_signed, "test designed for unsigned types only");
        // fill all but the last byte with bits
        static constexpr T const largemask = (std::numeric_limits<T>::digits > 8) ?
            cues::bitmask<std::numeric_limits<T>::digits-8u>() << 8u
            :
            0
        ;
        // fill only the most significant byte with bits
        static constexpr T const highmask = (std::numeric_limits<T>::digits > 8) ?
            (T{0xFFu} << (std::numeric_limits<T>::digits - 8))
            :
            0
        ;

        static constexpr T const lowmask = 1u;

        static constexpr bool common_input_value_check(T nibble, unsigned int intended)
        {
            return (cues::count_trailing_zeros(nibble) == intended)
            // test with bits in higher bytes set to 1
            && (cues::count_trailing_zeros(static_cast<T>(largemask + nibble)) == intended)
            // test with bits in highest byte set to 1 (intermediate set to 0)
            && (cues::count_trailing_zeros(static_cast<T>(highmask + nibble)) == intended)
            // test with this nibble in the higher nibble
            && (cues::count_trailing_zeros(static_cast<T>(nibble << 4u)) == 4u + intended)
            // test with this nibble in both nibbles
            && (cues::count_trailing_zeros(static_cast<T>((nibble << 4u) + nibble)) == intended)
            ;
        }

        template<typename U = T>
        static constexpr typename std::enable_if<
            std::numeric_limits<U>::digits >= 16
        , bool >::type bits16_input_value_check(U nibble, unsigned int intended)
        {
            static_assert(std::is_same<U,T>::value, "invalid use of sfinae parameter");
            return (cues::count_trailing_zeros(static_cast<U>(nibble << 8u))  == 8u + intended)
                && (cues::count_trailing_zeros(static_cast<U>((nibble << 8u) + nibble))  == intended)
                && (cues::count_trailing_zeros(static_cast<U>(nibble << 12u)) == 12u + intended)
                && (cues::count_trailing_zeros(static_cast<U>((nibble << 12u) + nibble)) == intended)
            ;
        }

        template<typename U = T>
        static constexpr typename std::enable_if<
            std::numeric_limits<U>::digits < 16
        , bool >::type bits16_input_value_check(U , unsigned int )
        {
            static_assert(std::is_same<U,T>::value, "invalid use of sfinae parameter");
            return true;
        }

        template<typename U = T>
        static constexpr typename std::enable_if<
            std::numeric_limits<U>::digits >= 32
        , bool >::type bits32_input_value_check(U nibble, unsigned int intended)
        {
            static_assert(std::is_same<U,T>::value, "invalid use of sfinae parameter");
            return (cues::count_trailing_zeros(static_cast<U>(nibble << 16u))  == 16u + intended)
                && (cues::count_trailing_zeros(static_cast<U>((nibble << 16u) + nibble))  == intended)
                && (cues::count_trailing_zeros(static_cast<U>(nibble << 20u))  == 20u + intended)
                && (cues::count_trailing_zeros(static_cast<U>(nibble << 24u))  == 24u + intended)
                && (cues::count_trailing_zeros(static_cast<U>(nibble << 28u))  == 28u + intended)
                && (cues::count_trailing_zeros(static_cast<U>((nibble << 28u) + nibble))  == intended)
            ;
        }

        template<typename U = T>
        static constexpr typename std::enable_if<
            std::numeric_limits<U>::digits < 32
        , bool >::type bits32_input_value_check(U , unsigned int )
        {
            static_assert(std::is_same<U,T>::value, "invalid use of sfinae parameter");
            return true;
        }

        template<typename U = T>
        static constexpr typename std::enable_if<
            std::numeric_limits<U>::digits >= 64
        , bool >::type bits64_input_value_check(U nibble, unsigned int intended)
        {
            static_assert(std::is_same<U,T>::value, "invalid use of sfinae parameter");
            return (cues::count_trailing_zeros(static_cast<U>(nibble << 32u))  == 32u + intended)
                // it seems reasonable that if the intermediate steps worked for 16 and 32,
                // they will also be correct for 64, and we can test the limits
                && (cues::count_trailing_zeros(static_cast<U>(nibble << 60u))  == 60u + intended)
                && (cues::count_trailing_zeros(static_cast<U>((nibble << 60u) + nibble))  == intended)
            ;
        }

        template<typename U = T>
        static constexpr typename std::enable_if<
            std::numeric_limits<U>::digits < 64
        , bool >::type bits64_input_value_check(U , unsigned int )
        {
            static_assert(std::is_same<U,T>::value, "invalid use of sfinae parameter");
            return true;
        }


        static constexpr bool check_input_value(T nibble, unsigned int intended)
        {
            return common_input_value_check(nibble, intended)
            && bits16_input_value_check(nibble, intended)
            && bits32_input_value_check(nibble, intended)
            && bits64_input_value_check(nibble, intended)
            ;
        }


        static_assert(check_input_value(T{0x01u},0u), "failed trailing zeros for nibble= 1");
        static_assert(check_input_value(T{0x02u},1u), "failed trailing zeros for nibble= 2");
        static_assert(check_input_value(T{0x03u},0u), "failed trailing zeros for nibble= 3");
        static_assert(check_input_value(T{0x04u},2u), "failed trailing zeros for nibble= 4");
        static_assert(check_input_value(T{0x05u},0u), "failed trailing zeros for nibble= 5");
        static_assert(check_input_value(T{0x06u},1u), "failed trailing zeros for nibble= 6");
        static_assert(check_input_value(T{0x07u},0u), "failed trailing zeros for nibble= 7");
        static_assert(check_input_value(T{0x08u},3u), "failed trailing zeros for nibble= 8");
        static_assert(check_input_value(T{0x09u},0u), "failed trailing zeros for nibble= 9");
        static_assert(check_input_value(T{0x0Au},1u), "failed trailing zeros for nibble=10");
        static_assert(check_input_value(T{0x0Bu},0u), "failed trailing zeros for nibble=11");
        static_assert(check_input_value(T{0x0Cu},2u), "failed trailing zeros for nibble=12");
        static_assert(check_input_value(T{0x0Du},0u), "failed trailing zeros for nibble=13");
        static_assert(check_input_value(T{0x0Eu},1u), "failed trailing zeros for nibble=14");
        static_assert(check_input_value(T{0x0Fu},0u), "failed trailing zeros for nibble=15");

    };

    static constexpr check_trailing_zero_composition<std::uint_least8_t> count_zero_8_test;
    static constexpr check_trailing_zero_composition<std::uint_least16_t> count_zero_16_test;
    static constexpr check_trailing_zero_composition<std::uint_least32_t> count_zero_32_test;
    static constexpr check_trailing_zero_composition<std::uint_least64_t> count_zero_64_test;


    template<typename T>
    struct check_limit_viewable_digits
    {
        static_assert(   std::numeric_limits<T>::digits
                      == cues::minimum_display_digits<cues::number_base::binary>::calculate((std::numeric_limits<T>::max)())
            , "max did not provide the expected number of digits in binary"
        );

        // digits10 isn't quite what we want; it's the number of base-10 digits (of any value)
        // that can go into T and back without error (eg only 2 for unsigned 8),
        // whereas we want the number of base10 digits required to represent T without loss
        // (eg 3 for unsigned 8).  This is what max_digit10 is supposed to be, but that is
        // not valid for integer types.  There is no value in 8,16,32,64 bits such that
        // 2^value == 10^x, for any x, so digits10 will never be the correct answer itself.
        // However, if 10^digits10+1 were larger than 2^value, then that number type would be
        // able to represent 10^digits10+1, which violates the definition of digits10, thus
        // for unsigned digits10+1 is the correct answer
        static_assert(   std::numeric_limits<T>::digits10 + 1u
                      == cues::minimum_display_digits<cues::number_base::decimal>::calculate((std::numeric_limits<T>::max)())
            , "max did not provide the expected number of digits in decimal"
        );

        static_assert(1 ==
                      cues::minimum_display_digits<number_base::binary>::calculate(static_cast<T>(0))
            , "zero did not provide the expected number of digits in binary"
        );

        static_assert(1 ==
                      cues::minimum_display_digits<number_base::decimal>::calculate(static_cast<T>(0))
            , "zero did not provide the expected number of digits in decimal"
        );
    };

    check_limit_viewable_digits<std::uint_least8_t>  u8_viewable_test;
    check_limit_viewable_digits<std::uint_least16_t> u16_viewable_test;
    check_limit_viewable_digits<std::uint_least32_t> u32_viewable_test;
    check_limit_viewable_digits<std::uint_least64_t> u64_viewable_test;
    check_limit_viewable_digits<std::uintmax_t>      umx_viewable_test;

    check_limit_viewable_digits<std::int_least8_t>  s8_viewable_test;
    check_limit_viewable_digits<std::int_least16_t> s16_viewable_test;
    check_limit_viewable_digits<std::int_least32_t> s32_viewable_test;
    check_limit_viewable_digits<std::int_least64_t> s64_viewable_test;
    check_limit_viewable_digits<std::intmax_t>      smx_viewable_test;


    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                   static_cast<unsigned char>(9)
           )
        , "decimal digits wrong for unsigned char"
    );

    static_assert(
         2 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                   static_cast<unsigned char>(99)
           )
        , "decimal digits wrong for unsigned char"
    );

    static_assert(
         3 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                   static_cast<signed char>(127)
           )
        , "decimal digits wrong for signed char"
    );

    static_assert(
         3 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<unsigned char>(0377)
           )
        , "octal digits wrong for unsigned char"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<unsigned char>(00)
           )
        , "octal digits wrong for unsigned char"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<unsigned char>(01)
           )
        , "octal digits wrong for unsigned char"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<signed char>(-7)
           )
        , "octal digits wrong for signed char"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<unsigned char>(07)
           )
        , "octal digits wrong for unsigned char"
    );

    static_assert(
         2 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<unsigned char>(017)
           )
        , "octal digits wrong for unsigned char"
    );

    static_assert(
         3 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<signed char>(0177)
           )
        , "octal digits wrong for signed char"
    );

    static_assert(
         3 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<signed char>(-127)
           )
        , "octal digits wrong for signed char"
    );

    static_assert(
         3 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   static_cast<unsigned char>(0100)
           )
        , "octal digits wrong for unsigned char"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   static_cast<unsigned char>(0x00)
           )
        , "hex digits wrong for unsigned char"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   static_cast<signed char>(0x01)
           )
        , "hex digits wrong for signed char"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   static_cast<unsigned char>(0x0F)
           )
        , "hex digits wrong for unsigned char"
    );

    static_assert(
         2 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   static_cast<unsigned char>(0x1F)
           )
        , "hex digits wrong for unsigned char"
    );
    static_assert(
         2 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   static_cast<unsigned char>(0xFF)
           )
        , "hex digits wrong for unsigned char"
    );

    static_assert(
         2 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   static_cast<signed char>(0x7F)
           )
        , "hex digits wrong for signed char"
    );

    static_assert(
         2 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   static_cast<signed char>(-127)
           )
        , "hex digits wrong for signed char"
    );


    static_assert(
         6 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   0177777u
           )
        , "octal digits wrong for unsigned int"
    );

    static_assert(
         5 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                   65535u
           )
        , "decimal digits wrong for unsigned int"
    );

    static_assert(
         5 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                   10000u
           )
        , "decimal digits wrong for unsigned int"
    );

    static_assert(
         4 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                   9999u
           )
        , "decimal digits wrong for unsigned int"
    );

    static_assert(
         3 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                   999u
           )
        , "decimal digits wrong for unsigned int"
    );

    static_assert(
         5 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                   -32767
        )
        , "decimal digits wrong for signed int"
    );

    static_assert(
         5 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   077777
        )
        , "octal digits wrong for signed int"
    );

    static_assert(
         5 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   -32767
        )
        , "octal digits wrong for signed int"
    );

    static_assert(
         4 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   01000u
         )
        , "octal digits wrong for unsigned int"
    );

    static_assert(
         4 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   0xFFFFu
        )
        , "hex digits wrong for unsigned int"
    );

    static_assert(
         4 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   0x7FFF
        )
        , "hex digits wrong for signed int"
    );

    static_assert(
         3 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   0x0F00u
        )
        , "hex digits wrong for unsigned int"
    );

    static_assert(
         7 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   0x03C0A55Aul
        )
        , "hex digits wrong for unsigned long int"
    );

    static_assert(
         11 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   012345676543ul
        )
        , "octal digits wrong for unsigned long int"
    );

    static_assert(
         11 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   017777777777l
        )
        , "octal digits wrong for signed long (positive)"
    );

    static_assert(
         11 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   -017777777777l
        )
        , "octal digits wrong for signed long (negative)"
    );

    static_assert(
         11 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   037777777777ul
        )
        , "octal digits wrong for unsigned long"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   00ul
        )
        , "octal digits wrong for unsigned long"
    );

    static_assert(
         1 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
                   00000000001ul
        )
        , "octal digits wrong for unsigned long int"
    );
    static_assert(
        10 == cues::minimum_display_digits<cues::number_base::decimal>::calculate(
                    2123456789ul
        )
        , "decimal digits wrong for unsigned long int"
    );

    static_assert(
         14 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   0x00FFFFFFFFFFFFFFll
        )
        , "hex digits wrong for signed long long int"
    );

    static_assert(
         15 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   -0x0FFFFFFFFFFFFFFFll
        )
        , "hex digits wrong for signed long long int"
    );

    static_assert(
         2 == cues::minimum_display_digits<cues::number_base::hexadecimal>::calculate(
                   0x018ull
        )
        , "hex digits wrong for unsigned long long int"
    );

    static_assert(
         22 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
            01777777777777777777777ull
        )
        , "octal digits wrong for unsigned long long int"
    );

    static_assert(
        21 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
            0777777777777777777777ll
        )
        , "octal digits wrong for long long int"
    );

    static_assert(
        21 == cues::minimum_display_digits<cues::number_base::octal>::calculate(
            -0777777777777777777777ll
        )
        , "octal digits wrong for long long int"
    );


    template<typename T>
    constexpr bool test_div_roundup_value(T num, T denom, T expected)
    {
        return expected == cues::div_roundup<T, true>::calculate(num, denom)
        && expected == cues::div_roundup<T, false>::calculate(num, denom);
    }

    template<typename T>
    struct div_roundup_limit_tests
    {
        static constexpr T const maximum = (std::numeric_limits<T>::max)();
        static_assert(test_div_roundup_value(T{0}, T{1} ,T{0}),        "failed 0/1");
        static_assert(test_div_roundup_value(T{0}, maximum, T{0}),     "failed 0/maximum");
        static_assert(test_div_roundup_value(T{1}, T{1}, T{1}),        "failed 1/1");
        static_assert(test_div_roundup_value(T{1}, maximum, T{1}),     "failed 1/maximum");
        static_assert(test_div_roundup_value(maximum, T{1}, maximum),  "failed maximum/1");
        static_assert(test_div_roundup_value(maximum, maximum, T{1}),  "failed maximum/maximum");

    };

    div_roundup_limit_tests<std::uint_least8_t>   u8_divroundup_limit_test;
    div_roundup_limit_tests<std::uint_least16_t> u16_divroundup_limit_test;
    div_roundup_limit_tests<std::uint_least32_t> u32_divroundup_limit_test;
    div_roundup_limit_tests<std::uint_least64_t> u64_divroundup_limit_test;
    div_roundup_limit_tests<std::uintmax_t>     umax_divroundup_limit_test;



    // test some prime number pairs
    static_assert( test_div_roundup_value(89u, 97u,  1u), "failed 89/97");
    static_assert( test_div_roundup_value( 3u,  2u,  2u), "failed 3/2"  );
    static_assert( test_div_roundup_value(13u,  7u,  2u), "failed 13/7" );
    static_assert( test_div_roundup_value(97u, 47u,  3u), "failed 97/47");
    static_assert( test_div_roundup_value(17u,  5u,  4u), "failed 17/5" );
    static_assert( test_div_roundup_value(29u,  7u,  5u), "failed 29/7" );
    static_assert( test_div_roundup_value(61u, 13u,  5u), "failed 61/13");
    static_assert( test_div_roundup_value(37u,  7u,  6u), "failed 37/7" );
    static_assert( test_div_roundup_value(67u, 11u,  7u), "failed 67/11");
    static_assert( test_div_roundup_value(79u,  2u, 40u), "failed 79/2" );

    // test some non-primes divided by primes, exact results
    static_assert(test_div_roundup_value(122u, 61u,  2u), "failed 116/29");
    static_assert(test_div_roundup_value( 15u,  5u,  3u), "failed 15/5");
    static_assert(test_div_roundup_value(116u, 29u,  4u), "failed 116/29");


    static_assert(cues::popcount(static_cast<unsigned char>(0xFFu)) == 8, "popcount failed on 8-bit 0xff");
    static_assert(cues::popcount(static_cast<unsigned char>(0xA5u)) == 4, "popcount failed on 8-bit 0xA5");
    static_assert(cues::popcount(static_cast<unsigned char>(0x5Au)) == 4, "popcount failed on 8-bit 0x5A");
    static_assert(cues::popcount(static_cast<unsigned char>(0xF0u)) == 4, "popcount failed on 8-bit 0xF0");
    static_assert(cues::popcount(static_cast<unsigned char>(0x0Fu)) == 4, "popcount failed on 8-bit 0x0F");
    static_assert(cues::popcount(static_cast<unsigned char>(0x30u)) == 2, "popcount failed on 8-bit 0x30");
    static_assert(cues::popcount(static_cast<unsigned char>(0x03u)) == 2, "popcount failed on 8-bit 0x03");
    static_assert(cues::popcount(static_cast<unsigned char>(0x80u)) == 1, "popcount failed on 8-bit 0x80");
    static_assert(cues::popcount(static_cast<unsigned char>(0x10u)) == 1, "popcount failed on 8-bit 0x10");
    static_assert(cues::popcount(static_cast<unsigned char>(0x08u)) == 1, "popcount failed on 8-bit 0x08");
    static_assert(cues::popcount(static_cast<unsigned char>(0x04u)) == 1, "popcount failed on 8-bit 0x04");
    static_assert(cues::popcount(static_cast<unsigned char>(0x02u)) == 1, "popcount failed on 8-bit 0x02");
    static_assert(cues::popcount(static_cast<unsigned char>(0x01u)) == 1, "popcount failed on 8-bit 0x01");
    static_assert(cues::popcount(static_cast<unsigned char>(0x0u)) == 0, "popcount failed on 8-bit 0");

    static_assert(cues::popcount(static_cast<std::uint_least16_t>(0)) == 0, "popcount failed on std::uint_least16_t 0");
    static_assert(cues::popcount(static_cast<std::uint_least16_t>(1)) == 1, "popcount failed on std::uint_least16_t 1");
    static_assert(cues::popcount(static_cast<std::uint_least16_t>(0xA55Au)) == 8, "popcount failed on std::uint_least16_t 0xA55A");
    static_assert(cues::popcount((std::numeric_limits<std::uint_least16_t>::max)())
                   == std::numeric_limits<std::uint_least16_t>::digits, "popcount failed on max std::uint_least16_t");

    static_assert(cues::popcount(0x0ull) == 0, "popcount failed on 0ull");
    static_assert(cues::popcount(0xA55AA55AA55AA55Aull) == 32, "popcount failed on 0xA55AA55AA55AA55Aull");
    static_assert(cues::popcount((std::numeric_limits<unsigned long long>::max)())
                   == std::numeric_limits<unsigned long long>::digits, "popcount failed on max unsigned long long");

    static_assert(!cues::is_inf((std::numeric_limits<unsigned long long>::max())), "unsigned long long max should not be infinite");
    static_assert(!cues::is_inf((std::numeric_limits<signed long long>::max())), "signed long long max should not be infinite");
    static_assert(!cues::is_inf((std::numeric_limits<signed long long>::min())), "signed long long min should not be infinite");
    static_assert(!cues::is_inf((std::numeric_limits<unsigned long >::max())), "unsigned long max should not be infinite");
    static_assert(!cues::is_inf((std::numeric_limits<unsigned int >::max())), "unsigned int max should not be infinite");
    static_assert(!cues::is_inf((std::numeric_limits<unsigned char >::max())), "unsigned char max should not be infinite");
    static_assert(!cues::is_inf((std::numeric_limits<signed char >::min())), "signed char min should not be infinite");
    static_assert(!cues::is_inf(0xF8000000ul), "unsigned long with -infinity bit pattern should not be infinite");
    static_assert(!cues::is_inf(0x78000000ul), "unsigned long with +infinity bit pattern should not be infinite");
    static_assert(!cues::is_inf(0xFFF0000000000000ull), "unsigned long long with -infinity bit pattern should not be infinite");
    static_assert(!cues::is_inf(0x7FF0000000000000ull), "unsigned long long with +infinity bit pattern should not be infinite");

    static_assert(
        !std::numeric_limits<float>::has_infinity
        || cues::is_inf(std::numeric_limits<float>::infinity()),
        "if float infinity exists, it should be considered infinite"
    );

    static_assert(
        !std::numeric_limits<float>::has_infinity
        || cues::is_inf(-std::numeric_limits<float>::infinity()),
        "if float -infinity exists, it should be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<float>::max()),
        "max finite float should not be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<float>::lowest()),
        "lowest finite float should not be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<float>::quiet_NaN()),
        "NaN float should not be considered infinite"
    );

    static_assert(
        !std::numeric_limits<double>::has_infinity
        || cues::is_inf(std::numeric_limits<double>::infinity()),
        "if double infinity exists, it should be considered infinite"
    );

    static_assert(
        !std::numeric_limits<double>::has_infinity
        || cues::is_inf(-std::numeric_limits<double>::infinity()),
        "if double -infinity exists, it should be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<double>::max()),
        "max finite double should not be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<double>::lowest()),
        "lowest finite double should not be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<double>::quiet_NaN()),
        "NaN double should not be considered infinite"
    );

    static_assert(
        !std::numeric_limits<long double>::has_infinity
        || cues::is_inf(std::numeric_limits<long double>::infinity()),
        "if long double infinity exists, it should be considered infinite"
    );

    static_assert(
        !std::numeric_limits<long double>::has_infinity
        || cues::is_inf(-std::numeric_limits<long double>::infinity()),
        "if long double -infinity exists, it should be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<long double>::max()),
        "max finite long double should not be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<long double>::lowest()),
        "lowest finite long double should not be considered infinite"
    );

    static_assert(
        !cues::is_inf(std::numeric_limits<long double>::quiet_NaN()),
        "NaN long double should not be considered infinite"
    );


    static_assert(!cues::is_nan(0xFC000000ul), "unsigned long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0x7C000000ul), "unsigned long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0xF8000001ul), "unsigned long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0x78000001ul), "unsigned long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0xFFFFFFFFul), "unsigned long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0xFFF8000000000000ull), "unsigned long long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0x7FF8000000000000ull), "unsigned long long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0xFFF0000000000001ull), "unsigned long long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0x7FF0000000000001ull), "unsigned long long with NaN bit pattern should not be infinite");
    static_assert(!cues::is_nan(0xFFFFFFFFFFFFFFFFull), "unsigned long long with NaN bit pattern should not be infinite");

    static_assert(
        !std::numeric_limits<float>::has_quiet_NaN
        ||
        cues::is_nan(std::numeric_limits<float>::quiet_NaN()),
        "if float has quiet NaN, it should be recognized as NaN"
    );

    static_assert(
        !std::numeric_limits<float>::has_signaling_NaN
        ||
        cues::is_nan(std::numeric_limits<float>::signaling_NaN()),
        "if float has signaling NaN, it should be recognized as NaN"
    );

    static_assert(
        !std::numeric_limits<double>::has_quiet_NaN
        ||
        cues::is_nan(std::numeric_limits<double>::quiet_NaN()),
        "if double has quiet NaN, it should be recognized as NaN"
    );

    static_assert(
        !std::numeric_limits<double>::has_signaling_NaN
        ||
        cues::is_nan(std::numeric_limits<double>::signaling_NaN()),
        "if double has signaling NaN, it should be recognized as NaN"
    );

    static_assert(
        !std::numeric_limits<long double>::has_quiet_NaN
        ||
        cues::is_nan(std::numeric_limits<long double>::quiet_NaN()),
        "if long double has quiet NaN, it should be recognized as NaN"
    );

    static_assert(
        !std::numeric_limits<long double>::has_signaling_NaN
        ||
        cues::is_nan(std::numeric_limits<long double>::signaling_NaN()),
        "if long double has signaling NaN, it should be recognized as NaN"
    );

}
}

#endif // CUES_TEST_CONSTEXPR_MATH_TEST_HPP_INCLUDED
