#ifndef CUES_ASSOCIATIVE_DETAIL_HPP
#define CUES_ASSOCIATIVE_DETAIL_HPP

#include <type_traits>
#include <utility>
#include <memory>
#include <new>

#include "cues/version_helpers.hpp"
#include "cues/void_t.hpp"
#include "cues/memory.hpp"

namespace cues {

template<typename T, class Allocator>
class basic_node_handle : private cues::allocated_unique_ptr<T, Allocator>
{
    public:

    using base_type = cues::allocated_unique_ptr<T, Allocator>;

    using value_type      = typename base_type::value_type;
    using allocator_type  = typename base_type::allocator_type;

    basic_node_handle() = default;
    basic_node_handle(basic_node_handle &&) = default;

    basic_node_handle & operator=(basic_node_handle &&) = default;

    explicit basic_node_handle(base_type && b)
    : base_type(std::move(b))
    {}

    basic_node_handle(value_type * p, allocator_type a)
    : base_type(p, a)
    {}

    CUES_NODISCARD bool empty() const noexcept
    {
        return base_type::get() == nullptr;
    }

    explicit operator bool() const noexcept
    {
        return base_type::get() != nullptr;
    }

    void swap(basic_node_handle & other)
    {
        swap(static_cast<typename basic_node_handle::base_type &>(*this),
             static_cast<typename basic_node_handle::base_type &>(other)
        );
    }

    using base_type::get_allocator;
    using base_type::release;

    //! PRECONDITION: !empty()
    value_type & value() const
    {
        return *(base_type::get());
    }

    value_type const & operator*() const
    {
        return *(base_type::get());
    }

    value_type & operator*()
    {
        return *(base_type::get());
    }
};

template<typename T1, typename T2, class Allocator>
class basic_node_handle<std::pair<T1 const, T2>, Allocator>
: private cues::allocated_unique_ptr<std::pair<T1 const, T2>, Allocator>
{
    public:
    using key_type    = T1;
    using mapped_type = T2;

    using base_type = cues::allocated_unique_ptr<
        std::pair<key_type const, mapped_type>,
        Allocator
    >;

    struct key_proxy
    {
        key_proxy()
        : actual_key(nullptr)
        {}

        explicit key_proxy(key_type const & k)
        : actual_key(&k)
        {}

        // make sure this exists so it is an exact match for operator=
        // and chosen over the write-through operator= for key_type
        key_proxy & operator=(key_proxy const &) = default;

        // implicit conversion to reference to const key_type
        operator key_type const & () const
        {
            return *std::launder(actual_key);
        }

        // if this type were being used generally, or put into containers,
        // this would be a bad idea, but scoped inside this class,
        // and returned as a proxy, that seems unlikely
        key_type const * operator& () const
        {
            return std::launder(actual_key);
        }

        // assignment via destruct and construct in-place
        key_proxy & operator=(key_type const & v)
        {
            actual_key->~key_type();
            new (const_cast<key_type *>(actual_key)) key_type{v};
            return *this;
        }

    private:
        key_type const * actual_key;
    };

    using allocator_type = Allocator;

    basic_node_handle() = default;
    basic_node_handle(basic_node_handle &&) = default;

    basic_node_handle & operator=(basic_node_handle &&) = default;

    explicit basic_node_handle(base_type && b)
    : base_type(std::move(b))
    {}

    basic_node_handle(typename base_type::pointer p, allocator_type a)
    : base_type(p, a)
    {}

    CUES_NODISCARD bool empty() const noexcept
    {
        return base_type::get() == nullptr;
    }

    explicit operator bool() const noexcept
    {
        return base_type::get() != nullptr;
    }

    void swap(basic_node_handle & other)
    {
        swap(static_cast<typename basic_node_handle::base_type &>(*this),
             static_cast<typename basic_node_handle::base_type &>(other)
        );
    }

    using base_type::get_allocator;
    using base_type::release;

    mapped_type & mapped() const
    {
        return std::launder(base_type::get())->second;
    }

    // this is a departure from key() in the standard containers,
    // which seems to return a reference to non-const key_type.
    // For our containers, the object actually created is
    // std::pair<key_type const, mapped_type>, not
    // std::pair<key_type      , mapped_type>
    // so const_cast would be undefined behavior
    // and, because the types are not similar, so would
    // pointer aliasing via
    // reinterpret_cast or static_cast through void *
    // Similarly if the containers instead create
    // std::pair<key_type, mapped_type>, they run into
    // the issue in the other direction when returning
    // a reference to pair<key_type const, mapped_type).
    // so, we return a proxy object which implicitly
    // converts to reference to const key_type,
    // and provides an assignment operator that
    // destructs and reconstructs the key_type,
    // since destructors and constructors work
    // even when the object is const
    // (provided the key is not somehow stored
    // in memory that is itself read-only)
    key_proxy key() const
    {
        return key_proxy{std::launder(base_type::get())->first};
    }

    typename base_type::element_type const & operator*() const
    {
        return *std::launder(base_type::get());
    }

    typename base_type::element_type & operator*()
    {
        return *std::launder(base_type::get());
    }

};

template<typename T, class Allocator>
void swap(basic_node_handle<T, Allocator> & lhs, basic_node_handle<T, Allocator> & rhs)
{
    lhs.swap(rhs);
}


template<class Container, class Node = typename Container::node_type, typename Selector = void>
struct has_node_positional_splice : public std::false_type
{};

template<class Container, class Node>
struct has_node_positional_splice<
    Container,
    Node,
    cues::void_t<
        decltype(std::declval<Container &>().splice(std::declval<typename Container::const_iterator>(), std::declval<Node>()))
    >
>
: public std::true_type
{};

template<class Container, class Node = typename Container::node_type, typename Selector = void>
struct has_node_positional_insert : public std::false_type
{};

template<class Container, class Node>
struct has_node_positional_insert<
    Container,
    Node,
    cues::void_t<
        decltype(std::declval<Container &>().insert(std::declval<typename Container::const_iterator>(), std::declval<Node>()))
    >
>
: public std::true_type
{};

template<class Container, class Other = Container, typename Selector = void>
struct has_iterator_range_splice : public std::false_type
{};

template<class Container, class Other>
struct has_iterator_range_splice<
    Container,
    Other,
    cues::void_t<
        decltype(
            std::declval<Container &>().splice(
                std::declval<typename Container::const_iterator>(),
                std::declval<Other>(),
                std::declval<typename Other::const_iterator>(),
                std::declval<typename Other::const_iterator>()
            )
        )
    >
>
: public std::true_type
{};

template<class Container, class Node = typename Container::node_type, typename Selector = void>
struct has_node_positional_extract : public std::false_type
{};

template<class Container, class Node>
struct has_node_positional_extract<
    Container,
    Node,
    cues::void_t<
        typename std::enable_if<
            std::is_constructible<
                Node,
                decltype(
                    std::declval<Container &>().extract(
                        std::declval<typename Container::const_iterator>()
                    )
                )
            >::value
        >::type
    >
> : public std::true_type
{};

template<class Container, class Other, typename Selector = void>
struct has_container_positional_splice : public std::false_type
{};

template<class Container, class Other>
struct has_container_positional_splice<
    Container,
    Other,
    cues::void_t<
        decltype(
            std::declval<Container &>().splice(
                std::declval<typename Container::const_iterator>(),
                std::declval<Other>(),
                std::declval<typename Other::const_iterator>()
            )
        )
    >
>
: public std::true_type
{};

template<class T, class Container, class Selector = void>
struct can_construct_from_extracted_node : public std::false_type
{};

template<class T, class Container>
struct can_construct_from_extracted_node<
    T,
    Container,
    cues::void_t<
        typename std::enable_if<
            std::is_constructible<
                T,
                decltype(
                    std::declval<Container>().extract(
                        std::declval<typename Container::const_iterator>()
                    )
                )
            >::value
        >::type
    >
>
: public std::true_type
{};

template<class Container, class Other, class Selector=void>
struct can_insert_extracted_node : std::false_type
{};

template<class Container, class Other>
struct can_insert_extracted_node<
    Container,
    Other,
    cues::void_t<
        decltype(
            std::declval<Container>().insert(
                std::declval<typename Container::const_iterator>(),
                std::declval<
                    typename std::enable_if<
                        can_construct_from_extracted_node<
                            typename Container::node_type,
                            Other
                        >::value,
                        typename Container::node_type
                    >::type
                >()
            )
        )
    >
> : public std::true_type
{};

template<typename T, typename selector = void>
class direct_or_indirect_iterator
{
public:
    using iterator       = typename T::iterator;
    using const_iterator = typename T::const_iterator;

    static constexpr const_iterator apply_begin(T const & c)
    {
        return c.begin();
    }

    static constexpr iterator apply_begin(T & c)
    {
        return c.begin();
    }

    static constexpr const_iterator apply_end(T const & c)
    {
        return c.end();
    }

    static constexpr iterator apply_end(T & c)
    {
        return c.end();
    }

    static constexpr const_iterator apply_conversion_to_direct(T const &, const_iterator i)
    {
        return i;
    }

    static constexpr iterator apply_conversion_to_direct(T &, iterator i)
    {
        return i;
    }

    static constexpr const_iterator apply_conversion_to_indirect(T const & , typename T::const_iterator i)
    {
        return i;
    }

    static constexpr iterator apply_conversion_to_indirect(T & , typename T::iterator i)
    {
        return i;
    }
};

template<typename T>
class direct_or_indirect_iterator<
    T,
    cues::void_t<
        typename T::indirect_iterator,
        typename T::indirect_const_iterator,
        decltype(std::declval<T>().indirect_begin()),
        decltype(std::declval<T>().indirect_end())
    >
>
{
public:
    using iterator       = typename T::indirect_iterator;
    using const_iterator = typename T::indirect_const_iterator;

    static constexpr const_iterator apply_begin(T const & c)
    {
        return c.indirect_begin();
    }

    static constexpr iterator apply_begin(T & c)
    {
        return c.indirect_begin();
    }

    static constexpr const_iterator apply_end(T const & c)
    {
        return c.indirect_end();
    }

    static constexpr iterator apply_end(T & c)
    {
        return c.indirect_end();
    }

    static constexpr typename T::const_iterator apply_conversion_to_direct(T const & c, const_iterator i)
    {
        return c.begin() + (i - c.indirect_begin());
    }

    static constexpr typename T::iterator apply_conversion_to_direct(T & c, iterator i)
    {
        return c.begin() + (i - c.indirect_begin());
    }

    static constexpr const_iterator apply_conversion_to_indirect(T const & c, typename T::const_iterator i)
    {
        return c.indirect_begin() + (i - c.begin());
    }

    static constexpr iterator apply_conversion_to_indirect(T & c, typename T::iterator i)
    {
        return c.indirect_begin() + (i - c.begin());
    }
};

template<typename T>
auto direct_or_indirect_begin(T && c)
{
    using container_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
    return direct_or_indirect_iterator<container_type>::apply_begin(c);
}

template<typename T>
auto direct_or_indirect_end(T && c)
{
    using container_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
    return direct_or_indirect_iterator<container_type>::apply_end(c);
}


template<typename T>
typename T::const_iterator make_direct(T const & c,
    typename direct_or_indirect_iterator<
        typename std::remove_const<typename std::remove_reference<T>::type>::type
    >::const_iterator i
) {
    using container_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
    return direct_or_indirect_iterator<container_type>::apply_conversion_to_direct(c, i);
}

template<typename T>
typename T::iterator make_direct(T & c, typename direct_or_indirect_iterator<
        typename std::remove_const<typename std::remove_reference<T>::type>::type
    >::iterator i
) {
    return direct_or_indirect_iterator<T>::apply_conversion_to_direct(c, i);
}

template<typename T>
typename direct_or_indirect_iterator<T>::const_iterator make_indirect(T const & c, typename T::const_iterator i)
{
    return direct_or_indirect_iterator<T>::apply_conversion_to_indirect(c, i);
}

template<typename T>
typename direct_or_indirect_iterator<T>::iterator make_indirect(T & c, typename T::iterator  i)
{
    return direct_or_indirect_iterator<T>::apply_conversion_to_indirect(c, i);
}

template<typename Compare, typename Container, typename Selector = void>
struct compare_or_indirect_compare
{
    using type = Compare;
};

template<typename Compare, typename Container>
struct compare_or_indirect_compare<
    Compare,
    Container,
    cues::void_t<
        typename Container::indirect_compare<Compare>
    >
>
{
    using type = typename Container::indirect_compare<Compare>;
};

template<typename Compare, typename Container, typename Selector = void>
struct compare_constructor_proxy
{
    using compare_type = typename compare_or_indirect_compare<Compare, Container>::type;

    constexpr compare_constructor_proxy(Compare comp, Container const & cont) noexcept(noexcept(compare_type(comp)))
    : proxied(comp)
    {}

    constexpr operator compare_type const & () const noexcept
    {
        return proxied;
    }
private:
    compare_type proxied;
};

template<typename Compare, typename Container>
struct compare_constructor_proxy<Compare, Container,
    cues::void_t<
        typename Container::indirect_compare<Compare>
    >
>
{
    using compare_type = typename compare_or_indirect_compare<Compare, Container>::type;

    constexpr compare_constructor_proxy(Compare comp, Container const & cont) noexcept(noexcept(compare_type(comp,cont)))
    : proxied(comp, cont)
    {}

    constexpr operator compare_type const &() const noexcept
    {
        return proxied;
    }

private:
    compare_type proxied;
};


} // namespace cues

#endif // CUES_ASSOCIATIVE_DETAIL_HPP
