#include "triple_buffer_test.hpp"

#include "cues/version_helpers.hpp"

#if (__cplusplus < 201703L && (!defined ( __cpp_lib_launder) || !defined(__cpp_lib_if_constexpr) || !defined(__cpp_lib_optional)))
    #pragma message "NOTE: skipping test of cues::triple_buffer because it requires std::launder, std::in_palce_t, and constexpr if, which are not available."
namespace cues
{
namespace test
{

std::size_t check_triple_buffer_test_runtime_assertions() {return 0;}

}
}
#else

#include <atomic>
#include <vector>
#include <array>
#include <tuple>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <thread>
#include <future>
#include <string_view>
#include <string>
#include <random>
#include <cstdlib>

#include "cues/triple_buffer.hpp"

#include "helper_types.hpp"
#include "common_container_functions.hpp"

namespace cues
{
namespace test
{

struct nonmovable_uints
{
    nonmovable_uints() = delete;
    nonmovable_uints(std::uint_least32_t a, std::uint_least16_t b, std::uint_least16_t c)
    : data_a(a), data_b(b), data_c(c)
    {}

    nonmovable_uints(nonmovable_uints const &) = delete;
    nonmovable_uints(nonmovable_uints &&) = delete;

    nonmovable_uints & operator=(nonmovable_uints const &) = delete;
    nonmovable_uints & operator=(nonmovable_uints &&) = delete;

    std::uint_least32_t data_a;
    std::uint_least16_t data_b;
    std::uint_least16_t data_c;
};

using nonmovable_uints_raw_data_type = std::tuple<std::uint_least32_t, std::uint_least16_t, std::uint_least16_t>;

// returns (consumer_period / period) + 2 last results (not necessarily in order)
std::vector<nonmovable_uints_raw_data_type> nonmovable_producer(cues::triple_buffer<nonmovable_uints> &, std::atomic<bool> const & running_flag, std::chrono::milliseconds period, std::chrono::milliseconds consumer_period);

// returns last consumed data
nonmovable_uints_raw_data_type nonmovable_consumer(cues::triple_buffer<nonmovable_uints> &, std::atomic<bool> const & running_flag, std::chrono::milliseconds period);

// returns single relevant character of the last (consumer_period / period) + 2 results (not necessarily in order)
std::vector<char> char_vec_producer(cues::triple_buffer<std::vector<char>> &, std::atomic<bool> const & running_flag, std::chrono::milliseconds period, std::chrono::milliseconds consumer_period);

// returns single relevant character of the last consumed data
char char_vec_consumer(cues::triple_buffer<std::vector<char>> &, std::atomic<bool> const & running_flag, std::chrono::milliseconds );

std::vector<char> char_arr_producer(cues::triple_buffer<unsigned char [256]> &, std::atomic<bool> const & running_flag, std::chrono::milliseconds period, std::chrono::milliseconds consumer_period);

char char_arr_consumer(cues::triple_buffer<unsigned char [256]> &, std::atomic<bool> const & running_flag, std::chrono::milliseconds );

counted_counter_type assertion_failures_sequential_triple_buffer_multidimensional_array_test();


template<class CONTAINED_TYPE, class PRODUCER_FUNCTION, class CONSUMER_FUNCTION>
counted_counter_type assertion_failures_last_value_check(PRODUCER_FUNCTION * producer, std::chrono::milliseconds producer_period, CONSUMER_FUNCTION * consumer, std::chrono::milliseconds consumer_period);


constexpr std::size_t producer_history_size(std::chrono::milliseconds period, std::chrono::milliseconds consumer_period)
{
    return 2u + ((consumer_period / period) > 0 ? (consumer_period / period) : 1u);
}

std::vector<char> char_vec_producer(cues::triple_buffer<std::vector<char>> & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period, std::chrono::milliseconds consumer_period)
{
    std::vector<char> retval;
    std::size_t const history_size = producer_history_size( period, consumer_period);
    std::size_t vector_idx;
    counted_counter_type counter = 0;
    char temp;

    retval.reserve(history_size);

    for (counter = 0;
    counter < 1000 && std::atomic_load_explicit(&running_flag, std::memory_order_acquire);
    ++counter)
    {
        vector_idx = counter % history_size;
        temp = (char)((counter % (126-48)) + 48);

        if (vector_idx >= retval.size())
        {
            retval.push_back(temp);
        }
        else
        {
            retval[vector_idx] = temp;
        }

        buffer.emplace_back({'C','h','a','r',' ','d','a','t','a',' ','"',temp,'"','.','\n'}, std::allocator<char>());


        std::this_thread::sleep_for(period);
    }

    return retval;
}

char char_vec_consumer(cues::triple_buffer<std::vector<char>> & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period)
{

    std::vector<char > local_copy;
    counted_counter_type counter = 0;
    char retval = 0;

    for (counter = 0;
    counter < 100000 && std::atomic_load_explicit(&running_flag, std::memory_order_acquire);
    ++counter)
    {
        if (buffer.has_unconsumed_data())
        {
            local_copy = std::move(buffer.consume());
        }

        std::this_thread::sleep_for(period);
    }

    // pull it into a string
    static std::string const prepend = "Char data \"";
    std::string stringified_data{local_copy.cbegin(), local_copy.cend()};
    std::size_t pos = stringified_data.find(prepend);
    assert(((pos != std::string::npos) && "Unexpected mismatch in data."));
    if (pos != std::string::npos)
    {
        // get the next character after the prepend
        pos = pos + prepend.size();
        assert(((pos < stringified_data.size()) && "unexpected truncation of data."));
        if (pos < stringified_data.size())
        {
            retval = stringified_data[pos];
        }
    }

    return retval;
}

std::vector<char> char_arr_producer(cues::triple_buffer<unsigned char [256]> & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period, std::chrono::milliseconds consumer_period)
{
    std::vector<char> retval;
    std::size_t const history_size = producer_history_size( period, consumer_period);
    std::size_t vector_idx;
    counted_counter_type counter = 0;
    char const prefix[] = "Char data \"";
    char const postfix[] = "\".\n";
    unsigned char * arr_ptr_begin;
    unsigned char * arr_ptr_working;
    char temp;

    for (counter = 0;
    counter < 1000 && std::atomic_load_explicit(&running_flag, std::memory_order_acquire);
    ++counter)
    {
        vector_idx = counter % history_size;
        temp = (counter % (126-48)) + 48;

        if (vector_idx >= retval.size())
        {
            retval.push_back(temp);
        }
        else
        {
            retval[vector_idx] = temp;
        }

        {
            cues::triple_buffer<unsigned char [256]>::access_token char_arr_token(std::move(buffer.construct_data()));
            arr_ptr_begin = static_cast<unsigned char *>(*char_arr_token);
            arr_ptr_working = std::copy(std::begin(prefix), std::end(prefix)-1, arr_ptr_begin);

            std::fill(arr_ptr_working, arr_ptr_working + 32u, temp);
            arr_ptr_working = arr_ptr_working + 32;
            std::copy(std::begin(postfix), std::end(postfix), arr_ptr_working);

            buffer.commit(std::move(char_arr_token));
        }

        std::this_thread::sleep_for(period);
    }


    return retval;
}

char char_arr_consumer(cues::triple_buffer<unsigned char [256]> & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period)
{
    counted_counter_type counter = 0;
    std::size_t pos;
    char retval = 0;

    for (counter = 0;
    counter < 100000 && std::atomic_load_explicit(&running_flag, std::memory_order_acquire);
    ++counter)
    {
        if (buffer.has_unconsumed_data())
        {
            std::basic_string_view<unsigned char> stringified(static_cast<unsigned char const *>(*(buffer.peek_front())));
            pos = stringified.find('"');

            assert(((pos != std::string::npos) && "Unexpected data in buffer."));
            if (pos != std::string::npos)
            {
                ++pos;
                assert(((pos < stringified.size()) && "unexpected truncation in data"));
                if (pos < stringified.size())
                {
                    retval = stringified[pos];
                }
            }

            buffer.pop_front();
        }

        std::this_thread::sleep_for(period);
    }

    return retval;
}

std::vector<nonmovable_uints_raw_data_type> nonmovable_producer(cues::triple_buffer<nonmovable_uints> & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period, std::chrono::milliseconds consumer_period)
{

    std::size_t const history_size = producer_history_size( period, consumer_period);
    std::vector<nonmovable_uints_raw_data_type> retval;
    counted_counter_type counter = 0;
    std::size_t vector_idx;
    uint_least32_t temp_a;
    uint_least16_t temp_b;
    uint_least16_t temp_c;

    retval.reserve(history_size);

    for (counter = 0;
    counter < 1000 && std::atomic_load_explicit(&running_flag, std::memory_order_acquire);
    ++counter)
    {
        {
            temp_a = counter;
            temp_b = counter % 97 + 3;
            temp_c = counter % 51 + 17;
            vector_idx = counter % history_size;
            if (vector_idx >= retval.size())
            {
                retval.emplace_back(std::make_tuple(temp_a, temp_b, temp_c));
            }
            else
            {
                retval[vector_idx] = std::make_tuple(temp_a, temp_b, temp_c);
            }

            buffer.emplace_back(temp_a, temp_b, temp_c );
        }

        std::this_thread::sleep_for(period);
    }

    return retval;
}

nonmovable_uints_raw_data_type nonmovable_consumer(cues::triple_buffer<nonmovable_uints> & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period)
{
    counted_counter_type counter = 0;

    nonmovable_uints const * access_ptr;

    nonmovable_uints_raw_data_type retval;

    for (counter = 0;
    counter < 1000000 && std::atomic_load_explicit(&running_flag, std::memory_order_acquire);
    ++counter)
    {
        if (buffer.has_unconsumed_data())
        {
            access_ptr = &(*(buffer.peek_front()));

            retval = std::make_tuple(access_ptr->data_a, access_ptr->data_b, access_ptr->data_c);

            buffer.pop_front();
        }

        std::this_thread::sleep_for(period);
    }

    return retval;
}

void fill_multidimensional_char_array(cues::array_proxy<char [8][16][32]> target, std::uniform_int_distribution<char> & distribution, std::mt19937 engine)
{
    static_assert(sizeof(target) <= (std::numeric_limits<std::intmax_t>::max)(), "array size too large for std::div");
    std::size_t indexA, indexB, indexC;
    for (std::size_t flat_index = 0;
        flat_index < sizeof(target)/sizeof(target[0][0][0]);
        ++flat_index)
    {
        auto result = std::div(static_cast<std::intmax_t>(flat_index), static_cast<std::intmax_t>(sizeof(target[0][0])/sizeof(target[0][0][0])));
        indexC = result.rem;
        result = std::div(result.quot, static_cast<std::intmax_t>(sizeof(target[0])/sizeof(target[0][0])));
        indexB = result.rem;
        indexA = result.quot;

        target[indexA][indexB][indexC] = distribution(engine);
    }

}

counted_counter_type assertion_failures_sequential_triple_buffer_multidimensional_array_test()
{
    counted_counter_type running_assertion_failures = 0;
    cues::triple_buffer<char[8][16][32]> buffer;
    // seeding with 0 so that the pseudo-random output is predictable on every test,
    // which simplifies comparing results when debugging from one test to another.
    // a very poor random number engine would suffice here, we're only using random numbers
    // at all to decrease the likelihood that unused memory somehow got the numeric sequence
    // used in the test, as might be more likely with for example a monotonically increasing sequence
    std::mt19937 rng_engine(0);
    // full range of char is acceptable, we don't need to print any of this.
    std::uniform_int_distribution<char> rng_distribution;
    char raw_data[8][16][32];
    cues::array_proxy<decltype(raw_data)> proxy_to_raw(raw_data);

    fill_multidimensional_char_array(proxy_to_raw, rng_distribution, rng_engine);

    if (buffer.has_unconsumed_data())
    {
        ++running_assertion_failures;
        assert((0 == running_assertion_failures && "default buffer should be empty."));
    }

    {
        auto write_token = buffer.construct_data();
        *write_token = proxy_to_raw;
        buffer.commit(std::move(write_token));
    }

    if (buffer.empty())
    {
        ++running_assertion_failures;
        assert((0 == running_assertion_failures && "buffer should not be empty after publishing."));
    }

    {
        auto read_token = buffer.peek_front();
        // verify that peek did not consume data
        if (buffer.empty())
        {
            ++running_assertion_failures;
            assert((0 == running_assertion_failures && "peek should not consume data."));
        }

        if (*read_token != proxy_to_raw)
        {
            ++running_assertion_failures;
            assert((0 == running_assertion_failures && "buffer did not read first publishing."));
        }
        buffer.mark_consumed(std::move(read_token));
    }

    // that was the only thing published, now the buffer should report itself empty

    if (!buffer.empty())
    {
        ++running_assertion_failures;
        assert((0 == running_assertion_failures && "buffer should be empty after consuming published data."));
    }

    // test that we don't see old data
    // first, writes that we don't expect to see
    for (std::size_t itrs = 0; itrs < 5; ++itrs)
    {
        auto write_token = buffer.construct_data();
        fill_multidimensional_char_array(*write_token, rng_distribution, rng_engine);
        buffer.commit(std::move(write_token));
    }
    // now one that we do expect to see
    {
        auto write_token = buffer.construct_data();
        fill_multidimensional_char_array(proxy_to_raw, rng_distribution, rng_engine);
        *write_token = proxy_to_raw;
        buffer.commit(std::move(write_token));
    }

    if (buffer.empty())
    {
        ++running_assertion_failures;
        assert((0 == running_assertion_failures && "buffer should not be empty after publishing."));
    }

    {
        auto read_token = buffer.peek_front();

        if (*read_token != proxy_to_raw)
        {
            ++running_assertion_failures;
            assert((0 == running_assertion_failures && "buffer did not read most recent publishing."));
        }
        buffer.mark_consumed(std::move(read_token));
    }
    // although we published multiple times, we should not read old data

    if (buffer.has_unconsumed_data())
    {
        ++running_assertion_failures;
        assert((0 == running_assertion_failures && "buffer allowed read of old data."));
    }

    {
        auto write_token = buffer.construct_data();
        buffer.roll_back(std::move(write_token));
        if (buffer.has_unconsumed_data())
        {
            ++running_assertion_failures;
            assert((0 == running_assertion_failures && "rollback committed data instead."));
        }

    }


    return running_assertion_failures;

}


template<class CONTAINED_TYPE, class PRODUCER_FUNCTION, class CONSUMER_FUNCTION>
counted_counter_type assertion_failures_last_value_check(PRODUCER_FUNCTION * producer, std::chrono::milliseconds producer_period, CONSUMER_FUNCTION * consumer, std::chrono::milliseconds consumer_period)
{
    counted_counter_type assertion_failures = 0;
     // using std::packaged_task instead of std::asynch so we can more directly control threads
    std::packaged_task<PRODUCER_FUNCTION> producer_task(producer);
    std::packaged_task<CONSUMER_FUNCTION> consumer_task(consumer);
    cues::triple_buffer<CONTAINED_TYPE> buffer;
    std::atomic<bool> keep_running = true;

    auto write_future = producer_task.get_future();
    auto read_future  = consumer_task.get_future();


    std::thread consumer_thread(
        std::move(consumer_task),
        std::ref(buffer),
        std::ref(keep_running),
        consumer_period
    );

    std::thread producer_thread(
         std::move(producer_task),
         std::ref(buffer),
         std::ref(keep_running),
         producer_period,
         consumer_period
    );
    producer_thread.join();
    atomic_store_explicit(&keep_running, false, std::memory_order_release);
    consumer_thread.join();

    auto most_recent_writes = write_future.get();
    auto most_recent_read   = read_future.get();

    if (most_recent_writes.cend() == std::find(most_recent_writes.cbegin(), most_recent_writes.cend(), most_recent_read))
    {++assertion_failures;}

    return assertion_failures;
}

template
counted_counter_type assertion_failures_last_value_check<nonmovable_uints, decltype(nonmovable_producer), decltype(nonmovable_consumer)>(
    decltype(nonmovable_producer) *,
    std::chrono::milliseconds ,
    decltype(nonmovable_consumer) *,
    std::chrono::milliseconds
);

template
counted_counter_type assertion_failures_last_value_check<std::vector<char>, decltype(char_vec_producer), decltype(char_vec_consumer)>(
    decltype(char_vec_producer) *,
    std::chrono::milliseconds ,
    decltype(char_vec_consumer) *,
    std::chrono::milliseconds
);

template
counted_counter_type assertion_failures_last_value_check<unsigned char [256], decltype(char_arr_producer), decltype(char_arr_consumer)>(
    decltype(char_arr_producer) *,
    std::chrono::milliseconds ,
    decltype(char_arr_consumer) *,
    std::chrono::milliseconds
);

counted_counter_type check_triple_buffer_test_runtime_assertions()
{
    static constexpr unsigned int const fast_milliseconds = 1;
    static constexpr unsigned int const slow_milliseconds = 3;

    counted_counter_type assertion_failures = 0;

    {
        assertion_failures += cues::test::assertion_failures_last_value_check<
            cues::test::nonmovable_uints,
            decltype(cues::test::nonmovable_producer),
            decltype(cues::test::nonmovable_consumer)
        >(
              cues::test::nonmovable_producer,
              std::chrono::milliseconds(fast_milliseconds),
              cues::test::nonmovable_consumer,
              std::chrono::milliseconds(slow_milliseconds)
        );
        assert(((0 == assertion_failures) && ("triple_buffer did not read a value in the most recent writes with producer faster than consumer.")));

        // as above, but now producer is slower than consumer
        assertion_failures += cues::test::assertion_failures_last_value_check<
            cues::test::nonmovable_uints,
            decltype(cues::test::nonmovable_producer),
            decltype(cues::test::nonmovable_consumer)
        >(
              cues::test::nonmovable_producer,
              std::chrono::milliseconds(slow_milliseconds),
              cues::test::nonmovable_consumer,
              std::chrono::milliseconds(fast_milliseconds)
        );
        assert(((0 == assertion_failures) && ("triple_buffer did not read a value in the most recent writes with consumer faster than producer.")));
    }

    // test with vector<char>
    assertion_failures += cues::test::assertion_failures_last_value_check<
        std::vector<char>,
        decltype(cues::test::char_vec_producer),
        decltype(cues::test::char_vec_consumer)
    >(
          cues::test::char_vec_producer,
          std::chrono::milliseconds(fast_milliseconds),
          cues::test::char_vec_consumer,
          std::chrono::milliseconds(slow_milliseconds)
    );
    assert(((0 == assertion_failures) && ("triple_buffer did not read a value in the most recent writes for buffer of vector<char>.")));

    assertion_failures += cues::test::assertion_failures_last_value_check<
        unsigned char [256],
        decltype(cues::test::char_arr_producer),
        decltype(cues::test::char_arr_consumer)
    >(
          cues::test::char_arr_producer,
          std::chrono::milliseconds(fast_milliseconds),
          cues::test::char_arr_consumer,
          std::chrono::milliseconds(slow_milliseconds)
    );
    assert(((0 == assertion_failures) && ("triple_buffer did not read a value in the most recent writes for buffer of unsigned char[].")));

    assertion_failures += cues::test::assertion_failures_sequential_triple_buffer_multidimensional_array_test();
    assert(((0 == assertion_failures) && ("triple_buffer did not pass sequential correctness test with multidimensional array.")));

    using leaktest_triple_buffer = cues::triple_buffer<cues::test::counted<counted_counter_type>>;

    static_assert(
        std::is_constructible<
            typename leaktest_triple_buffer::value_type, cues::test::counted<counted_counter_type>
        >::value,
        "Failed move constructible sanity check."
    );

    static_assert(
      std::is_same<typename std::enable_if<
        std::is_constructible<
            typename leaktest_triple_buffer::value_type,
            typename std::add_rvalue_reference<cues::test::counted<counted_counter_type>>::type
        >::value,
        bool
        >::type,
      bool
      >::value,
      "enable_if didn't work"
    );

    static_assert(has_emplace_back<
      leaktest_triple_buffer,
      void,
      typename std::add_rvalue_reference<cues::test::counted<counted_counter_type>>::type
    >::value,
    "triple buffer should have emplace_back");



    static_assert(has_construct_data<
      leaktest_triple_buffer,
      leaktest_triple_buffer::access_token
    >::value,
    "triple_buffer should have construct_data");

    assertion_failures += cues::test::container_leak_test_failures<cues::triple_buffer<cues::test::counted<counted_counter_type>>, in_place_t>(
          std::chrono::milliseconds(fast_milliseconds),
          std::chrono::milliseconds(slow_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in triple_buffer, faster producer.")));

    assertion_failures += cues::test::container_leak_test_failures<cues::triple_buffer<cues::test::counted<counted_counter_type>>, in_place_t>(
          std::chrono::milliseconds(slow_milliseconds),
          std::chrono::milliseconds(fast_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in triple_buffer, faster consumer.")));

    assertion_failures += cues::test::container_leak_test_failures<cues::triple_buffer<cues::test::random_failure_then_counted>, in_place_t>(
          std::chrono::milliseconds(fast_milliseconds),
          std::chrono::milliseconds(slow_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in triple_buffer, random exception before counting, faster producer.")));

    assertion_failures += cues::test::container_leak_test_failures<cues::triple_buffer<cues::test::random_failure_then_counted>, in_place_t>(
          std::chrono::milliseconds(slow_milliseconds),
          std::chrono::milliseconds(fast_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in triple_buffer, random exception before counting, faster consumer.")));

    assertion_failures += cues::test::container_leak_test_failures<cues::triple_buffer<cues::test::random_failure_then_counted>, in_place_t>(
          std::chrono::milliseconds(fast_milliseconds),
          std::chrono::milliseconds(slow_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in triple_buffer, counting before random exception, faster producer.")));

    assertion_failures += cues::test::container_leak_test_failures<cues::triple_buffer<cues::test::random_failure_then_counted>, in_place_t>(
          std::chrono::milliseconds(slow_milliseconds),
          std::chrono::milliseconds(fast_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in triple_buffer, counting before random exception, faster consumer.")));

    return assertion_failures;
}

}
}
#endif
