#ifndef CUES_TEST_ARRAY_PROXY_TEST_HPP_INCLUDED
#define CUES_TEST_ARRAY_PROXY_TEST_HPP_INCLUDED

#include <cstddef>
#include "cues/version_helpers.hpp"

#if (__cplusplus < 201703L && !defined ( __cpp_lib_launder) )
    #pragma message "NOTE: skipping test of cues::array_proxy because it requires std::launder, which is not available."
    #define CUES_TEST_ARRAY_PROXY 0
    namespace cues
    {
    namespace test
    {
        std::size_t check_array_proxy_runtime_assertions();
    }
    }
#else
    #define CUES_TEST_ARRAY_PROXY 1
#include <cstdint>
#include <type_traits>
#include <new>

#include "cues/array_proxy.hpp"
#include "cues/version_helpers.hpp"

namespace cues
{
namespace test
{

    using u32_proxy_array_type    = std::uint_least32_t[32];
    using u16_proxy_3darray_type  = std::uint_least16_t[4][8][16];
    using u16_proxy_subarray_type = std::uint_least16_t[16];

    // raw type should not be const
    static_assert(std::is_same<cues::array_proxy<u32_proxy_array_type>, cues::array_proxy<u32_proxy_array_type, false>>::value,
                  "Assertion violation: did not capture type as intended.");

    static_assert(std::is_same<cues::array_proxy<u16_proxy_3darray_type>, cues::array_proxy<u16_proxy_3darray_type, false>>::value,
                  "Assertion violation: did not capture type as intended.");

    static_assert(std::is_same<cues::array_proxy<typename std::add_const<u32_proxy_array_type>::type>,
                   cues::array_proxy<typename std::add_const<u32_proxy_array_type>::type, true>>::value,
                  "Assertion violation: did not capture const type as intended.");

    static_assert(std::is_same<cues::array_proxy<u16_proxy_3darray_type const>,
                  cues::array_proxy<u16_proxy_3darray_type const, true>>::value,
                  "Assertion violation: did not capture const type as intended.");

    static_assert(std::is_same<typename cues::array_proxy<u32_proxy_array_type>::convertable_iterator_type::proxy_element_type,
                  typename std::remove_extent<u32_proxy_array_type>::type
                  >::value, "proxying wrong type");

    static_assert(std::is_same<typename cues::array_proxy<u16_proxy_3darray_type>::convertable_iterator_type::proxy_element_type,
                  typename std::remove_extent<u16_proxy_3darray_type>::type
                  >::value, "proxying wrong type");

    static_assert( cues::array_proxy<u32_proxy_array_type>::rank() == std::rank<u32_proxy_array_type>::value,
                  "incorrect rank capture");

    static_assert( cues::array_proxy<u16_proxy_3darray_type>::rank() == std::rank<u16_proxy_3darray_type>::value,
                  "incorrect rank capture");

    static_assert( cues::array_proxy<u32_proxy_array_type>::extent() == std::extent<u32_proxy_array_type,0>::value,
                  "incorrect size capture");

    static_assert( cues::array_proxy<u16_proxy_3darray_type>::extent() == std::extent<u16_proxy_3darray_type,0>::value,
                  "incorrect size capture");
    static_assert( cues::array_proxy<typename std::remove_extent<u16_proxy_3darray_type>::type>::extent()
                   == std::extent<u16_proxy_3darray_type,1>::value,
                  "incorrect size capture");

    static_assert( cues::array_proxy<
                        typename std::remove_extent<
                            typename std::remove_extent<u16_proxy_3darray_type>::type
                        >::type
                    >::extent()
                   == std::extent<u16_proxy_3darray_type,2>::value,
                  "incorrect size capture");

    static_assert(std::is_same<
                    typename cues::array_proxy_iterator<u16_proxy_3darray_type>::reference,
                    cues::array_proxy<typename std::remove_extent<u16_proxy_3darray_type>::type>
                  >::value,
                    "incorrect sub-proxy"
                  );

    void fill_3d_forward_via_bracket( cues::array_proxy<cues::test::u16_proxy_3darray_type> & proxy);
    void fill_3d_forward_via_iterator( cues::array_proxy<cues::test::u16_proxy_3darray_type> & proxy);
    std::size_t validate_fill_3d_forward_via_iterator(cues::array_proxy<cues::test::u16_proxy_3darray_type const> const & proxy);
    std::size_t validate_fill_3d_forward_via_bracket(cues::array_proxy<cues::test::u16_proxy_3darray_type const> const & proxy);

    // helper class to make sure objects exist in fake array
    template<class T>
    class default_placement_RAII
    {
    public:

        default_placement_RAII(cues::raw_byte_type * begin, std::size_t elements)
        : m_begin(begin)
        , m_elements(elements)
        {
            using iterator = cues::raw_byte_type *;
            for (iterator itr = begin, end = begin+sizeof(T)*elements;
                itr != end;
                itr += sizeof(T)
            )
            {
                new (itr) T{};
            }
        }

        default_placement_RAII() = delete;
        default_placement_RAII(default_placement_RAII const &) = delete;
        default_placement_RAII(default_placement_RAII &&) = delete;

        ~default_placement_RAII()
        {
            using iterator = cues::raw_byte_type *;
            for (iterator itr = m_begin, end = m_begin+sizeof(T)*m_elements;
                itr != end;
                itr += sizeof(T)
            )
            {
               std::launder(reinterpret_cast<T *>(itr))->~T();
            }
        }

    private:
        cues::raw_byte_type * m_begin;
        std::size_t m_elements;
    };

    std::size_t check_array_proxy_runtime_assertions();

}
}

#endif
#endif // CUES_TEST_ARRAY_PROXY_TEST_HPP_INCLUDED
