#ifndef CUES_TEST_PAIR_UNSIGNED_HASH_TEST_HPP
#define CUES_TEST_PAIR_UNSIGNED_HASH_TEST_HPP

#include <cstddef>
#include <cstdint>
#include <type_traits>

#include "cues/pair_unsigned_hash.hpp"
#include "cues/wrapped_number.hpp"

namespace cues {
namespace test {

// std::hash is not required to be constexpr
template<typename T>
class constexpr_trivial_hasher
{
public:
    static_assert(
        std::is_constructible<std::size_t, T>::value, "constexpr trivial hasher expects to be able to be able to create std::size_t from T"
    );
    constexpr std::size_t operator()(T const & val) const noexcept
    {
        return static_cast<std::size_t>(val);
    }

};

enum weak_enum_type
{
    placeholder  = 0u,
    limit_forcer = 100000ul
};

enum class strong_enum_type : std::uint_least8_t
{
    min_val = 0u
};

// smallest pair
using test_pair_type_1 = std::pair<std::uint_least8_t, std::uint_least8_t> ;
// possibly introduce padding, different type sizes
using test_pair_type_2 = std::pair<std::uint_least8_t, std::uint_least16_t> ;
using test_pair_type_3 = std::pair<std::uint_least16_t, std::uint_least8_t> ;
// largest pair guaranteed to fit into uint_least32_t
using test_pair_type_4 = std::pair<std::uint_least16_t, std::uint_least16_t> ;
// larger gap
using test_pair_type_5 = std::pair<std::uint_least8_t, std::uint_least32_t> ;
using test_pair_type_6 = std::pair<std::uint_least32_t, std::uint_least8_t> ;
// largest combination guaranteed to fit into uint_least64_t
using test_pair_type_7 = std::pair<std::uint_least32_t, std::uint_least32_t> ;

using nonfundamental_type_1 = cues::wrapped_number<std::uint_least8_t, 0, std::hash<std::uint_least8_t>>;
using nonfundamental_type_2 = cues::wrapped_number<std::uint_least32_t, 0, std::hash<std::uint_least32_t>>;
using nonfundamental_type_3 = cues::wrapped_number<std::uint_least32_t, 1, std::hash<std::uint_least32_t>>;


// not fundamental integer
using test_pair_type_8 = std::pair<std::uint_least8_t, nonfundamental_type_1>;
using test_pair_type_9 = std::pair<nonfundamental_type_1, std::uint_least8_t>;
using test_pair_type_10 = std::pair<nonfundamental_type_1, nonfundamental_type_1>;
using test_pair_type_11 = std::pair<std::uint_least8_t, nonfundamental_type_2>;
using test_pair_type_12 = std::pair<nonfundamental_type_2, std::uint_least8_t>;
using test_pair_type_13 = std::pair<nonfundamental_type_2, nonfundamental_type_3>;

using test_pair_type_14 = std::pair<std::uint_least8_t, weak_enum_type>;
using test_pair_type_15 = std::pair<weak_enum_type, std::uint_least32_t>;
using test_pair_type_16 = std::pair<weak_enum_type, weak_enum_type>;

using test_pair_type_17 = std::pair<weak_enum_type, strong_enum_type>;
using test_pair_type_18 = std::pair<std::uint_least32_t, strong_enum_type>;
using test_pair_type_19 = std::pair<strong_enum_type, std::uint_least16_t>;

template<typename DUT>
struct pair_hash_tester
{
    using dut_first_type = typename std::remove_reference<decltype(std::declval<DUT>().first)>::type;
    using dut_second_type = typename std::remove_reference<decltype(std::declval<DUT>().second)>::type;
    using dut_first_limit_type = typename cues::type_or_underlying<dut_first_type>::type;
    using dut_second_limit_type = typename cues::type_or_underlying<dut_second_type>::type;

    static constexpr std::size_t hash_0 = cues::pair_unsigned_hash<DUT, constexpr_trivial_hasher>()(
        DUT(
            static_cast<dut_first_type>(0u),
            static_cast<dut_second_type>(0u)
        )
    );

    static constexpr std::size_t hash_xx = cues::pair_unsigned_hash<DUT, constexpr_trivial_hasher>()(
        DUT(
            static_cast<dut_first_type>((std::numeric_limits<dut_first_limit_type>::max)()),
            static_cast<dut_second_type>((std::numeric_limits<dut_second_limit_type>::max)())
        )
    );

    static constexpr std::size_t hash_nx = cues::pair_unsigned_hash<DUT, constexpr_trivial_hasher>()(
        DUT(
            static_cast<dut_first_type>((std::numeric_limits<dut_first_limit_type>::min)()),
            static_cast<dut_second_type>((std::numeric_limits<dut_second_limit_type>::max)())
        )
    );

    static constexpr std::size_t hash_xn = cues::pair_unsigned_hash<DUT, constexpr_trivial_hasher>()(
        DUT(
            static_cast<dut_first_type>((std::numeric_limits<dut_first_limit_type>::max)()),
            static_cast<dut_second_type>((std::numeric_limits<dut_second_limit_type>::min)())
        )
    );

    static_assert(hash_0 != hash_xx, "unexpected hash collision");
    static_assert(hash_0 != hash_xn, "unexpected hash collision");
    static_assert(hash_0 != hash_nx , "unexpected hash collision");
    static_assert(hash_nx != hash_xn, "unexpected hash collision");
    static_assert(hash_nx != hash_xx, "unexpected hash collision");
    static_assert(hash_xn != hash_xx , "unexpected hash collision");
};

constexpr pair_hash_tester<test_pair_type_1 > pair_hash_test_1;
constexpr pair_hash_tester<test_pair_type_2 > pair_hash_test_2;
constexpr pair_hash_tester<test_pair_type_3 > pair_hash_test_3;
constexpr pair_hash_tester<test_pair_type_4 > pair_hash_test_4;
constexpr pair_hash_tester<test_pair_type_5 > pair_hash_test_5;
constexpr pair_hash_tester<test_pair_type_6 > pair_hash_test_6;
constexpr pair_hash_tester<test_pair_type_7 > pair_hash_test_7;
constexpr pair_hash_tester<test_pair_type_8 > pair_hash_test_8;
constexpr pair_hash_tester<test_pair_type_9 > pair_hash_test_9;
constexpr pair_hash_tester<test_pair_type_10> pair_hash_test_10;
constexpr pair_hash_tester<test_pair_type_11> pair_hash_test_11;
constexpr pair_hash_tester<test_pair_type_12> pair_hash_test_12;
constexpr pair_hash_tester<test_pair_type_13> pair_hash_test_13;
constexpr pair_hash_tester<test_pair_type_14> pair_hash_test_14;
constexpr pair_hash_tester<test_pair_type_15> pair_hash_test_15;
constexpr pair_hash_tester<test_pair_type_16> pair_hash_test_16;
constexpr pair_hash_tester<test_pair_type_17> pair_hash_test_17;
constexpr pair_hash_tester<test_pair_type_18> pair_hash_test_18;
constexpr pair_hash_tester<test_pair_type_19> pair_hash_test_19;

} // namespace test
} // namespace cues

#endif // CUES_TEST_PAIR_UNSIGNED_HASH_TEST_HPP
