#ifndef CUES_UTF8_ITERATOR_HPP
#define CUES_UTF8_ITERATOR_HPP

#include "cues/version_helpers.hpp"

#include <cstddef>
#include <cstdint>
#include <array>
#include <iterator>
#include <stdexcept>
#include <algorithm>
#include <string>
#include <type_traits>

#if defined (CUES_HAS_STRING_VIEW)
#include <string_view>
#endif

#if __cplusplus >= 202002L || defined (__cpp_lib_constexpr_string)
    #define CUES_AS_CONSTEXPR_AS_STD_STRING constexpr
#else
    #define CUES_AS_CONSTEXPR_AS_STD_STRING
#endif

//! note: a random access (instead of bidirectional) iterator
//! is not actually required by the standard, as far as I can tell;
//! This is to work around a bug in gcc 10.3 or earlier
//! where operator--/operator++ for char const * is not always accepted as constexpr
//! even when specifying for c++14 or later when constexpr != const.
//! The only bug reports I have found for those versions deal with pointer comparison,
//! not decrement, but it was empirically confirmed to work on gcc 11.1, 11.2, 11.3, and 12.1
#if !defined (__clang__) && !defined(__INTEL_COMPILER) && defined(__GNUC__) && __GNUC__ < 11
    #define CUES_UTF8_NEEDS_RANDOM_ACCESS
#endif


#include "cues/type_traits.hpp"

namespace cues {

class invalid_utf8 : public std::runtime_error
{
    public:
    invalid_utf8( std::string const  & m )
    : std::runtime_error(m)
    {}

    invalid_utf8( char const * m )
    : std::runtime_error(m)
    {}
};

constexpr char32_t const unicode_replacement_code{0xFFFDul};
static constexpr unsigned char utf8_first_byte_data_mask_by_size[] = {0x7Fu, 0x1Fu, 0x0Fu, 0x07u};
static constexpr unsigned char continuation_mask_value = 0x3Fu;
static constexpr std::uint_least8_t byte_shift_value[] = {0,6,12,18};

//! 0 is not a valid byte length for a code point and is used to indicate
//! an invalid starting byte.
//! Note this only examines the first byte, it does not validate subsequent bytes.
template<typename char_type>
constexpr std::uint_least8_t expected_utf8_byte_length(char_type first_byte) noexcept
{
    static_assert(
        sizeof(char_type) == sizeof(char),
        "UTF-8 character type is expected to use the same size as char"
    );

    // UTF-8 table 3-7 from Unicode standard version 6
    // jumps from 7F to C2 in the first byte as permitted first bytes
    return (static_cast<unsigned char>(first_byte) < 0x80u
    || (static_cast<unsigned char>(first_byte) < 0xF5u && static_cast<unsigned char>(first_byte) > 0xC1u) ) ?
        static_cast<std::uint_least8_t>(
            // these values are based on Unicode standard version 6 table 3-6
            1u
            + static_cast<unsigned>(static_cast<unsigned char>(first_byte) > 0xEFu)
            + static_cast<unsigned>(static_cast<unsigned char>(first_byte) > 0xDFu)
            + static_cast<unsigned>(static_cast<unsigned char>(first_byte) > 0x7Fu)
        )
        :
        static_cast<std::uint_least8_t>(0)
    ;
}

//! REQUIREMENT: code point is valid per Unicode 6
constexpr std::uint_least8_t expected_utf8_byte_length(char32_t code_point)
{
    return (code_point < 0x80u) ?
    1
    : (
        (code_point < 0x800u) ?
        2
        : (
            (code_point <= 0x10000ul) ?
            3
            : (
                (code_point <= 0x110000ul) ?
                4
                :
                throw invalid_utf8("code point outside range of Unicode 6")
            )
        )
    );
}
template<typename char_type>
constexpr std::uint_least8_t is_continuation_byte(char_type first_byte) noexcept
{
    static_assert(
        sizeof(char_type) == sizeof(char),
        "UTF-8 character type is expected to use the same size as char"
    );

    return (static_cast<unsigned char>(first_byte) & 0xC0u) == 0x80u;
}



//! Each of the following policies is permitted
//! as of Unicode 6 for ways to treat invalid UTF-8 sequenecs.

//! the harshest policy, but useful if you want a hard error if your string
//! contains any invalid utf-8
class throw_on_invalid_unicode;
//! replaces invalid sequences with replacement char U+FFFD;
//! recommended "best practice" as of Unicode 6
class replace_on_invalid_unicode;
//! gentlest policy; simply skips any and all invalid characters.
//! Leaves output clean, but also has no way to indicate that the source
//! contained any invalid data.
class skip_on_invalid_unicode;

//! Iterator that adapts single-byte char types as though they were unicode code points.
//! Advance will advance the full code point, dereference will return 32-bit code point.
//! Does not validate that the code point has a defined meaning, only that the source is
//! valid UTF-8 and can be interpreted as a valid code point value.
//! PRECONDITION: requires null-terminated string
template<typename BYTE_ITR = char *, class INVALID_CODE_POLICY = replace_on_invalid_unicode>
class utf8_const_iterator
{
    using underlying_iterator_type = BYTE_ITR;

    public:

    using underlying_iterator_category = typename std::iterator_traits<underlying_iterator_type>::iterator_category;
    static_assert(std::is_base_of<
            #ifdef CUES_UTF8_NEEDS_RANDOM_ACCESS
            std::random_access_iterator_tag,
            #else
            std::bidirectional_iterator_tag
            #endif
            underlying_iterator_category
        >::value,
        "iterator category not sufficient for utf8 adapter"
    );

    using char_type = typename std::iterator_traits<underlying_iterator_type>::value_type;

    static_assert(
        cues::is_character_type<char_type>::value && sizeof(char_type) == sizeof(char),
        "iterator must be to a character type the same size as char"
    );

    using value_type     = char32_t;
    using reference      = char32_t;
    using pointer        = void;
    using iterator_category = std::bidirectional_iterator_tag;
    using difference_type = std::ptrdiff_t;

    friend INVALID_CODE_POLICY;

    constexpr utf8_const_iterator() noexcept
    : uitr{}
    {}

    //! REQUIRES: data is null-terminated (specifically, last character,
    //! at position end-1, is null byte)
    constexpr utf8_const_iterator(
        underlying_iterator_type begin_itr,
        underlying_iterator_type end_itr
    )
    : uitr( (begin_itr == end_itr
        || (*(
           constexpr_advance_copy(end_itr,-1)
         ) == char_type{0})) ?
      begin_itr
      : throw std::invalid_argument("sequence ending in null character required")
    )
    {}

    //! PRECONDITION: underlying iterator is valid (not default-constructed)
    //! and dereferenceable (not an end iterator)
    explicit constexpr operator char_type const * () const noexcept
    {
        return &*uitr;
    }

    //! REQUIRES: pointer is not null
    constexpr char32_t operator*()
    {
        return (uitr != underlying_iterator_type{}) ?
        nonnull_dereference(expected_utf8_byte_length(*uitr))
        :
        throw std::invalid_argument("Cannot dereference a null iterator")
        ;
    }

    //! REQUIRES: pointer is not null
    CUES_CONSTEXPR_BUT_NOT_CONST utf8_const_iterator & operator++()
    {
        return (uitr != underlying_iterator_type{}) ?
        (
            // comma operator and operator|| are used to make sure
            // that we try to advance, invoke the policy only if
            // we see an invalid sequence, and then return a reference
            // regardless of the outcome (other than exceptions)
            (  advance_to_next_potentially_valid(expected_utf8_byte_length(*uitr))
               || (INVALID_CODE_POLICY{}.invalid_increment(*this), false)
             )
            ,
            *this
        )
        :
        throw std::invalid_argument("Cannot advance a null iterator")
        ;
    }

    CUES_CONSTEXPR_AFTER_CPP14 utf8_const_iterator operator++(int)
    {
        utf8_const_iterator tmp(*this);
        operator++();
        return tmp;
    }

    //! WARNING: unlike operator++, which benefits from the prerequisite that
    //! the string is null-terminated and thus can avoid exceeding the string boundary,
    //! operator-- has no sentinel to guard it; in particular this means that:
    //! for the skip policy, it is only safe to use operator--
    //! if you somehow know there is a valid UTF-8 character earlier
    //! in the string, for example from using operator++ on a valid code point,
    //! or use with a hard-coded string literal with valid code points.
    CUES_CONSTEXPR_BUT_NOT_CONST utf8_const_iterator & operator--()
    {
        return ((uitr != underlying_iterator_type{})) ?
        (
            // comma operator and operator|| are used to make sure
            // that we try to advance, invoke the policy only if
            // we see an invalid sequence, and then return a reference
            // regardless of the outcome (other than exceptions)
            (  retreat_to_previous_potentially_valid(expected_utf8_byte_length(*uitr))
               || (INVALID_CODE_POLICY{}.invalid_decrement(*this), false)
             )
            ,
            *this
        )
        :
        throw std::invalid_argument("Cannot advance a null iterator")
        ;

    }

    CUES_CONSTEXPR_AFTER_CPP14 utf8_const_iterator operator--(int)
    {
        utf8_const_iterator tmp(*this);
        operator--();
        return tmp;
    }

    //! for more tightly-coupled algorithms that want to bypass the policy
    //! to examine the underlying byte stream;
    //! operator++, operator--, and operator*
    //! already check validity and call the policy as appropriate.
    //! PRECONDITION: underlying iterator is either default-constructed
    //! or is dereferenceable
    constexpr bool is_valid() const noexcept
    {
        return uitr != underlying_iterator_type{}
        && validate_sequence(expected_utf8_byte_length(*uitr));
    }

    //! for more tightly-coupled algorithms that want to bypass the policy
    //! to examine the underlying byte stream;
    //! operator++, operator--, and operator*
    //! already check validity and call the policy as appropriate.
    //! PRECONDITION: underlying iterator is either default-constructed
    //! or is dereferenceable
    constexpr std::uint_least8_t maximal_subpart_length() const noexcept
    {
        return (uitr == underlying_iterator_type{}) ?
        0
        :
        valid_length(expected_utf8_byte_length(*uitr));
    }

    private:

    underlying_iterator_type uitr;

    // final byte of more than one byte, same for 2,3,and 4 bytes
    static constexpr unsigned char const general_successor_byte_range[] = {0x80u,0xC0u};

    template<typename ITR = underlying_iterator_type>
    static constexpr typename std::enable_if<
        std::is_base_of<
            std::random_access_iterator_tag,
            typename std::iterator_traits<ITR>::iterator_category
        >::value
    , ITR >::type & constexpr_advance(ITR & bg, std::ptrdiff_t distance)
    {
        return (
            (bg += static_cast<typename std::iterator_traits<ITR>::difference_type>(distance)),
            bg
        );
    }

    template<typename ITR = underlying_iterator_type>
    static constexpr typename std::enable_if<
        !std::is_base_of<
            std::random_access_iterator_tag,
            typename std::iterator_traits<ITR>::iterator_category
        >::value
    , ITR >::type & constexpr_advance(ITR & bg, typename std::iterator_traits<ITR>::difference_type distance)
    {
        return (distance == 0) ? bg :
        (
            (distance > 0) ?
            constexpr_advance_copy(++bg, --distance)
            :
            constexpr_advance_copy(--bg, ++distance)
        );
    }

    static constexpr underlying_iterator_type constexpr_advance_copy(underlying_iterator_type bg, std::ptrdiff_t distance)
    {
        return constexpr_advance(bg, distance);
    }

    // precondition: sequence is valid, and thus length > 0
    constexpr char32_t build_known_valid_sequence(std::uint_least8_t length) const noexcept
    {
        return
        (static_cast<char32_t>(static_cast<unsigned char>(*uitr) & utf8_first_byte_data_mask_by_size[length-1]) << byte_shift_value[length-1])
        | (
            (length < 2) ? 0 :
            (static_cast<char32_t>(*(
                constexpr_advance_copy(uitr,1)
            ) & continuation_mask_value) << byte_shift_value[length-2])
        )
        | (
           (length < 3) ? 0 :
           (static_cast<char32_t>(*(
            constexpr_advance_copy(uitr,2)
            ) & continuation_mask_value) << byte_shift_value[length-3])
        )
        | (
           (length < 4) ? 0 :
           (static_cast<char32_t>(*(
            constexpr_advance_copy(uitr,3)
            ) & continuation_mask_value) << byte_shift_value[length-4])
        );

    }

    CUES_CONSTEXPR_BUT_NOT_CONST char32_t nonnull_dereference(std::uint_least8_t length)
    noexcept( noexcept(INVALID_CODE_POLICY{}.invalid_dereference(*this) ))
    {
        return validate_sequence(length) ?
        build_known_valid_sequence(length)
        :
        INVALID_CODE_POLICY{}.invalid_dereference(*this)
        ;
    }

    // range follows the min is inclusive, max is exclusive paradigm of standard library functions
    constexpr bool validate_byte(
        unsigned char byte,
        unsigned char min_inclusive,
        unsigned char max_exclusive
    ) const noexcept
    {
        return
            byte >= min_inclusive
         && byte < max_exclusive
        ;
    }

    CUES_CONSTEXPR_BUT_NOT_CONST bool advance_and_validate_later_byte() noexcept
    {
        return validate_byte(
            static_cast<unsigned char>(*constexpr_advance(uitr, 1)),
            general_successor_byte_range[0],
            general_successor_byte_range[1]
        );
    }

    constexpr bool validate_second_byte(unsigned char first_byte, unsigned char second_byte) const noexcept
    {
        return
        // otherwise awkward nesting of conditionals allows constexpr even in c++11
        (first_byte == 0xE0u) ?
        validate_byte(second_byte,0xA0u,0xC0u)
        :
        (
            (first_byte == 0xEDu) ?
            validate_byte(second_byte,0x80u,0xA0u)
            :
            (
                (first_byte == 0xF0u) ?
                validate_byte(second_byte,0x90u,0xC0u)
                :
                (
                    (first_byte == 0xF4) ?
                    validate_byte(second_byte,0x80u,0x90u)
                    :
                    validate_byte(
                        second_byte,
                        general_successor_byte_range[0],
                        general_successor_byte_range[1]
                    )
                )
            )
        );
    }

    CUES_CONSTEXPR_BUT_NOT_CONST bool advance_and_validate_second_byte(unsigned char first_byte) noexcept
    {
        return validate_second_byte(first_byte, static_cast<unsigned char>(*constexpr_advance(uitr, 1)));
    }

    constexpr std::uint_least8_t valid_length(std::uint_least8_t expected_length) const noexcept
    {
        return (expected_length <= 1u) ?
        expected_length :
        (   // if expected_length != 0, the first byte is valid by itself
            1 +
            // at least equal to 2
            static_cast<std::uint_least8_t>(validate_second_byte(
                static_cast<unsigned char>(*uitr),
                static_cast<unsigned char>(*constexpr_advance_copy(uitr,1))
            ))
            + ((expected_length > 2u) ?
                static_cast<std::uint_least8_t>(validate_byte(
                    static_cast<unsigned char>(*constexpr_advance_copy(uitr,2)),
                    general_successor_byte_range[0],
                    general_successor_byte_range[1]
                ))
                :
                0
            )
            + ((expected_length > 3u) ?
               static_cast<std::uint_least8_t>(validate_byte(
                    static_cast<unsigned char>(*constexpr_advance_copy(uitr,3)),
                    general_successor_byte_range[0],
                    general_successor_byte_range[1]
                ))
                :
                0
            )
        );
    }

    constexpr bool validate_sequence(std::uint_least8_t distance) const noexcept
    {
        // known single byte is valid
        return distance != 0 && distance == valid_length(distance);
    }

    CUES_CONSTEXPR_BUT_NOT_CONST bool retreat_to_previous_potentially_valid(std::uint_least8_t cumulative_distance) noexcept
    {
        // continue decrementing while the byte could be a continuation byte
        return (++cumulative_distance, is_continuation_byte(*constexpr_advance(uitr, -1))) ?
        (
            (cumulative_distance < 4u) ?
            retreat_to_previous_potentially_valid(cumulative_distance)
            :
            false
        )
        : // not a continuation byte
        (
            // does the expected length match how many continuation bytes we saw?
            (cumulative_distance == expected_utf8_byte_length(*uitr)) ?
            // check that the sequence as a whole is valid for this length
            validate_sequence(cumulative_distance)
            : // if not, we passed some kind of invalid data
            false
        );
    }

    CUES_CONSTEXPR_BUT_NOT_CONST bool advance_to_next_potentially_valid(std::uint_least8_t distance) noexcept
    {
        // a single valid or invalid byte
        return (distance < 2u) ?
        (constexpr_advance(uitr, 1), (distance == 1u))
        :
        (
            // the boolean result we want to return
            (
                // short-circuiting is ok here because if a byte is invalid, we want to check the byte
                // after it to see if that byte could be a new valid sequence, instead of continuing to skip
                // also conveniently gives sequence points for advancing the iterator
                (advance_and_validate_second_byte(static_cast<unsigned char>(*uitr)))
                && (distance >= 3u ? advance_and_validate_later_byte() : true)
                && (distance >= 4u ? advance_and_validate_later_byte() : true)
            ) ?
            // we need one more advance to go past the end, whether it was 2,3, or 4,
            // if the result was valid; if it was invalid, we should stop because
            // we may have advanced to a byte that could be a valid start of a new sequence.
            // and this needs to happen after any other increments, so we need a sequence point,
            // but we want to apply this one unconditionally, so we use a
            // combination of the ? operator and , operator to return the bool we want
            // but also increment again before returning but after the previous checks
            (constexpr_advance(uitr, 1), true)
            :
            false
        );
    }
};

//! equality operators for iteration
template<typename itr_type, class LHS_POLICY, class RHS_POLICY>
constexpr bool operator==(utf8_const_iterator<itr_type, LHS_POLICY> const &lhs, utf8_const_iterator<itr_type, RHS_POLICY> rhs) noexcept
{
    using char_type = typename utf8_const_iterator<itr_type, LHS_POLICY>::char_type;
    return static_cast<char_type const *>(lhs) == static_cast<char_type const *>(rhs);
}

template<typename itr_type, class LHS_POLICY, class RHS_POLICY>
constexpr bool operator!=(utf8_const_iterator<itr_type, LHS_POLICY> lhs, utf8_const_iterator<itr_type, RHS_POLICY> rhs) noexcept
{
    return !(lhs == rhs);
}

//! general template, probably not constexpr prior to c++17
template<typename C, class INVALID_CODE_POLICY = replace_on_invalid_unicode>
struct utf8_iterator_factory
{
    using iterator_type = typename std::remove_reference<
        decltype(
            std::begin(std::declval<C const &>())
        )
    >::type;

    using char_type = typename std::remove_const<
        typename std::iterator_traits<iterator_type>::value_type
    >::type;

    static_assert(
        cues::is_iterator_to_contiguous_memory<
            iterator_type
        >::value,
        "construction of utf8_iterator requires contiguous sequence."
    );

    static_assert(
        cues::is_character_type<char_type>::value
        && sizeof(char_type) == 1,
        "utf8_iterator is meant to iterate single-byte characters."
    );

    static constexpr utf8_const_iterator<iterator_type, INVALID_CODE_POLICY> make_begin(C const & container)
    {
        return utf8_const_iterator<iterator_type, INVALID_CODE_POLICY>(
           std::begin(container),
           std::end(container)
        );
    }

    static constexpr utf8_const_iterator<iterator_type, INVALID_CODE_POLICY> make_end(C const & container)
    {
        return utf8_const_iterator<iterator_type, INVALID_CODE_POLICY>(
            std::end(container),
            std::end(container)
        );
    }
};

//! specialization for raw array, constexpr even in c++11, instead of waiting for c++14
template<typename char_type, std::size_t N, class INVALID_CODE_POLICY>
struct utf8_iterator_factory<char_type[N], INVALID_CODE_POLICY>
{
    static_assert(
        cues::is_character_type<typename std::remove_const<char_type>::type>::value,
        "Error: treating non-character as character."
    );

    static_assert(sizeof(char_type) == 1, "utf8_iterator is meant to iterate single-byte characters");

    static constexpr utf8_const_iterator<char_type const *, INVALID_CODE_POLICY> make_begin(char_type const (&arr)[N]) noexcept
    {
        return utf8_const_iterator<char_type const *, INVALID_CODE_POLICY>(&arr[0], &arr[0]+N);
    }

    static constexpr utf8_const_iterator<char_type const *, INVALID_CODE_POLICY> make_end(char_type const (&arr)[N]) noexcept
    {
        return utf8_const_iterator<char_type const *, INVALID_CODE_POLICY>(&arr[0]+N, &arr[0]+N);
    }
};

// helper function for std::array to be constexpr even in c++11
// std::begin and std::array::begin, std::array::operator[], std::array::data are not
// constexpr in c++11; this is for compatibility
template<typename T, std::size_t N>
constexpr T const * constexpr_std_array_begin(std::array<T,N> const & arr)
{
    static_assert(std::is_standard_layout<T>::value, "array value_type must be standard layout.");
    static_assert(
        sizeof(arr) == sizeof(T)*N,
        "buggy std::array implementation"
    );
    // std::array is specified to contain only T[N], and we just
    // covered that T is standard layout,
    // thus the std::array is also standard layout, being an aggregate of
    // an aggregate of a standard layout type.
    // we are thus allowed to reinterpret cast to the first member
    // (cast the pointer to std::array to a pointer to the raw array),
    // and then get a pointer to the first element from the raw array,
    // but reinterpret_cast is not allowed in constexpr functions,
    // so we static_cast through void (const) * instead
    return
        // cast the pointer to const void to a
        // pointer to array of N const T,
        // dereference pointer to get reference to array of const T,
        // get the 0th const T, then take its address
        &(
            (*static_cast<T const (*)[N]>(
                static_cast<void const *>(&arr)
            ))[0]
        );
}

//! specialization for std::array to be constexpr before c++14
template<typename char_type, std::size_t N, class INVALID_CODE_POLICY >
struct utf8_iterator_factory<std::array<char_type,N>, INVALID_CODE_POLICY>
{
    static_assert(
        cues::is_character_type<char_type>::value
        && sizeof(char_type) == 1,
        "utf8_iterator is meant to iterate single-byte characters."
    );

    static constexpr utf8_const_iterator<char_type const *, INVALID_CODE_POLICY> make_begin(std::array<char_type, N> const & arr) noexcept
    {
        return utf8_const_iterator<char_type const *, INVALID_CODE_POLICY>(
            constexpr_std_array_begin(arr),
            constexpr_std_array_begin(arr) + N
        );
    }
    static constexpr utf8_const_iterator<char_type const *, INVALID_CODE_POLICY> make_end(std::array<char_type, N> const & arr) noexcept
    {
        return utf8_const_iterator<char_type const *, INVALID_CODE_POLICY>(
            constexpr_std_array_begin(arr)+N,
            constexpr_std_array_begin(arr)+N
        );
    }
};

// function for convenient type deduction
template<typename C, class INVALID_CODE_POLICY = replace_on_invalid_unicode>
constexpr decltype(utf8_iterator_factory<C, INVALID_CODE_POLICY>::make_begin(std::declval<C>()))
utf8_begin(C const & container)
{
    return utf8_iterator_factory<C, INVALID_CODE_POLICY>::make_begin(container);
}

template<typename C, class INVALID_CODE_POLICY = replace_on_invalid_unicode >
constexpr decltype(utf8_iterator_factory<C, INVALID_CODE_POLICY>::make_end(std::declval<C>()))
utf8_end(C const & container)
{
    return utf8_iterator_factory<C, INVALID_CODE_POLICY>::make_end(container);
}

class throw_on_invalid_unicode
{
public:

    template<typename itr_type>
    constexpr char32_t invalid_dereference(utf8_const_iterator<itr_type, throw_on_invalid_unicode> const & ) const
    {
        throw invalid_utf8("Attempted to dereference invalid UTF-8 code point.");
        return 0;
    }

    template<typename itr_type>
    constexpr void invalid_increment(utf8_const_iterator<itr_type, throw_on_invalid_unicode> const & ) const
    {
        throw invalid_utf8("Attempted to increment iterator past invalid UTF-8 code point.");
    }

    template<typename itr_type>
    constexpr void invalid_decrement(utf8_const_iterator<itr_type, throw_on_invalid_unicode> const & ) const
    {
        throw invalid_utf8("Attempted to decrement iterator past invalid UTF-8 code point.");
    }
};

class skip_on_invalid_unicode
{
    public:

    template<typename itr_type>
    constexpr char32_t invalid_dereference(utf8_const_iterator<itr_type, skip_on_invalid_unicode> & itr) const noexcept
    {
        // skip until a valid one is found
        // then dereference that one
        return (invalid_increment(itr), *itr);
    }

    // repeatedly skip until a valid starting point is found
    template<typename itr_type>
    CUES_CONSTEXPR_AFTER_CPP14 void invalid_increment(utf8_const_iterator<itr_type, skip_on_invalid_unicode> & itr) const noexcept
    {
        for (auto expected_len = expected_utf8_byte_length(*(itr.uitr));
            !itr.validate_sequence(expected_len);
            itr.advance_to_next_potentially_valid(expected_len),
            expected_len = expected_utf8_byte_length(*(itr.uitr))
        ) {
        }
    }

    template<typename itr_type>
    CUES_CONSTEXPR_AFTER_CPP14 void invalid_decrement(utf8_const_iterator<itr_type, skip_on_invalid_unicode> & itr) const noexcept
    {
        for (;
            !itr.validate_sequence(expected_utf8_byte_length(*(itr.uitr)));
            itr.retreat_to_previous_potentially_valid(0)
        ) {
        }
    }
};

class replace_on_invalid_unicode
{
    public:

    template<typename itr_type>
    constexpr char32_t invalid_dereference(utf8_const_iterator<itr_type, replace_on_invalid_unicode> const & ) const noexcept
    {
        return unicode_replacement_code;
    }

    template<typename itr_type>
    constexpr void invalid_increment(utf8_const_iterator<itr_type, replace_on_invalid_unicode> const & ) const noexcept
    {
        // iterator already advances to the next possibly valid sequence;
        // we will check and, if necessary, replace, at that point.
        ;
    }

    template<typename itr_type>
    constexpr void invalid_decrement(utf8_const_iterator<itr_type, replace_on_invalid_unicode> const & ) const noexcept
    {
        ;
    }
};


} // namespace cues

#endif // CUES_UTF8_ITERATOR_HPP
