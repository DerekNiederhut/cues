#include "array_proxy_test.hpp"

namespace cues
{
namespace test
{
#if CUES_TEST_ARRAY_PROXY

void fill_3d_forward_via_bracket( cues::array_proxy<cues::test::u16_proxy_3darray_type> & proxy)
{
    // using indices

    for (std::size_t index_A = 0, size_A = proxy.size();
        index_A < size_A;
        ++index_A
    )
    {
        for (std::size_t index_B = 0,
             size_B = cues::array_proxy<cues::test::u16_proxy_3darray_type>::proxy_extents::template nth<1>();
             index_B < size_B;
             ++index_B
        )
        {
            for (std::size_t index_C = 0,
                size_C = cues::array_proxy<cues::test::u16_proxy_3darray_type>::proxy_extents::template nth<2>();
                index_C < size_C;
                ++index_C
            )
            {
                proxy[index_A][index_B][index_C] = index_C + 100 * index_B + 1000 * index_A;
            }
        }
    }
}

void fill_3d_forward_via_iterator( cues::array_proxy<cues::test::u16_proxy_3darray_type> & proxy)
{
    for (auto first_A = proxy.begin(), itr_A = first_A, end_A = proxy.end();
            itr_A != end_A;
            itr_A = std::next(itr_A)
    )
    {
        for (auto first_B = itr_A->begin(), itr_B = first_B, end_B = itr_A->end();
            itr_B != end_B;
            std::advance(itr_B,1)
        )
        {
            for (auto first_C = (*itr_B).begin(), itr_C = first_C, end_C = (*itr_B).end();
                itr_C != end_C;
                itr_C++
            )
            {
                *itr_C = 1000*std::distance(first_A, itr_A) + 100*std::distance(first_B, itr_B) + std::distance(first_C, itr_C);

            }
        }
    }
}

std::size_t validate_fill_3d_forward_via_iterator(cues::array_proxy<cues::test::u16_proxy_3darray_type const> const & proxy)
{
    std::size_t validation_errors = 0;
    // using iterators
    for (auto first_A = proxy.begin(), itr_A = first_A, end_A = proxy.end();
            itr_A != end_A;
            ++itr_A
    )
    {
        for (auto first_B = itr_A->begin(), itr_B = first_B, end_B = itr_A->end();
            itr_B != end_B;
            itr_B += 1
        )
        {
            for (auto first_C = (*itr_B).begin(), itr_C = first_C, end_C = (*itr_B).end();
                itr_C != end_C;
                itr_C = itr_C + 1
            )
            {
                if ((*itr_C ) != 1000*(static_cast<std::size_t>(itr_A - first_A)) + 100*(itr_B - first_B) + (itr_C - first_C) )
                {
                    ++validation_errors;
                }
            }
        }
    }

    return validation_errors;
}

std::size_t validate_fill_3d_forward_via_bracket(cues::array_proxy<cues::test::u16_proxy_3darray_type const> const & proxy)
{
    std::size_t validation_errors = 0;
    for (std::size_t index_A = 0, size_A = proxy.size();
        index_A < size_A;
        ++index_A
    )
    {
        for (std::size_t index_B = 0,
             size_B = cues::array_proxy<cues::test::u16_proxy_3darray_type>::proxy_extents::template nth<1>();
             index_B < size_B;
             ++index_B
        )
        {
            for (std::size_t index_C = 0,
                size_C = cues::array_proxy<cues::test::u16_proxy_3darray_type>::proxy_extents::template nth<2>();
                index_C < size_C;
                ++index_C
            )
            {
                if (proxy[index_A][index_B][index_C] != (index_C + 100 * index_B + 1000 * index_A))
                {
                    ++validation_errors;
                }
            }
        }
    }
    return validation_errors;
}

std::size_t check_array_proxy_runtime_assertions()
{
    // really an array according to built-in type definitions
    cues::test::u16_proxy_3darray_type real_array;
    // 1-D array of bytes, but with the alignment of the real type,
    // and size large enough to fit an array of the real type.
    alignas (alignof(typename std::remove_all_extents<decltype(real_array)>::type))
    cues::raw_byte_type placement_memory[sizeof(typename std::remove_all_extents<decltype(real_array)>::type)
                            * cues::extent_sequence<cues::test::u16_proxy_3darray_type>::product()];
    // 1-D array matching subarray of 3-D array
    alignas (alignof(cues::test::u16_proxy_subarray_type))
    cues::raw_byte_type placement_subarray_memory[sizeof(cues::test::u16_proxy_subarray_type)];

    std::size_t assertion_failures = 0;

    {
        // create proxy object to real array taking real array as reference
        cues::array_proxy<cues::test::u16_proxy_3darray_type> proxy_to_real{real_array};
        // fill through proxy
        cues::test::fill_3d_forward_via_bracket(proxy_to_real);
        // construct const proxy from non-const proxy
        cues::array_proxy<cues::test::u16_proxy_3darray_type const> cproxy_to_real(proxy_to_real);
        // verify that reads match the writes
        assertion_failures += (cues::test::validate_fill_3d_forward_via_iterator(cproxy_to_real) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy forward iterator reads do not match forward bracket writes")));
    }
    {
        // placement-new default construct objects on that memory, destruct when leaving scope
        cues::test::default_placement_RAII<typename std::remove_all_extents<decltype(real_array)>::type>
        object_RAII(&placement_memory[0], cues::extent_sequence<cues::test::u16_proxy_3darray_type>::product());
        cues::test::default_placement_RAII<typename std::remove_all_extents<decltype(placement_subarray_memory)>::type>
        subobject_RAII(reinterpret_cast<cues::raw_byte_type *>(&placement_subarray_memory), std::extent<cues::test::u16_proxy_subarray_type,0>::value);
        // proxy the 1-D placement array-like memory as the 3D array
        cues::array_proxy<cues::test::u16_proxy_3darray_type> proxy_to_placed{
            &placement_memory[0],
            cues::extent_sequence<cues::test::u16_proxy_3darray_type>()
        };
        cues::array_proxy<cues::test::u16_proxy_subarray_type> proxy_to_subarray{
            reinterpret_cast<cues::raw_byte_type *>(&placement_subarray_memory),
            cues::extent_sequence<cues::test::u16_proxy_subarray_type>()
        };
        // fill proxy to the placement array with the same data
        cues::test::fill_3d_forward_via_iterator(proxy_to_placed);
        // validate data
        assertion_failures += (cues::test::validate_fill_3d_forward_via_bracket(proxy_to_placed) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy forward bracket reads do not match forward iterator writes")));
        // create new proxy object to const real array
        cues::array_proxy<cues::test::u16_proxy_3darray_type const> cproxy_to_real{static_cast<cues::test::u16_proxy_3darray_type const &>(real_array)};
        // verify that the two proxies compare equal
        assertion_failures += ((proxy_to_placed != cproxy_to_real) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy comparison failed")));

        // copy one subarray
        proxy_to_subarray = proxy_to_placed.at(0).at(0);
        // verify that proxy types of the same array compare equal
        assertion_failures += ((proxy_to_subarray != proxy_to_placed[0][0]) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy first subarray equality comparison failed")));
        // also check subarray other than the first one
        proxy_to_subarray = proxy_to_placed[1][2];
        assertion_failures += (!(proxy_to_subarray == proxy_to_placed.at(1).at(2)) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy non-first subarray equality comparison failed")));
        // change an element
        proxy_to_placed[1][2][3] = 0;
        // confirm no longer equal
        assertion_failures += ((proxy_to_subarray == proxy_to_placed[1][2]) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy first subarray inequality comparison failed")));

        // confirm size and max_size match (array size is fixed)
        assertion_failures += ((proxy_to_placed.size() != proxy_to_placed.max_size())  ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy size does not match max_size")));
        // confirm static and instance sizes agree
        assertion_failures += ((proxy_to_placed.size() != decltype(proxy_to_placed)::extent())  ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy size does not match type")));
        // confirm array not of size 0 is not empty
        assertion_failures += ((proxy_to_placed.empty()) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy incorrectly reports itself as empty")));
        // verify that returns of checked and non-checked methods match,
        // using subproxies
        assertion_failures += ((proxy_to_placed[2] != proxy_to_placed.at(2)) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy bracket subproxy comparison failed")));
        // using iterators
        assertion_failures += ((proxy_to_subarray[10] != proxy_to_subarray.at(10)) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy iterator subproxy comparison failed")));

    }

    {
        // placement-new default construct objects on the memory again
        cues::test::default_placement_RAII<typename std::remove_all_extents<decltype(real_array)>::type>
        object_RAII(&placement_memory[0], cues::extent_sequence<cues::test::u16_proxy_3darray_type>::product());

        // proxy the 1-D placement array-like memory as the 3D array
        cues::array_proxy<cues::test::u16_proxy_3darray_type> proxy_to_placed{
            &placement_memory[0],
            cues::extent_sequence<cues::test::u16_proxy_3darray_type>()
        };

        // create new proxy object to const real array
        cues::array_proxy<cues::test::u16_proxy_3darray_type const> cproxy_to_real{static_cast<cues::test::u16_proxy_3darray_type const &>(real_array)};
        // should be clear now
        assertion_failures += ((proxy_to_placed == cproxy_to_real) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy should not have compared equal after being cleared")));
        // use operator=
        proxy_to_placed = cproxy_to_real;
        // verify equality
        assertion_failures += ((proxy_to_placed != cproxy_to_real) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy should have compared equal after assignment")));
        // verify that operator= in the "wrong" direction (assign mutable to const) is not possible.
        static_assert(!std::is_copy_assignable<decltype(cproxy_to_real)>::value, "should not be able to assign to a proxy to const.");
    }

    {
        cues::test::u32_proxy_array_type real_1d_array = {31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0};
        alignas(alignof(decltype(real_1d_array))) cues::raw_byte_type placement_1D_memory[sizeof(real_1d_array)];

        cues::test::default_placement_RAII<typename std::remove_all_extents<decltype(real_1d_array)>::type>
        constructor_RAII(reinterpret_cast<cues::raw_byte_type *>(&placement_1D_memory), sizeof(real_1d_array)/sizeof(real_1d_array[0]));

        cues::array_proxy<decltype(real_1d_array)> proxy_to_placement(
            reinterpret_cast<cues::raw_byte_type *>(&placement_1D_memory),
            cues::extent_sequence<decltype(real_1d_array)>()
        );

        cues::array_proxy<decltype(real_1d_array) > proxy_to_real(real_1d_array);

        // copy real to proxy using reverse iterators
        std::copy(proxy_to_real.crbegin(), proxy_to_real.crend(), proxy_to_placement.rbegin());
        // verify equality
        assertion_failures += ((proxy_to_real != proxy_to_placement) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy not equal after reverse iterator writes")));
        // copy reverse range to forward range
        std::copy(proxy_to_placement.crbegin(), proxy_to_placement.crend(), proxy_to_real.begin());
        // verify they no longer compare equal
        assertion_failures += ((proxy_to_real == proxy_to_placement) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy compared equal after reversing element order")));
        // copy assign
        proxy_to_placement = proxy_to_real;
        // verify they are equal again
        assertion_failures += ((proxy_to_real != proxy_to_placement) ? 1 : 0);
        assert(((0 == assertion_failures) && ("array_proxy not equal after re-assignment")));

        try
        {
            uint_fast32_t temp = proxy_to_placement.at(proxy_to_placement.size());
            (void)temp;
        }
        catch (std::out_of_range const &)
        {
            ; // this is the exception we expected for out of bounds access
        }
        catch (...)
        {
            ++assertion_failures;
            assert(((0 == assertion_failures) && ("array_proxy did not throw expected exception for out of range at() call")));
        }

    }

    return assertion_failures;
}
#else
std::size_t check_array_proxy_runtime_assertions()
{
    return 0;
}

#endif
}
}
