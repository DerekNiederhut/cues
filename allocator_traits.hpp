#ifndef CUES_ALLOCATOR_TRAITS_HPP
#define CUES_ALLOCATOR_TRAITS_HPP

#include <cstddef>
#include <limits>
#include <type_traits>
#include <memory>
#include "cues/void_t.hpp"
#include "cues/version_helpers.hpp"

namespace cues {


//! tag for providing no functionality beyond the named Allocator requirements
struct allocator_tag
{};

//! tag if trait is applied to something other than an Allocator
struct not_allocator_tag
{};

template<class Allocator, class Selector=void>
struct is_allocator : public std::false_type
{
};

template<class Allocator>
struct is_allocator<
    Allocator,
    cues::void_t<
        typename std::enable_if< std::is_same<
            typename std::allocator_traits<Allocator>::pointer,
            decltype(
                std::declval<Allocator>().allocate(
                    std::declval<typename std::allocator_traits<Allocator>::size_type>()
                )
            )
        >::value >::type,
        decltype(
            std::declval<Allocator>().deallocate(
                std::declval<typename std::allocator_traits<Allocator>::pointer>(),
                std::declval<typename std::allocator_traits<Allocator>::size_type>()
            )
        )
    >
> : public std::true_type
{};

//! describes an allocator with a fixed storage;
//! that is, one that will neither call operator new
//! nor malloc after it has been constructed (it could
//! for example be constructed on the free store using
//! operator new).
//! An allocator<T> which supplies this tag shall provide the member functions:

//! size_type remaining_size() const, which shall return the maximum
//! number of T objects that could still be allocated (though it does not
//! have to guarantee that a given call to allocate them will succeed;
//! an allocator may be able to allocate n T's at once but not
//! one T n times, or vice versa).  This is in contrast to max_size(),
//! which is supposed to be the maximum size supported, and thus is the
//! largest number that could succeed when empty.

//! std::size_t storage_size() const, which shall return the total size
//! of the fixed storage, where U is an unsigned integral type
//! large enough to hold that value.  std::size_t would
//! always be appropriate but implementations may use a smaller
//! size if they cannot hold that many objects


struct fixed_allocator_tag
: public allocator_tag
{};

template<class Allocator, typename Selector = void>
struct is_fixed_allocator : public std::false_type
{};

template<class Allocator>
struct is_fixed_allocator<
    Allocator,
    cues::void_t<
        typename std::enable_if<cues::is_allocator<Allocator>::value>::type,
        typename std::enable_if<
            std::is_same<
                typename std::allocator_traits<Allocator>::size_type,
                decltype(std::declval<Allocator const &>().remaining_size())
            >::value
        >::type,
        typename std::enable_if<
            std::is_same<
                std::size_t,
                decltype(std::declval<Allocator const &>().storage_size())
            >::value
        >::type
    >
>
: public std::true_type
{};


//! describes an allocator whose storage region is contiguous,
//! and from which pointers can be compared; that is, allocated objects
//! are placed in some array of bytes and subtraction of pointers
//! to allocated objects (after first converting to pointers to byte).
//! is defined behavior.
//! Allocators which supply his tag shall provide the member functions:

//! std::byte const * data() const if std::byte exists,
//! OR, unsigned char const * data() const otherwise
//! for obtaining a pointer to the first byte of storage

//! std::size_t block_size() const, which shall return
//! the amount of space in bytes allocated for a single object of type T,
//! which is permitted to be larger than sizeof(T).
//! this may be useful for example if metadata must be stored,
//! or if an allocator overaligns data for performance reasons

struct contiguous_allocator_tag :
public fixed_allocator_tag
{};

template<class Allocator, typename Selector = void>
struct is_contiguous_allocator : public std::false_type
{};

template<class Allocator>
struct is_contiguous_allocator<
    Allocator,
    cues::void_t<
        typename std::enable_if<cues::is_fixed_allocator<Allocator>::value>::type,
        typename std::enable_if<
            std::is_same<
                cues::raw_byte_type const *,
                decltype(std::declval<Allocator const &>().data())
            >::value
        >::type,
         typename std::enable_if<
            std::is_same<
                std::size_t,
                decltype(std::declval<Allocator const &>().block_size())
            >::value
        >::type
    >
>
: public std::true_type
{};


template<class Allocator>
struct allocator_traits
{
    using allocator_category = typename std::conditional<
        cues::is_contiguous_allocator<Allocator>::value,
        contiguous_allocator_tag,
        typename std::conditional<
            cues::is_fixed_allocator<Allocator>::value,
            fixed_allocator_tag,
            typename std::conditional<
                cues::is_allocator<Allocator>::value,
                allocator_tag,
                not_allocator_tag
            >::type
        >::type
    >::type;

    // similar to default implementation from n4258
    // but also includes possibility for allocator to have
    // members but still always compare equal if they would not need
    // to propogate (that is, the allocator has state but the state
    // is not actually important, for example a cache of some sort)
    static constexpr bool const is_stateless =
        // if you don't store any state, how could you have any state?
        std::is_empty<Allocator>::value
    || (
            // if you never have to propogate (copy state)
            // how could you have any state?
           !std::allocator_traits<Allocator>::propagate_on_container_swap::value
        && !std::allocator_traits<Allocator>::propagate_on_container_move_assignment::value
        && !std::allocator_traits<Allocator>::propagate_on_container_copy_assignment::value
    );
};

template<class T, typename Selector = void>
struct provides_allocator : public std::false_type
{};

template<class T>
struct provides_allocator<
    T,
    cues::void_t<
        typename T::allocator_type,
        typename std::enable_if<
            cues::is_allocator<
                typename T::allocator_type
            >::value
        >::type,
        typename std::enable_if<
            std::is_same<
                typename T::allocator_type,
                typename std::remove_cv<
                    typename std::remove_reference<
                        decltype(std::declval<T>().get_allocator())
                    >::type
                >::type
            >::value
        >::type
    >
>
: public std::true_type
{};


} // namespace cues

#endif // CUES_ALLOCATOR_TRAITS_HPP
