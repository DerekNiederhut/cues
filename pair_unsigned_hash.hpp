#ifndef CUES_PAIR_UNSIGNED_HASH_HPP_INCLUDED
#define CUES_PAIR_UNSIGNED_HASH_HPP_INCLUDED

#pragma once

#include <utility>      // std::pair, std::declval
#include <functional>   // std::hash
#include <type_traits>  // various
#include <limits>       // digits
#include "cues/enum_type_traits.hpp"
#include "cues/integer_selector.hpp"
#include "cues/void_t.hpp"
#include "cues/constexpr_math.hpp"

namespace cues
{


//! for finding the size in bytes of potentially nested pairs of
//! unsigned integers or enums which have an underlying integer as their
//! underlying type.  Note it assumes the integers use their full bit space,
//! so if a type doesn't use the full bit space it will be overestimated
//! PRECONDITION: T is an integral type or enum, or a std::pair of such values
template<class T, class Enable=void>
struct recursive_pair_component_digits;

template<class T>
struct recursive_pair_component_digits<T, cues::void_t<typename std::enable_if<cues::is_integral_or_enum<T>::value>::type>>
{
    constexpr static unsigned int calculate()
    {
        return std::numeric_limits<typename cues::type_or_underlying<T>::type>::digits;
    }
};

template<class T>
struct recursive_pair_component_digits<T,
    cues::void_t< decltype(std::declval<T>().first), decltype(std::declval<T>().second)>
>
{
    using first_type = typename std::remove_reference<
         decltype(std::declval<T>().first)
    >::type;
    using second_type = typename std::remove_reference<
         decltype(std::declval<T>().second)
    >::type;

    constexpr static std::size_t calculate()
    {
        return recursive_pair_component_digits<first_type>::calculate()
                +  recursive_pair_component_digits<second_type>::calculate();
    }
};


template<typename Intermediate_Type, class T>
constexpr typename std::enable_if<cues::is_integral_or_enum<T>::value,
Intermediate_Type >::type recursive_pair_composite(T const &  value)
{
    using numeric_type = typename cues::type_or_underlying<T>::type;
    // cast to unsigned is used to preserve bit pattern of signed types with
    // narrower width than unsigned Intermediate_Type, so the sign extension
    // doesn't throw off the bit-wise operations later.

    // make_unsigned will return the type if it specializes numeric_limits, but
    // a custom type might not be castable to size_t, run it through type_or_underlying
    // as well.
    return static_cast<Intermediate_Type>(
        static_cast<typename cues::make_unsigned<numeric_type>::type>(
            static_cast<numeric_type>(value)
        )
    );
}

//! PRECONDITION: this overload is for std::pair<A,B> where A and B are integral or enum types
//! or a pair of such types.
template<typename Intermediate_Type, class T>
constexpr typename std::enable_if<
    std::is_same<void,
        cues::void_t<
            decltype(std::declval<T const>().first),
            decltype(std::declval<T const>().second)
        >
    >::value,
Intermediate_Type >::type recursive_pair_composite(T const & value)
{
     using second_type = typename std::remove_reference<
         decltype(std::declval<T>().second)
    >::type;

    return
    (
        (
            (
                recursive_pair_composite<Intermediate_Type>(value.first)
            )
            << // shift the value by the number of digits.
            // sizeof() might include padding / alignment, which we don't want,
            // and could introduce unnecessary bits for custom integer types; use
            // recursive_pair_component_digits instead.
            recursive_pair_component_digits<second_type>::calculate()
         )
     |  // note that because we cast to unsigned and shifted, there should be no overlap
        // of bits.
       recursive_pair_composite<Intermediate_Type>(value.second)
     );
}


//! PRECONDITION: T has members first and second of types A and B, respectively,
//! where A and B are:
//! unsigned integers;
//! or, enums with an underlying type that is an unsigned integer;
//! or, another type U that can recurse (that is, it has members
//!     first and second of types C and D, subject to the same rules
//!     as types A and B);
//! whose total digits as reported by std::numeric_limits<>
//! are at most equal to those of uint_max_t
//! Note this means that although it was designed for std::pair<>,
//! or possibly recursive std::pair, it can also work with other
//! types, for example custom POD aggregates that name their members
//! similarly.
template<class Pair_Type, template<class> class hasher = std::hash>
class pair_unsigned_hash
{
    // find the intermediate unsigned type to hold the composite value
    using intermediate_type = typename cues::select_fast_integer_by_digits<
        recursive_pair_component_digits<Pair_Type>::calculate(),
        cues::integer_selector_signedness::use_unsigned
    >::type;


    public:
        constexpr std::size_t operator() (Pair_Type const & value) const noexcept
        {
            return hasher<intermediate_type>()(recursive_pair_composite<intermediate_type, Pair_Type>(value));
        }

};


}

#endif // CUES_PAIR_UNSIGNED_HASH_INCLUDED
