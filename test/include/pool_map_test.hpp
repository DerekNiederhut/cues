#ifndef POOL_MAP_TEST_HPP_INCLUDED
#define POOL_MAP_TEST_HPP_INCLUDED

#include <cstddef>

namespace cues
{
namespace test
{
std::size_t check_pool_map_test_runtime_assertions();
}
}

#endif // POOL_MAP_TEST_HPP_INCLUDED
