#include "ring_buffer_test.hpp"

#include "cues/version_helpers.hpp"

#if (__cplusplus < 201703L && (!defined ( __cpp_lib_launder) || !defined(__cpp_lib_if_constexpr)))
    #pragma message "NOTE: skipping test of cues::ring_buffer because it requires std::launder and constexpr if, which are not available."
    namespace cues
    {
    namespace test
    {

    std::size_t check_ring_buffer_test_runtime_assertions() {return 0;}
    }
    }
#else


#include <thread>
#include <future>
#include <algorithm>
#include <functional> // for reference_wrapper
#include <cstdint>
#include <type_traits>
#include <random>

#include "cues/ring_buffer.hpp"
#include "cues/array_proxy.hpp"




#include "helper_types.hpp"
#include "common_container_functions.hpp"





namespace cues
{
namespace test
{
template<class T, std::size_t N, typename index_type = std::size_t, std::size_t DATA_ALIGN = alignof(T), std::size_t INDEX_ALIGN = alignof(index_type)>
struct ring_buffer_traits
{
    using container_type = ring_buffer<T,N,index_type,DATA_ALIGN,INDEX_ALIGN>;
    using atomic_index_type = std::atomic<typename container_type::size_type>;

    static constexpr bool const std_layout = std::is_standard_layout<container_type>::value;
    static constexpr std::size_t const index_alignment   = std::max(alignof(atomic_index_type), INDEX_ALIGN);
    static constexpr std::size_t const data_alignment    = std::max(alignof(typename container_type::value_type), DATA_ALIGN);
    static constexpr std::size_t const overall_alignment = std::max(index_alignment, data_alignment);

    static constexpr std::size_t const expected_size_through_second_index =
        std::max(
            // alignment of the second vs size of the first
            index_alignment,
            sizeof(atomic_index_type)
        )
        // plus size of the second
        + sizeof(atomic_index_type)
    ;

    // assumes no padding at end
    static constexpr std::size_t const min_expected_size =
    // sizeof data
    container_type::max_size() * sizeof(typename container_type::value_type)
    +
    // find required size before data
    ( (data_alignment > expected_size_through_second_index) ?
        // if the alignment is larger, data starts at the alignment
        data_alignment
        :
        // otherwise, jump to the next data_alignment
        data_alignment*(
            // divide to drop remainder after whole amounts of data_aligment
            (expected_size_through_second_index / data_alignment)
            // add only one more data_alignment chunk iff there is any remainder
            + ((expected_size_through_second_index % data_alignment) > 0)
        )
    );

    // add padding at end
    static constexpr std::size_t const total_expected_size =
    overall_alignment * (
        (min_expected_size / overall_alignment)
        + ((min_expected_size % overall_alignment) > 0)
    );

};

static constexpr std::size_t leaktest_max_size = 7u;
using leaktest_ringbuffer_traits = ring_buffer_traits<counted<counted_counter_type>, leaktest_max_size, std::uint_least16_t> ;
using leaktest_ring_buffer       = typename leaktest_ringbuffer_traits::container_type;


// has to be standard layout for the math to be right
static_assert(leaktest_ringbuffer_traits::std_layout, "leaktest ring_buffer not standard layout.");
// check size, alignment
static_assert(sizeof(leaktest_ring_buffer) == leaktest_ringbuffer_traits::total_expected_size, "leaktest ring_buffer size or alignment wrong");

static_assert(leaktest_ringbuffer_traits::expected_size_through_second_index == 4, "unexpected index size");
static_assert(leaktest_ringbuffer_traits::data_alignment == 1, "unexpected data alignment");

using fail_then_count_leaktest_ringbuffer_traits = ring_buffer_traits<random_failure_then_counted, leaktest_max_size, std::uint_fast8_t>;
using fail_then_count_leaktest_ring_buffer       = typename fail_then_count_leaktest_ringbuffer_traits::container_type;

using count_then_fail_leaktest_ringbuffer_traits = ring_buffer_traits<counted_then_random_failure, leaktest_max_size, std::uint_fast8_t>;
using count_then_fail_leaktest_ring_buffer       = typename count_then_fail_leaktest_ringbuffer_traits::container_type;

class scoped_size_t : public scoped_only
{
public:
    explicit constexpr scoped_size_t(std::size_t init)
    : scoped_only()
    , value(init)
    {}

    scoped_size_t() = default;

    explicit operator std::size_t() const
    {
        return value;
    }

private:
    std::size_t value;
};



using scoped_ringbuffer_traits = ring_buffer_traits<scoped_size_t, 32, std::uint_least8_t, 16, 8> ;
using scoped_ring_buffer       = typename scoped_ringbuffer_traits::container_type;

static_assert(scoped_ringbuffer_traits::std_layout, "scoped ring_buffer not standard layout.");
static_assert(sizeof(scoped_ring_buffer) == scoped_ringbuffer_traits::total_expected_size, "scoped ring_buffer size or alignment wrong");



using moveonly_ringbuffer_traits = ring_buffer_traits<move_only, 14, std::size_t, 1, 1> ;
using move_only_ring_buffer      = typename moveonly_ringbuffer_traits::container_type;

static_assert(moveonly_ringbuffer_traits::std_layout, "moveonly ring_buffer not standard layout.");
static_assert(sizeof(move_only_ring_buffer) == moveonly_ringbuffer_traits::total_expected_size, "moveonly ring_buffer size or alignment wrong");

static constexpr std::size_t uint_arr_ringbuffer_max_size = 16u;
using uint_arr = std::uint_least16_t [4];
using uintarr_ringbuffer_traits = ring_buffer_traits<uint_arr, uint_arr_ringbuffer_max_size, std::size_t, 64, 64> ;
using uint_arr_ring_buffer = typename uintarr_ringbuffer_traits::container_type;

static_assert(uintarr_ringbuffer_traits::std_layout, "uint array ring_buffer not standard layout.");
static_assert(sizeof(uint_arr_ring_buffer) == uintarr_ringbuffer_traits::total_expected_size, "uint array ring_buffer size or alignment wrong");

using trivial_ringbuffer_traits = ring_buffer_traits<trivial_aggregate, 5, std::uint_least8_t> ;
using trivial_ring_buffer       = typename trivial_ringbuffer_traits::container_type;

static_assert(trivial_ringbuffer_traits::std_layout, "trivial ring_buffer not standard layout.");
static_assert(sizeof(trivial_ring_buffer) == trivial_ringbuffer_traits::total_expected_size, "trivial ring_buffer size or alignment wrong");

static constexpr std::size_t const scoped_sample_size = 10000;

using buffer_of_non_default = cues::ring_buffer<std::reference_wrapper<unsigned long long>, 8u>;

static buffer_of_non_default should_compile;

void scoped_size_t_producer(scoped_ring_buffer & buffer, std::vector<std::size_t> const & data, std::atomic<bool> const & running_flag, std::chrono::milliseconds period )
{
    auto data_end = data.cend();
    for (auto itr = data.cbegin();
    itr != data_end && atomic_load_explicit(&running_flag, std::memory_order_relaxed);
    itr += buffer.try_emplace_back(*itr))
    {
       std::this_thread::sleep_for(period);
    }
}

void scoped_size_t_consumer(scoped_ring_buffer & buffer, std::vector<std::size_t> & data, std::atomic<bool> const & running_flag, std::chrono::milliseconds period)
{
    for (std::size_t acquisitions = 0;
    acquisitions < scoped_sample_size && atomic_load_explicit(&running_flag, std::memory_order_relaxed);
    )
    {
        auto token = buffer.peek_front();
        // scoped object cannot be moved or copied, and so has to be converted
        while (token.has_value())
        {
            data.push_back(static_cast<std::size_t>(*token));
            buffer.mark_consumed(std::move(token));
            ++acquisitions;
            token = buffer.peek_front();
        }

        std::this_thread::sleep_for(period);
    }
}

counted_counter_type assertion_failures_ring_buffer_scoped_sequence_check()
{

    counted_counter_type assertion_failures = 0;

    scoped_ring_buffer buffer;
    std::vector<std::size_t> input_values;
    std::vector<std::size_t> output_values;
    std::atomic<bool> keep_running = true;
    std::chrono::milliseconds consumer_period = std::chrono::milliseconds(5);
    std::chrono::milliseconds producer_period = std::chrono::milliseconds(1);

    input_values.reserve( scoped_sample_size);
    output_values.reserve(scoped_sample_size);

    std::mt19937 rng_engine(0);
    // full range of char is acceptable, we don't need to print any of this.
    std::uniform_int_distribution<std::size_t> rng_distribution;
    for (std::size_t elements = 0;
    elements < scoped_sample_size;
    ++elements)
    {
        input_values.push_back(rng_distribution(rng_engine));
    }

    std::thread consumer_thread(
        &scoped_size_t_consumer,
        std::ref(buffer),
        std::ref(output_values),
        std::ref(keep_running),
        consumer_period
    );

    std::thread producer_thread(
         &scoped_size_t_producer,
         std::ref(buffer),
         std::ref(input_values),
         std::ref(keep_running),
         producer_period
    );
    producer_thread.join();
    consumer_thread.join();

    assertion_failures += input_values != output_values;

    return assertion_failures;
}


counted_counter_type assertion_failures_exact_ringbuffer_array_check()
{
    static_assert(4 == cues::extent_sequence<uint_arr>::product(), "unexpected sequence size");
    // using uint_arr = std::uint_least16_t [4];
    uint_arr_ring_buffer buffer;
    uint_arr_ring_buffer const * const_forcer = &buffer;
    counted_counter_type assertion_failures = 0;

    assertion_failures += uint_arr_ringbuffer_max_size != const_forcer->max_size();
    assert(((0 == assertion_failures) && ("unexpected buffer.max_size().")));

    assertion_failures += (!const_forcer->empty() || const_forcer->has_unconsumed_data() || (const_forcer->size() != 0));
    assert(((0 == assertion_failures) && ("empty buffer not empty.")));

    assertion_failures += (const_forcer->peek_front().has_value());
    assert(((0 == assertion_failures) && ("empty buffer provided non-empty token.")));

    // overload where each element of list can initialize innermost_type
    buffer.try_emplace_back({0xA55Au, 0xC33Cu, 0x3CC3u, 0x5AA5u});

    assertion_failures += (buffer.size() != 1);
    assert(((0 == assertion_failures) && ("wrong buffer size.")));

    // overload where entire list can initialize one innermost_type
    buffer.try_emplace_back({0xF00Fu});
    // overload where single value can initialize one innermost type
    buffer.try_emplace_back(0x0FF0u);
    // overload where each value can initialize one innermost_type
    buffer.try_emplace_back(0x55AAu, 0xFF00u, 0x00FFu, 0xAA55u);

    assertion_failures += (const_forcer->size() != 4);
    assert(((0 == assertion_failures) && ("wrong buffer size.")));

    {
        static constexpr std::size_t const testarr_len = 3;
        uint_arr temp_array_of_array[testarr_len];
        // note that because we have an array of arrays, an iterator of the array of arrays would
        // have an array as its value_type, and arrays do not have an assignment operator.
        // so, we borrow array_proxy to take care of it for us.
        cues::array_proxy<decltype(temp_array_of_array) > proxy_for_copying(temp_array_of_array);
        std::size_t copied_arrays = buffer.consume_n(proxy_for_copying.begin(), testarr_len);
        assertion_failures += (copied_arrays != testarr_len);
        assert(((0 == assertion_failures) && ("unexpectedly small copies from consume_n for array.")));
        assertion_failures +=
        (
            temp_array_of_array[0][0] != 0xA55Au
         || temp_array_of_array[0][1] != 0xC33Cu
         || temp_array_of_array[0][2] != 0x3CC3u
         || temp_array_of_array[0][3] != 0x5AA5u
         || temp_array_of_array[1][0] != 0xF00Fu
         || temp_array_of_array[2][0] != 0x0FF0u
        );
        assert(((0 == assertion_failures) && ("incorrect value consumed.")));

        assertion_failures += (const_forcer->size() != 4 - testarr_len);
        assert(((0 == assertion_failures) && ("improper size after consumption.")));
    }

    {
        auto token = const_forcer->peek_front();
        assertion_failures += !token.has_value();
        assert(((0 == assertion_failures) && ("missing value.")));

        assertion_failures +=
        (
               (*token)[0] != 0x55AAu
            || (*token)[1] != 0xFF00u
            || (*token)[2] != 0x00FFu
            || (*token)[3] != 0xAA55u
        );
        assert(((0 == assertion_failures) && ("incorrect value peeked.")));
    }



    return assertion_failures;
}

counted_counter_type single_context_ringbuffer_leak_check()
{
    using value_type = typename leaktest_ring_buffer::value_type;
    using size_type = typename leaktest_ring_buffer::size_type;

    counted_counter_type assertion_failures = 0;
    counted_counter_type volatile prev_cs;
    counted_counter_type volatile current_cs;
    counted_counter_type volatile prev_ds;
    counted_counter_type volatile current_ds;

    counted_counter_type expected_constructions = 0;
    counted_counter_type expected_destructions  = 0;

    assertion_failures += static_cast<counted_counter_type>(cues::test::counted<counted_counter_type>::constructor_destructor_mismatch());
    assert((!cues::test::counted<counted_counter_type>::constructor_destructor_mismatch() && "Previous leak invalidates future tests."));

    {
        leaktest_ring_buffer buffer {};

        prev_ds = counted<counted_counter_type>::destructed_count();
        prev_cs = counted<counted_counter_type>::default_copy_move_constructed_count();

        expected_constructions = static_cast<counted_counter_type>(buffer.try_emplace_back(value_type{}));

        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::default_copy_move_constructed_count();
        // moves, copies, and their associated destructors may all be elided
        assertion_failures += static_cast<counted_counter_type>(current_cs <= prev_cs);
        assert((((current_cs - prev_cs) >= 1) && "didn't construct as expected."));


        prev_ds = current_ds;
        prev_cs = current_cs;
        {
            value_type const temp{};
            expected_constructions = 1;
            expected_destructions  = 1;
            // ends in the copy constructor
            expected_constructions += static_cast<counted_counter_type>(buffer.try_emplace_back(static_cast<value_type const &>(temp)));
        }
        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::default_copy_move_constructed_count();
        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions) && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = current_ds;
        prev_cs = current_cs;
        {
             value_type temp[4] {{},{},{},{}};

             expected_constructions = sizeof(temp)/sizeof(temp[0]);
             expected_destructions  = sizeof(temp)/sizeof(temp[0]);

             expected_destructions  += buffer.consume_n(&temp[0], size_type{sizeof(temp)/sizeof(temp[0])});
             // we should have consumed two
             assertion_failures += static_cast<counted_counter_type>((expected_destructions - expected_constructions) != 2);
             assert(((expected_destructions == (expected_constructions + 2)) && "didn't consume expected number of objects."));
        }
        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::default_copy_move_constructed_count();
        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions) && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = current_ds;
        prev_cs = counted<counted_counter_type>::in_place_constructed_count();
        expected_constructions = static_cast<counted_counter_type>(buffer.try_emplace_back(in_place_t{}));
        expected_destructions = 0;
        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::in_place_constructed_count();
        // emplace from these arguments should not have any temporary objects or copies
        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions) && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = current_ds;
        prev_cs = counted<counted_counter_type>::default_constructed_count();

        {
            auto token = buffer.try_construct_data();
            expected_constructions = static_cast<counted_counter_type>(nullptr != token);
            expected_destructions = static_cast<counted_counter_type>(nullptr != token);
            buffer.roll_back(std::move(token));
            token = buffer.try_construct_data();
            expected_constructions += static_cast<counted_counter_type>(nullptr != token);
            buffer.commit(std::move(token));
        }

        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::default_constructed_count();

        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions) && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = current_ds;
        prev_cs = counted<counted_counter_type>::default_copy_move_constructed_count();
        {
            value_type temp[8] {{},{},{},{},{},{},{},{}};
            expected_constructions = sizeof(temp)/sizeof(temp[0]);
            expected_destructions = sizeof(temp)/sizeof(temp[0]);
            auto push_begin_point = std::cbegin(temp);
            auto push_finish_point = buffer.push_back_n(push_begin_point, std::cend(temp));

            expected_constructions += (push_finish_point - push_begin_point);
        }
        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::default_copy_move_constructed_count();

        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions)  && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = current_ds;
        prev_cs = counted<counted_counter_type>::default_copy_move_constructed_count();
        assert((!buffer.empty() && "need elements to consume."));
        {
            value_type temp {buffer.consume()};

            expected_constructions = 1; // for temp
            expected_destructions  = 2; // temp and container element
        }
        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::default_copy_move_constructed_count();

        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions)  && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = current_ds;
        prev_cs = counted<counted_counter_type>::default_copy_move_constructed_count();
        {
            value_type temp{};
            expected_constructions = 1;
            expected_destructions  = 1;
            buffer.consume(temp);
            expected_destructions  += 1;
        }
        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::default_copy_move_constructed_count();

        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions)  && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = current_ds;
        prev_cs = counted<counted_counter_type>::total_constructed_count();
        {
            auto token = buffer.peek_front();
            expected_destructions  = (token != nullptr);
            expected_constructions = 0;
            buffer.mark_consumed(std::move(token));
        }
        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::total_constructed_count();

        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions)  && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = current_ds;
        prev_cs = counted<counted_counter_type>::total_constructed_count();
        {
            buffer.pop_front();
            expected_destructions  = 1;
            expected_constructions = 0;
        }
        current_ds = counted<counted_counter_type>::destructed_count();
        current_cs = counted<counted_counter_type>::total_constructed_count();

        assertion_failures += static_cast<counted_counter_type>((current_cs - prev_cs) != expected_constructions);
        assert((((current_cs - prev_cs) == expected_constructions) && "didn't construct as expected."));
        assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
        assert((((current_ds - prev_ds) == expected_destructions)  && "didn't destruct as expected."));
        assertion_failures += static_cast<counted_counter_type>(buffer_size_check(buffer));

        prev_ds = counted<counted_counter_type>::destructed_count();
        expected_destructions = buffer.size();
        assert(((expected_destructions > 0) && "failed to test cleanup because there were no objects left in container."));
    }

    current_ds = counted<counted_counter_type>::destructed_count();
    assertion_failures += static_cast<counted_counter_type>((current_ds - prev_ds) != expected_destructions);
    assert((((current_ds - prev_ds) == expected_destructions) && "didn't clean up on container desstruction as expected."));


    return assertion_failures;
}

void trivial_producer(trivial_ring_buffer & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period)
{
    std::size_t elements_inserted;
    std::uint_least32_t limiting_counter;
    std::uint_least32_t productions = 0;

    trivial_aggregate pending_publication [3] = {
        {{static_cast<std::uint_least16_t>(productions  )},{static_cast<std::uint_least8_t>(productions+1)}},
        {{static_cast<std::uint_least16_t>(productions+2)},{static_cast<std::uint_least8_t>(productions+3)}},
        {{static_cast<std::uint_least16_t>(productions+4)},{static_cast<std::uint_least8_t>(productions+5)}}
    };

    constexpr std::size_t const size_quotient  = trivial_ring_buffer::max_size() / (sizeof(pending_publication)/sizeof(pending_publication[0])) ;
    constexpr std::size_t const size_remainder = trivial_ring_buffer::max_size() % (sizeof(pending_publication)/sizeof(pending_publication[0])) ;

    static_assert((sizeof(pending_publication)/sizeof(pending_publication[0])) > 1 && (size_remainder != 0),
                  "test of behavior when copying across modulo boundary will never cause producer to copy across modulo boundary." );

    for (limiting_counter = 0, productions = 0;
        std::atomic_load_explicit(&running_flag, std::memory_order_relaxed)
        && limiting_counter < 10000
        && productions < (std::numeric_limits<trivial_ring_buffer::size_type>::max)() - 6;
        ++limiting_counter
         )
    {
        // size is not necessarily accurate.  However, the production side comes from our thread, so
        // it should be up-to-date, and if the consumer side lags behind, it should mean we think there
        // is not room when there is--in which case we just try again later--rather than thinking there
        // is room when there isn't.
        if (static_cast<std::size_t>(buffer.max_size() - buffer.size()) > (sizeof(pending_publication)/sizeof(pending_publication[0])))
        {
            elements_inserted = buffer.push_back_n(std::cbegin(pending_publication), std::cend(pending_publication)) - std::cbegin(pending_publication);
            // if there was room for all, they all should have been inserted.
            assert((elements_inserted == static_cast<std::size_t>(std::cend(pending_publication) - std::cbegin(pending_publication)) && "failed to insert when there should have been room"));
            if (elements_inserted == static_cast<std::size_t>(std::cend(pending_publication) - std::cbegin(pending_publication)))
            {
                productions += 6;
                pending_publication[0] = {{static_cast<std::uint_least16_t>(productions  )},{static_cast<std::uint_least8_t>(productions+1)}};
                pending_publication[1] = {{static_cast<std::uint_least16_t>(productions+2)},{static_cast<std::uint_least8_t>(productions+3)}};
                pending_publication[2] = {{static_cast<std::uint_least16_t>(productions+4)},{static_cast<std::uint_least8_t>(productions+5)}};

            }
        }
        std::this_thread::sleep_for(period);
    }

    assert(((productions >= size_quotient*size_remainder) && "inadequate overlap testing in producer."));

}

void trivial_consumer(trivial_ring_buffer & buffer, std::size_t & failures, std::atomic<bool> const & running_flag, std::chrono::milliseconds period)
{
    std::size_t consumed_elements;
    std::uint_least32_t limiting_counter;
    std::uint_least32_t consumptions;
    failures = 0;

    trivial_aggregate cached_consumption [2];

    constexpr std::size_t const size_quotient  = trivial_ring_buffer::max_size() / (sizeof(cached_consumption)/sizeof(cached_consumption[0])) ;
    constexpr std::size_t const size_remainder = trivial_ring_buffer::max_size() % (sizeof(cached_consumption)/sizeof(cached_consumption[0])) ;

    static_assert((sizeof(cached_consumption)/sizeof(cached_consumption[0])) > 1 && (size_remainder != 0),
                  "test of behavior when copying across modulo boundary will never cause consumer to copy across modulo boundary." );

    for (limiting_counter = 0, consumptions = 0;
        std::atomic_load_explicit(&running_flag, std::memory_order_relaxed)
        && limiting_counter < 10000
        && consumptions < (std::numeric_limits<trivial_ring_buffer::size_type>::max)() - 4;
        ++limiting_counter
         )
    {
        if ((buffer.size()) > (sizeof(cached_consumption)/sizeof(cached_consumption[0])))
        {
            consumed_elements = buffer.consume_n(&cached_consumption[0], sizeof(cached_consumption)/sizeof(cached_consumption[0]));

            assert(((consumed_elements == sizeof(cached_consumption)/sizeof(cached_consumption[0])) && "failed to consume available elements"));

            if (consumed_elements == sizeof(cached_consumption)/sizeof(cached_consumption[0]))
            {
                failures += static_cast<std::size_t>(
                               (cached_consumption[0].first  != consumptions)
                            || (cached_consumption[0].second != consumptions + 1)
                            || (cached_consumption[1].first  != consumptions + 2)
                            || (cached_consumption[1].second != consumptions + 3)
                );

                assert((failures == 0));
                consumptions += 4;

            }
        }
        std::this_thread::sleep_for(period);
    }

    assert(((consumptions >= size_quotient*size_remainder) && "inadequate overlap testing in consumer"));

}


counted_counter_type assertion_failures_ringbuffer_trivial_copy_check()
{

    counted_counter_type assertion_failures = 0;

    trivial_ring_buffer buffer;
    std::atomic<bool> keep_running = true;
    constexpr std::uint_fast8_t const consumer_period_ms = 4;
    constexpr std::uint_fast8_t const producer_period_ms = 1;

    std::thread consumer_thread(
        &trivial_consumer,
        std::ref(buffer),
        std::ref(assertion_failures),
        std::ref(keep_running),
        std::chrono::milliseconds(consumer_period_ms)
    );

    std::thread producer_thread(
         &trivial_producer,
         std::ref(buffer),
         std::ref(keep_running),
         std::chrono::milliseconds(producer_period_ms)
    );
    producer_thread.join();
    consumer_thread.join();

    return assertion_failures;
}


counted_counter_type check_ring_buffer_test_runtime_assertions()
{

    static_assert(has_consume_n<leaktest_ring_buffer, cues::test::counted<counted_counter_type> *>::value, "ring_buffer should have consume_n function.");
    static_assert(has_push_back_n<leaktest_ring_buffer, cues::test::counted<counted_counter_type> const *>::value, "ring_buffer should have push_back_n function.");

    static_assert(has_try_emplace_back<leaktest_ring_buffer, bool, typename std::add_rvalue_reference<cues::test::counted<counted_counter_type>>::type>::value,
              "ring buffer should have try_emplace_back");

    static_assert(!has_emplace_back<leaktest_ring_buffer, void, typename std::add_rvalue_reference<cues::test::counted<counted_counter_type>>::type>::value,
              "ring buffer should not have emplace_back");

    static_assert(
        has_try_construct_data<
          leaktest_ring_buffer,
          leaktest_ring_buffer::access_token
        >::value, "ring_buffer should have try_construct_data()");

    static constexpr unsigned int const fast_milliseconds = 1;
    static constexpr unsigned int const slow_milliseconds = 3;

    counted_counter_type assertion_failures = 0;

    assertion_failures += single_context_ringbuffer_leak_check();
    assert(((0 == assertion_failures) && ("leak in ring_buffer.")));

    assertion_failures += cues::test::container_leak_test_failures<leaktest_ring_buffer, in_place_t>(
          std::chrono::milliseconds(fast_milliseconds),
          std::chrono::milliseconds(slow_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in ring_buffer.")));

    assertion_failures += assertion_failures_ring_buffer_scoped_sequence_check();
    assert(((0 == assertion_failures) && ("FIFO not maintained in ring_buffer for scoped data.")));

    assertion_failures += assertion_failures_exact_ringbuffer_array_check();
    assert(((0 == assertion_failures) && ("ring_buffer failed for array type.")));

    // still to test:
    // test leaks when constructor throws
    assertion_failures += cues::test::container_leak_test_failures<fail_then_count_leaktest_ring_buffer, in_place_t>(
        std::chrono::milliseconds(fast_milliseconds),
        std::chrono::milliseconds(slow_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in ring_buffer when throwing before counting, faster producer.")));

    assertion_failures += cues::test::container_leak_test_failures<fail_then_count_leaktest_ring_buffer, in_place_t>(
        std::chrono::milliseconds(slow_milliseconds),
        std::chrono::milliseconds(fast_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in ring_buffer when throwing before counting, faster consumer.")));

    assertion_failures += cues::test::container_leak_test_failures<count_then_fail_leaktest_ring_buffer, in_place_t>(
        std::chrono::milliseconds(fast_milliseconds),
        std::chrono::milliseconds(slow_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in ring_buffer when counting before throwing, faster producer.")));

    assertion_failures += cues::test::container_leak_test_failures<count_then_fail_leaktest_ring_buffer, in_place_t>(
        std::chrono::milliseconds(slow_milliseconds),
        std::chrono::milliseconds(fast_milliseconds)
    ) ? 1 : 0;
    assert(((0 == assertion_failures) && ("leak in ring_buffer when counting before throwing, faster consumer.")));

    // test trivial type with and without wraparound to verify memcpy is correct

    assertion_failures += assertion_failures_ringbuffer_trivial_copy_check();
    assert(((0 == assertion_failures) && ("error in ring_buffer when copying trivial structure across modulo boundary.")));

    return assertion_failures;



}


}
}

#endif
