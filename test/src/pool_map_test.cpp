#include "cues/version_helpers.hpp"
#include "pool_map_test.hpp"


#if (__cplusplus < 201703L && !defined(__cpp_lib_if_constexpr))
    #pragma message "NOTE: skipping test of cues::pool_map because constexpr if is not available."
namespace cues
{
namespace test
{
std::size_t check_pool_map_test_runtime_assertions() {return 0;}
}
}
#else

#include <cstdint>

#include CUES_VARIANT_INCLUDE

#include "cues/pool_map.hpp"
#include "cues/homogenous_pool.hpp"
#include "cues/pool_list.hpp"
#include "helper_types.hpp"

namespace cues
{
namespace test
{

template<
    typename key_type,
    typename mapped_type,
    typename container_type,
    typename compare
>
std::size_t check_valid_element_access_errors(
    cues::pool_map<key_type, mapped_type, container_type, compare> & c,
    typename cues::pool_map<key_type, mapped_type, container_type, compare>::const_iterator itr
)
{
    std::size_t errs = 0;
    std::size_t const previous_size = c.size();
    assert(((void)"operator bracket does not compare equal to iterator", itr->second == c[itr->first]));
    errs += static_cast<std::size_t>(itr->second != c[itr->first]);
    assert(((void)"operator bracket unexpectedly inserted element", previous_size == c.size()));
    errs += static_cast<std::size_t>(previous_size != c.size());

    try
    {
        assert(((void)"at does not compare equal to iterator", itr->second == c.at(itr->first)));
        errs += static_cast<std::size_t>(itr->second != c.at(itr->first));
    }
    catch(...)
    {
        assert(((void)"should not have thrown exception", false));
        ++errs;
    }
    return errs;
}

template<
    typename key_type,
    typename mapped_type,
    typename container_type,
    typename compare
>
std::size_t check_valid_element_access_errors(
    cues::pool_multimap<key_type, mapped_type, container_type, compare> & ,
    typename cues::pool_multimap<key_type, mapped_type, container_type, compare>::const_iterator
)
{
    return 0;
}

template<
    typename key_type,
    typename mapped_type,
    typename container_type,
    typename compare
>
std::size_t check_invalid_element_access_errors(
    cues::pool_map<key_type, mapped_type, container_type, compare> & c,
    typename cues::pool_map<key_type, mapped_type, container_type, compare>::key_type key
)
{
    try
    {
        (void)c.at(key);
        assert(((void)"should have thrown exception", false));

    }
    catch(std::out_of_range const &)
    {
        return 0;
    }
    catch(...)
    {
        assert(((void)"threw wrong exception type", false));
    }
    return 1u;
}

template<
    typename key_type,
    typename mapped_type,
    typename container_type,
    typename compare
>
std::size_t check_invalid_element_access_errors(
    cues::pool_multimap<key_type, mapped_type, container_type, compare> & ,
    typename cues::pool_multimap<key_type, mapped_type, container_type, compare>::key_type
)
{
    return 0u;
}


template<
    typename key_type,
    typename mapped_type,
    typename container_type,
    typename compare
>
std::size_t check_map_only_errors(
    cues::pool_map<key_type, mapped_type, container_type, compare> & c,
    std::size_t pool_size
)
{
    std::size_t errs = 0;
    try
    {
        std::size_t const safe_size = (std::max)(c.get_allocator().remaining_size(), c.max_size());
        std::pair<typename cues::pool_map<key_type, mapped_type, container_type, compare>::iterator, bool>
         fn_result;

        c.clear();
        fn_result = c.try_emplace(cues::test::make_value(9u), mapped_type{});
        //< should be begin, success
        assert(((void)"try_emplace should have succeeded at begin", fn_result.second && fn_result.first == c.cbegin()));
        errs += static_cast<std::size_t>(!fn_result.second || fn_result.first != c.cbegin());

        fn_result = c.try_emplace(c.cbegin(), cues::test::make_value(9u), mapped_type{});
        //< should be begin, failure
        assert(((void)"try_emplace should have been blocked by begin", !fn_result.second && fn_result.first == c.cbegin()));
        errs += static_cast<std::size_t>(fn_result.second || fn_result.first != c.cbegin());

        fn_result = c.try_emplace(c.cbegin(), cues::test::make_value(5u), mapped_type{});
        //< should be begin, success
        assert(((void)"try_emplace should have succeeded at begin", fn_result.second && fn_result.first == c.cbegin()));
        errs += static_cast<std::size_t>(!fn_result.second || fn_result.first != c.cbegin());

        fn_result = c.try_emplace(c.end(), cues::test::make_value(99u), mapped_type{});
        //< should be end - 1, success
        assert(((void)"try_emplace should have succeeded at end - 1", fn_result.second && fn_result.first == --(c.cend())));
        errs += static_cast<std::size_t>(!fn_result.second || fn_result.first != --(c.cend()));

        fn_result = c.try_emplace(cues::test::make_value(9u), mapped_type{});
        //< should be begin+1, failure
        assert(((void)"try_emplace should have been blocked by begin+1", !fn_result.second && fn_result.first == ++(c.cbegin())));
        errs += static_cast<std::size_t>(fn_result.second || fn_result.first != ++(c.cbegin()));

        fn_result = c.insert_or_assign(cues::test::make_value(99u), mapped_type{});
        //< should return end - 1, false
        assert(((void)"insert_or_assign should have assigned to end-1", !fn_result.second && fn_result.first == --(c.cend())));
        errs += static_cast<std::size_t>(fn_result.second || fn_result.first != --(c.cend()));

        fn_result = c.insert_or_assign(cues::test::make_value(120u), mapped_type{});
        //< should return end - 1, true
        assert(((void)"insert_or_assign should have inserted to end", fn_result.second && fn_result.first == --(c.cend())));
        errs += static_cast<std::size_t>(!fn_result.second || fn_result.first != --(c.cend()));

        fn_result = c.insert_or_assign(cues::test::make_value(5u), mapped_type{});
        //< should return begin, false
        assert(((void)"insert_or_assign should have assigned to begin", !fn_result.second && fn_result.first == c.cbegin()));
        errs += static_cast<std::size_t>(fn_result.second || fn_result.first != c.cbegin());

        c.erase(cues::test::make_value(5u));
        fn_result = c.insert_or_assign(cues::test::make_value(5u), mapped_type{});
        //< should return begin, true
        assert(((void)"insert_or_assign should have inserted to begin", fn_result.second && fn_result.first == c.cbegin()));
        errs += static_cast<std::size_t>(!fn_result.second || fn_result.first != c.cbegin());

    }
    catch(std::bad_alloc const &)
    {
        bool const allocator_maxed = c.get_allocator().remaining_size() == 0
                                  || c.get_allocator.block_size() < sizeof(std::pair<key_type const, mapped_type>);
        bool const container_maxed = c.size() == c.max_size();
        // verify size is max_size or pool is out of room
        assert(((void)"should not have thrown exception", allocator_maxed || container_maxed));
        errs += static_cast<std::size_t>(!allocator_maxed && !container_maxed);
    }
    catch(...)
    {
        assert(((void)"should not have thrown exception", false));
        ++errs;
    }
    return errs;
}

template<
    typename key_type,
    typename mapped_type,
    typename container_type,
    typename compare
>
std::size_t check_map_only_errors(
    cues::pool_multimap<key_type, mapped_type, container_type, compare> & ,
    std::size_t
)
{
    return 0u;
}



template<
    typename key_type,
    typename mapped_type,
    bool first_is_multimap,
    bool second_is_multimap,
    std::size_t Pool_Size,
    std::size_t First_Container_Size = Pool_Size/2,
    std::size_t Second_Container_Size = Pool_Size - First_Container_Size
>
void do_map_tests(std::size_t & cumulative_assertion_failures)
{
    static constexpr std::size_t const pool_size = Pool_Size;
    using test_element_type = std::pair<key_type const, mapped_type>;
    using test_pool_type = cues::homogenous_pool<test_element_type, pool_size>;

    using test_allocator_type = typename test_pool_type::allocator_type;
    test_pool_type test_pool;

    using first_container_type = cues::pool_list<
        test_element_type,
        First_Container_Size,
        typename cues::select_least_integer_by_digits<
            cues::minimum_bits(First_Container_Size), cues::integer_selector_signedness::use_unsigned
        >::type,
        test_allocator_type
    >;

    using second_container_type = cues::pool_list<
        test_element_type,
        Second_Container_Size,
        typename cues::select_least_integer_by_digits<
            cues::minimum_bits(Second_Container_Size), cues::integer_selector_signedness::use_unsigned
        >::type,
        test_allocator_type
    >;

    using first_map_type = typename std::conditional<
        first_is_multimap,
        cues::pool_multimap<key_type, mapped_type, cues::compare_first_or_value<std::less<key_type>, test_element_type>, first_container_type>,
        cues::pool_map<     key_type, mapped_type, cues::compare_first_or_value<std::less<key_type>, test_element_type>, first_container_type>
    >::type;

    // same constraints, potentially smaller container
    using second_map_type = typename std::conditional<
        second_is_multimap,
        cues::pool_multimap<key_type, mapped_type, cues::compare_first_or_value<std::less<key_type>, test_element_type>, second_container_type>,
        cues::pool_map<     key_type, mapped_type, cues::compare_first_or_value<std::less<key_type>, test_element_type>, second_container_type>
    >::type;

    // different constraints
    using third_map_type = typename std::conditional<
        first_is_multimap,
        cues::pool_multimap<key_type, mapped_type, cues::compare_first_or_value<std::greater<key_type>, test_element_type>, first_container_type>,
        cues::pool_map<     key_type, mapped_type, cues::compare_first_or_value<std::greater<key_type>, test_element_type>, first_container_type>
    >::type;

    // same ordering, but different, transparent comparator
    using fourth_map_type = typename std::conditional<
        first_is_multimap,
        cues::pool_multimap<key_type, mapped_type, cues::compare_first_or_value<cues::less, test_element_type>, first_container_type>,
        cues::pool_map<     key_type, mapped_type, cues::compare_first_or_value<cues::less, test_element_type>, first_container_type>
    >::type;


    static_assert(cues::provides_allocator<typename first_map_type::node_type>::value,
                      "allocated_unique_ptr defeats its purpose of providing the allocator");
    static_assert(cues::provides_allocator<typename second_map_type::node_type>::value,
                      "allocated_unique_ptr defeats its purpose of providing the allocator");
    static_assert(cues::provides_allocator<typename third_map_type::node_type>::value,
                      "allocated_unique_ptr defeats its purpose of providing the allocator");

    static_assert(
             std::is_same<typename first_map_type::allocator_type, typename second_map_type::allocator_type>::value
          && std::is_same<typename second_map_type::allocator_type, typename third_map_type::allocator_type>::value
          , "allocators are supposed to be the same for the maps."
    );


    first_map_type  first_map{ test_allocator_type{test_pool}};
    second_map_type second_map{test_allocator_type{test_pool}};
    third_map_type  third_map{ test_allocator_type{test_pool}};
    std::size_t const common_container_size = (std::min<std::size_t>)(first_map.max_size(), second_map.max_size());


    assert(((void)"new map not empty",first_map.empty()));
    cumulative_assertion_failures += static_cast<std::size_t>(!first_map.empty());

    assert(((void)"new map size != 0",first_map.size() == 0u));
    cumulative_assertion_failures += static_cast<std::size_t>(first_map.size() != 0u);

    assert(((void)"new map max_size != N",first_map.max_size() == First_Container_Size));
    cumulative_assertion_failures += static_cast<std::size_t>(first_map.max_size() != First_Container_Size);

    assert(((void)"new map max_size != N",second_map.max_size() == Second_Container_Size));
    cumulative_assertion_failures += static_cast<std::size_t>(second_map.max_size() != Second_Container_Size);

    assert(((void)"two empty maps do not compare equal",first_map == second_map));
    cumulative_assertion_failures += static_cast<std::size_t>(first_map != second_map);

    try{
        auto eitr = first_map.emplace_hint(first_map.cbegin(), make_value<key_type>(0u), make_value<mapped_type>(100u));

        assert(((void)"size != 1 after one emplace",first_map.size() == 1u));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map.size() != 1u);

        auto fitr = first_map.find(make_value<key_type>(0u));

        assert(((void)"could not find emplaced element",first_map.end() != fitr));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map.end() == fitr);

        assert(((void)"emplaced iterator does not compare equal to found iterator",eitr == fitr));
        cumulative_assertion_failures += static_cast<std::size_t>(eitr != fitr);

        cumulative_assertion_failures += check_valid_element_access_errors(first_map, eitr);

        cumulative_assertion_failures += check_invalid_element_access_errors(
            second_map, make_value<key_type>(0u)
        );

        first_map.erase(eitr);

    }
    catch(...)
    {
        assert(((void)"should not have thrown exception", false));
        ++cumulative_assertion_failures;
    }

    try{
        std::size_t const safe_size = (std::min<std::size_t>)(common_container_size/2, pool_size/2u);
        std::size_t const first_loop_count = safe_size;
        std::size_t const second_loop_count = safe_size;

        // fill first_map with odd numbers
        for (std::size_t count = 0;
            count < first_loop_count;
            ++count
        ) {

            first_map.emplace(make_value<test_element_type>(1u+2u*count));
        }
        assert(((void)"unexpected size after fill",first_map.size() == first_loop_count));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map.size() != first_loop_count);
        // verify sorted
        cumulative_assertion_failures += container_sort_errors(first_map.cbegin(), first_map.cend(), first_map.value_comp());
        // fill second-Map with even numbers
        {
            auto hint = first_map.begin();
            for (std::size_t count = 0;
                count < second_loop_count;
                ++count//, hint+=2
            ) {
                hint = first_map.emplace_hint(hint, make_value<test_element_type>(2u*count));
            }
        }
        assert(((void)"unexpected size after fill",first_map.size() == second_loop_count+first_loop_count));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map.size() != second_loop_count+first_loop_count);

        cumulative_assertion_failures += container_sort_errors(first_map.cbegin(), first_map.cend(), first_map.value_comp());

        // second map currently empty, steal entire first map
        second_map.merge(first_map);
        assert(((void)"unexpected size after merge",second_map.size() == 2u*safe_size));
        cumulative_assertion_failures += static_cast<std::size_t>(second_map.size() != 2u*safe_size);

        assert(((void)"unexpected size after merge",first_map.size() == 0u));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map.size() != 0u);

        // verify sorted
        cumulative_assertion_failures += container_sort_errors(second_map.cbegin(), second_map.cend(), second_map.value_comp());

        // merge into third map, which uses a different compare type
        third_map.merge(second_map);
        assert(((void)"unexpected size after merge",third_map.size() == 2u*safe_size));
        cumulative_assertion_failures += static_cast<std::size_t>(third_map.size() != 2u*safe_size);

        assert(((void)"unexpected size after merge",second_map.size() == 0u));
        cumulative_assertion_failures += static_cast<std::size_t>(second_map.size() != 0u);

        // verify sorted
        cumulative_assertion_failures += container_sort_errors(third_map.cbegin(), third_map.cend(), third_map.value_comp());

        // third_map sorts with greater, so it should be in the opposite order;
        // that is, if we start at the end and extract toward beginning, we should
        // always be inserting into the end of a map that sorts with less
        for( auto itr = third_map.crbegin();
            third_map.crend() - itr > 1 ;
            first_map.insert(first_map.cend(), third_map.extract((++itr).base())),
            second_map.insert(second_map.cend(), third_map.extract((++itr).base()))
        )
        { }
        // verify the size of first map and second map adds to the expected amount
        assert(((void)"unexpected size after extraction", third_map.size() <= 1u));
        cumulative_assertion_failures += static_cast<std::size_t>(third_map.size() > 1u);
        assert(((void)"unexpected size after extraction", second_map.size() == first_map.size()));
        cumulative_assertion_failures += static_cast<std::size_t>(second_map.size() != first_map.size());
        // 0u to promote to unsigned int if size-type is smaller, avoiding compiler warnings about
        // comparing signed to unsigned by promoting to unsigned int instead of int
        assert(((void)"unexpected size after extraction", 0u + second_map.size() + first_map.size() + third_map.size() == 2u*safe_size));
        cumulative_assertion_failures += static_cast<std::size_t>(
            0u + second_map.size()
            + first_map.size()
            + third_map.size() != 2u*safe_size
        );
        // because first_map took first, each element should be less than that in second_map
        {
            static_assert(
                std::is_same<
                    typename first_map_type::key_compare,
                    typename second_map_type::key_compare
                >::value,
            "change of comparison breaks relative ordering test.");

            auto first_itr = first_map.cbegin();
            auto second_itr = second_map.cbegin();
            auto comp = first_map.key_comp();
            for ( ;
                first_itr != first_map.cend() && second_itr != second_map.cend();
                ++first_itr, ++second_itr
            ) {
                bool const first_is_less = comp((*first_itr).first, (*second_itr).first);
                assert(((void)"ordering failure", first_is_less ));
                cumulative_assertion_failures += static_cast<std::size_t>(!first_is_less);
            }
        }
        // now test merge when not empty and interspersed
        first_map.merge(second_map);
        first_map.merge(third_map);

        assert(((void)"unexpected size after merge",first_map.size() == 2u*safe_size));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map.size() != 2u*safe_size);

        cumulative_assertion_failures += container_sort_errors(first_map.cbegin(), first_map.cend(), first_map.value_comp());

    }
    catch(...)
    {
        assert(((void)"should not have thrown exception", false));
        ++cumulative_assertion_failures;
    }

    try
    {
        // first_map may be using the full allocation range
        // erase until we are at most half the pool size,
        // erasing from the back
        for (std::size_t element_count = first_map.size();
            element_count > pool_size/2u;
            --element_count
        )
        {
            // pool_size/2u is at least 0, so if element_count,
            // representing the current size, is > 0,
            // --end is valid
            first_map.erase(--first_map.cend());
        }

        assert(((void)"unexpected size after erase",first_map.size() <= pool_size/2u));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map.size() > pool_size/2u);

        fourth_map_type fourth_map{ test_allocator_type{test_pool}};

        // different comparator, but same ordering.  copy from first into fourth
        for (auto itr = first_map.cbegin();
            itr != first_map.cend();
            ++itr
        )
        {
            if ((static_cast<std::size_t>(itr - first_map.cbegin()) & 1u) != 0)
            {
                fourth_map.insert(cues::test::make_value<test_element_type>(itr->first));
            }
            else
            {
                fourth_map.insert(fourth_map.cend(), cues::test::make_value<test_element_type>(itr->first));
            }
        }

        assert(((void)"unexpected size after erase",first_map.size() == fourth_map.size()));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map.size() != fourth_map.size());

        // manual equality comparison
        {
            auto first_itr = first_map.cbegin();
            auto fourth_itr = fourth_map.cbegin();
            for ( ;
                   first_itr != first_map.cend()
                && fourth_itr != fourth_map.cend();
                ++first_itr, ++fourth_itr
            ) {
                assert(((void)"copy or equivalent ordering failed",*first_itr == *fourth_itr));
                cumulative_assertion_failures += static_cast<std::size_t>(*first_itr != *fourth_itr);
            }
        }

        // change the value of the last key by removing and inserting a different one
        if (fourth_map.size() > 0)
        {
            fourth_map.erase(--fourth_map.end());
        }
        fourth_map.insert(fourth_map.end(), make_value<test_element_type>(2u*pool_size + 1u));

        assert(((void)"modification or equality operator failed", first_map != fourth_map));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map == fourth_map);
        // erase the last element
        if (fourth_map.size() > 0)
        {
            fourth_map.erase(--fourth_map.cend());
        }
        if (first_map.size() > 0)
        {
            first_map.erase(--first_map.cend());
        }
        // now they should compare equal again, test with operator==
        assert(((void)"copy or equivalent ordering failed", first_map == fourth_map));
        cumulative_assertion_failures += static_cast<std::size_t>(first_map != fourth_map);
        // now erase just one element, should be unequal
        if (fourth_map.size() > 0)
        {
            fourth_map.erase(--fourth_map.cend());
            assert(((void)"modification or equality operator failed", fourth_map != first_map));
            cumulative_assertion_failures += static_cast<std::size_t>(fourth_map == first_map);
        }

    }
    catch(...)
    {
        assert(((void)"should not have thrown exception", false));
        ++cumulative_assertion_failures;
    }

    try
    {
        std::size_t const safe_size = (std::min<std::size_t>)(common_container_size/2, pool_size/2);
        first_map.clear();
        second_map.clear();
        third_map.clear();

        {
            auto hint = first_map.cend();
            for (std::size_t count = 0;
                count < safe_size;
                ++count, ++hint
            )
            {
                // emplace unique
                hint = first_map.emplace_hint(hint, make_value<test_element_type>(2u*count));
                // attempt to emplace duplicate
                auto duplicate_itr = first_map.emplace_hint(hint+1, make_value<test_element_type>(2u*count));

                bool const duplicate_handled_ok =
                    // for map, the insertion should have failed, and returned an iterator to the blocking element
                    // (in this case, hint)
                    (!first_is_multimap && duplicate_itr == hint)
                    ||
                    // for multimap, the insertion should have succeeded, and returned an iterator to the
                    // inserted element, which should be hint + 1
                    (first_is_multimap && duplicate_itr == (hint + 1))
                ;

                assert(((void)"did not handle duplicate entries properly", duplicate_handled_ok));
                cumulative_assertion_failures += static_cast<std::size_t>(!duplicate_handled_ok);
                // if we inserted a duplicate, advance the hint again to skip the duplicate
                hint += (first_is_multimap ? 1u : 0u);
            }
        }
        // verify that container is in order, whether it has duplicates or not
        cumulative_assertion_failures += container_sort_errors(first_map.cbegin(), first_map.cend(), first_map.value_comp());

        {
            std::pair<typename first_map_type::const_iterator,
                      typename first_map_type::const_iterator
            > range;

            for (std::size_t count = 0;
                count < 2u*safe_size;
                ++count
            )
            {
                auto val = make_value<key_type>(count);
                range = const_cast<first_map_type const &>(first_map).equal_range(val);

                assert(((void)"violation of range order", range.first <= range.second ));
                cumulative_assertion_failures += static_cast<std::size_t>(range.first > range.second);

                {
                    typename first_map_type::const_iterator temp_itr = const_cast<first_map_type const &>(first_map).lower_bound(val);
                    assert(((void)"equal_range().first should equal lower_bound()", range.first == temp_itr));
                    cumulative_assertion_failures += static_cast<std::size_t>(range.first != temp_itr);

                    temp_itr = const_cast<first_map_type const &>(first_map).upper_bound(val);
                    assert(((void)"equal_range().second should equal upper_bound()", range.second == temp_itr));
                    cumulative_assertion_failures += static_cast<std::size_t>(range.second != temp_itr);
                }

                typename first_container_type::size_type eq_size = range.second - range.first;
                {
                    typename first_container_type::size_type ct_size = first_map.count(val);
                    assert(((void)"range size does not match count", eq_size == ct_size));
                    cumulative_assertion_failures += static_cast<std::size_t>(eq_size != ct_size);
                }

                // we previously inserted even values, and we tried to insert two, so:
                // count is odd: range should be 0
                // count is even and this is multimap: count should be 2
                // count is even and this is not multimap : count should be 1
                {
                    typename first_container_type::size_type expected_size = 0;
                    if ((count & 1u) == 0)
                    {
                        if CUES_CONSTEXPR_OR_RUNTIME_IF (first_is_multimap)
                        {
                            expected_size = 2;
                        }
                        else
                        {
                            expected_size = 1;
                        }
                    }
                    assert(((void)"unexpected range size", eq_size == expected_size));
                    cumulative_assertion_failures += static_cast<std::size_t>(eq_size != expected_size);
                }

                {
                    bool exists = first_map.contains(val);
                    assert(((void)"contains did not match result of equal_range", exists == (range.second != range.first)));
                    cumulative_assertion_failures += static_cast<std::size_t>(exists != (range.second != range.first));

                }
            }
        }

        // now that we might have duplicates, try merging
        second_map.merge(first_map);
        {
            bool const sizing_ok =
                // if both maps were multimaps, second should have 2*safe_size elements and first should have 0.
                (first_is_multimap && second_is_multimap && second_map.size() == 2u*safe_size && first_map.size() == 0)
                // if second is map and first is multimap, second and first should both
                // have safe_size elements.
                ||
                (first_is_multimap && !second_is_multimap && second_map.size() == safe_size && first_map.size() == safe_size)
                ||
                // if first is map, second should have safe_size and first should have zero
                // whether second is map or multimap
                (!first_is_multimap && second_map.size() == safe_size && first_map.size() == 0)
            ;
            assert(((void)"unexpected size after merge", sizing_ok));
            cumulative_assertion_failures += static_cast<std::size_t>(!sizing_ok);
        }
        // verify that both are in order, whether it has duplicates or not
        cumulative_assertion_failures += container_sort_errors(first_map.cbegin(), first_map.cend(), first_map.value_comp());
        cumulative_assertion_failures += container_sort_errors(second_map.cbegin(), second_map.cend(), second_map.value_comp());
        // merge back into first
        first_map.merge(second_map);
        {
            // the only way to have all 2*safe_size elements is if first is a multimap, in which
            // case it should now have them all.
            // if first is not a multimap, it only had safe_size elements to begin with,
            // which should have been returned
            // in both cases, second_map should be empty
            bool const sizing_ok = second_map.size() == 0 &&
                (first_map.size() == safe_size * (first_is_multimap ? 2u : 1u))
            ;
            assert(((void)"unexpected size after merge", sizing_ok));
            cumulative_assertion_failures += static_cast<std::size_t>(!sizing_ok);
        }
        first_map.clear();

        // fill again, with potential duplicates
        {
            auto hint = second_map.cend();
            for (std::size_t count = 0;
                count < safe_size;
                ++count, ++hint
            )
            {
                hint = second_map.insert(hint, cues::test::make_value<test_element_type>(1u + count));
                // attempt to emplace duplicate
                auto duplicate_itr = second_map.insert(hint+1, cues::test::make_value<test_element_type>(hint->first));

                bool const duplicate_handled_ok =
                    // for map, the insertion should have failed, and returned an iterator to the blocking element
                    // (in this case, hint)
                    (!second_is_multimap && duplicate_itr == hint)
                    ||
                    // for multimap, the insertion should have succeeded, and returned an iterator to the
                    // inserted element, which should be hint + 1
                    (second_is_multimap && duplicate_itr == (hint + 1))
                ;

                assert(((void)"did not handle duplicate entries properly", duplicate_handled_ok));
                cumulative_assertion_failures += static_cast<std::size_t>(!duplicate_handled_ok);
                // if we inserted a duplicate, advance the hint again to skip the duplicate
                hint += (second_is_multimap ? 1u : 0u);
            }
        }

        // merge with third_map, which uses a different ordering
        third_map.merge(second_map);
        {
            bool const sizing_ok =
                // as with first and second, except now it is second and third;
                // third is multimap if first is multiomap
                (second_is_multimap && first_is_multimap && third_map.size() == 2u*safe_size && second_map.size() == 0)
                // as with first and second, but now second and third
                ||
                (second_is_multimap && !first_is_multimap && third_map.size() == safe_size && second_map.size() == safe_size)
                ||
                // as with first and second, but now second and third
                (!second_is_multimap && third_map.size() == safe_size && second_map.size() == 0)
            ;
            assert(((void)"unexpected size after merge", sizing_ok));
            cumulative_assertion_failures += static_cast<std::size_t>(!sizing_ok);
        }

        // verify that both are in order, whether it has duplicates or not
        cumulative_assertion_failures += container_sort_errors(third_map.cbegin(), third_map.cend(), third_map.value_comp());
        cumulative_assertion_failures += container_sort_errors(second_map.cbegin(), second_map.cend(), second_map.value_comp());

        // merge back
        second_map.merge(third_map);
        {
            // as with first and second, but now with second and third
            bool const sizing_ok = third_map.size() == 0 &&
                (second_map.size() == safe_size * (second_is_multimap ? 2u : 1u))
            ;
            assert(((void)"unexpected size after merge", sizing_ok));
            cumulative_assertion_failures += static_cast<std::size_t>(!sizing_ok);
        }
        // verify sorting
        cumulative_assertion_failures += container_sort_errors(second_map.cbegin(), second_map.cend(), second_map.value_comp());

    }
    catch(...)
    {
        assert(((void)"should not have thrown exception", false));
        ++cumulative_assertion_failures;
    }

}


std::size_t check_pool_map_test_runtime_assertions()
{
    std::size_t accumulated_violations = 0;

    do_map_tests<std::uint_least16_t, std::uint_least64_t, false, false, 32u>(accumulated_violations);

    do_map_tests<std::uint_least16_t, std::uint_least32_t, false, true,  65535u>(accumulated_violations);

    do_map_tests<std::uint_least32_t, std::uint_least64_t, true,  false, 29u>(accumulated_violations);
    do_map_tests<std::uint_least8_t,  std::uint_least8_t,  true,  true,  255u>(accumulated_violations);

    // test absolute smallest possible
    do_map_tests<std::uint_least8_t, std::uint_least8_t, false, false, 2u>(accumulated_violations);

    // test move-only mapped type

    do_map_tests<std::uint_least8_t, cues::test::move_only, false, false, 63u>(accumulated_violations);
    do_map_tests<std::uint_least8_t, cues::test::move_only, true, true, 127u>(accumulated_violations);

    // test variant key
    do_map_tests<
        CUES_VARIANT_NAMESPACE::variant<std::pair<std::uint_least16_t, std::size_t>, std::size_t> ,
        std::uint_least32_t,
        false,
        false,
        32u
    >(accumulated_violations);

    //test use of pair as key
    do_map_tests<
        std::pair<std::size_t, std::uint_least32_t>,
        std::uint_least32_t,
        false,
        false,
        16u
    >(accumulated_violations);


    return accumulated_violations;
}

}
}

#endif
