#ifndef UTF8_ITERATOR_TEST_HPP_INCLUDED
#define UTF8_ITERATOR_TEST_HPP_INCLUDED

#include "cues/utf8_iterator.hpp"

#include <cstddef>
#include <array>
#include <iterator>

#if defined CUES_HAS_STRING_VIEW
    #include <string_view>
#endif


namespace cues {
namespace test {

std::size_t check_utf8_iterator_test_runtime_assertions();


// std::distance is only constexpr since c++17, but operator++ can be constexpr
// already in c++14.  For these simple test cases, O(n) vs O(1) is not worth
// SFINAE overloads, especially since utf8 iterator is not random access anyway
// and we only use char * to compare to the raw strings sometimes.
template<typename ITR>
CUES_CONSTEXPR_AFTER_CPP14 typename std::iterator_traits<ITR>::difference_type distance(ITR itr_begin, ITR itr_end)
{
    using acc_type = typename std::iterator_traits<ITR>::difference_type;
    acc_type retval = 0;
    for ( ;
        itr_begin != itr_end;
        ++itr_begin, ++retval
    ) {
    }
    return retval;
}

template<typename char_type, std::size_t N>
CUES_CONSTEXPR_AFTER_CPP14 bool verify_fewer_code_points_than_characters(char_type const (&str)[N])
{
    return distance(utf8_begin(str), utf8_end(str))
    <  distance(&str[0], &str[0]+sizeof(str));
}

template<typename char_type, std::size_t N>
CUES_CONSTEXPR_AFTER_CPP14 bool verify_skip_fewer_than_replaced(char_type const (&str)[N])
{
    using container_type = typename std::remove_reference<decltype(str)>::type;

    return distance(
        utf8_begin<container_type, skip_on_invalid_unicode>(str),
        utf8_end<container_type, skip_on_invalid_unicode>(str)
    )
    <  distance(
        utf8_begin<container_type, replace_on_invalid_unicode>(str),
        utf8_end<container_type, replace_on_invalid_unicode>(str)
    );
}

template<typename char_type, std::size_t N>
CUES_CONSTEXPR_AFTER_CPP14 bool verify_skip_less_replaced_less_chars(char_type const (&str)[N])
{
    return verify_fewer_code_points_than_characters(str)
    && verify_skip_fewer_than_replaced(str);
}

template<typename ITR>
CUES_CONSTEXPR_AFTER_CPP14 typename std::iterator_traits<ITR>::difference_type count_replacement_chars(ITR itr_begin, ITR itr_end)
{
    typename std::iterator_traits<ITR>::difference_type replacements = 0;
    for (;
        itr_begin != itr_end;
        ++itr_begin
    ) {
        replacements += static_cast<unsigned>(*itr_begin == unicode_replacement_code);
    }
    return replacements;
}

template<typename char_type, std::size_t N>
CUES_CONSTEXPR_AFTER_CPP14 std::ptrdiff_t count_replacement_chars( char_type (&arr)[N] )
{
    return count_replacement_chars(utf8_begin(arr), utf8_end(arr));
}

template<typename char_type, std::size_t N>
CUES_CONSTEXPR_AFTER_CPP14 bool ascii_matches_utf32(char_type (&str)[N])
{
    std::size_t index = 0;
    for (auto itr = utf8_begin(str), itr_end = utf8_end(str);
        itr != itr_end;
        ++itr, ++index
    ) {
        if (index >= N || *itr != static_cast<unsigned char>(str[index]))
        {
            return false;
        }
    }
    return true;
}

// also valid utf-8, one-byte sequences only
// always char
constexpr char const valid_ascii_sequence[] = "Here it is:\a some valid\t ascii text!";

#if defined ( CUES_HAS_NON_CONST_CONSTEXPR )

static_assert(
    distance(&valid_ascii_sequence[0], &valid_ascii_sequence[0]+sizeof(valid_ascii_sequence))
    == distance(utf8_begin(valid_ascii_sequence), utf8_end(valid_ascii_sequence)),
    "utf8 iterator failed to advance properly through ascii text."
);

static_assert(ascii_matches_utf32(valid_ascii_sequence),"ascii did not match its utf32 value");

#endif

// utf-8 literals changed type in c++20
#if __cplusplus >= 202002L
using utf8_char_type = char8_t;
#else
using utf8_char_type = char;
#endif
    // some single-byte, some two-byte
constexpr utf8_char_type const valid_utf8_sequence_1[] = u8"Mixéd, ńo ööpß, mæt some Ļatiŋ-×tendeđ";
    // some single-byte, some 3-byte
constexpr utf8_char_type const valid_utf8_sequence_2[] = u8"Bengali numerals: ১২৩৪.৫৬৭৮৯০";
// some single-byte, some 4-byte
constexpr utf8_char_type const valid_utf8_sequence_3[] = u8"Cherokee: ᎠᎡᎥᎡᏀ\nMath:∀∃∈∋\nsymbols:😟🤔🪐🫀";

#if defined ( CUES_HAS_NON_CONST_CONSTEXPR )

static_assert(
    verify_fewer_code_points_than_characters(valid_utf8_sequence_1),
    "utf8 codes not less than character count for string with multibyte characters."
);

static_assert(
    verify_fewer_code_points_than_characters(valid_utf8_sequence_2),
    "utf8 codes not less than character count for string with multibyte characters."
);

static_assert(
    verify_fewer_code_points_than_characters(valid_utf8_sequence_3),
    "utf8 codes not less than character count for string with multibyte characters."
);

#endif


constexpr unsigned char const invalid_utf8_sequence_1[] = {
    'V','a','l','i','d',' ',
    // first byte would be valid, and second byte
    // could be valid for a different first byte,
    // but the combination is ill-formed
    0xE0, 0x9F, 0x80,
    ',',' ',
    'a','r','o','u','n','d',' ',
    'i','n','v','a','l','i','d','\0'
};

// 4-byte sequence+null that would be valid except Unicode says
// 0xF5 is not valid.
constexpr unsigned char const invalid_utf8_sequence_2[] = {
    0xF5,0xA0,0x80,0x81,0
};
// 6-byte sequence that was valid as proposed in 1993 but not since 2003
constexpr unsigned char const invalid_utf8_sequence_3[] = {
    0xFD,0xBF,0x80,0x80,0x80,0x80, 0
};

// sequence that would be valid if not for abrupt null at the end
constexpr unsigned char const invalid_utf8_sequence_4[] = {
    0xF0, 0x9F, 0xF0, 0x80, 0xF0, 0x9F, 0x98, 0
};

// sequence that starts valid but misses continuation byte at end
constexpr unsigned char const invalid_utf8_sequence_5[] = {
    0xF0, 0x9F, 0xF0, 0x80, 0xF0, 0x9F, 0x98, 0x3B, 0
};

// sequence that starts valid but has invalid byte at end
constexpr unsigned char const invalid_utf8_sequence_6[] = {
    0xF0, 0x9F, 0xF0, 0x80, 0xF0, 0x9F, 0x98, 0xC0, 0
};

// sequence that would be valid but misses continuation byte where one is expected
constexpr unsigned char const invalid_utf8_sequence_7[] = {
    0x3B, 0xF0, 0x9F, 0x3B, 0x80, 0x3B, 0
};

// sequence that is valid except for one invalid byte
constexpr unsigned char const invalid_utf8_sequence_8[] = {
    0x3B, 0xF0, 0x9F, 0x80, 0xC0, 0x3B, 0
};

// sequence that starts with invalid byte (unexpected continuation byte)
constexpr unsigned char const invalid_utf8_sequence_9[] = {
    0x80, 0x9F, 0x98, 0x80, 0x3B, 0
};
// sequence that starts with invalid byte (byte is always invalid)
constexpr unsigned char const invalid_utf8_sequence_10[] = {
    0xC0, 0x9F, 0x98, 0x80, 0x3B, 0
};

constexpr std::size_t maximal_subpart_replacement_count_invalid_tests[] = {
    3u,
    4u,
    6u,
    4u,
    4u,
    5u,
    2u,
    2u,
    4u,
    4u
};

constexpr std::size_t valid_code_count_invalid_tests[] = {
    21u,
    0u,
    0u,

};

#if defined ( CUES_HAS_NON_CONST_CONSTEXPR )

static_assert(
    verify_skip_fewer_than_replaced(invalid_utf8_sequence_1),
    "failed invalid utf8 test sequence 1"
);

static_assert(
    verify_skip_fewer_than_replaced(invalid_utf8_sequence_2),
    "skip should be less than replace for test sequence 2."
);

static_assert(
    verify_skip_fewer_than_replaced(invalid_utf8_sequence_3),
    "skip should be less than replace for test sequence 3."
);

static_assert(
    verify_skip_less_replaced_less_chars(invalid_utf8_sequence_4),
    "test of invalid sequence 4 failed"
);

static_assert(
    verify_skip_less_replaced_less_chars(invalid_utf8_sequence_5),
    "test of invalid sequence 5 failed"
);

static_assert(
    verify_skip_less_replaced_less_chars(invalid_utf8_sequence_6),
    "test of invalid sequence 6 failed."
);

static_assert(
    verify_fewer_code_points_than_characters(invalid_utf8_sequence_7),
    "test of invalid sequence 7 failed"
);

static_assert(
    verify_skip_less_replaced_less_chars(invalid_utf8_sequence_8),
    "test of invalid sequence 8 failed"
);

static_assert(
    verify_skip_fewer_than_replaced(invalid_utf8_sequence_9),
    "test of invalid sequence 9 failed"
);

static_assert(
    verify_skip_fewer_than_replaced(invalid_utf8_sequence_10),
    "test of invalid sequence 10 failed"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_1) == maximal_subpart_replacement_count_invalid_tests[0],
    "unexpected number of replacement characters in sequence 1"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_2) == maximal_subpart_replacement_count_invalid_tests[1],
    "unexpected number of replacement characters in sequence 2"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_3) == maximal_subpart_replacement_count_invalid_tests[2],
    "unexpected number of replacement characters in sequence 3"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_4) == maximal_subpart_replacement_count_invalid_tests[3],
    "unexpected number of replacement characters in sequence 4"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_5) == maximal_subpart_replacement_count_invalid_tests[4],
    "unexpected number of replacement characters in sequence 5"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_6) == maximal_subpart_replacement_count_invalid_tests[5],
    "unexpected number of replacement characters in sequence 6"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_7) == maximal_subpart_replacement_count_invalid_tests[6],
    "unexpected number of replacement characters in sequence 7"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_8) == maximal_subpart_replacement_count_invalid_tests[7],
    "unexpected number of replacement characters in sequence 8"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_9) == maximal_subpart_replacement_count_invalid_tests[8],
    "unexpected number of replacement characters in sequence 9"
);

static_assert(
    count_replacement_chars(invalid_utf8_sequence_10) == maximal_subpart_replacement_count_invalid_tests[9],
    "unexpected number of replacement characters in sequence 10"
);

#endif

}
}

#endif // UTF8_ITERATOR_TEST_HPP_INCLUDED
