#ifndef CUES_POOL_LIST_HPP
#define CUES_POOL_LIST_HPP

#include "cues/version_helpers.hpp" // includes <version> or <ciso646>
#if (__cplusplus < 201703L && (!defined ( __cpp_lib_launder) || !defined(__cpp_lib_if_constexpr)))
    #error "cues::pool_list requires std::launder and if constexpr, but they are not available"
#endif

#include <cstddef>
#include <array>
#include <type_traits>
#include <iterator>
#include <new>
#include <stdexcept>
#include <algorithm>

#include "cues/type_traits.hpp"
#include "cues/constexpr_math.hpp"
#include "cues/integer_selector.hpp"
#include "cues/homogenous_pool.hpp"
#include "cues/non_owning_ptr.hpp"
#include "cues/wrapped_number.hpp"
#include "cues/allocator_traits.hpp"
#include "cues/memory.hpp"

namespace cues {


//! *************************************************************************
//! Overview:

//! Container that uses pool_allocator to allocate its elements.
//! Pointers/references to elements are valid until erasure.
//! It is tailored to minimize time for LIFO allocations,
//! though it also permits random erasure in O(1) if order need not be
//! preserved.
//! Note that the (valid) pointers or references may point to different
//! *values* if the non-const iterator types are used, including with standard
//! algorithms.
//! An alternative mechanism is provided to reorder elements without
//! swapping, moving, or copying value_types, so that, when used in such
//! a manner, pointers and references remain pointing to the same value
//! after for example using std::sort().

//! Meets the Container requirements, ReversibleContainer requirements,
//! AllocatorAwareContainer requirements (though it will not accept arbitrary
//! allocators, they must satisfy cues::is_contiguous_allocator), meets the
//! SequenceContainer requirements.

//! Most closely matches the complexity provided by std::vector, though the
//! the elements being affected are size_type instead of value_type, which
//! may result in faster completion despite the same complexity.
//! Includes also some functions similar to std::list.

//! Iterators are random access iterators, but general algorithms that use them
//! to modify the container contents, for example sorting algorithms, may lose
//! out on efficiency by moving or copying objects when indices could be changed
//! instead.  An indirect functor, and indirect iterators, are provided to allow
//! these algorithms to apply functions (like comparison functions) to the
//! value_type but perform writes to the index_type.

//! *************************************************************************
//! Iterator and reference invalidation:

//! Erasing:
//! Like boost::stable vector but unlike std::vector, when erasing,
//! pointers/references always remain valid, other than to the erased element,
//! regardless of position in the sequence.
//! Like std::vector but unlike boost::stable_vector, iterators at or past the
//! erased element are invalidated, but earlier iterators are not.
//! Note that because pop_back always erases at the end, only the erased iterator
//! and the end iterator will be invalidated.
//! Because erase() returns a (valid) iterator to the element after the erased one
//! (or to end), erasing can be done in a loop by assigning the returned iterator
//! to your loop iterator, though a loop in reverse order
//! (rbegin to rend instead of begin to end) should not save iterators it encounters
//! along the way as they will be invalidated.

//! Note that members called erase() will preserve order and have O(n*m) time complexity,
//! where n is the container size and m is the count of erased elements,
//! where members called reordering_erase() may change order but have O(m) time complexity.


//! Inserting:
//! For calls to insert() or emplace(), pointers/references remain valid, iterators past the inserted element
//! do not.  Note that push_back() and emplace_back() always insert at the end, so only the end iterator
//! will be invalidated; those calls also take O(1).
//! Unlike erase, there is no reordering version of insert; if you don't care about order, you can
//! use push_back or emplace_back to add an element in O(1).



//! *************************************************************************

//! comparison to alternatives

//! Unlike boost::stable_vector, this class does not add pointers to the stored type,
//! giving up iterator stability in exchange for smaller container size.
//! Unlike boost::stable_vector, instead of storing an array of pointers to nodes,
//! this container stores indices, which for size_type smaller than the size of
//! a pointer would further reduce container size.
//! Because iterators store a pointer to the container and position in the index
//! array, the iterators are likely to be larger than those of boost::stable_vector,
//! so this container is likely not as well-suited for applications where the number
//! of iterators in memory at any given time is not smaller than the number of
//! objects in the container.

//! Unlike std::vector or boost::vector, this container provides as member functions
//! erasure that can reorder the contents (as reordering_erase and reordering_pop_front)
//! that use "swap and pop" to complete in O(1) time.  This could also be done manually
//! but member access to private internals means the objects themselves do not need to
//! be swapped, just the indices.

//! Because it uses homogenous_pool to do its allocations, the objects will also
//! be contiguous in memory iff always allocated in LIFO order on a pool with sufficient
//! contiguous free space to do so (the simplest case being a newly constructed pool)



// forward declarations
template<
    typename T,
    std::size_t N,
    typename SIZE_TYPE,
    class Allocator,
    bool IS_CONST_ITERATOR = std::is_const<typename std::remove_all_extents<T>::type>::value
>
class pool_list_iterator;

// helper class to differentiate pool_list's use of wrapped_number
// without making it different for each list size or size_type
template<class T, class Allocator>
struct pool_list_discriminator
{};

template<
    class T,
    std::size_t N,
    typename SIZE_TYPE = typename cues::select_least_integer_by_digits<
          cues::minimum_bits(N)
        , cues::integer_selector_signedness::use_unsigned
    >::type,
    class Allocator = homogenous_allocator<T, N, SIZE_TYPE, ((alignof(T) > alignof(SIZE_TYPE)) ? alignof(T) : alignof(SIZE_TYPE)), T>
>
class pool_list
{

    static_assert(std::is_base_of<
            contiguous_allocator_tag,
            typename cues::allocator_traits<Allocator>::allocator_category
        >::value, "pool_list requires an allocator with contiguous storage."
    );

    template<typename U, std::size_t M, typename SZ, class Al>
    friend class pool_list;

    public:

        using size_type              = SIZE_TYPE;
        using value_type             = T;
        using reference              = T &;
        using const_reference        = T const &;
        using pointer                = T *;
        using const_pointer          = T const *;

        using iterator                  = pool_list_iterator<T , N, size_type, Allocator, false>;
        using const_iterator            = pool_list_iterator<T , N, size_type, Allocator, true>;

        using reverse_iterator          = std::reverse_iterator<iterator>;
        using const_reverse_iterator    = std::reverse_iterator<const_iterator>;

        using allocator_type            = Allocator;
        using index_type                = cues::wrapped_number<typename allocator_type::size_type, 0, pool_list_discriminator<value_type, allocator_type>>;
        //! iterator to the array of index_type, rather than an iterator to the value_type.
        //! can be used with indirect_comparator<> to apply standard algorithms
        //! without swapping, moving, or copying value_types, because it will compare
        //! against the value_type but swap the index_type.
        using indirect_iterator         = index_type *;
        using indirect_const_iterator   = index_type const *;

        using node_type = cues::allocated_unique_ptr<value_type, allocator_type>;

        static_assert(cues::provides_allocator<node_type>::value,
                      "node_type is supposed to provide an allocator");

        //! empty container with an allocator
        explicit pool_list(allocator_type const & a) noexcept
        : my_alloc(a)
        , map_end(0)
        {}

        //! n values allocated via allocator (if possible)
        pool_list(allocator_type const & a, size_type n, T const & val)
        : my_alloc(a)
        , map_end(0)
        {
            for (; n > 0; --n)
            {
                push_back(val);
            }
        }

        template<typename Itr, class Enable =
            typename std::enable_if<
                std::is_base_of<std::input_iterator_tag, typename std::iterator_traits<Itr>::iterator_category>::value
            >::type
        >
        pool_list(allocator_type const & a, Itr first, Itr past_last)
        : my_alloc(a)
        , map_end(0)
        {
            for (; first != past_last; ++first)
            {
                emplace_back(*first);
            }
        }

        pool_list(allocator_type const & a, std::initializer_list<T> il)
        : pool_list(a, il.begin(), il.end())
        {
        }


        pool_list(pool_list && other) noexcept
        : my_alloc(std::move(other.my_alloc))
        , map_end(other.map_end)
        , index_map(other.index_map)
        {
            other.map_end = size_type{0};
        }

        pool_list(pool_list const & other)
        : my_alloc(std::allocator_traits<Allocator>::select_on_container_copy_construction(other.my_alloc))
        , map_end(0)
        {
            for (;
                map_end < other.map_end;
                ++map_end
            )
            {
                this->push_back( other[map_end]);
            }
        }

        //! Converting constructor for lvalue reference to pool_list
        //! with same value_type and allocator_type
        template<std::size_t FOREIGN_N, typename FOREIGN_SIZE_TYPE>
        explicit pool_list(pool_list<value_type, FOREIGN_N, FOREIGN_SIZE_TYPE, allocator_type> const & other)
        : my_alloc(std::allocator_traits<allocator_type>::select_on_container_copy_construction(other.get_allocator()))
        , map_end(0)
        {
            for (;
                map_end < other.map_end;
                ++map_end
            )
            {
                this->push_back( other[static_cast<FOREIGN_SIZE_TYPE>(map_end)]);
            }
        }

        //! Converting constructor for rvalue reference to pool_list
        //! with same value_type and allocator_type
        template<std::size_t FOREIGN_N, typename FOREIGN_SIZE_TYPE>
        explicit pool_list(pool_list<value_type, FOREIGN_N, FOREIGN_SIZE_TYPE, allocator_type> && other) noexcept(FOREIGN_N <= N)
        : my_alloc(std::move(other.my_alloc))
        , map_end(0u)
        {
            // splice should only fail if the input range is larger than
            // the container can hold, which is the condition on the noexcept
            // specification.
            this->splice(cbegin(), other, other.cbegin(), other.cend());
        }

        pool_list & operator=(pool_list && other)
        {
            if (&other == this) {return *this;}

            common_move_assign(std::move(other));

            return *this;
        }

        pool_list & operator=(pool_list const & other)
        {
            if (&other == this) {return *this;}

            common_copy_assign(other);

            return *this;
        }

        template<
            std::size_t FOREIGN_N,
            typename FOREIGN_SIZE_TYPE
        >
        pool_list & operator=(pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> const & other)
        {
            // note that the non-templated copy assignment operator should be
            // unambiguously preferred over the template overload, which means
            // this cannot == &other, because the types are different.
            // instead of self-assignment check, we need a size check.
            if (other.map_end > this->max_size())
            {
                throw std::bad_alloc();
            }
            common_copy_assign(other);
            return *this;
        }

        template<
            std::size_t FOREIGN_N,
            typename FOREIGN_SIZE_TYPE
        >
        pool_list & operator=(pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> && other)
        {
            if (other.map_end > this->max_size())
            {
                throw std::bad_alloc();
            }
            common_move_assign(std::move(other));
            return *this;
        }

        ~pool_list() noexcept
        {
            // if we have any items, we better have an allocator
            assert(((void)"memory leak", (map_end == 0 || my_alloc != nullptr)));

            clear();
        }

        allocator_type get_allocator() const noexcept
        {
            return my_alloc;
        }

        friend void swap(pool_list & lhs, pool_list & rhs) noexcept(noexcept(swap(lhs.index_map, rhs.index_map)))
        {
            using std::swap;
            if (std::allocator_traits<allocator_type>::propagate_on_container_swap::value)
            {
                swap(lhs.my_alloc, rhs.my_alloc);
            }
            // a temporary array for swapping adds linear time and linear memory;
            // can we avoid it?

            if (rhs.empty())
            {
                if (!lhs.empty())
                {
                    rhs.index_map = lhs.index_map;
                    rhs.map_end   = lhs.map_end;
                    lhs.map_end   = 0;
                }
                // if both are empty, there is nothing to do
            }
            else if (lhs.empty())
            {
                // already know !rhs.empty since we checked it
                lhs.index_map = rhs.index_map;
                lhs.map_end = rhs.map_end;
                rhs.map_end = 0;
            }
            else
            {
                // neither empty, have to swap all of it
                swap(lhs.index_map, rhs.index_map);
                swap(lhs.map_end, rhs.map_end);
            }
        }

        //! POSTCONDITION: all pointers, references, and iterators
        //! are invalidated, all objects that were in the container
        //! have been destructed and memory released.
        void clear() noexcept
        {
            if (nullptr == my_alloc) {return;}

            for (T * obj_ptr;
                map_end > 0;
            )
            {
                obj_ptr = local_index_to_pointer(--map_end);
                // destruct object
                obj_ptr->~T();
                // free space
                my_alloc.deallocate(obj_ptr, 1);
            }
        }

        //! PRECONDITION: idx is within range
        const_reference operator[](size_type idx) const noexcept
        {
            return *local_index_to_pointer(idx);
        }

        //! PRECONDITION: container is not empty
        reference operator[](size_type idx) noexcept
        {
            return *local_index_to_pointer(idx);
        }

        const_reference at(size_type idx) const
        {
            return *((idx >= this->map_end) ?
                throw std::out_of_range("at access out of bounds")
                :
                local_index_to_pointer(idx)
            );
        }

        reference at(size_type idx)
        {
            return const_cast<reference>(
                const_cast<pool_list const *>(this)->at(idx)
            );
        }

        //! PRECONDITION: container is not empty
        const_reference front() const noexcept
        {
            return *local_index_to_pointer(size_type{0});
        }

        //! PRECONDITION: container is not empty
        reference front() noexcept
        {
            return *local_index_to_pointer(size_type{0});
        }

        //! PRECONDITION: container is not empty
        const_reference back() const noexcept
        {
            return *local_index_to_pointer(map_end > 0 ? (map_end - 1) : size_type{0});
        }

        //! PRECONDITION: container is not empty
        reference back() noexcept
        {
            return const_cast<reference>(
                const_cast<pool_list const *>(this)->back()
            );
        }

        //! PRECONDITION: has valid allocator and room
        //! POSTCONDITION: object is at end of container,
        //!                end() iterators are invalidated,
        //!                other iterators and references are still valid.
        void push_back(T const & to_copy)
        {
            // may throw, won't construct nor publish if so
            T * storage = allocate_one();
            // will catch exceptions during construction,
            // deallocate, then rethrow
            construct_one(storage, to_copy);
            // only called if no exceptions, so stack of objects
            // remains valid
            publish_back(storage);
        }

        //! PRECONDITION: as with push_back(T const &)
        //! POSTCONDITION: as with push_back(T const &)
        void push_back(T && to_move)
        {
            T * storage = allocate_one();
            construct_one(storage, std::move(to_move));
            publish_back(storage);
        }

        //! PRECONDITION: container is not empty and has valid allocator
        //! POSTCONDITION: last element is destructed, memory is released,
        //!                end() iterator is invalidated, pointers/references
        //!                to last element are invalidated, other iterators
        //!                and references are still valid.
        void pop_back() noexcept
        {
            static_assert(std::is_nothrow_destructible<T>::value, "assumption violation: destructor of T can throw.");
            assert(((void)"attempted to pop when empty", map_end > 0));
            assert(((void)"attempted to deallocate without an allocator", nullptr != my_alloc));
            // pop from stack and get pointer to object
            T * obj = local_index_to_pointer(--map_end);
            // destruct
            obj->~T();
            // release memory
            my_alloc.deallocate(obj, 1);
        }

        //! O(n) complexity
        //! if you don't care about ordering, use push_back instead
        //! PRECONDITION:  container has valid allocator
        void push_front(T const & to_copy)
        {
            T * storage = allocate_one();
            construct_one(storage, to_copy);
            linear_publish_at_existing(storage, 0);
        }

        void push_front(T && to_move)
        {
            T * storage = allocate_one();
            construct_one(storage, std::move(to_move));
            linear_publish_at_existing(storage, 0);
        }

        void pop_front() noexcept
        {
            linear_pop_from_existing(0);
        }


        //! O(n) complexity
        //! if you don't care about ordering, use push_back instead
        //! PRECONDITION:  container has valid allocator
        //! POSTCONDITION: container has copy of value at position.
        //!                iterators at or beyond position are invalidated.
        //!                pointers and references are still valid
        iterator insert(const_iterator position, T const & to_copy)
        {
            assert(((void)"invalid iterator", (position.container == this)));
            assert(((void)"invalid iterator", (position.current_index <= map_end)));
            assert(((void)"invalid container", my_alloc != nullptr));

            T * storage = allocate_one();
            construct_one(storage, to_copy);
            if (position.current_index == map_end)
            {
                publish_back(storage);
            }
            else
            {
                linear_publish_at_existing(storage, position.current_index);
            }
            return iterator{this, position.current_index};
        }

        //! as with insert(const_iterator, T const &)
        iterator insert(const_iterator position, T && to_move)
        {
            assert(((void)"invalid iterator", (position.container == this)));
            T * storage = allocate_one();
            construct_one(storage, std::move(to_move));
            if (position.current_index == map_end)
            {
                publish_back(storage);
            }
            else
            {
                linear_publish_at_existing(storage, position.current_index);
            }
            return iterator{this, position.current_index};
        }

        //! O(n^2) complexity, but exceptions will not leak memory
        //! and if some number m < n objects are constructed before the
        //! exception, they will be available to use in the container.
        //! PRECONDITION:  container has valid allocator

        iterator insert(const_iterator position, size_type n, T const & to_copy)
        {
            assert(((void)"invalid iterator", (position.container == this)));
            // pool can allocate more than one T per block, for example if size_type is larger than T.
            // the pool can also fail to allocate one sequence of n Ts when it could allocate
            // n sequences of one T.
            // this container further assumes each T corresponds to exactly one block,
            // so we really don't want to allocate n Ts at once, we want to allocate one T
            // n times.

            T * storage;
            for ( size_type moving_index = position.current_index;
                n > 0 && moving_index < N;
                --n, ++moving_index
            ) {
                storage = allocate_one();
                construct_one(storage, to_copy);
                // this means O(n^2) worst case, but also means that
                // each successfully constructed object gets published,
                // so that further operations can use them, and eventually
                // destruct and release them, even if an exception occurs.
                if (moving_index == map_end)
                {
                    publish_back(storage);
                }
                else
                {
                    linear_publish_at_existing(storage, moving_index);
                }
            }

            return iterator{this, position.current_index};
        }

        //! O(n*(other_end - other_begin)) complexity, but exceptions will not leak memory
        //! and if some number m < n objects are constructed before the
        //! exception, they will be available to use in the container.
        //! PRECONDITION:  container has valid allocator

        template<typename Itr>
        typename std::enable_if<
            std::is_base_of<std::input_iterator_tag, typename std::iterator_traits<Itr>::iterator_category >::value,
        iterator >::type  insert(const_iterator position, Itr other_begin, Itr other_end)
        {
            assert(((void)"invalid iterator", (position.container == this)));
            T * storage;
            for ( size_type moving_index = position.current_index;
                other_begin != other_end && moving_index < N;
                ++other_begin, ++moving_index
            ) {
                storage = allocate_one();
                construct_one(storage, *other_begin);
                if (moving_index == map_end)
                {
                    publish_back(storage);
                }
                else
                {
                    linear_publish_at_existing(storage, moving_index);
                }
            }

            return iterator{this, position.current_index};
        }

        //! complexity, preconditions as with insert(const_iterator, size_type, T)
        iterator insert(const_iterator position, std::initializer_list<T> il)
        {
            return insert(position, il.begin(), il.end());
        }

        //! overload that mimics associative insert of node_type
        iterator insert(const_iterator dest_pos, node_type && node)
        {
            // splice also checks this but returns without exception
            // if empty, and we want to be able to assume
            // that insertion happened at dest_pos
            if (!static_cast<bool>(node))
            {
                return end();
            }

            splice(dest_pos, std::move(node));
            // if we didn't throw an exception, it will have been inserted
            // at the same index as dest_pos
            return {this, dest_pos.current_index};
        }

        //! complexity O(n)
        //! PRECONDITION: const_iterator is valid iterator,
        //!               which also implies container has valid allocator.
        //! POSTCONDITION: iterators between the passed iterator and end
        //!                are invalidated.  pointers and references to
        //!                erased elements are invalidated.  other pointers
        //!                and references remain valid.
        iterator erase(const_iterator position) noexcept
        {
            linear_pop_from_existing(position.current_index);
            // the index of the erased element is now either
            // the index of the next non-erased element, or the
            // end
            return iterator{this, position.current_index};
        }

        //! like erase, except:
        //! complexity O(1), changes order of contents
        iterator reordering_erase(const_iterator position) noexcept
        {
            reordering_pop_from_existing(position.current_index);
            return iterator{this, position.current_index};
        }

        //! complexity O(n*(last - first))
        //! PRECONDITION: [first, last) are valid iterators
        //! POSTCONDITION: iterators after first are all invalidated
        //!                pointers and references to erased elements
        //!                are invalidated.  Other pointers and references
        //!                remain valid.
        iterator erase(const_iterator first, const_iterator past_last) noexcept
        {
            for ( ;
                 first != past_last;
                 erase(--past_last)
            ) {
            }
            return iterator{this, first.current_index};
        }

        iterator reordering_erase(const_iterator first, const_iterator past_last) noexcept
        {
            for ( ;
                 first != past_last;
                 reordering_erase(--past_last)
            ) {
            }
            return iterator{this, first.current_index};
        }

        template<typename ... Args>
        iterator emplace(const_iterator position, Args && ... args)
        {
            assert(((void)"invalid iterator", (position.container == this)));
            assert(((void)"invalid iterator", (position.current_index < map_end)));
            assert(((void)"invalid container", my_alloc != nullptr));
            T * storage = allocate_one();
            construct_one(storage, std::forward<Args>(args)... );
            if (position.current_index == map_end)
            {
                publish_back(storage);
            }
            else
            {
                linear_publish_at_existing(storage, position.current_index);
            }
            return iterator{this, position.current_index};
        }

        template<typename ... Args>
        reference emplace_back(Args && ... args)
        {
            T * storage = allocate_one();
            construct_one(storage, std::forward<Args>(args)...);
            publish_back(storage);
            return *storage;
        }

        template<typename ... Args>
        reference emplace_front(Args && ... args)
        {
            T * storage = allocate_one();
            construct_one(storage, std::forward<Args>(args)...);
            linear_publish_at_existing(storage, 0);
            return *storage;
        }

        void assign(size_type n, T const & value)
        {
            // throw before clear so container is still valid after exception
            if (n > max_size())
            {
                throw std::bad_alloc();
            }
            clear();
            for (;
                n > 0;
                --n
             ) {
                 push_back(value);
             }
        }

        template<class Itr>
        typename std::enable_if<
                std::is_base_of< std::input_iterator_tag, typename std::iterator_traits<Itr>::iterator_category >::value,
        void >::type assign(Itr first, Itr past_last)
        {
            // emplace-back will throw via allocate_one() when it runs out of room;
            // this will leave the list in a usable condition with the successfully
            // assigned elements but cannot return the value of the iterator at which
            // it stopped.
            clear();
            for(;
                first != past_last;
                emplace_back(*(first++))
            )
            {
            }
        }

        void assign(std::initializer_list<T> il)
        {
            assign(il.begin(), il.end());
        }

        constexpr const_iterator begin() const noexcept
        {
            return const_iterator{this, 0};
        }

        constexpr iterator begin() noexcept
        {
            return iterator{this, 0};
        }

        constexpr const_iterator cbegin() const noexcept
        {
            return begin();
        }

        constexpr iterator end() noexcept
        {
            return iterator{this, map_end};
        }

        constexpr const_iterator end() const noexcept
        {
            return const_iterator{this, map_end};
        }

        constexpr const_iterator cend() const noexcept
        {
            return const_iterator{this, map_end};
        }

        constexpr reverse_iterator rbegin() noexcept
        {
            return reverse_iterator{end()};
        }

        constexpr const_reverse_iterator rbegin() const noexcept
        {
            return const_reverse_iterator{end()};
        }

        constexpr const_reverse_iterator crbegin() const noexcept
        {
            return const_reverse_iterator{end()};
        }

        constexpr reverse_iterator rend() noexcept
        {
            return reverse_iterator{begin()};
        }

        constexpr const_reverse_iterator rend() const noexcept
        {
            return const_reverse_iterator{begin()};
        }

        constexpr const_reverse_iterator crend() const noexcept
        {
            return rend();
        }

        constexpr size_type size() const
        {
            return map_end;
        }

        constexpr size_type max_size() const
        {
            return N;
        }

        constexpr bool empty() const
        {
            return 0 == size();
        }

        //! stable sort, like std::list.
        //! you can use the indirect iterators and indirect_comparator in the same
        //! manner to sort with other algorithms.
        template<class Compare>
        void sort(Compare comp)
        {
            std::stable_sort(indirect_begin(), indirect_end(), indirect_comparator<Compare>{comp, *this});
        }

        //! overload with no arguments, uses std::less
        void sort()
        {
            this->sort(std::less<T>{});
        }

        void reverse() noexcept
        {
            for (size_type lhs = 0u, rhs = map_end;
            rhs - lhs > 1u;
            swap(index_map[lhs], index_map[--rhs]), ++lhs)
            {}
        }

        //! because erasure can be O(n) in the worst case,
        //! unlike std::list, remove_if is not O(n),
        //! it can be O(n^2).  However, if all elements
        //! are removed, or if zero or one element is removed,
        //! it may still be Ω(n)
        template< class UnaryPredicate >
        size_type remove_if( UnaryPredicate p )
        {
            size_type removed_elements = 0;
            for (
                size_type idx = map_end;
                idx > 0;
                --idx
            )
            {
                if (p(*local_index_to_pointer(idx - 1u)))
                {
                    linear_pop_from_existing(idx - 1u);
                    ++removed_elements;
                }
            }

            return removed_elements;
        }

        size_type remove(value_type const & v)
        {
            return remove_if([&v](value_type const & o){return v == o;});
        }

        template< class BinaryPredicate >
        size_type unique( BinaryPredicate p )
        {
            size_type removed_elements = 0;
            for (
                size_type idx = map_end;
                idx > 1;
                --idx
            )
            {
                if (p(*local_index_to_pointer(idx-1u), *local_index_to_pointer(idx-2u)))
                {
                    linear_pop_from_existing(idx - 1u);
                    ++removed_elements;
                }
            }
            return removed_elements;
        }

        size_type unique()
        {
            return remove_if(std::equal_to<value_type>{});
        }

        friend iterator;
        friend const_iterator;

        //! indirect comparator functor for use with standard algorithms
        // Unfortunately the standard algorithms do not provide
        // an opportunity to inject customization of the swap behavior
        // (without overloading swap for all objects of the type, not just
        // objects stored in these containers).
        // using private inheritance instead of composition
        // to allow for empty base optimization in case Compare
        // does not need state.
        template<class Compare, bool is_transparent = false>
        struct indirect_comparator : public Compare
        {
            constexpr indirect_comparator(Compare const & comp, pool_list const & lst)
            : Compare(comp)
            , my_list(&lst)
            {}

            constexpr bool operator() (index_type const & lhs, index_type const & rhs) const
            {
                // apply comparison to the values that the indices refer to
                return Compare::operator()(
                    *std::launder(my_list->block_to_pointer(lhs)),
                    *std::launder(my_list->block_to_pointer(rhs))
                );
            }

            template<typename U = T>
            constexpr bool operator() (U const & lhs, index_type const & rhs) const
            {
                return Compare::operator()(
                    lhs,
                    *std::launder(my_list->block_to_pointer(rhs))
                );
            }

            template<typename U = T>
            constexpr bool operator() (index_type const & lhs, U const & rhs) const
            {
                return Compare::operator()(
                    *std::launder(my_list->block_to_pointer(lhs)),
                    rhs
                );
            }

            template<typename U = T, typename V = T>
            constexpr bool operator() (U const & lhs, V const & rhs) const
            {
                return Compare::operator()(
                    lhs,
                    rhs
                );
            }

            private:
            pool_list const * my_list;
        };

        template<class Compare>
        struct indirect_comparator<
            Compare,
            std::is_same<void, cues::void_t<typename Compare::is_transparent>>::value
        > : public indirect_comparator<Compare, false>
        {
            using is_transparent = typename Compare::is_transparent;

            constexpr indirect_comparator(Compare const & comp, pool_list const & lst)
            : indirect_comparator<Compare, false>(comp, lst)
            {}

            using indirect_comparator<Compare, false>::operator();
        };
        //! aliased to a name that more closely matches standard-like names
        template<class Compare>
        using indirect_compare = indirect_comparator<Compare>;

        //! indirect iterator access; indirect iterators
        //! can be used with indirect_comparator to use algorithms
        //! requiring random access iterators to be used with this
        //! class without having to swap values and thus invalidate
        //! pointers/references.  This may, depending on compiler optimization,
        //! also reduce overhead even for cases where the algorithm
        //! does not need to modify data, as the indirect iterators are
        //! smaller (just a pointer instead of pointer and size_type).
        //! To get back to value_type, use the provided indirect_dereference,
        //! as a dereferenced indirect_iterator is an index_type, not a
        //! value_type.
        //! However, indirect iterators *cannot* dereference to a value_type
        //! by themselves, because they do not have enough information.
        //! (They would need a pointer to the allocator or a means to access
        //! it, for example through a pointer to this, like the one stored
        //! in the indirect comparator).
        constexpr indirect_const_iterator indirect_begin() const noexcept
        {
            return &(index_map[0]);
        }

        constexpr indirect_iterator indirect_begin() noexcept
        {
            return &(index_map[0]);
        }

        constexpr indirect_const_iterator indirect_end() const noexcept
        {
            return &(index_map[map_end]);
        }

        constexpr indirect_iterator indirect_end() noexcept
        {
            return &(index_map[map_end]);
        }

        //! PRECONDITION: i within [indirect_begin(), indirect_end() )
        constexpr const_reference indirect_dereference(indirect_const_iterator i) const noexcept
        {
            assert(((void)"invalid indirect iterator", (i < indirect_end()) && (i >= indirect_begin())));
            return *local_index_to_pointer(*i);
        }

        constexpr reference indirect_dereference(indirect_iterator i) noexcept
        {
            return *const_cast<pointer>(
                (const_cast<pool_list const *>(this))->indirect_dereference(i)
            );
        }
        //! PRECONDITION: i within [indirect_begin(), indirect_end() )
        constexpr const_iterator indirect_iterator_to_iterator(indirect_const_iterator i) const noexcept
        {
            assert(((void)"invalid indirect iterator", (i < indirect_end()) && (i >= indirect_begin())));
            return {this, i-indirect_begin()};
        }

        constexpr iterator indirect_iterator_to_iterator(indirect_iterator i) noexcept
        {
            assert(((void)"invalid indirect iterator", (i < indirect_end()) && (i >= indirect_begin())));
            return {this, i - indirect_begin()};
        }

        node_type extract(const_iterator position) noexcept
        {
            assert(((void)"invalid iterator (wrong container) when extracting", (position.container == this)));
            if (position.container != this) {return node_type{};}
            return extract(position.current_index);
        }

        node_type extract(size_type position) noexcept
        {
            assert(((void)"index out of bounds when extracting", (position < map_end)));

            if (position >= map_end)
            {
                // provide the allocator, but no element
                return {nullptr, get_allocator()};
            }
            // noexcept
            pointer storage = local_index_to_pointer(position);

            // strictly speaking, std::copy is not noexcept.  however, if we
            // know we are passing valid pointers, and that the type is nothrow
            // copy-assignable, there shouldn't be any reason for it to throw,
            // so if it does throw, either the static_assert is wrong,
            // or the pointers are somehow invalid, or there is something
            // wrong with the implementation of std::copy, in which case
            // calling std::terminate seems like it would make more sense
            // than letting the caller deal with an exception
            static_assert(std::is_nothrow_copy_assignable<decltype(index_map[0])>::value, "copy assignment not noexcept");
            std::copy(&(index_map[position+1]), &(index_map[map_end]), &(index_map[position]));
            --map_end;
            // noexcept
            return {storage, get_allocator()};
        }

        //! unlike std::list::splice, this has O(n) complexity instead of O(1)
        //! complexity.  However, the values being copied are the index_type,
        //! not the elements themselves, so it is still likely to be faster than
        //! with a vector<T>, unless T is trivially copyable and similar in size
        //! to the index_type.
        template<
            std::size_t FOREIGN_N = N,
            typename FOREIGN_SIZE_TYPE = SIZE_TYPE
        >
        void splice(const_iterator position, pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> & other)
        {
            if (this == &other) {return;}
            move_from_other(position.current_index, other, other.index_map[0], other.index_map[other.map_end]);
            this->map_end += other.map_end;
            other.map_end = 0;
        }

        template<
            std::size_t FOREIGN_N = N,
            typename FOREIGN_SIZE_TYPE = SIZE_TYPE
        >
        void splice(const_iterator position, pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> && other)
        {
            // now it is an lvalue, our implementation does not need an rvalue
            // to take advantage of the fact that it will end up empty
            splice(position, other);
        }

        template<
            std::size_t FOREIGN_N = N,
            typename FOREIGN_SIZE_TYPE = SIZE_TYPE
        >
        void splice(const_iterator dest_pos, pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> & other,
                    typename pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator>::const_iterator src)
        {
            splice(dest_pos, other, src, (src + 1));
        }

        template<
            std::size_t FOREIGN_N = N,
            typename FOREIGN_SIZE_TYPE = SIZE_TYPE
        >
        void splice(const_iterator dest_pos, pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> && other,
                    typename pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator>::const_iterator src)
        {
            splice(dest_pos, other, src, ++(const_iterator{src}));
        }

        template<
            std::size_t FOREIGN_N = N,
            typename FOREIGN_SIZE_TYPE = SIZE_TYPE
        >
        void splice(const_iterator dest_pos, pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> & other,
            typename pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator>::const_iterator src_begin,
            typename pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator>::const_iterator src_end
        ) {
            if (src_begin == src_end) {return;}
            assert(((void)"cannot splice iterator that is not dereferenceable", src_begin.current_index < other.map_end ));
            assert(((void)"invalid iterator pair", src_begin.current_index <= src_end.current_index ));

            if (static_cast<void *>(&other) != static_cast<void *>(this))
            {
                move_from_other(
                       dest_pos.current_index,
                       other,
                       src_begin.current_index,
                       src_end.current_index
                );
                return;
            }

            // splicing within self
            assert(((void)"destination cannot be within source range",
                    dest_pos.current_index >= src_end.current_index
                    || dest_pos.current_index < src_begin.current_index
            ));

            // do we have enough spare room to bulk copy everything?
            if (this->max_size() - size() > src_end - src_begin)
            {
                expanding_move_within_self(dest_pos.current_index, src_begin.current_index, src_end.current_index);
                return;
            }

            // we can splice with fixed size overhead
            // by swapping values into their appropriate places
            // with one spare index_type (and other bookkeeping)
            if (dest_pos.current_index > src_end.current_index)
            {
                compact_move_within_self<backwards_move_tag>(
                        dest_pos.current_index,
                        src_begin.current_index,
                        src_end.current_index
                );
            }
            else
            {
                compact_move_within_self<forwards_move_tag>(
                        dest_pos.current_index,
                        src_begin.current_index,
                        src_end.current_index
                );

            }
        }

        template<
            std::size_t FOREIGN_N = N,
            typename FOREIGN_SIZE_TYPE = SIZE_TYPE
        >
        void splice(const_iterator dest_pos, pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> && other,
                typename pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator>::const_iterator src_begin,
                typename pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator>::const_iterator src_end
        ) {
            splice(dest_pos, other, src_begin, src_end);
        }



        //! splice overload for any "node type" which:
        //! -provides an allocator that is the same type as our allocator,
        //! -has an operator* that returns value_type (or a reference to it)
        //! -can be converted to bool via static_cast to indicate validity
        //! -has a release() method to relenquish ownership
        //! note this matches allocated_unique_ptr
        //! (and thus can be used with extract())
        //! but is more than what is required for std::unique_ptr,
        //! or the "node handle" of the standard associative containers
        template<class FOREIGN_NODE>
        typename std::enable_if<
            cues::provides_allocator<FOREIGN_NODE>::value
            &&
            std::is_same<
                typename FOREIGN_NODE::allocator_type,
                allocator_type
            >::value
            &&
            std::is_same<
                typename std::remove_reference<decltype(*(std::declval<FOREIGN_NODE &>()))>::type,
                value_type
            >::value
            &&
            std::is_same<
                bool,
                decltype(static_cast<bool>(std::declval<FOREIGN_NODE const &>()))
            >::value
            ,
            cues::void_t<
                decltype(std::declval<FOREIGN_NODE>().release())
            >
        >::type splice(const_iterator dest_pos, FOREIGN_NODE && node)
        {
            assert(((void)"invalid container", my_alloc != nullptr));
            assert(((void)"invalid iterator (wrong container)", dest_pos.container == this));

            if (dest_pos.container != this || dest_pos.current_index > map_end)
            {
                throw std::out_of_range("invalid iterator");
            }
            // nothing to do?
            if (!static_cast<bool>(node))
            {
                return;
            }
            // we don't need to allocate more storage, but we might
            // be out of space in the index array
            if (size() == max_size())
            {
                throw std::bad_alloc();
            }
            // not allocated by us; insert a move of the value
            if (node.get_allocator() != my_alloc)
            {
                // move it, let node destructor cleanup what we couldn't steal
                insert(dest_pos, std::move(*node));
                return;
            }
            index_type storage_block = pointer_to_block_number(&(*node));
            // if it does match our allocator, we can steal even more
            static_assert(std::is_nothrow_copy_assignable<decltype(index_map[0])>::value, "copy assignment not noexcept");
            std::copy_backward(
                &(index_map[dest_pos.current_index]),
                &(index_map[map_end]),
                &(index_map[map_end+1])
            );
            index_map[dest_pos.current_index] = storage_block;
            ++map_end;
            // container will clean it up now
            node.release();
            return;

        }

        //! PRECONDITION: lists are already sorted according to Compare
        //! Best case, other than the trivial and uninteresting case where one
        //! list is empty, is the case when the incoming list can be inserted
        //! all at once, in which case this takes Ω(n + m);
        //! the search is only log(n) to binary search for the insert position,
        //! but inserting n elements is at least Ω(n) and possible O(n + m),
        //! where m is the elements in *this that have to be moved, which
        //! will dominate the growth.
        //! worst case, each of the n elements has to be inserted individually,
        //! and this instead becomes O(n^2 + mn)

        template <class Compare, std::size_t FOREIGN_N = N, typename FOREIGN_SIZE_TYPE = SIZE_TYPE >
        void merge( pool_list<value_type, FOREIGN_N, FOREIGN_SIZE_TYPE, allocator_type> & other, Compare comp )
        {
            assert(((void)"invalid container", my_alloc != nullptr));
            assert(((void)"allocators must match", my_alloc == other.my_alloc));

            if (static_cast<void *>(&other) == static_cast<void *>(this))
            {
                return;
            }
            if (this->max_size() - this->size() < other.size())
            {
                throw std::out_of_range("trying to merge into list with insufficient capacity.");
            }

            // then, use ++upper_bound with lower_bound on other
            // use splice with upper_bound as destination,
            // other.begin as source begin, and lower_bound
            // as source_end
            index_type * dest = indirect_begin();
            auto src_end = other.indirect_end();
            indirect_comparator<Compare> icomp {comp, *this};
            for (;
                !other.empty();
                // splice would work but we already checked other != this
                // move from other shrinks other, so we are always looking
                // at index 0 to its end
                move_from_other(
                       dest - indirect_begin(),
                       other,
                       0,
                       src_end - other.indirect_begin()
                )
            )
            {
                // find intended position of the first other element
                // with upper_bound on this and indirect_compare.
                // this gives us the first element greater than other
                // element, and thus other should begin inserting at that
                // position
                dest = std::upper_bound(
                     dest,
                     indirect_end(),
                     *(other.local_index_to_pointer(0)),
                     icomp
                );
                // if there are no further elements in this,
                // insert all of other.
                if (dest == indirect_end())
                {
                    src_end = other.indirect_end();
                }
                // otherwise, we need to know how much of other fits
                // before destination
                else
                {
                    // lower_bound here instead of upper bound, because if it
                    // is equal, it should be ordered after list, as with std::list
                    src_end = std::lower_bound(
                        other.indirect_begin(),
                        other.indirect_end(),
                        *block_to_pointer(*(dest)),
                        icomp
                    );
                }
            }
        }

        template <class Compare, std::size_t FOREIGN_N = N, typename FOREIGN_SIZE_TYPE = SIZE_TYPE >
        void merge( pool_list<value_type, FOREIGN_N, FOREIGN_SIZE_TYPE, allocator_type> && other, Compare comp )
        {
            // pass as lvalue.
            // hypothetically, we could skip a copy in the
            // algorithm by bumping the starting point instead of
            // removing items from the list but this is not implemented.
            merge(other, comp);
        }


        // convenience use with std::less
        template<std::size_t FOREIGN_N = N, typename FOREIGN_SIZE_TYPE = SIZE_TYPE >
        void merge( pool_list<value_type, FOREIGN_N, FOREIGN_SIZE_TYPE, allocator_type> & other)
        {
            merge(other, std::less<T>{});
        }

        template<std::size_t FOREIGN_N = N, typename FOREIGN_SIZE_TYPE = SIZE_TYPE >
        void merge( pool_list<value_type, FOREIGN_N, FOREIGN_SIZE_TYPE, allocator_type> && other)
        {
            merge(std::move(other), std::less<T>{});
        }



    private:

        const_pointer block_to_pointer(index_type block_num) const noexcept
        {
            assert(((void)"invalid container", my_alloc != nullptr));
            // temporary variable because std::align modifies the parameter
            void * maybe_aligned = const_cast<void *>( //< remove const if applicable
                // cast byte (const?) * at beginning of block to void (const?) *
                static_cast<void const *>(
                    // get pointer to start of data
                    my_alloc.data() +
                    // get allocator block number, times block size
                    my_alloc.block_size() * static_cast<typename index_type::underlying_type>(block_num)
                )
            );

            // temporary value because std::align modifies parameter
            std::size_t modifiable_size = my_alloc.block_size();

            void * aligned_or_null = std::align(
                alignof(T),
                sizeof(T),
                maybe_aligned,
                modifiable_size
            );

            // if we created an object there, then it follows there ought to be space
            // to align it there, and thus a nullptr is a programmer error, either
            // in this library, or in the caller who called this for a value not created.
            assert(((void)"asked for object that cannot exist", aligned_or_null != nullptr));

            return std::launder(static_cast<const_pointer>(aligned_or_null));
        }

        const_pointer local_index_to_pointer(size_type index) const noexcept
        {

            assert(((void)"index invalid", (index <= map_end)));
            return block_to_pointer(index_map[index]);
        }

        pointer local_index_to_pointer(size_type index) noexcept
        {
            return const_cast<pointer>(
                const_cast<pool_list const &>(*this).local_index_to_pointer(index)
            );
        }

        template<typename ... Args>
        void construct_one(pointer storage, Args && ... args)
        {
            try{
                new (storage) T{std::forward<Args>(args)...};
            }
            catch(...)
            {
                // deallocate the storage we couldn't use
                my_alloc.deallocate(storage, 1);
                // rethrow, we made no other changes so there is
                // nothing else to clean up
                throw;
            }
        }

        pointer allocate_one()
        {
            assert(((void)"attempted to allocate without an allocator", nullptr != my_alloc));
            // the container may have a size less than the allocator.
            // simulate allocation failure if the container could not
            // allocate regardless of whether the allocator can
            if (map_end >= N) {throw std::bad_alloc();}

            // let std::bad_alloc propagate if allocator throws it
            return my_alloc.allocate(1);
        }

        void publish_back(pointer storage) noexcept
        {
             // set index of allocated object
            index_map[map_end] = pointer_to_block_number(storage);
            // publish at top of stack
            ++map_end;
        }

        void reordering_publish_at_existing(pointer storage, size_type index) noexcept
        {
            // put the block number already at the index at the end
            index_map[map_end] = index_map[index];
            // publish index corresponding to storage
            index_map[index] = pointer_to_block_number(storage);
            // bump stack size
            ++map_end;
        }

        void linear_publish_at_existing(pointer storage, size_type index) noexcept
        {
            static_assert(std::is_nothrow_copy_assignable<decltype(index_map[0])>::value, "copy assignment not noexcept");
            // copy all block numbers at or after index backward to the end
            std::copy_backward(&(index_map[index]), &(index_map[map_end]), &(index_map[map_end+1]));

            // publish index corresponding to storage
            index_map[index] = pointer_to_block_number(storage);
            // bump stack size
            ++map_end;
        }

        void linear_pop_from_existing(size_type index) noexcept
        {
            assert(((void)"attempted to pop when empty", (map_end > 0)));
            assert(((void)"attempted to pop invalid_index", (map_end > index)));
            // save the first index
            static_assert(std::is_nothrow_copy_assignable<decltype(index_map[0])>::value, "copy assignment not noexcept");
            index_type idx_to_remove = index_map[index];
            // copy subsequent indices in order
            // from array starting at index + 1, going to end
            //   to array starting at index
            std::copy(&(index_map[index + 1]), &(index_map[map_end]), &(index_map[index]));
            // put the one to be removed last
            index_map[map_end - 1] = idx_to_remove;
            // pop it off
            pop_back();
        }

        void reordering_pop_from_existing(size_type index) noexcept
        {
            static_assert(std::is_nothrow_copy_assignable<decltype(index_map[0])>::value, "copy assignment not noexcept");
            using std::swap;
            assert(((void)"attempted to pop when empty", (map_end > 0)));
            assert(((void)"attempted to pop invalid_index", (map_end > index)));
            swap(index_map[index], index_map[map_end - 1]);
            pop_back();
        }

        constexpr index_type pointer_to_block_number(pointer storage) const noexcept
        {
            return static_cast<index_type>(static_cast<size_type>(
                (static_cast<cues::raw_byte_type const *>(
                        static_cast<void const *>(storage)
                    )
                    - my_alloc.data()
                ) / my_alloc.block_size()
            ));
        }

        template<
            std::size_t FOREIGN_N = N,
            typename FOREIGN_SIZE_TYPE = SIZE_TYPE
        >
        void common_move_assign(pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> && other)
        {
            using common_size_type = typename std::conditional<
                std::numeric_limits<size_type>::digits < std::numeric_limits<FOREIGN_SIZE_TYPE>::digits,
                FOREIGN_SIZE_TYPE,
                size_type
            >::type;
            // steal existing allocation from other
            if (std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value
                || my_alloc == other.my_alloc
            )
            {
                clear();
                // we're stealing
                if (std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value)
                {
                    my_alloc = std::move(other.my_alloc);
                }
                splice(cend(), other, other.cbegin(), other.cend());

            }
            // keping this->my_alloc, move elements to new allocation there
            else
            {
                // allocators were different and we weren't supposed to propagate
                common_size_type idx {0};
                // move into existing allocated space if possible
                for (;
                    idx < other.size() && idx < this->map_end;
                    ++idx
                ) {
                    if constexpr (std::is_move_assignable<T>::value)
                    {
                        this->operator[](static_cast<size_type>(idx)) = std::move(other[static_cast<FOREIGN_SIZE_TYPE>(idx)]);
                    }
                    else // move constructible but not assignable
                    {
                        pointer storage = this->local_index_to_pointer(idx);
                        // destruct and copy construct on top of that space
                        storage->~T();
                        construct_one(storage, std::move(other[static_cast<FOREIGN_SIZE_TYPE>(idx)]));
                    }
                }

                // allocate more space as needed
                for (;
                    idx < other.size();
                    ++idx
                ) {
                    emplace_back(std::move(other[static_cast<FOREIGN_SIZE_TYPE>(idx)]));
                }

                // clean up unused space
                for (;
                    idx < map_end;
                    ++idx
                )
                {
                    pointer storage = this->local_index_to_pointer(static_cast<size_type>(idx));
                    // destruct and copy construct on top of that space
                    storage->~T();
                    my_alloc.deallocate(storage, 1);
                }

            }
        }

        template<
            std::size_t FOREIGN_N = N,
            typename FOREIGN_SIZE_TYPE = SIZE_TYPE
        >
        void common_copy_assign(pool_list<T, FOREIGN_N, FOREIGN_SIZE_TYPE, Allocator> const & other)
        {
            using common_size_type = typename std::conditional<
                std::numeric_limits<size_type>::digits < std::numeric_limits<FOREIGN_SIZE_TYPE>::digits,
                FOREIGN_SIZE_TYPE,
                size_type
            >::type;

            if (std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value
                && my_alloc != other.get_allocator()
            )
            {
                clear();
                my_alloc = other.get_allocator();
            }

            common_size_type idx {0};
            // make copies into existing allocated space if possible
            for (;
                idx < other.size() && idx < this->map_end;
                ++idx
            ) {
                if constexpr (std::is_copy_assignable<T>::value)
                {
                    this->operator[](static_cast<size_type>(idx)) = (other[static_cast<FOREIGN_SIZE_TYPE>(idx)]);
                }
                else // copy constructible but not assignable
                {
                    pointer storage = this->local_index_to_pointer(idx);
                    // destruct and copy construct on top of that space
                    storage->~T();
                    construct_one(storage, other[static_cast<FOREIGN_SIZE_TYPE>(idx)]);
                }
            }
            // allocate more space as needed
            for (;
                idx < other.size();
                ++idx
            ) {
                emplace_back(other[static_cast<FOREIGN_SIZE_TYPE>(idx)]);
            }
            // clean up unused space
            for (;
                idx < this->map_end;
                ++idx
            )
            {
                pointer storage = this->local_index_to_pointer(idx);
                // destruct and copy construct on top of that space
                storage->~T();
                my_alloc.deallocate(storage, 1);
            }
        }

        template<std::size_t FOREIGN_M, typename FOREIGN_SIZE_TYPE>
        void move_from_other(size_type dest_idx, pool_list<value_type, FOREIGN_M, FOREIGN_SIZE_TYPE, allocator_type> & other,
            typename pool_list<value_type, FOREIGN_M, FOREIGN_SIZE_TYPE, allocator_type>::size_type src_begin_idx,
            typename pool_list<value_type, FOREIGN_M, FOREIGN_SIZE_TYPE, allocator_type>::size_type src_end_idx
        )
        {
            using common_size_type = typename std::conditional<
                std::numeric_limits<size_type>::digits < std::numeric_limits<FOREIGN_SIZE_TYPE>::digits,
                FOREIGN_SIZE_TYPE,
                size_type
            >::type;

            assert(((void)"undefined behavior splicing with unequal allocators",
                    my_alloc != nullptr
                    && my_alloc == other.my_alloc)
            );
            assert(((void)"input source begin < end", src_end_idx >= src_begin_idx));
            assert(((void)"attempting to take more elements than exist", src_end_idx - src_begin_idx <= other.size()));

            common_size_type num_elements = src_end_idx - src_begin_idx;
            // nothing to do
            if (num_elements == 0) {return;}
            // do we have room to add that many nodes?
            if (max_size() - size() < num_elements)
            {
                throw std::out_of_range("list is out of room to store node indices.");
            }

            static_assert(std::is_nothrow_copy_assignable<decltype(index_map[0])>::value, "copy assignment not noexcept");
            // otherwise, copy our elements backward
            // to make room for the new elements
            std::copy_backward(&(index_map[dest_idx]), &(index_map[map_end]), &(index_map[map_end+num_elements]));

            if constexpr(std::is_same<
                index_type,
                typename pool_list<value_type, FOREIGN_M, FOREIGN_SIZE_TYPE, allocator_type>::index_type
            >::value)
            {
                // then copy from the other
                std::copy(&(other.index_map[src_begin_idx]), &(other.index_map[src_end_idx]), &(index_map[dest_idx]));
            }
            else
            {
                static_assert(
                    std::is_nothrow_constructible<
                        FOREIGN_SIZE_TYPE,
                        decltype(other.index_map[0])
                    >::value
                    &&
                    std::is_nothrow_constructible<
                        size_type,
                        FOREIGN_SIZE_TYPE
                    >::value
                    &&
                    std::is_nothrow_constructible<
                        index_type,
                        size_type
                    >::value
                , "copy assignment (via conversion) not noexcept");

                for(common_size_type offset = 0;
                    offset < src_end_idx - src_begin_idx;
                    offset++
                )
                {
                    index_map[dest_idx + offset] = static_cast<index_type>(
                        static_cast<size_type>(
                            static_cast<FOREIGN_SIZE_TYPE>(other.index_map[src_begin_idx + offset])
                        )
                    );
                }

            }

            // are there remaining elements in other to move?
            if (src_end_idx < other.map_end)
            {
                std::copy(
                      &(other.index_map[src_end_idx]),
                      &(other.index_map[other.map_end]),
                      &(other.index_map[src_begin_idx])
                );
            }

            this->map_end += num_elements;
            other.map_end -= num_elements;
        }

        // uses spare available memory in order to use three bulk copies to do all the moving,
        // which, for trivial size_type, may well be faster than multiple smaller copies
        // PRECONDITION: have enough spare space to shift elements by the number of elements to move
        // (aka N - size() >= src_end - src_begin
        void expanding_move_within_self(size_type dest_idx, size_type src_begin_idx, size_type src_end_idx)
        {
            assert(((void)"input source begin < end", src_end_idx >= src_begin_idx));
            size_type num_elements = src_end_idx - src_begin_idx;

            assert(((void)"insufficient room for expanding move",
                    static_cast<size_type>(N - this->size()) >= static_cast<size_type>(src_end_idx - src_begin_idx)));
            assert(((void)"destination is within source range", src_begin_idx > dest_idx ||
                    dest_idx >= src_end_idx));

            static_assert(std::is_nothrow_copy_assignable<decltype(index_map[0])>::value, "copy assignment not noexcept");
            // copy everything after the destination back to make room
            std::copy_backward(
                &(index_map[dest_idx]),
                &(index_map[map_end]),
                &(index_map[map_end + num_elements])
            );
            // at this point, the source indices may no longer point to the intended objects
            // if they were moved by the copy
            if (src_begin_idx >= dest_idx)
            {
                src_begin_idx += num_elements;
                src_end_idx   += num_elements;
                // copying "forward", dest <= source
                std::copy(
                    &(index_map[src_begin_idx]),
                    &(index_map[src_end_idx]),
                    &(index_map[dest_idx])
                );
            } // otherwise they were not affected by the copy and are still valid
            // but we are copying the other direction
            else
            {
                std::copy_backward(
                    &(index_map[src_begin_idx]),
                    &(index_map[src_end_idx]),
                    &(index_map[dest_idx+num_elements])
                );
            }

            // move any elements after the end of the source to its beginning,
            // effectively deleting the duplicate source range and/or shifting
            // previously copied elements back into place
            std::copy(
                &(index_map[src_end_idx]),
                &(index_map[map_end + num_elements]),
                &(index_map[src_begin_idx])
            );
        }


        struct forwards_move_tag
        {
            // moving source to the left, we might start with, for example:
            // [ 0, 1, 2, D, 4, 5, 6,S0,S1,S2,S3,S4,S5,13,14,15]
            // before any movement.  After splicing, we want it to look like:
            // [ 0, 1, 2,S0,S1,S2,S3,S4,S5, D, 4, 5, 6,13,14,15]
            // that is, we know we want [D] to get [S0],
            // and then [S0] gets [S0 + (S0 - D)]
            // if S is long enough, this can chain, where (S0 - D) is the step size,
            // then [S0 + n*step_size] = [S0 + (n+1)*step_size]
            // similarly [D+1] gets [SO+1]
            // and then [S0 +1] gets [S0 + 1 + S0 - D]
            // and then [D + (SE - S0)] gets [D]
            // more generally, for x in [0, step_size)
            // for n in [0, D + SE - S0)
            // [D+x] = [S0 + x], x in [0, step_size)
            // [S0 + x + n*step_size] = [S0 + x + (n+1)*step_size]
            // [D + (SE - S0) + x] gets [D + x]

            // but we know S0 = D + S0 - D, aka S0 = D + step_size, aka D = S0 - step_size
            // so:
            // [D + x] = [S0 + x]
            // becomes
            // [D + x] = [D + step_size + x]
            // and
            // [S0 + x + n*step_size] = [S0 + x + (n+1)*step_size]
            // becomes
            // [D + step_size + x + n*step_size] = [D + step_size + x + (n+1)*step_size]
            // [D + x + (n+1)*step_size] = [D + x + (n+2)*step_size]
            // and with the earlier subset
            // [D + x] = [D + x + step_size ]
            // but [D + x + (-1 + 1)*step_size] = [D + x]
            // and [D + x + (-1 + 2)*step_size] = [D + x + step_size]
            // so now n can go from [-1, D + SE - S0)
            // but that is not convenient for an unsigned type, so instead
            // subtract one in the formula and add one to the range, so that
            // [D + x + n*step_size] = [D + x + (n+1)*step_size]
            // for n in [0, D + SE - S0 + 1)
            // n in [0, D + SE - S0 + 1)
            //      [0, S0 - step_size + SE - S0 + 1]
            //      [0, SE + 1 - step_size]
            // followed by
            // [D + x + (SE - S0)] gets [D + x]

            // or, can also loop accumulating steps:

            // cache [D + x]
            // for acc_steps = 0; acc_steps < (SE +1) / step_size - 1 ; acc_steps += step_size
            // [D + x + acc_steps] = [D + x + acc_steps + step_size]
            // end loop, followed by
            // [D + x + (SE - S0)] = [D + x]
            static constexpr size_type calculate_step_size(
                 size_type dest_idx,
                 size_type src_begin_idx,
                 size_type // src_end_idx
            )
            {
                return src_begin_idx - dest_idx;
            }

            static constexpr size_type accumulated_step_limit(
                size_type ,//dest_idx
                size_type ,//src_begin_idx
                size_type src_end_idx,
                size_type step_size
            )
            {
                // note because step_size = S0 - D, SE >= S0, 0 <= D < S0, S0 > 0
                // SE - D >= S0 - D, so SE - D >= step_size, so the subtraction cannot
                // overflow.  The function is already undefined if
                // destination is within [source begin, source end)
                // so step_size also has to be at least one, which means that
                // the addition and division cannot overflow, either
                return  ((src_end_idx - step_size) + 1u)/step_size ;
            }

            static constexpr size_type initial_step_offset() {return size_type{0u};}

            static constexpr size_type loop_destination_index(size_type dest_idx, size_type step_offset, size_type accumulated_steps)
            {
                return dest_idx + step_offset + accumulated_steps;
            }

            static constexpr size_type loop_source_index(size_type dest_idx, size_type step_offset, size_type accumulated_steps, size_type step_size)
            {
                return loop_destination_index(dest_idx, step_offset, accumulated_steps) + step_size;
            }

            static constexpr size_type cached_value_source_index(size_type dest_idx, size_type step_offset)
            {
                return dest_idx + step_offset;
            }

            static constexpr size_type cached_value_destination_index(
                  size_type dest_idx,
                  size_type step_offset,
                  size_type ,// accumulated_steps,
                  size_type range_size
            )
            {
                return dest_idx + range_size + step_offset;
            }
        };

        struct backwards_move_tag
        {
            // if moving source to the right (backward), we might start with,
            // for example:
            // [ 0, 1, 2,S0,S1,S2,S3,S4,S5, 9,10, D,12,13,14,15]
            // before any movement.  After splicing, we want it to look like:
            // [ 0, 1, 2, 9,10,S0,S1,S2,S3,S4,S5, D,12,13,14,15]
            // that is, we know that we want slot [D-1] to get value
            // from slot [S(E - 1)]  (in this example, SE-1 is S5)
            // and we want to put the value previously in slot [D-1]
            // to slot [S0 + (D - SE) - 1] (in this case, [S1])
            // then, what happens to that value? it would need to go
            // to slot [S0 + 2*(D - SE) - 1] (in this case, [S3]).
            // then, what happens to that value?  it would need to go
            // to slot [S0 + 3*(D - SE) - 1] (in this case, [S5].
            // we are now back to the original move, which case
            // the original value of [S0 + 3*(D - SE) - 1], aka [S(E-1)]
            // goes into [S0 + 4*(D - SE) -1], aka [D - 1]
            // if we set this up as an iteration, we have a step size of
            // (D - SE), and we need to:
            // store [D - x]
            // loop n = 0; n < (D-S0)/step size; ++n
            // [D - x - n*step_size] = [D - x - (n+1)*step_size];
            // end loop
            // after the loop, [D - x - n*step_size] = stored [D-x]
            // or, equivalently,
            // loop acc_steps = 0; acc_steps < (D - S0) - step_size; acc_steps += step_size
            // [D - x - acc_steps] = [D - x - acc_steps - step_size];
            // end loop
            // after the loop, [D - x - acc_steps] = stored [D-x]
            // and then that whole loop needs to be run for
            // x = [1 , step size),
            // giving a total of step_size*(D-S0)/step_size iterations,
            // for O(D-S0), linear in the distance between the beginning
            // of the source and the destination
            static constexpr size_type calculate_step_size(
                size_type dest_idx,
                size_type , //src_begin_idx,
                size_type src_end_idx
            )
            {
                return dest_idx - src_end_idx;
            }

            static constexpr size_type accumulated_step_limit(
                size_type dest_idx,
                size_type src_begin_idx,
                size_type ,//src_end_idx
                size_type step_size
            )
            {
                return (dest_idx - src_begin_idx) - step_size;
            }

            static constexpr size_type initial_step_offset() {return size_type{1u};}

            static constexpr size_type loop_destination_index(size_type dest_idx, size_type step_offset, size_type accumulated_steps)
            {
                return dest_idx - step_offset - accumulated_steps;
            }

            static constexpr size_type loop_source_index(size_type dest_idx, size_type step_offset, size_type accumulated_steps, size_type step_size)
            {
                return loop_destination_index(dest_idx, step_offset, accumulated_steps) - step_size;
            }

            static constexpr size_type cached_value_source_index(size_type dest_idx, size_type step_offset)
            {
                return dest_idx - step_offset;
            }

            static constexpr size_type cached_value_destination_index(
                  size_type dest_idx,
                  size_type step_offset,
                  size_type accumulated_steps,
                  size_type //range_size
            )
            {
                return dest_idx - accumulated_steps - step_offset;
            }

        };

        // unlike expanding_move_within_self, does not require O(n) spare space;
        // however, it may be slower due to making O(n) individual moves instead of
        // bulk moving O(n) three times.
        template<class MOVE_TAG>
        void compact_move_within_self(size_type dest_idx, size_type src_begin_idx, size_type src_end_idx)
        {

            size_type const step_size = MOVE_TAG::calculate_step_size(
                dest_idx, src_begin_idx, src_end_idx
            );

            size_type const step_limit = MOVE_TAG::accumulated_step_limit(
                dest_idx, src_begin_idx, src_end_idx, step_size
            );

            size_type const range_size = src_end_idx - src_begin_idx;

            size_type accumulated_steps = 0;
            size_type step_offset = MOVE_TAG::initial_step_offset();

            for ( index_type cached_value = index_map[MOVE_TAG::cached_value_source_index(dest_idx, step_offset)];
                 step_offset < step_size;
                 accumulated_steps += step_size
            ){
                if (accumulated_steps >= step_limit)
                {
                    // put the cached value where it belongs
                    index_map[ MOVE_TAG::cached_value_destination_index(
                        dest_idx , step_offset , accumulated_steps, range_size
                    )] = cached_value;

                    ++step_offset;
                    accumulated_steps = 0;

                    if (step_offset >= step_size)
                    {
                        break;
                    }
                    // get a new cached value
                    cached_value = index_map[MOVE_TAG::cached_value_source_index(dest_idx, step_offset)];
                }
                index_map[MOVE_TAG::loop_destination_index(dest_idx, step_offset, accumulated_steps)] =
                index_map[MOVE_TAG::loop_source_index(dest_idx, step_offset, accumulated_steps, step_size)];
            }

        }


        allocator_type my_alloc;
        // allocator can allocate non-contiguously, so store a contiguous
        // array of allocator block indices that refer to the allocated items
        size_type map_end;
        std::array<index_type, N> index_map;
};


template<typename T, std::size_t N, typename SIZE_TYPE,       class ALLOCATOR,
                     std::size_t U, typename OTHER_SIZE_TYPE, class OTHER_ALLOCATOR
>
bool operator==(pool_list<T,N,SIZE_TYPE,       ALLOCATOR>       const & lhs,
                pool_list<T,U,OTHER_SIZE_TYPE, OTHER_ALLOCATOR> const & rhs)
{
    if (lhs.size() != rhs.size())
    {
        return false;
    }
    for (SIZE_TYPE idx = 0;
        idx < lhs.size();
        ++idx
    ) {
        if (lhs[idx] != rhs[idx]) {return false;}
    }
    return true;
}

template<typename T, std::size_t N, typename SIZE_TYPE,       class ALLOCATOR,
                     std::size_t U, typename OTHER_SIZE_TYPE, class OTHER_ALLOCATOR
>
bool operator!=(pool_list<T,N,SIZE_TYPE,       ALLOCATOR>       const & lhs,
                pool_list<T,U,OTHER_SIZE_TYPE, OTHER_ALLOCATOR> const & rhs)
{
    return !(lhs == rhs);
}

template<
    typename T,
    std::size_t N,
    typename SIZE_TYPE,
    class Allocator,
    bool IS_CONST_ITERATOR
>
class pool_list_iterator
{
    public:

    using size_type       = SIZE_TYPE;
    using value_type      = typename std::conditional<IS_CONST_ITERATOR, typename std::add_const<T>::type, T>::type;
    using pointer         = typename std::add_pointer<value_type>::type;
    using reference       = typename std::add_lvalue_reference<value_type>::type;
    using difference_type = typename std::conditional<
        std::numeric_limits<size_type>::digits == std::numeric_limits<std::uintmax_t>::digits
        , typename  std::make_signed<size_type>::type
        // the cues library has no need to promote this type, but the implementation of some stl algorithms
        // (in particular, __merge_adaptive in libstdc++10)
        // don't cast the result of a subtraction to difference_type, meaning that
        // it will get promoted by the subtraction and cause template deduction problems
        // when using iterators having difference_type smaller than int
        , typename cues::type_or_promoted<typename cues::make_signed<size_type>::type>::type
    >::type;

    // random access, but definitely not contiguous
    using iterator_category = std::random_access_iterator_tag;

    using list_type = pool_list<T,N,SIZE_TYPE,Allocator>;
    using list_ptr_type = typename std::conditional<
        IS_CONST_ITERATOR,
         list_type const *
        , list_type *
    >::type;


    // helps with non-const -> const conversion
    friend class pool_list_iterator<T, N, SIZE_TYPE, Allocator, true>;

    template<typename U, std::size_t M, typename SZ, class Al>
    friend class pool_list;

    pool_list_iterator() = default;

    explicit constexpr pool_list_iterator(std::nullptr_t) noexcept
    : container(nullptr)
    , current_index(0)
    {}

    pool_list_iterator(list_ptr_type c, size_type i)
    : container(c)
    , current_index(i)
    {}

    pool_list_iterator(pool_list_iterator const &) noexcept = default;

    //! conditionally enable converting constructor
    template<bool ENABLE_CONST_CONVERSION = IS_CONST_ITERATOR>
    constexpr pool_list_iterator(
        typename std::enable_if<
            ENABLE_CONST_CONVERSION,
            pool_list_iterator<T, N, SIZE_TYPE, Allocator, false>
        >::type const & other
    ) noexcept
    : container(other.container)
    , current_index(other.current_index)
    {}

    pool_list_iterator & operator=(pool_list_iterator const &) noexcept = default;

    constexpr pool_list_iterator & operator=(std::nullptr_t) noexcept
    {
        container     = nullptr;
        current_index = 0;
    }

    friend constexpr bool operator== (pool_list_iterator const & lhs, std::nullptr_t) noexcept
    {
        return lhs.container == nullptr;
    }

    friend constexpr bool operator== (std::nullptr_t, pool_list_iterator const & rhs) noexcept
    {
        return rhs.container == nullptr;
    }

    friend constexpr bool operator!= (pool_list_iterator const & lhs, std::nullptr_t) noexcept
    {
        return !(lhs == nullptr);
    }

    friend constexpr bool operator!= (std::nullptr_t, pool_list_iterator const & rhs) noexcept
    {
        return !(rhs == nullptr);
    }

    friend constexpr bool operator== (pool_list_iterator const & lhs, pool_list_iterator const & rhs) noexcept
    {
        return lhs.container == rhs.container && lhs.current_index == rhs.current_index;
    }

    friend constexpr bool operator!= (pool_list_iterator const & lhs, pool_list_iterator const & rhs) noexcept
    {
        return !(lhs == rhs);
    }

    //! PRECONDITION: only valid for iterators from the same container
    friend constexpr bool operator< (pool_list_iterator const & lhs, pool_list_iterator const & rhs) noexcept
    {
        assert(((void)"comparison of iterators from different containers", lhs.container == rhs.container));
        return lhs.current_index < rhs.current_index;
    }

    friend constexpr bool operator>  (pool_list_iterator const & lhs, pool_list_iterator const & rhs) noexcept
    {
        return rhs < lhs;
    }

    friend constexpr bool operator<= (pool_list_iterator const & lhs, pool_list_iterator const & rhs) noexcept
    {
        return !(rhs < lhs);
    }

    friend constexpr bool operator>= (pool_list_iterator const & lhs, pool_list_iterator const & rhs) noexcept
    {
        return !(lhs < rhs);
    }

    //! PRECONDITION: iterator is valid, n must be within range
    constexpr pool_list_iterator & operator+=(difference_type n) noexcept
    {
        assert(((void)"invalid iterator", container != nullptr));
        // there are more cases where the index could be invalid,
        // but since we don't store anything to remember where begin()
        // is, we can't check those cases here.
        // permit up to extent to get end
        assert(((void)"index invalid", ((n < 0) ? (-n) : n) <= N));
        current_index += n;
        return *this;
    }

    //! PRECONDITION: iterator is valid, not end
    constexpr pool_list_iterator & operator++() noexcept
    {
        current_index++;
        return *this;
    }

    //! PRECONDITION: iterator is valid, not end
    constexpr pool_list_iterator operator++(int) noexcept
    {
        pool_list_iterator temp (*this);
        operator++();
        return temp;
    }



    //! PRECONDITION: iterator is valid,
    constexpr pool_list_iterator & operator-=(difference_type n) noexcept
    {
        assert(( (void)"out of range",
               (n >= 0 && current_index > n)
               ||
               (n < 0 &&
                // want current_index > -n, but
                // -n by itself might overflow.
                // in fact we have multiple possible sources of overflow:
                // -n is greater than difference_type::max
                // -n is greater than size_type::max
                // size_type::max (and thus current_index) is greater than difference_type::max
                // -size_type::max (and thus -current_index) is less than difference_type::min
                // but we know that difference_type::max + n cannot overflow
                // and size_type::max - current_index cannot overflow
                // if we know which max is larger, and the compiler does,
                // then we can make further overflow guarantees for the larger max
                // and say:
                // max + current_index > max - n
                // max + current_index + n > max
                // max + n > max - current_index
                // neither side of which could overflow
                ((std::numeric_limits<difference_type>::max)() > (std::numeric_limits<size_type>::max)()) ?
                    (
                     (std::numeric_limits<difference_type>::max)() + n >
                     (std::numeric_limits<difference_type>::max)() - current_index
                    )
                :
                    (
                     (std::numeric_limits<size_type>::max)() + n >
                     (std::numeric_limits<size_type>::max)() - current_index
                    )
               )
            )
        );
        current_index -= n;
        return *this;
    }

    //! PRECONDITION: iterator is valid (or one past the end) but not begin
    constexpr pool_list_iterator & operator--() noexcept
    {
        assert(( (void)"out of range", current_index > 0));
        current_index--;
        return *this;
    }

    //! PRECONDITION: iterator is valid (or one past the end) but not begin
    constexpr pool_list_iterator operator--(int) noexcept
    {
        pool_list_iterator temp (*this);
        operator--();
        return temp;
    }

    //! PRECONDITION: both iterators are valid (or one "past" the end),
    //! and the distance between them fits in difference_type
    friend constexpr difference_type operator-(pool_list_iterator const & lhs, pool_list_iterator const & rhs) noexcept
    {
        assert(((void)"subtracting iterators from different containers", lhs.container == rhs.container));
        return lhs.current_index - rhs.current_index;
    }


    //! PRECONDITION: iterator is valid
    reference operator*() const
    {
        assert(((void)"invalid iterator", container != nullptr));
        return *(container->local_index_to_pointer(current_index));
    }

    //! PRECONDITION:iterator is valid AND n is within range
    reference operator[](difference_type n) const
    {
        assert(((void)"invalid iterator", container != nullptr));
        return *(container->local_index_to_pointer(current_index + n));
    }

    pointer operator->() const
    {
        assert(((void)"invalid iterator", container != nullptr));
        return container->local_index_to_pointer(current_index);
    }

    private:

    list_ptr_type container;

    typename list_type::size_type current_index;

};

template<
    typename T,
    std::size_t N,
    typename SIZE_TYPE,
    class Allocator,
    bool IS_CONST_ITERATOR
>
constexpr pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR> operator+(
    pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR>  lhs,
    typename pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR>::difference_type rhs) noexcept
{
    lhs += rhs;
    return lhs;
}

template<
    typename T,
    std::size_t N,
    typename SIZE_TYPE,
    class Allocator,
    bool IS_CONST_ITERATOR
>
constexpr pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR> operator+(
    typename pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR>::difference_type lhs,
    pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR> rhs) noexcept
{
    return rhs + lhs;
}

template<
    typename T,
    std::size_t N,
    typename SIZE_TYPE,
    class Allocator,
    bool IS_CONST_ITERATOR
>
constexpr pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR> operator-(
    pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR> lhs,
    typename pool_list_iterator<T, N, SIZE_TYPE, Allocator, IS_CONST_ITERATOR>::difference_type rhs) noexcept
{
    lhs -= rhs;
    return lhs;
}

} // namespace cues

#endif // CUES_POOL_LIST_HPP
