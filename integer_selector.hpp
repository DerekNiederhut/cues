#ifndef CUES_INTEGER_SELECTOR_HPP_INCLUDED
#define CUES_INTEGER_SELECTOR_HPP_INCLUDED

#pragma once

#include <type_traits>
#include <limits>
#include <cstdint>
#include <cstddef>

#include "cues/void_t.hpp"

namespace cues
{
    enum class integer_selector_signedness : std::uint_least8_t
    {
        use_unsigned,
        use_signed
    };

    enum class integer_selector_category : std::uint_least8_t
    {
        fast,
        least
    };

    //! select an integer to use based on the number of digits you need, whether it is signed or unsigned,
    //! and whether you want fast or least size
    template<std::size_t NUM_DIGITS, integer_selector_signedness signedness, integer_selector_category category, class Enable=void >
    struct select_integer_by_digits {};

    template<std::size_t NUM_DIGITS, integer_selector_signedness signedness, integer_selector_category category>
    struct select_integer_by_digits<NUM_DIGITS, signedness, category,
        cues::void_t<typename std::enable_if<
            (NUM_DIGITS < 8u)
            || ((NUM_DIGITS == 8u) && (signedness == integer_selector_signedness::use_unsigned))
        >::type>
    >
    {
        using type = typename std::conditional<
            signedness == integer_selector_signedness::use_signed,
            typename std::conditional<category == integer_selector_category::fast, std::int_fast8_t,  std::int_least8_t>::type,
            typename std::conditional<category == integer_selector_category::fast, std::uint_fast8_t, std::uint_least8_t>::type
        >::type;
    };

    template<std::size_t NUM_DIGITS, integer_selector_signedness signedness, integer_selector_category category>
    struct select_integer_by_digits<NUM_DIGITS, signedness, category,
        cues::void_t<typename std::enable_if<
            (NUM_DIGITS > 8u && NUM_DIGITS < 16u)
            || ((NUM_DIGITS == 8u) && (signedness == integer_selector_signedness::use_signed))
            || ((NUM_DIGITS == 16u) && (signedness == integer_selector_signedness::use_unsigned))
        >::type>
    >
    {
        using type = typename std::conditional<
            signedness == integer_selector_signedness::use_signed,
            typename std::conditional<category == integer_selector_category::fast, std::int_fast16_t,  std::int_least16_t>::type,
            typename std::conditional<category == integer_selector_category::fast, std::uint_fast16_t, std::uint_least16_t>::type
        >::type;
    };

    template<std::size_t NUM_DIGITS, integer_selector_signedness signedness, integer_selector_category category>
    struct select_integer_by_digits<NUM_DIGITS, signedness, category,
        cues::void_t<typename std::enable_if<
            (NUM_DIGITS > 16u && NUM_DIGITS < 32u)
            || ((NUM_DIGITS == 16u) && (signedness == integer_selector_signedness::use_signed))
            || ((NUM_DIGITS == 32u) && (signedness == integer_selector_signedness::use_unsigned))
        >::type>
    >
    {
        using type = typename std::conditional<
            signedness == integer_selector_signedness::use_signed,
            typename std::conditional<category == integer_selector_category::fast, std::int_fast32_t,  std::int_least32_t>::type,
            typename std::conditional<category == integer_selector_category::fast, std::uint_fast32_t, std::uint_least32_t>::type
        >::type;
    };

    template<std::size_t NUM_DIGITS, integer_selector_signedness signedness, integer_selector_category category>
    struct select_integer_by_digits<NUM_DIGITS, signedness, category,
        cues::void_t<typename std::enable_if<
            (NUM_DIGITS > 32u && NUM_DIGITS < 64u)
            || ((NUM_DIGITS == 32u) && (signedness == integer_selector_signedness::use_signed))
            || ((NUM_DIGITS == 64u) && (signedness == integer_selector_signedness::use_unsigned))
        >::type>
    >
    {
        using type = typename std::conditional<
            signedness == integer_selector_signedness::use_signed,
            typename std::conditional<category == integer_selector_category::fast, std::int_fast64_t,  std::int_least64_t>::type,
            typename std::conditional<category == integer_selector_category::fast, std::uint_fast64_t, std::uint_least64_t>::type
        >::type;
    };

    // conditionally pull in the max types if they are larger than 64
    template<std::size_t NUM_DIGITS, integer_selector_signedness signedness, integer_selector_category category>
    struct select_integer_by_digits<NUM_DIGITS, signedness, category,
        cues::void_t<typename std::enable_if<
        (    (NUM_DIGITS > 64u)
            && (signedness == integer_selector_signedness::use_unsigned)
            && (std::numeric_limits<std::uintmax_t>::digits >= NUM_DIGITS)
        )
        || (
               (NUM_DIGITS >= 64u)
            && (signedness == integer_selector_signedness::use_signed)
            && (std::numeric_limits<std::intmax_t>::digits >= NUM_DIGITS)
        )
        >::type>
    >
    {
        using type = typename std::conditional<
            signedness == integer_selector_signedness::use_signed,
            std::intmax_t,
            std::uintmax_t
        >::type;
    };


    //! convenience for automatically selecting fast un/signed integer based on number of digits
    template<std::size_t NUM_DIGITS, integer_selector_signedness signedness>
    struct select_fast_integer_by_digits
    {
        using type = typename select_integer_by_digits<NUM_DIGITS, signedness, integer_selector_category::fast>::type;
    };

    //! convenience for automatically selecting least un/signed integer based on number of digits
    template<std::size_t NUM_DIGITS, integer_selector_signedness signedness>
    struct select_least_integer_by_digits
    {
        using type = typename select_integer_by_digits<NUM_DIGITS, signedness, integer_selector_category::least>::type;
    };

    //! for automatically selecting integer based on number of bytes, instead of digits.
    //! this is older code and has been kept for backward-compatibility;
    //! selection by digits may be more general, as it will work correctly
    //! for user-defined numeric types that specialize std::numeric_limits
    //! appropriately, even if their capacity is not simply the 2's-complement
    //! interpretation of their size.
    template<std::size_t NUM_BYTES, integer_selector_signedness signedness, integer_selector_category category>
    struct select_integer
    {
        using type = typename select_integer_by_digits<
            NUM_BYTES*std::numeric_limits<unsigned char>::digits
            - (NUM_BYTES > 0 && (signedness == integer_selector_signedness::use_signed) ? 1u : 0u),
            signedness,
            category
        >::type;
    };

    //! convenience for automatically selecting fast un/signed integer based on number of bytes
    template<std::size_t NUM_BYTES, integer_selector_signedness signedness>
    struct select_fast_integer
    {
        using type = typename select_integer<NUM_BYTES, signedness, integer_selector_category::fast>::type;
    };

    //! convenience for automatically selecting least un/signed integer based on number of bytes
    template<std::size_t NUM_BYTES, integer_selector_signedness signedness>
    struct select_least_integer
    {
        using type = typename select_integer<NUM_BYTES, signedness, integer_selector_category::least>::type;
    };

    //! for generating byte masks that match the size of the
    //! selected unsigned fast type at compile-time.
    //! only uses unsigned types because all bit operations are
    //! well-defined for unsigned types, but not all of them are well-defined
    //! for signed types.
    //! note: the use of recursion here means that if you don't know the
    //! size at compile-time, you may want to use an intermediate mechanism.
    template<std::size_t NUM_BYTES>
    constexpr typename select_least_integer<NUM_BYTES, integer_selector_signedness::use_unsigned>::type bytemask() noexcept
    {
        static_assert(NUM_BYTES <= sizeof(typename select_least_integer<NUM_BYTES, integer_selector_signedness::use_unsigned>::type),"ASSERTION VIOLATION: asked for too large a mask.");
        return (NUM_BYTES > 0) ?
                (bytemask<NUM_BYTES/2>() << ((NUM_BYTES - NUM_BYTES/2) * std::numeric_limits<unsigned char>::digits)
                | bytemask<NUM_BYTES - NUM_BYTES/2>() )
            :
                bytemask<0>();
    }

    template<>
    constexpr typename select_least_integer<1, integer_selector_signedness::use_unsigned>::type bytemask<1>() noexcept
    {
        return 0xFF;
    }
    template<>
    constexpr typename select_least_integer<0, integer_selector_signedness::use_unsigned>::type bytemask<0>() noexcept
    {
        return 0;
    }

}

#endif // CUES_INTEGER_SELECTOR_HPP_INCLUDED
