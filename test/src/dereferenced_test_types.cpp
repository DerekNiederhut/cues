#include "dereference_test_types.hpp"

#include <set>
#include <cassert>

namespace cues
{
namespace test
{
// test non-trivial iterators
std::size_t check_dereference_nontrivial_iterators()
{
    std::size_t assertion_failures = 0;
    std::set< std::int_least8_t> set_of_test_data {
        cues::test::dereferenced_test_set.cbegin(),
        cues::test::dereferenced_test_set.cend()
    };
    if (!cues::test::recursive_array_check_by_iterator(set_of_test_data.cbegin(), set_of_test_data.cend()))
    {
        assertion_failures++;
    }
    assert((0 == assertion_failures) && "Assertion violation: functor_wrapper_dereference failed on iterator");
    return assertion_failures;
}

}
}
