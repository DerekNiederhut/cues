#ifndef CUES_ARRAY_PROXY_HPP
#define CUES_ARRAY_PROXY_HPP

#if (__cplusplus < 201703L && !defined ( __cpp_lib_launder) )
    #error "cues::array_proxy requires std::launder, which is not availabe."
#endif

#include <cstddef>
#include <type_traits>
#include <limits>
#include <new>
#include <iterator>
#include <stdexcept>
#include <cassert>
#include <memory>
#include <algorithm>

#include "cues/array_type_traits.hpp"
#include "cues/version_helpers.hpp"

namespace cues {

// forward-declare proxy class so iterator can refer to it
template<class T, bool IS_CONST = std::is_const<typename std::remove_all_extents<T>::type>::value>
class array_proxy;

//! iterator to proxied array
//! takes care of the actual pointer adjustments and laundering
//! For multi-dimensional arrays, will construct intermediate proxy objects
//! to proxy the sub-arrays; that is, a proxy iterator to T[M][N] iterates
//! the M-sized array of T[N], and dereferencing via operator* or operator[]
//! returns a proxy_array of T[N], to which you can get a (distinct)
//! array_proxy_iterator, or call operator[] directly
template<class T, bool IS_CONST_ITERATOR = std::is_const<typename std::remove_all_extents<T>::type>::value>
class array_proxy_iterator
{
private:
    using proxy_extents = extent_sequence<T>;
    static constexpr bool is_last_extent    = std::rank<T>::value <= 1;
    static constexpr bool is_const_iterator = std::is_const<typename std::remove_all_extents<T>::type>::value;
    static_assert(std::is_array<T>::value, "Logic error: array proxy iterator is only meant to proxy array types.");
    static_assert(is_const_iterator == IS_CONST_ITERATOR,
                  "Logic error: attempting to mix const-ness of type and iterator.");

public:
    //! for explicit constructor
    using underlying_pointer_type = typename std::add_pointer<
        typename std::conditional<
            is_const_iterator,
            cues::raw_byte_type const,
            cues::raw_byte_type
        >::type
    >::type;

    //! the "element" of this level of the array proxy
    using proxy_element_type = typename std::remove_extent<T>::type;


    using difference_type   = std::ptrdiff_t;
    // if this is the last extent of an array, use the value type;
    // otherwise, use the array_proxy_iterator of the type with one extent removed
    using value_type        = typename std::conditional<
                                  is_last_extent,
                                  typename std::remove_extent<T>::type,
                                  array_proxy<typename std::remove_extent<T>::type, is_const_iterator>
                              >::type;
    // if we've reached the point of being able to point to something,
    // define the pointer to that thing.  if we're still in proxy levels,
    // define as unique_pointer to construct a unique_pointer to the proxied type.
    using pointer 	        = typename std::conditional<
                                  is_last_extent,
                                  typename std::add_pointer<value_type>::type,
                                  std::unique_ptr<value_type>
                              >::type;
    // if we've reached the last level, a reference to that.
    // otherwise, the proxy object; not a reference to the proxy object,
    // which may not exist until a function is called that returns it,
    // but a copy of the value of the proxy.
    using reference 	    = typename std::conditional<
                                  is_last_extent,
                                  typename std::add_lvalue_reference<value_type>::type,
                                  value_type
                               >::type;

    // this very nearly meets std::contiguous_iterator_tag,
    // except that because the objects may not have been created
    // as an array (for example they may have been created as
    // repeated contiguous calls to placement new, as opposed to a single
    // call to placement new []), so the pointer math on the reinterpreted
    // type could be undefined behavior.
    using iterator_category = std::random_access_iterator_tag;

    friend constexpr bool operator== (array_proxy_iterator const & lhs, std::nullptr_t) noexcept
    {
        return lhs.data == nullptr;
    }

    friend constexpr bool operator== (std::nullptr_t, array_proxy_iterator const & rhs) noexcept
    {
        return rhs.data == nullptr;
    }

    friend constexpr bool operator!= (array_proxy_iterator const & lhs, std::nullptr_t) noexcept
    {
        return !(lhs == nullptr);
    }

    friend constexpr bool operator!= (std::nullptr_t, array_proxy_iterator const & rhs) noexcept
    {
        return !(rhs == nullptr);
    }

    friend constexpr bool operator== (array_proxy_iterator const & lhs, array_proxy_iterator const & rhs) noexcept
    {
        return lhs.data == rhs.data;
    }

    friend constexpr bool operator!= (array_proxy_iterator const & lhs, array_proxy_iterator const & rhs) noexcept
    {
        return lhs.data != rhs.data;
    }

    friend constexpr bool operator< (array_proxy_iterator const & lhs, array_proxy_iterator const & rhs) noexcept
    {
        return 0 < (lhs - rhs);
    }

    friend constexpr bool operator>  (array_proxy_iterator const & lhs, array_proxy_iterator const & rhs) noexcept
    {
        return rhs < lhs;
    }

    friend constexpr bool operator<= (array_proxy_iterator const & lhs, array_proxy_iterator const & rhs) noexcept
    {
        return !(rhs < lhs);
    }

    friend constexpr bool operator>= (array_proxy_iterator const & lhs, array_proxy_iterator const & rhs) noexcept
    {
        return !(lhs < rhs);
    }


    //! PRECONDITION: iterator is valid, n must be within range
    constexpr array_proxy_iterator & operator+=(difference_type n) noexcept
    {
        assert(((void)"invalid iterator", data != nullptr));
        // there are more cases where the index could be invalid,
        // but since we don't store anything to remember where begin()
        // is, we can't check those cases here.
        // permit up to extent to get end
        assert(((void)"index invalid", ((n < 0) ? (-n) : n) <= (proxy_extents::template nth<0>)()));
        data = data + n*sizeof(proxy_element_type);
        return *this;
    }

    //! PRECONDITION: iterator is valid
    constexpr array_proxy_iterator & operator++() noexcept
    {
        return operator+=(1);
    }

    //! PRECONDITION: iterator is valid
    constexpr array_proxy_iterator operator++(int) noexcept
    {
        array_proxy_iterator temp (*this);
        operator++();
        return temp;
    }

    //! PRECONDITION: iterator is valid, abs(n) must be within range
    constexpr array_proxy_iterator & operator-=(difference_type n) noexcept
    {
        assert(( (void)"overflow",
                n > (std::numeric_limits<difference_type>::min)()
               || -(std::numeric_limits<difference_type>::max)() == (std::numeric_limits<difference_type>::min)())
        );
        return operator+=(-n);
    }

    //! PRECONDITION: iterator is valid (or one past the end) but not begin
    constexpr array_proxy_iterator & operator--() noexcept
    {
        return operator-=(1);
    }

    //! PRECONDITION: iterator is valid (or one past the end) but not begin
    constexpr array_proxy_iterator operator--(int) noexcept
    {
        array_proxy_iterator temp (*this);
        operator--();
        return temp;
    }

    //! PRECONDITION: both iterators are valid (or one "past" the end),
    //! and the distance between them fits in difference_type
    constexpr difference_type operator-(array_proxy_iterator const & rhs) noexcept
    {
        return (this->data - rhs.data)/sizeof(proxy_element_type);
    }

    //! PRECONDITION:iterator is valid AND n is within range
    reference operator[](difference_type n) const
    {
        return to_reference(n);
    }

    //! PRECONDITION: iterator is valid
    reference operator*() const
    {
        return to_reference(0);
    }

    //! WARNING: because this is a proxy object, implementing
    //! operator-> for multi-dimensional arrays means needing
    //! to create a temporary proxy object that can act as a pointer.
    //! To avoid a dangling pointer, unique_ptr is used.
    //! this means a trip to the heap for each call.
    //! AVOID using this function for multi-dimensional arrays and instead
    //! use operator* or operator[], which work better through the proxy.
    //! operator-> is provided because it is a requirement of LegacyInputIterator
    //! and thus, by extension, LegacyRandomAccessIterator
    pointer operator->() const
    {
        return to_pointer(0);
    }


    array_proxy_iterator() noexcept = default;
    array_proxy_iterator(array_proxy_iterator const &) noexcept = default;
    array_proxy_iterator & operator=(array_proxy_iterator const &) noexcept = default;

    array_proxy_iterator & operator=(std::nullptr_t) noexcept
    {
        this->data = nullptr;
        return *this;
    }

    explicit constexpr array_proxy_iterator(std::nullptr_t) noexcept
    : data(nullptr)
    {}

    //! conditionally enable converting constructor
    template<bool ENABLE_CONST_CONVERSION = is_const_iterator>
    constexpr array_proxy_iterator(
        typename std::enable_if<
            ENABLE_CONST_CONVERSION,
            array_proxy_iterator<typename std::remove_const<T>::type, false>
        >::type const & other
    ) noexcept
    : data(other.data)
    {}

    //! construct from reference to an actual object
    explicit array_proxy_iterator(typename std::add_lvalue_reference<T>::type obj) noexcept
    : data(reinterpret_cast<underlying_pointer_type>(std::addressof(obj)))
    {}

    explicit constexpr operator underlying_pointer_type() const noexcept
    {
        return data;
    }

    friend class array_proxy<typename std::add_const<T>::type, true>;
    friend class array_proxy<T, false>;

    template<typename, bool>
    friend class array_proxy_iterator;

private:

    // construct from underlying pointer
    explicit constexpr array_proxy_iterator(underlying_pointer_type placed_data) noexcept
    : data(placed_data) {}

    // version when value_type is no longer another array_proxy
    template<typename INTEGER_TYPE,
        typename std::enable_if<
            is_last_extent && std::is_integral<INTEGER_TYPE>::value, bool
        >::type ENABLE_REINTERPRET = is_last_extent
    >
    reference to_reference( INTEGER_TYPE index) const noexcept
    {
        // operator += checks <= extent, so end can be obtained.
        // but you can't dereference end, so check additionally here only <
        assert(((void)"index invalid", ((index < 0) ? (-index) : index) < (proxy_extents::template nth<0>)()));
        return *(to_pointer(index));
    }

    // version when value_type is another array_proxy;
    // return by value a copy of the proxy object instead of
    // a reference to an object that doesn't exist.
    template<typename INTEGER_TYPE,
        typename std::enable_if<
            !is_last_extent && std::is_integral<INTEGER_TYPE>::value, bool
        >::type ENABLE_PROXY = !is_last_extent
    >
    reference to_reference(INTEGER_TYPE index) const noexcept
    {
        using forwarded_proxy_type = typename array_proxy_iterator<proxy_element_type, is_const_iterator>::proxy_extents;
        assert(((void)"invalid iterator", data != nullptr));
        assert(((void)"index invalid", ((index < 0) ? (-index) : index) < (proxy_extents::template nth<0>)()));
        return reference(
            (array_proxy_iterator(*this) += index).data,
            forwarded_proxy_type{}
        );
    }

    // version when we are down to a value, not another proxy_array
    template<typename INTEGER_TYPE,
        typename std::enable_if<
            is_last_extent && std::is_integral<INTEGER_TYPE>::value, bool
        >::type ENABLE_REINTERPRET = is_last_extent
    >
    pointer to_pointer( INTEGER_TYPE index) const noexcept
    {
        assert(((void)"invalid iterator", data != nullptr));
        return (std::launder(
            reinterpret_cast<pointer>(
                (array_proxy_iterator(*this) += index).data
            )
        ));
    }

    template<typename INTEGER_TYPE,
        typename std::enable_if<
            !is_last_extent && std::is_integral<INTEGER_TYPE>::value, bool
        >::type ENABLE_PROXY = !is_last_extent
    >
    pointer to_pointer(INTEGER_TYPE index) const noexcept
    {
        using forwarded_proxy_type = typename array_proxy_iterator<proxy_element_type, is_const_iterator>::proxy_extents;
        assert(((void)"invalid iterator", data != nullptr));
        return std::make_unique<value_type>(
            (array_proxy_iterator(*this) += index).data,
            forwarded_proxy_type{}
       );
    }

    underlying_pointer_type data;
};


//! PRECONDITION: iterator is valid, n is within range
template<class T, bool IS_CONST_ITERATOR>
inline constexpr array_proxy_iterator<T, IS_CONST_ITERATOR> operator+(
    array_proxy_iterator<T, IS_CONST_ITERATOR> lhs,
    typename array_proxy_iterator<T, IS_CONST_ITERATOR>::difference_type n
) noexcept
{
    return (lhs += n);
}

//! PRECONDITION: iterator is valid, n is within range
template<class T, bool IS_CONST_ITERATOR>
inline constexpr array_proxy_iterator<T, IS_CONST_ITERATOR> operator+(
    typename array_proxy_iterator<T, IS_CONST_ITERATOR>::difference_type n,
    array_proxy_iterator<T, IS_CONST_ITERATOR> rhs
) noexcept
{
    return (rhs += n);
}

//! PRECONDITION: iterator is valid, abs(n) is within range
template<class T, bool IS_CONST_ITERATOR>
inline constexpr array_proxy_iterator<T, IS_CONST_ITERATOR> operator-(
    array_proxy_iterator<T, IS_CONST_ITERATOR> lhs,
    typename array_proxy_iterator<T, IS_CONST_ITERATOR>::difference_type n
) noexcept
{
    return (lhs -= n);
}

//! PRECONDITION: iterator is valid, abs(n) is within range
template<class T, bool IS_CONST_ITERATOR>
inline constexpr array_proxy_iterator<T, IS_CONST_ITERATOR> operator-(
    typename array_proxy_iterator<T, IS_CONST_ITERATOR>::difference_type n,
    array_proxy_iterator<T, IS_CONST_ITERATOR> rhs
) noexcept
{
    return (rhs += n);
}





//! Helper class for types that manage memory with placement new.
//! array placement new can take an extra amount of space, which
//! cannot (at time of writing) be determined at compile time.
//! An alternative is to use placement new individually for each
//! element at the appropriate location,but only access them through
//! objects which operate on the underlying byte array to avoid undefined
//! behavior as described in the aliasing rules.
//! for single-dimension arrays, this is reasonably straightforward,
//! but it becomes more complicated for multi-dimensional arrays.
//! This proxy object provides operator[] overloads; it purposefully
//! omits the conversion to pointer, std::begin(), etc. because
//! math on those pointers is not guaranteed to work by the standard
//! since they weren't actually created as an array

template<class T, bool IS_CONST_PROXY>
class array_proxy
{
private:
    static constexpr bool is_const_proxy = std::is_const<typename std::remove_all_extents<T>::type>::value;
    static_assert(is_const_proxy == IS_CONST_PROXY,
                  "Logic error: attempting to mix const-ness of type and proxy.");
public:
    using const_iterator         = array_proxy_iterator<typename std::add_const<T>::type,true>;
    using iterator               = array_proxy_iterator<T,is_const_proxy>;
    using reverse_iterator       = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using proxy_extents          = extent_sequence<T>;

    using convertable_iterator_type = typename std::conditional<
        is_const_proxy,
        const_iterator,
        iterator
    >::type;

    static_assert(std::is_array<T>::value, "array proxy is only meant to proxy array types.");

    //! deleted default constructor to avoid creating invalid proxy objects.
    array_proxy() = delete;

    //! construct from pointer and description of proxy type
    //! PRECONDITION: pointer points to first element of proxied array
    constexpr array_proxy(
        typename convertable_iterator_type::underlying_pointer_type ptr,
        proxy_extents
    ) noexcept
    : m_begin(iterator{ptr})
    {}

    //! construct from (lvalue) reference to exact type to be proxied
    explicit constexpr array_proxy(typename std::add_lvalue_reference<T>::type actual_data)
    : m_begin(iterator{actual_data})
    {}

    array_proxy(array_proxy const &) = default;
    array_proxy & operator=(array_proxy const & other) = default;

    template<bool IS_MUTABLE = !is_const_proxy>
    array_proxy & operator=(
        typename std::enable_if<IS_MUTABLE,
            array_proxy<typename std::add_const<T>::type>
         >::type const & other
    ) noexcept
    {
        std::copy(other.begin(), other.end(), this->begin());
        return *this;
    }


    friend class array_proxy<
        typename std::conditional<
            is_const_proxy,
            typename std::remove_const<T>::type,
            typename std::add_const<T>::type
        >::type,
        !is_const_proxy
    >;

    //! conditionally enable converting constructor
    //! creating a proxy to const from a proxy to non-const
    //! can be a shallow copy; the proxy to const cannot change
    //! anyway so there is no need to copy the data
    template<bool ENABLE_CONST_CONVERSION = is_const_proxy>
    constexpr array_proxy(
        typename std::enable_if<ENABLE_CONST_CONVERSION,
            array_proxy<typename std::remove_const<T>::type>
         >::type const & other
    ) noexcept
    : m_begin( iterator{static_cast<typename array_proxy<typename std::remove_const<T>::type>::iterator>(other.m_begin)})
    {}

    static constexpr std::size_t rank() noexcept
    {
        return proxy_extents::size();
    }

    //! extent at this rank level
    static constexpr std::size_t extent() noexcept
    {
        return (proxy_extents::template nth<0>)();
    }

    //! size at this rank level.
    //! same as extent, name provided for compatibility
    //! with code meant for other containers/ranges
    constexpr std::size_t size() const noexcept
    {
        return extent();
    }

    constexpr std::size_t empty() const noexcept
    {
        return extent() == 0;
    }

    constexpr std::size_t max_size() const noexcept
    {
        return extent();
    }

    //! PRECONDITION: proxy is valid
    typename iterator::reference operator[](std::size_t index) const noexcept
    {
        assert(((void)"index out of bounds", index < extent()));
        return *(begin() += index);
    }

    typename iterator::reference at(std::size_t index) const
    {
        if (index < extent()) {return this->operator[](index);}
        throw std::out_of_range("Invalid index to array_proxy operator[]");
    }

    //! PRECONDITION: proxy is valid
    typename iterator::reference front() const noexcept
    {
        return operator[](0);
    }

    //! PRECONDITION: proxy is valid
    typename iterator::reference back() const noexcept
    {
        return operator[]((extent() > 0) ? (extent() - 1) : 0);
    }

    //! PRECONDITION: proxy is valid
    constexpr const_iterator cbegin() const noexcept
    {
        return begin();
    }

    //! PRECONDITION: proxy is valid
    constexpr const_iterator cend() const noexcept
    {
        return end();
    }

    //! PRECONDITION: proxy is valid
    constexpr iterator begin() const noexcept
    {
        return static_cast<iterator>(m_begin);
    }

    //! PRECONDITION: proxy is valid
    constexpr iterator end() const noexcept
    {
        return begin() + extent();
    }

    constexpr const_reverse_iterator crbegin() const noexcept
    {
        return rbegin();
    }

    constexpr const_reverse_iterator crend() const noexcept
    {
        return rend();
    }

    constexpr reverse_iterator rbegin() const noexcept
    {
        return reverse_iterator(end());
    }

    constexpr reverse_iterator rend() const noexcept
    {
        return reverse_iterator(begin());
    }

    friend bool operator==(array_proxy const & lhs, array_proxy const &rhs)
    {
        bool equivalent = true; // initialize to true for the empty case
        for (auto litr = lhs.cbegin(), ritr = rhs.cbegin();
            equivalent && litr != lhs.cend() && ritr != rhs.cend();
            ++litr, ++ritr)
        {
            // note: the comparison here can call operator== of the subarray if operator* returns
            // a proxy_array to a subindex of the array we are currently iterating;
            // for the final element type, provided an operator== is provided, it will
            // call that instead.
            equivalent = equivalent && (*litr == *ritr);
        }
        return equivalent;
    }

    friend bool operator!=(array_proxy const & lhs, array_proxy const &rhs)
    {
        return !(lhs == rhs);
    }

    // array decay only works properly for the last extent, where we actually have individual objects
    // sequentially organized.
    template<typename U = typename std::remove_cv<typename std::remove_extent<T>::type>::type,
        class Enable = typename std::enable_if<
            std::rank<U>::value == 0
            && std::is_convertible<typename convertable_iterator_type::underlying_pointer_type, U* >::value
        >::type
    >
    explicit operator U const * () const
    {
        return std::addressof(*(static_cast<convertable_iterator_type>(m_begin)));
    }

    template<typename U = typename std::remove_extent<T>::type,
        class Enable = typename std::enable_if<
         std::rank<U>::value == 0
         && std::is_convertible<typename convertable_iterator_type::underlying_pointer_type, U* >::value
         && !is_const_proxy
        >
    >
    explicit operator U * ()
    {
        return std::addressof(*(static_cast<convertable_iterator_type>(m_begin)));
    }

private:

    // helper type so that implicit copy constructor/assignment will
    // either do deep copy, or be deleted, based on whether the proxied type is const
    class array_proxy_deep_range
    {
    public:
        using shallow_iterator = convertable_iterator_type;

        array_proxy_deep_range() = delete;
        // can't copy through range if it doesn't exist yet, so the default shallow copy is fine
        array_proxy_deep_range(array_proxy_deep_range const & ) = default;
        // forward to deep copy implementation
        array_proxy_deep_range & operator=(array_proxy_deep_range const & other)
        {
            copy_impl(other);
            return *this;
        }


        explicit array_proxy_deep_range(shallow_iterator itr)
        : m_begin(itr)
        {}

        explicit operator shallow_iterator() const
        {
            return m_begin;
        }

    private:

        // iterative copy for
        template<class U = T>
        void copy_impl( typename
            std::enable_if<
                std::is_same<
                    typename std::remove_cv<T>::type,
                    typename std::remove_cv<U>::type
                >::value,
                typename array_proxy<U>::array_proxy_deep_range
            >::type const & other
        )
        {
            std::copy(other.m_begin, other.m_begin + extent_sequence<U>::template nth<0>(), m_begin);
        }

        shallow_iterator m_begin;
    };

    // PRECONDITION: itr is an iterator to the beginning of a (proxied) array
    explicit constexpr array_proxy(convertable_iterator_type itr) noexcept
    : m_begin(itr)
    {}

    typename std::conditional<
        is_const_proxy,
        typename std::add_const<array_proxy_deep_range>::type,
        array_proxy_deep_range
    >::type  m_begin;

};


} // namespace cues

#endif // CUES_ARRAY_PROXY_HPP
