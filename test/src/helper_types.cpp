#include "helper_types.hpp"

namespace cues
{
namespace test
{

    std::mt19937 randomly_fails_construction::internal_engine = std::mt19937{};
    std::uniform_int_distribution<randomly_fails_construction::integer_type> randomly_fails_construction::post_processor;
    std::atomic<randomly_fails_construction::counter_type> randomly_fails_construction::times_default_thrown = CUES_ATOMIC_VAR_INIT(0);
    std::atomic<randomly_fails_construction::counter_type> randomly_fails_construction::times_copy_thrown = CUES_ATOMIC_VAR_INIT(0);

}
}
