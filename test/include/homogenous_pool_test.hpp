#ifndef CUES_TEST_HOMOGENOUS_POOL_TEST_HPP_INCLUDED
#define CUES_TEST_HOMOGENOUS_POOL_TEST_HPP_INCLUDED

#include <cstddef>

namespace cues
{
namespace test
{
    std::size_t check_homogenous_pool_test_runtime_assertions();
}
}

#endif // CUES_TEST_HOMOGENOUS_POOL_TEST_HPP_INCLUDED
