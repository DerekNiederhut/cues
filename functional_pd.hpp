#ifndef CUES_FUNCTIONAL_PD_HPP
#define CUES_FUNCTIONAL_PD_HPP

/* LICENSE VARIATION
 * regardless of the license for other files within the cues library,
 * this file is not copyright and is in the public domain, in keeping
 * with the wishes of the authors of source on which it is based.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/
#include <cstddef>
#include <climits>
#include <iterator>
#include <type_traits>
#include "cues/integer_selector.hpp"
#include "cues/version_helpers.hpp"
#include "cues/type_traits.hpp"

namespace cues {

/* FNV hash, C implementation provided by Phong Vo, Glenn Fowler, and Landon Curt Noll
   Public domain C source available at http://www.isthe.com/chongo/tech/comp/fnv/#FNV-reference-source
   at time of writing.

   Original code disclaimer:
    * LANDON CURT NOLL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
    * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
    * EVENT SHALL LANDON CURT NOLL BE LIABLE FOR ANY SPECIAL, INDIRECT OR
    * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
    * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    * PERFORMANCE OF THIS SOFTWARE.
    *
    * By:
    *	chongo <Landon Curt Noll> /\oo/\
    *      http://www.isthe.com/chongo/

   This version differs in that it uses constexpr keywords, and is templated on iterator type
   so it works with inputs other than just pointer + length.  It is also templated on the bit length
   which can make it more portable for example to use with std::size_t,
   via std::numeric_limits<std::size_t>::digits, so the same source code is used
   whether size_t is 32 or 64 bits (though it would not work on platforms with other size_t).

*/

template<std::size_t BITS>
using fnv_val_type = typename cues::select_least_integer_by_digits<BITS, cues::integer_selector_signedness::use_unsigned>::type;

template<std::size_t BITS>
constexpr fnv_val_type<BITS> fnv_get_prime();

template<>
constexpr fnv_val_type<32u> fnv_get_prime<32u>()
{   // taken from public domain C source code
    return 16777619UL;
}

template<>
constexpr fnv_val_type<64u> fnv_get_prime<64u>()
{   // taken from public domain C source code
    return 1099511628211ULL;
}

template<std::size_t BITS>
constexpr fnv_val_type<BITS> fnv1_init();

template<>
constexpr fnv_val_type<32u> fnv1_init<32u>()
{   // taken from public domain C source code
    return 2166136261UL;
}

template<>
constexpr fnv_val_type<64u> fnv1_init<64u>()
{   // taken from public domain C source code
    return 14695981039346656037ULL;
}

//! overload taking iterators (including pointers) to elements that are single-byte types other than bool
//! note: for arrays of non-byte data, you can reinterpret_cast to pointer to char, unsigned char, or std::byte,
//!       but reinterpret_cast is not constexpr.
template<std::size_t BITS, class ITERATOR>
CUES_CONSTEXPR_AFTER_CPP14
typename std::enable_if<
       (sizeof(typename std::iterator_traits<ITERATOR>::value_type) == 1u)
    && (CHAR_BIT == 8u)
    && (!std::is_same<typename std::iterator_traits<ITERATOR>::value_type,bool>::value)
    , fnv_val_type<BITS>
>::type
fnv_1a(ITERATOR begin, ITERATOR end, fnv_val_type<BITS> previous)
{
    for (;
        begin != end;
        ++begin)
    {
        previous ^= static_cast< fnv_val_type<BITS> >(static_cast<unsigned char>(*begin));
        previous *= fnv_get_prime<BITS>();

    }
    return previous;
}

//! overload for first call, supplies appropriate "previous" value
template<std::size_t BITS, class ITERATOR>
CUES_CONSTEXPR_AFTER_CPP14
typename std::enable_if<
       (sizeof(typename std::iterator_traits<ITERATOR>::value_type) == 1u)
    && (CHAR_BIT == 8u)
    && (!std::is_same<typename std::iterator_traits<ITERATOR>::value_type,bool>::value)
    , fnv_val_type<BITS>
>::type
fnv_1a(ITERATOR begin, ITERATOR end)
{
    return fnv_1a<BITS, ITERATOR>(begin, end, fnv1_init<BITS>());
}

//! always constexpr version for compilers not supporting c++14 relaxed constexpr
template<std::size_t BITS, class ITERATOR>
constexpr
typename std::enable_if<
       (sizeof(typename std::iterator_traits<ITERATOR>::value_type) == 1u)
    && (CHAR_BIT == 8u)
    && (!std::is_same<typename std::iterator_traits<ITERATOR>::value_type,bool>::value)
    , fnv_val_type<BITS>
>::type
fnv_1a_consteval(ITERATOR begin, ITERATOR end, fnv_val_type<BITS> previous)
{
    #if __cplusplus >= 201402L
    return fnv_1a<BITS, ITERATOR>(begin, end, previous);
    #else
    return (begin == end) ?
    previous
    :
    fnv_1a_consteval<BITS, ITERATOR>(begin + 1, end,
        (previous ^ static_cast< fnv_val_type<BITS> >(static_cast<unsigned char>(*begin)))
        * fnv_get_prime<BITS>()
    );
    #endif
}

template<std::size_t BITS, class ITERATOR>
constexpr
typename std::enable_if<
       (sizeof(typename std::iterator_traits<ITERATOR>::value_type) == 1u)
    && (CHAR_BIT == 8u)
    && (!std::is_same<typename std::iterator_traits<ITERATOR>::value_type,bool>::value)
    , fnv_val_type<BITS>
>::type
fnv_1a_consteval(ITERATOR begin, ITERATOR end)
{
    #if __cplusplus >= 201402L
    return fnv_1a<BITS, ITERATOR>(begin, end, fnv1_init<BITS>());
    #else
    return fnv_1a_consteval<BITS,ITERATOR>(begin, end, fnv1_init<BITS>());
    #endif
}

template<std::size_t BITS, typename CHAR_TYPE>
CUES_CONSTEXPR_AFTER_CPP14
typename std::enable_if<
    (sizeof(CHAR_TYPE) == 1u)
    && (CHAR_BIT == 8u)
    && (!std::is_same<CHAR_TYPE,bool>::value),
    fnv_val_type<BITS>
>::type
fnv_1a(CHAR_TYPE const * data, fnv_val_type<BITS> previous)
{
    for (;
        *data != CHAR_TYPE{0};
        ++data
    ) {
        previous ^= static_cast< fnv_val_type<BITS> >(static_cast<unsigned char>(*data));
        previous *= fnv_get_prime<BITS>();
    }
    return previous;
}


template<std::size_t BITS, typename CHAR_TYPE>
CUES_CONSTEXPR_AFTER_CPP14
typename std::enable_if<
    (sizeof(CHAR_TYPE) == 1u)
    && (CHAR_BIT == 8u)
    && (!std::is_same<CHAR_TYPE,bool>::value),
    fnv_val_type<BITS>
>::type
fnv_1a(CHAR_TYPE const * data)
{
    return fnv_1a<BITS>(data, fnv1_init<BITS>());
}


//! convenience overload taking container of elements which are single-byte types other than bool.
//! this is handy in particular for temporary objects like a std::string_view made from a string literal.
template<std::size_t BITS, template<typename> class CONTAINER, class ELEMENT_TYPE>
CUES_CONSTEXPR_AFTER_CPP14
typename std::enable_if<
    (sizeof(ELEMENT_TYPE) == 1u)
    && (CHAR_BIT == 8u)
    && (!std::is_same<ELEMENT_TYPE,bool>::value),
    fnv_val_type<BITS>
>::type
fnv_1a(CONTAINER<ELEMENT_TYPE> const & vals, fnv_val_type<BITS> previous)
{
    return fnv_1a<BITS, typename CONTAINER<ELEMENT_TYPE>::const_iterator>(vals.cbegin(), vals.cend(), previous);
}

template<std::size_t BITS, template<typename> class CONTAINER, class ELEMENT_TYPE>
CUES_CONSTEXPR_AFTER_CPP14
typename std::enable_if<
    (sizeof(ELEMENT_TYPE) == 1u)
    && (CHAR_BIT == 8u)
    && (!std::is_same<ELEMENT_TYPE,bool>::value),
    fnv_val_type<BITS>
>::type
fnv_1a(CONTAINER<ELEMENT_TYPE> const & vals)
{
    return fnv_1a<BITS, typename CONTAINER<ELEMENT_TYPE>::const_iterator>(vals.cbegin(), vals.cend(), fnv1_init<BITS>());
}

// fnv1a is recommended to be used in place of fnv1


} // namespace cues

#endif // CUES_FUNCTIONAL_PD_HPP
