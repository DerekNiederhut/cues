#ifndef CUES_VARIADIC_TYPE_TRAITS_HPP_INCLUDED
#define CUES_VARIADIC_TYPE_TRAITS_HPP_INCLUDED

#include <type_traits>


namespace cues
{

// use standard types if available
#if __cplusplus >= 201703L
    using std::conjunction;
    using std::disjunction;
#else
// otherwise re-invent them.

// if zero elements, inherit from true
// this is necessary for the recursion; if all elements
// are true until 0 are left, the whole is true
template<class ...>
struct conjunction : public std::true_type {};

// if exactly one, inherit from that one
template<class T>
struct conjunction<T> : public T {};

// if more than one, and the first has a value of true, recurse on the rest.
// otherwise inherit the one with value of false
template<class T, class U, class ...OTHERS>
struct conjunction<T, U, OTHERS...>
    : public std::conditional<bool(T::value), conjunction<U, OTHERS...>, T>::type {};

// if zero, return false
// this is necessary for the recursion; if all are false until
// 0 are left, the whole is false
template<class ...>
struct disjunction : public std::false_type {};

// if exactly one, inherit from that one
template<class T>
struct disjunction<T> : public T {};

// if more than one, and the first one is true, inherit that one
// otherwise, recurse
template<class T, class U, class ...OTHERS>
struct disjunction<T, U, OTHERS...>
    : public std::conditional<bool(T::value), T, disjunction<U, OTHERS...>>::type {};

#endif // __cplusplus 17


template<class T, class ...OTHERS>
struct is_same_as_all : public std::integral_constant<bool, conjunction<std::is_same<T,OTHERS>...>::value> {};

template<class T, class...OTHERS>
struct is_same_as_any : public std::integral_constant<bool, disjunction<std::is_same<T,OTHERS>...>::value> {};

template<class T, class...OTHERS>
struct is_same_as_none : public std::integral_constant<bool, !disjunction<std::is_same<T,OTHERS>...>::value> {};

template<class T, class...OTHERS>
struct is_base_of_any : public std::integral_constant<bool, disjunction<std::is_base_of<T,OTHERS>...>::value> {};

template<class T, class...OTHERS>
struct any_is_base_of : public std::integral_constant<bool, disjunction<std::is_base_of<OTHERS,T>...>::value> {};

template<class T, class ...OTHERS>
struct is_constructible_from_each : public std::integral_constant<bool, conjunction<std::is_constructible<T,OTHERS>...>::value> {};

template<class T, class ...OTHERS>
struct each_is_constructible_from : public std::integral_constant<bool, conjunction<std::is_constructible<OTHERS,T>...>::value> {};

template<class T, class ...OTHERS>
struct is_nothrow_constructible_from_each : public std::integral_constant<bool, conjunction<std::is_nothrow_constructible<T,OTHERS>...>::value> {};

template<class T, class ...OTHERS>
struct each_is_nothrow_constructible_from : public std::integral_constant<bool, conjunction<std::is_nothrow_constructible<OTHERS,T>...>::value> {};


// unlike is_same_as_none, this checks not just the first type,
// but all types in the list
// base, matches zero parameters
template<class ... TYPE_SET>
struct all_are_unique : public std::true_type
{};

// specialization for one or more parameters
// one or more parameters
template<class FIRST, class ... OTHERS>
struct all_are_unique<FIRST, OTHERS...> : public std::integral_constant<
    bool,
    // this one isn't the same as any others
    !is_same_as_any<FIRST, OTHERS...>::value
    // recurse, removing this one from the list
    && all_are_unique<OTHERS...>::value
>
{};

//! alternative to std::tuple for type deduction of parameter packs
template<typename ... TYPES>
struct type_list
{
    static constexpr std::size_t parameter_count = sizeof...(TYPES);
};

template<typename ... T>
struct is_type_list : public std::false_type
{};

template<typename ... PARAMS>
struct is_type_list<type_list<PARAMS...>> : public std::true_type
{};

template<typename ... T>
struct is_nested_type_list : public std::false_type
{};

template<typename ... TYPES>
struct is_nested_type_list<type_list<TYPES...> > : public disjunction<is_type_list<TYPES>...>
{};


template<typename T>
struct type_or_typelist
{
    using type = typename std::conditional<
        is_type_list<T>::value,
        T,
        type_list<T>
    >;
};

//! base template : zero parameters or more than zero parameters
//! where the first parameter is not a type_list
template<typename ... PACK>
struct strip_type_list_and_concatenate
{
    using type = type_list<PACK...>;
};

//! specialization : first parameter is a type list, unpacks that type list
//! and sends the other types, followed by those types, on, possibly to
//! base template or to another specialization
template<typename ... FIRST_PACK, typename ... REMAINDER>
struct strip_type_list_and_concatenate<type_list<FIRST_PACK...>, REMAINDER...>
{
    using type = typename strip_type_list_and_concatenate<REMAINDER..., FIRST_PACK...>::type;
};

//! conditionally put a type_list over types that don't have one, and
//! forward that list to strip the type lists off and concatenate the types.
//! works to concatenate one level of type_list depth.
template<typename ... TYPES>
struct concatenate_type_list
{
    using type = typename strip_type_list_and_concatenate<
        typename type_or_typelist<TYPES>::type ...
    >::type;
};

template<typename TYPE, bool FINISHED >
struct flatten_type_list;

template<typename ... TYPES>
struct recursive_concatenate_type_list
{};

template<typename TYPE>
struct flatten_type_list<TYPE, true>
{
    using type = TYPE;
};

template<typename TYPE>
struct flatten_type_list< TYPE, false>
{
    using type = typename recursive_concatenate_type_list<
        typename strip_type_list_and_concatenate<TYPE>::type
    >::type;
};

template<typename SINGLE>
struct recursive_concatenate_type_list<SINGLE>
{
    using type = typename flatten_type_list<
        SINGLE,
        !is_nested_type_list<SINGLE>::value
    >::type;
};

//! will concatenate multiple levels of type list
template<typename FIRST, typename SECOND, typename ... TYPES>
struct recursive_concatenate_type_list<FIRST, SECOND, TYPES...>
{
    private:
    using intermediate_type = typename std::conditional<
        conjunction<is_type_list<FIRST>, is_type_list<SECOND>, is_type_list<TYPES>...>::value,
        typename strip_type_list_and_concatenate<FIRST, SECOND, TYPES...>::type,
        typename concatenate_type_list<FIRST, SECOND, TYPES...>::type
    >::type;

    public:
    using type = typename flatten_type_list<
        intermediate_type,
        !is_nested_type_list<intermediate_type>::value
    >::type;
};


//! like std::tuple_element, but does not require declaring a tuple type
//! just to get the type at a certain point, and thus does not require
//! including the <tuple> header just for that, unless you don't already
//! have the parameters as a pack
// default case: zero types remaining
template<std::size_t N, typename ... TYPES>
struct nth_type
{
    static_assert(N < sizeof...(TYPES), "not enough types in list for the requested spot.");
};

// special case: at least one type remains: recurse
template<std::size_t N, typename FIRST, typename ... TYPES>
struct nth_type<N, FIRST, TYPES...>
{
    using type = typename nth_type<N - 1u, TYPES...>::type;
};

// specialest case: at the index, at least one type is here
template<typename FIRST, typename ... TYPES>
struct nth_type<0, FIRST, TYPES...>
{
    using type = FIRST;
};

// 0 or more
template<typename NEEDLE, typename ... HAYSTACK>
struct first_match_index;

// exactly zero
template<typename NEEDLE>
struct first_match_index<NEEDLE> : public std::integral_constant<
    std::size_t,
    // do not recurse, nothing more to check;
    // for size 0, "0" can be interpreted as "one past the end" and fits the
    // more general size N, index N is "one past the end"
    0u
>
{};

// one or more
template<typename NEEDLE, typename HAYSTACK_FIRST, typename ... HAYSTACK_REMAINING>
struct first_match_index<NEEDLE, HAYSTACK_FIRST, HAYSTACK_REMAINING...> : public std::integral_constant<
    std::size_t,
    std::is_same<NEEDLE, HAYSTACK_FIRST>::value ? 0u : 1u + first_match_index<NEEDLE, HAYSTACK_REMAINING...>::value
>
{};

// 0 or more
template<template<typename...> class TRAIT, typename NEEDLE, typename ... HAYSTACK>
struct first_trait_match_index;

// exactly zero
template<template<typename...> class TRAIT, typename NEEDLE>
struct first_trait_match_index<TRAIT, NEEDLE> : public std::integral_constant<
    std::size_t,
    0
>
{};

// one or more
template<template<typename...> class TRAIT, typename NEEDLE, typename HAYSTACK_FIRST, typename ... HAYSTACK_REMAINING>
struct first_trait_match_index<TRAIT, NEEDLE, HAYSTACK_FIRST, HAYSTACK_REMAINING...> : public std::integral_constant<
    std::size_t,
    TRAIT<NEEDLE, HAYSTACK_FIRST>::value ? 0u : 1u + first_trait_match_index<TRAIT, NEEDLE, HAYSTACK_REMAINING...>::value
>
{};

}

#endif // CUES_VARIADIC_TYPE_TRAITS_HPP_INCLUDED
