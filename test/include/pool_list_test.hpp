#ifndef POOL_LIST_TEST_HPP_INCLUDED
#define POOL_LIST_TEST_HPP_INCLUDED

#include <cstddef>

namespace cues
{
namespace test
{
std::size_t check_pool_list_test_runtime_assertions();
}
}

#endif // POOL_LIST_TEST_HPP_INCLUDED
