#ifndef CUES_ACCESS_TOKEN_HPP
#define CUES_ACCESS_TOKEN_HPP

#include <cstddef>
#include <type_traits>

#include "cues/new.hpp"

namespace cues {


//! access tokens are intended to be used by "container-like" classes (ACCESSEE)
//! that want to provide some limited access to a contained object (ACCESSED).
//! They have a couple advantages over raw pointers:
//! 1) the move constructor is public but the copy constructor is private, which more clearly indicates
//!    that access is not meant to be shared (but transfer may be ok).  For containers which also take
//!    an (rvalue reference to) access token as a parameter, this can be used to indicate "giving up"
//!    control of the access back to the container.
//! 2) they dereference to the reference of the ACCESSEE, not to std::add_reference<ACCESSED>.  This in turn
//!    means they can be used with containers whose reference type is a proxy object, like cues::array_proxy.
//! 3) it does not provide increment or decrement overloads like iterators, which is useful for containers
//!    which are not designed to be iterated.
template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS = true>
struct access_token_impl
{
    public:

    friend ACCESSEE;

    friend class access_token_impl<ACCESSEE, ACCESSED, !IS_CONST_ACCESS>;

    using reference = typename std::conditional<
        IS_CONST_ACCESS,
        typename ACCESSEE::const_reference,
        typename ACCESSEE::reference
    >::type;

    using pointer = typename std::conditional<
        IS_CONST_ACCESS,
        typename std::add_pointer<typename std::add_const<typename std::remove_all_extents<ACCESSED>::type>::type>::type,
        typename std::add_pointer<typename std::remove_all_extents<ACCESSED>::type>::type
    >::type;

    explicit constexpr access_token_impl(std::nullptr_t) noexcept
    : data(nullptr)
    {}

    // copying an access token is not necessary and could
    // result in accidentally trying to re-use the token.
    //! access tokens are not atomic and should not be shared across context boundaries.
    access_token_impl(access_token_impl &&) = default;
    access_token_impl & operator=(access_token_impl &&) = default;

    explicit constexpr operator bool () const noexcept {return has_value();}
    constexpr bool has_value() const noexcept {return (data != nullptr);}

    //! PRECONDITION: has_value()
    explicit constexpr operator reference () {return this->operator*();}
    constexpr reference operator*()
    {
        if constexpr (std::is_array<ACCESSED>::value)
        {
            using extent_type = typename reference::proxy_extents;
            return reference(data, extent_type{});
        }
        else
        {
            return *as_pointer();
        }
    }

    explicit constexpr operator pointer () const
    {
        return as_pointer();
    }

    //! NOTE: you still need to use an r-value to construct
    //! a const token from a non-const token to avoid copying
    //! the token.
    template<bool ENABLE_CONST_CONVERSION = IS_CONST_ACCESS>
    constexpr access_token_impl(
        typename std::enable_if<
            ENABLE_CONST_CONVERSION,
            access_token_impl<ACCESSEE, typename std::remove_const<ACCESSED>::type, false>
        >::type && other
    ) noexcept
    : data(other.data)
    {}

    private:

        using internal_pointer_type = typename std::add_pointer<
            typename std::conditional<IS_CONST_ACCESS,
                cues::raw_byte_type const,
                cues::raw_byte_type
            >::type
        >::type;

        using accessed_pointer_type = typename std::add_pointer<
            typename std::conditional<IS_CONST_ACCESS,
                ACCESSED const,
                ACCESSED
            >::type
        >::type;
        pointer as_pointer() const noexcept
        {
            return std::launder(reinterpret_cast<pointer>(data));
        }


        access_token_impl() : data{nullptr} {}
        access_token_impl(internal_pointer_type ptr) : data{ptr} {}
        access_token_impl(access_token_impl const &) = default;
        access_token_impl & operator=(access_token_impl const &) = default;


        internal_pointer_type data;
};

template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS>
bool operator!=(access_token_impl<ACCESSEE, ACCESSED, IS_CONST_ACCESS> const & lhs, std::nullptr_t )
{
    return lhs.has_value();
}

template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS>
bool operator!=(std::nullptr_t , access_token_impl<ACCESSEE, ACCESSED, IS_CONST_ACCESS> const & rhs )
{
    return (rhs != nullptr);
}

template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS>
bool operator==(access_token_impl<ACCESSEE, ACCESSED, IS_CONST_ACCESS> const & lhs, std::nullptr_t )
{
    return !(lhs != nullptr);
}

template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS>
bool operator==(std::nullptr_t, access_token_impl<ACCESSEE, ACCESSED, IS_CONST_ACCESS> const & rhs  )
{
    return !(rhs != nullptr);
}


template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS = true>
class unpublished_placement_access_token
{
    public:

    friend ACCESSEE;
    using nonowning_access_token = access_token_impl<ACCESSEE, ACCESSED, IS_CONST_ACCESS>;
    using pointer                = typename nonowning_access_token::pointer;
    using reference              = typename nonowning_access_token::reference;


    unpublished_placement_access_token(unpublished_placement_access_token && other) noexcept
    : token(std::move(other.token))
    {
        other.token = nonowning_access_token(nullptr);
    }

    unpublished_placement_access_token & operator=(unpublished_placement_access_token && other) noexcept
    {
        maybe_destruct_pointee();
        this->token = std::move(other.token);
        other.token = nonowning_access_token(nullptr);
        return *this;
    }

    ~unpublished_placement_access_token()
    {
        maybe_destruct_pointee();
    }

    explicit constexpr operator bool () const noexcept {return token.has_value();}
    constexpr bool has_value() const noexcept {return token.has_value();}

    //! PRECONDITION: has_value()
    explicit constexpr operator reference () {return this->operator*();}
    constexpr reference operator*()
    {
        return *(this->token);
    }

    explicit constexpr operator pointer () const
    {
        return static_cast<pointer>(this->token);
    }


    private:

        unpublished_placement_access_token(nonowning_access_token && src)
        : token(std::move(src))
        {}
        // does not call destructor
        void rebind(nonowning_access_token && src) noexcept
        {
            this->token = std::move(src);
        }

        void maybe_destruct_pointee()
        {
            if (this->has_value())
            {
                flattened_destruct_backward_from<ACCESSED>(
                    reinterpret_cast<cues::raw_byte_type *>(static_cast<pointer>(this->token)),
                    extent_sequence<ACCESSED>::product() - 1
                );
            }
        }

        nonowning_access_token token;

};

template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS>
bool operator!=(unpublished_placement_access_token<ACCESSEE, ACCESSED, IS_CONST_ACCESS> const & lhs, std::nullptr_t )
{
    return lhs.has_value();
}

template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS>
bool operator!=(std::nullptr_t , unpublished_placement_access_token<ACCESSEE, ACCESSED, IS_CONST_ACCESS> const & rhs )
{
    return rhs != nullptr;
}

template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS>
bool operator==(unpublished_placement_access_token<ACCESSEE, ACCESSED, IS_CONST_ACCESS> const & lhs, std::nullptr_t )
{
    return !(lhs != nullptr);
}

template<class ACCESSEE, class ACCESSED, bool IS_CONST_ACCESS>
bool operator==(std::nullptr_t, unpublished_placement_access_token<ACCESSEE, ACCESSED, IS_CONST_ACCESS> const & rhs  )
{
    return !(rhs != nullptr);
}

} // namespace cues

#endif // CUES_ACCESS_TOKEN_HPP
