#ifndef CUES_TEST_COMMON_BUFFER_FUNCTIONS_HPP_INCLUDED
#define CUES_TEST_COMMON_BUFFER_FUNCTIONS_HPP_INCLUDED

#include <type_traits>
#include "cues/type_traits.hpp"
#include "helper_types.hpp"

#include <vector>
#include <new>

namespace cues
{
namespace test
{



template<typename T, T> struct validate_type;

template<typename T, typename ITERATOR_TYPE, class Enable = void>
class has_consume_n : public std::false_type {};

template<typename T, typename ITERATOR_TYPE>
class has_consume_n<T, ITERATOR_TYPE,
    cues::void_t<
        typename std::enable_if<
            std::is_same<
                typename std::remove_reference<
                    decltype(std::declval<T>().consume_n(std::declval<ITERATOR_TYPE>(), std::declval<typename T::size_type>()))
                >::type,
                typename T::size_type
            >::value
        >::type
    >
>
: public std::true_type
{};

template<typename T, typename ITERATOR_TYPE, class enable = void>
class has_push_back_n : public std::false_type {};

template<typename T, typename ITERATOR_TYPE>
class has_push_back_n<T, ITERATOR_TYPE,
    cues::void_t<
        typename std::enable_if<
            std::is_same<
                typename std::remove_reference<
                    decltype(std::declval<T>().push_back_n(std::declval<ITERATOR_TYPE>(), std::declval<ITERATOR_TYPE>()))
                >::type,
                ITERATOR_TYPE
            >::value
        >::type
    >
>
: public std::true_type
{};


template<typename T, class Return_Type, typename... Args>
struct has_emplace_back
{
    private:

    using success_t = unsigned char;
    // one simple struct to avoid needing to include <array>
    struct failure_t
    {
        unsigned char impl[2];
    };


    template<typename U>
    static constexpr success_t has_fn(validate_type<
            typename pointer_to_member_function<U,
                Return_Type,
                false,
                Args...
            >::type,
            static_cast<Return_Type (U::*)(Args ...) noexcept(false)>(&U::template emplace_back)
          > *
    );

    template<typename U>
    static constexpr success_t has_fn(validate_type<
            typename pointer_to_member_function<U,
                Return_Type,
                true,
                Args...
            >::type,
            static_cast<Return_Type (U::*)(Args ...) noexcept(true)>(&U::template emplace_back)
          > *
    );

    template<typename U>
    static constexpr failure_t has_fn(...);

public:
    static constexpr bool const value = sizeof((has_fn<T>(nullptr))) == sizeof(success_t);
};

template<typename T, class Return_Type, typename... Args>
struct has_try_emplace_back
{
    private:

    using success_t = unsigned char;
    // one simple struct to avoid needing to include <array>
    struct failure_t
    {
        unsigned char impl[2];
    };


    template<typename U >
    static constexpr success_t has_fn(
          validate_type<
            typename pointer_to_member_function<U,
                Return_Type,
                false,
                Args...
            >::type,
            static_cast<Return_Type (U::*)(Args ...) noexcept(false)>(&U::template try_emplace_back)
          > *
    );

    template<typename U >
    static constexpr success_t has_fn(
          validate_type<
            typename pointer_to_member_function<U,
                Return_Type,
                true,
                Args...
            >::type,
            static_cast<Return_Type (U::*)(Args ...) noexcept(true)>(&U::template try_emplace_back)
          > *
    );

    template<typename U>
    static constexpr failure_t has_fn(...);

public:
    static constexpr bool const value = sizeof(has_fn<T>(nullptr)) == sizeof(success_t);
};

template<typename T, class Return_Type, typename... Args>
struct has_construct_data
{
    private:

    using success_t = unsigned char;
    // one simple struct to avoid needing to include <array>
    struct failure_t
    {
        unsigned char impl[2];
    };

    template<typename U>
    static constexpr success_t has_fn(
        validate_type<
            typename pointer_to_member_function<U,
                Return_Type,
                false,
                Args...
            >::type,
            static_cast<Return_Type (U::*)(Args ...) noexcept(false)>(&U::template construct_data)
          > *
    );

    template<typename U>
    static constexpr success_t has_fn(
        validate_type<
            typename pointer_to_member_function<U,
                Return_Type,
                true,
                Args...
            >::type,
            static_cast<Return_Type (U::*)(Args ...) noexcept(true)>(&U::template construct_data)
          > *
    );

    template<typename U>
    static constexpr failure_t has_fn(...);

public:
    static constexpr bool const value = sizeof(has_fn<T>(nullptr)) == sizeof(success_t);
};

template<typename T, class Return_Type, typename... Args>
class has_try_construct_data
{
    private:

    using success_t = unsigned char;
    // one simple struct to avoid needing to include <array>
    struct failure_t
    {
        unsigned char impl[2];
    };


    template<typename U>
    static constexpr success_t has_fn(
       validate_type<
            typename pointer_to_member_function<U,
                Return_Type,
                false,
                Args...
            >::type,
            static_cast<Return_Type (U::*)(Args ...) noexcept(false)>(&U::template try_construct_data)
          > *
    );

    template<typename U>
    static constexpr success_t has_fn(
       validate_type<
            typename pointer_to_member_function<U,
                Return_Type,
                true,
                Args...
            >::type,
            static_cast<Return_Type (U::*)(Args ...) noexcept(true)>(&U::template try_construct_data)
          > *
    );

    template<typename U>
    static constexpr failure_t has_fn(...);

public:
    static constexpr bool const value = sizeof(has_fn<T>(nullptr)) == sizeof(success_t);
};

enum class container_consumption_method
{
    return_by_value,
    pass_reference,
    peek_and_consume,
    pass_iterator,
    discard
};

enum class container_production_method
{
    emplace_rvalue,
    emplace_lvalue,
    emplace_args,
    publish_default,
    pass_iterator
};

}
}

#include <iostream>
#include <iomanip>


namespace cues
{
namespace test
{


template< class container_type>
void counted_consumer(container_type & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period)
{
    using value_type = typename container_type::value_type;
    using size_type  = typename container_type::size_type;
    static_assert(std::is_base_of<cues::test::counted<counted_counter_type>, value_type>::value, "counted consumer relies on having counter<counted_counter_type> as value_type");

    constexpr static bool const construct_with_no_random_failure = std::is_base_of<randomly_fails_construction, typename std::remove_all_extents<value_type>::type>::value;

    constexpr static bool const use_consume_n = has_consume_n<container_type, value_type *>::value;
    container_consumption_method current_method = container_consumption_method::return_by_value;

    counted_counter_type volatile prev_cs;
    counted_counter_type volatile current_cs;
    counted_counter_type volatile prev_ds;
    counted_counter_type volatile current_ds;
    counted_counter_type volatile expected_constructions = 0;
    counted_counter_type volatile expected_destructions = 0;

    for (counted_counter_type counter = 0;
    counter < 10000 && std::atomic_load_explicit(&running_flag, std::memory_order_acquire);
    ++counter)
    {
        expected_constructions = 0;
        expected_destructions = 0;

        if (!buffer.empty())
        {
            switch(current_method)
            {
                case container_consumption_method::return_by_value:
                {
                    prev_ds = counted<counted_counter_type>::destructed_count();
                    prev_cs = counted<counted_counter_type>::total_constructed_count();

                    try
                    {
                        value_type temp {buffer.consume()};

                        expected_constructions += 1; // for temp
                        expected_destructions += 2; // temp and container element

                    }
                    // normally, catching and not handling is bad practice.
                    // However, for the purpose of testing the container's
                    // behavior when a constructor throws, and using the
                    // cues::test::random_test_failure_exception specifically
                    // written to randomly simulate exceptions in constructors
                    // when used with for example
                    // cues::test::counted_then_random_failure, the logging
                    // in cues::test::counted is already taking care of the handling we
                    // would want to do here, and we're testing that the container
                    // takes care of the rest of the handling.
                    catch(cues::test::random_test_failure_exception const &)
                    {}

                    current_ds = counted<counted_counter_type>::destructed_count();
                    current_cs = counted<counted_counter_type>::total_constructed_count();

                    assert(((current_cs - prev_cs >= expected_constructions) && "unexpected construction amount"));
                    assert(((current_ds - prev_ds >= expected_destructions) && "unexpected destruction amount"));

                }
                current_method = container_consumption_method::pass_reference;
                break;

                case container_consumption_method::pass_reference:
                {
                    prev_ds = counted<counted_counter_type>::destructed_count();
                    prev_cs = counted<counted_counter_type>::default_constructed_count();
                    try
                    {
                        if constexpr (construct_with_no_random_failure)
                        {
                            value_type temp{no_random_failure_tag{}};
                            expected_constructions +=1;
                            expected_destructions +=1;
                            buffer.consume(temp);
                            // for object within container.
                            // assignment likely to be used instead of construction here.
                            expected_destructions +=1;
                        }
                        else
                        {
                            value_type temp{};
                            expected_constructions +=1;
                            expected_destructions +=1;
                            buffer.consume(temp);
                            expected_destructions +=1;
                        }
                    }
                    catch(cues::test::random_test_failure_exception const &)
                    {}

                    current_ds = counted<counted_counter_type>::destructed_count();
                    current_cs = counted<counted_counter_type>::default_constructed_count();

                    assert(((current_cs - prev_cs >= expected_constructions) && "unexpected construction amount"));
                    assert(((current_ds - prev_ds >= expected_destructions) && "unexpected destruction amount"));

                }
                current_method = container_consumption_method::peek_and_consume;
                break;

                case container_consumption_method::peek_and_consume:
                {
                    prev_ds = counted<counted_counter_type>::destructed_count();
                    try
                    {
                        auto token = buffer.peek_front();
                        expected_destructions += (token != nullptr);
                        buffer.mark_consumed(std::move(token));
                    }
                    catch(cues::test::random_test_failure_exception const &)
                    {}
                    current_ds = counted<counted_counter_type>::destructed_count();
                    assert(((current_ds - prev_ds >= expected_destructions) && "unexpected destruction amount"));
                }
                current_method = container_consumption_method::discard;
                break;

                case container_consumption_method::discard:
                {
                    prev_ds = counted<counted_counter_type>::destructed_count();
                    try
                    {
                        buffer.pop_front();
                        expected_destructions += 1;
                    }
                    catch(cues::test::random_test_failure_exception const &)
                    {}

                    current_ds = counted<counted_counter_type>::destructed_count();
                    assert(((current_ds - prev_ds >= expected_destructions) && "unexpected destruction amount"));

                    if constexpr (use_consume_n)
                    {
                        current_method = container_consumption_method::pass_iterator;
                    }
                    else
                    {
                        current_method = container_consumption_method::return_by_value;
                    }
                }
                break;

                case container_consumption_method::pass_iterator:
                {
                    assert(use_consume_n);

                    prev_ds = counted<counted_counter_type>::destructed_count();
                    prev_cs = counted<counted_counter_type>::default_constructed_count();

                    if constexpr (use_consume_n)
                    {
                        try
                        {
                            if constexpr (construct_with_no_random_failure)
                            {
                                std::size_t index = 0;
                                alignas (value_type) cues::raw_byte_type raw [4*sizeof(value_type)];
                                try
                                {
                                    counted_counter_type volatile temp_count_a;
                                    counted_counter_type volatile temp_count_b;
                                    counted_counter_type volatile temp_count_c = counted<counted_counter_type>::default_constructed_count();

                                    for (;
                                         index < sizeof(raw)/sizeof(value_type);
                                         ++index
                                    )
                                    {
                                        temp_count_a = counted<counted_counter_type>::default_constructed_count();
                                        new (&(raw[(index)*sizeof(value_type)])) value_type{no_random_failure_tag{}};
                                        temp_count_b = counted<counted_counter_type>::default_constructed_count();
                                        assert((((temp_count_b - temp_count_a) >= 1) && "did not construct as expected"));
                                    }
                                    assert((index == sizeof(raw)/sizeof(value_type) && index == 4));
                                    assert((((temp_count_b - temp_count_c) >= index) && "total ctors unexpectedly small"));

                                    expected_constructions += sizeof(raw)/sizeof(value_type);
                                    expected_destructions  += sizeof(raw)/sizeof(value_type);

                                    temp_count_c = counted<counted_counter_type>::destructed_count();
                                    expected_destructions  += buffer.consume_n(
                                         std::launder(reinterpret_cast<value_type *>(&(raw[0]))),
                                         size_type{sizeof(raw)/sizeof(value_type)}
                                    );
                                    temp_count_b = counted<counted_counter_type>::destructed_count();
                                    assert((((temp_count_b - temp_count_c) >= (expected_destructions - expected_destructions)) && "dtor count unexpectedly small."));

                                    for (;
                                        index > 0;
                                        std::launder(reinterpret_cast<value_type *>(&(raw[(--index)*sizeof(value_type)])))->~value_type()
                                    )
                                    {}

                                }
                                catch (...)
                                {
                                    for (;
                                        index > 0;
                                        std::launder(reinterpret_cast<value_type *>(&(raw[(--index)*sizeof(value_type)])))->~value_type()
                                    )
                                    {}

                                    throw;
                                }

                            }
                            else
                            {
                                 value_type temp[4] {{},{},{},{}};

                                 expected_constructions += sizeof(temp)/sizeof(temp[0]);
                                 expected_destructions  += sizeof(temp)/sizeof(temp[0]);

                                 expected_destructions  += buffer.consume_n(&temp[0], size_type{sizeof(temp)/sizeof(temp[0])});
                            }


                            current_method = container_consumption_method::return_by_value;
                        }
                        catch(cues::test::random_test_failure_exception const &)
                        {}

                    }
                    current_cs = counted<counted_counter_type>::default_constructed_count();
                    current_ds = counted<counted_counter_type>::destructed_count();
                    assert(((current_cs - prev_cs >= expected_constructions) && "unexpected construction amount"));
                    assert(((current_ds - prev_ds >= expected_destructions) && "unexpected destruction amount"));
                }
                break;
            }

        }
        std::this_thread::sleep_for(period);
    }
}



template< class container_type, class ... emplace_constructor_args >
void counted_producer(container_type & buffer, std::atomic<bool> const & running_flag, std::chrono::milliseconds period)
{
    using value_type = typename container_type::value_type;

    constexpr static bool const construct_with_no_random_failure = std::is_base_of<randomly_fails_construction, typename std::remove_all_extents<value_type>::type>::value;

    constexpr static bool const use_push_back_n        = has_push_back_n<container_type, value_type const *>::value;
    constexpr static bool const use_emplace_back       = has_emplace_back<container_type, void, value_type &&>::value;
    constexpr static bool const use_try_emplace_back   = has_try_emplace_back<container_type, bool, value_type &&>::value;
    constexpr static bool const use_construct_data     = has_construct_data<container_type, typename container_type::access_token>::value;
    constexpr static bool const use_try_construct_data = has_try_construct_data<container_type, typename container_type::access_token>::value;
    constexpr static bool const use_emplacement_args   = sizeof...(emplace_constructor_args) > 0;

    static_assert(std::is_base_of<cues::test::counted<counted_counter_type>, value_type>::value, "counted producer relies on having counter<counted_counter_type> as value_type");

    counted_counter_type volatile prev_cs;
    counted_counter_type volatile current_cs;
    counted_counter_type volatile prev_ds;
    counted_counter_type volatile current_ds;

    counted_counter_type expected_constructions = 0;
    counted_counter_type expected_destructions  = 0;

    container_production_method current_method = container_production_method::emplace_rvalue;
    for (counted_counter_type counter = 0;
    counter < 1000 && std::atomic_load_explicit(&running_flag, std::memory_order_acquire);
    ++counter)
    {
        expected_constructions = 0;
        expected_destructions = 0;

        switch(current_method)
        {
            case container_production_method::emplace_rvalue:
            {
                prev_ds = counted<counted_counter_type>::destructed_count();
                prev_cs = counted<counted_counter_type>::default_constructed_count();

                try
                {
                    if constexpr(construct_with_no_random_failure)
                    {
                        if constexpr (use_emplace_back)
                        {
                            buffer.emplace_back(value_type{no_random_failure_tag{}});
                            ++expected_constructions;
                        }
                        else
                        {
                            expected_constructions += static_cast<counted_counter_type>(buffer.try_emplace_back(value_type{no_random_failure_tag{}}));
                        }
                    }
                    else
                    {
                        if constexpr (use_emplace_back)
                        {
                            buffer.emplace_back(value_type{});
                            ++expected_constructions;
                        }
                        else
                        {
                            expected_constructions += static_cast<counted_counter_type>(buffer.try_emplace_back(value_type{}));
                        }
                    }

                }
                catch(cues::test::random_test_failure_exception const &)
                {}

                current_ds = counted<counted_counter_type>::destructed_count();
                current_cs = counted<counted_counter_type>::default_constructed_count();
                assert(((current_cs - prev_cs >= expected_constructions) && "unexpected construction amount"));
                // any moves/copies may be elided here
                assert(((current_ds >= prev_ds) && "unexpected destruction amount"));
            }
            current_method = container_production_method::emplace_lvalue;
            break;

            case container_production_method::emplace_lvalue:
            {
                prev_ds = counted<counted_counter_type>::destructed_count();
                prev_cs = counted<counted_counter_type>::default_copy_move_constructed_count();

                try
                {
                    if constexpr(construct_with_no_random_failure)
                    {
                        value_type const temp{no_random_failure_tag{}};
                        ++expected_constructions;
                        ++expected_destructions;
                        if constexpr (use_emplace_back)
                        {
                            buffer.emplace_back(static_cast<value_type const &>(temp));
                            ++expected_constructions;
                        }
                        else if constexpr (use_try_emplace_back)
                        {
                            expected_constructions += static_cast<counted_counter_type>(buffer.try_emplace_back(static_cast<value_type const &>(temp)));
                        }
                    }
                    else
                    {
                        value_type const temp{};
                        ++expected_constructions;
                        ++expected_destructions;
                        if constexpr (use_emplace_back)
                        {
                            buffer.emplace_back(static_cast<value_type const &>(temp));
                            ++expected_constructions;
                        }
                        else if constexpr (use_try_emplace_back)
                        {
                            expected_constructions += static_cast<counted_counter_type>(buffer.try_emplace_back(static_cast<value_type const &>(temp)));
                        }
                    }

                }
                catch(cues::test::random_test_failure_exception const &)
                {}

                current_ds = counted<counted_counter_type>::destructed_count();
                current_cs = counted<counted_counter_type>::default_copy_move_constructed_count();
                assert(((current_cs - prev_cs >= expected_constructions) && "unexpected construction amount"));
                assert(((current_ds - prev_ds >= 1) && "unexpected destruction amount"));

            }
            current_method = use_emplacement_args ? container_production_method::emplace_args : container_production_method::publish_default;
            break;

            case container_production_method::emplace_args:
            {
                assert(use_emplacement_args);
                prev_ds = counted<counted_counter_type>::destructed_count();
                prev_cs = counted<counted_counter_type>::in_place_constructed_count();

                if constexpr (use_emplacement_args)
                {
                    try
                    {
                        if constexpr (use_emplace_back)
                        {
                            buffer.emplace_back(emplace_constructor_args()...);
                            ++expected_constructions;
                        }
                        else if constexpr (use_try_emplace_back)
                        {
                            expected_constructions += static_cast<counted_counter_type>(buffer.try_emplace_back(emplace_constructor_args()...));
                        }
                    }
                    catch(cues::test::random_test_failure_exception const &)
                    {}

                    current_ds = counted<counted_counter_type>::destructed_count();
                    current_cs = counted<counted_counter_type>::in_place_constructed_count();
                    assert(((current_ds >= prev_ds) && "unexpected destruction amount"));
                    assert(((current_cs - prev_cs >= expected_constructions) && "unexpected construction amount"));

                }
            }

            current_method = container_production_method::publish_default;
            break;

            case container_production_method::publish_default:
            {
                prev_ds = counted<counted_counter_type>::destructed_count();
                prev_cs = counted<counted_counter_type>::default_constructed_count();

                try
                {
                    if constexpr (use_construct_data)
                    {
                        auto token = buffer.construct_data();
                        expected_constructions += static_cast<counted_counter_type>(nullptr != token);
                        expected_destructions += static_cast<counted_counter_type>(nullptr != token);
                        buffer.roll_back(std::move(token));
                        token = buffer.construct_data();
                        expected_constructions += static_cast<counted_counter_type>(nullptr != token);
                        buffer.commit(std::move(token));
                    }
                    else if constexpr (use_try_construct_data)
                    {
                        auto token = buffer.try_construct_data();
                        expected_constructions += static_cast<counted_counter_type>(nullptr != token);
                        expected_destructions += static_cast<counted_counter_type>(nullptr != token);
                        buffer.roll_back(std::move(token));
                        token = buffer.try_construct_data();
                        expected_constructions += static_cast<counted_counter_type>(nullptr != token);
                        buffer.commit(std::move(token));
                    }
                }
                catch(cues::test::random_test_failure_exception const &)
                {}

                current_ds = counted<counted_counter_type>::destructed_count();
                current_cs = counted<counted_counter_type>::default_constructed_count();
                assert(((current_cs - prev_cs >= expected_constructions) && "unexpected construction amount"));
                assert(((current_ds - prev_ds >= expected_destructions)  && "unexpected destruction amount"));

                if constexpr (use_push_back_n)
                {
                    current_method = container_production_method::pass_iterator;
                }
                else
                {
                    current_method = container_production_method::emplace_rvalue;
                }
            }
            break;

            case container_production_method::pass_iterator:
            {
                assert(use_push_back_n);
                if constexpr (use_push_back_n)
                {
                    prev_ds = counted<counted_counter_type>::destructed_count();
                    prev_cs = counted<counted_counter_type>::default_copy_move_constructed_count();

                    try
                    {

                        if constexpr(construct_with_no_random_failure)
                        {

                            std::size_t index = 0;
                            alignas (value_type) cues::raw_byte_type raw [4*sizeof(value_type)];
                            try
                            {

                                for (;
                                    index < 4;
                                    ++index
                                )
                                {
                                    new (&(raw[(index)*sizeof(value_type)])) value_type{no_random_failure_tag{}};
                                }

                                expected_constructions += index;
                                expected_destructions = expected_constructions;

                                auto push_begin_point = std::launder(reinterpret_cast<value_type const *>(&(raw[0])));

                                auto push_finish_point = buffer.push_back_n(
                                    push_begin_point,
                                    std::launder(reinterpret_cast<value_type const *>(&(raw[index*sizeof(value_type)])))
                                );

                                expected_constructions += (push_finish_point - push_begin_point);

                                for (;
                                    index > 0;
                                    std::launder(reinterpret_cast<value_type *>(&(raw[(--index)*sizeof(value_type)])))->~value_type()
                                )
                                {}

                            }
                            catch (...)
                            {
                                for (;
                                    index > 0;
                                    std::launder(reinterpret_cast<value_type *>(&(raw[(--index)*sizeof(value_type)])))->~value_type()
                                )
                                {}
                                throw;
                            }

                        }
                        else
                        {
                            // this line causes problems with constructor/destructor count on gcc 9.
                            // my understanding is that the array is copy-initialized from the temporary default-constructed
                            // objects.  Copy elision may happen, in which case the value should be default-constructed in place,
                            // or a temporary could be default-constructed, then copy constructed and destructed.
                            // somehow the destructor is being called when there have not already been more constructor calls
                            // than destructor calls.
                            value_type temp[4] {{},{},{},{}};
                            expected_constructions += sizeof(temp)/sizeof(temp[0]);
                            expected_destructions += sizeof(temp)/sizeof(temp[0]);
                            auto push_begin_point = std::cbegin(temp);
                            auto push_finish_point = buffer.push_back_n(push_begin_point, std::cend(temp));

                            expected_constructions += (push_finish_point - push_begin_point);

                        }

                    }
                    catch(cues::test::random_test_failure_exception const &)
                    {}

                    current_method = container_production_method::emplace_rvalue;

                    current_ds = counted<counted_counter_type>::destructed_count();
                    current_cs = counted<counted_counter_type>::default_copy_move_constructed_count();
                    assert(((current_ds - prev_ds >= expected_destructions) && "unexpected destruction amount"));
                    assert(((current_cs - prev_cs >= expected_constructions) && "unexpected construction amount"));
                }
            }
            break;
        }

        std::this_thread::sleep_for(period);
    }
}

}
}

namespace cues
{
namespace test
{

template< class container_type, class ... emplace_constructor_args>
bool container_leak_test_failures(std::chrono::milliseconds producer_period, std::chrono::milliseconds consumer_period)
{
    using test_value_type = typename container_type::value_type;
    counted_counter_type previous_default_throws = 0;
    counted_counter_type previous_copy_throws    = 0;
    // anonymous scope so that container destructs remaining data before assertion check
    {
        container_type container {};
        std::atomic<bool> keep_running = true;


        if constexpr (std::is_base_of<randomly_fails_construction, test_value_type>::value)
        {
            previous_default_throws = test_value_type::default_constructor_thrown_count();
            previous_copy_throws    = test_value_type::copy_constructor_thrown_count();
        }

        std::thread consumer_thread(
            counted_consumer<container_type>,
            std::ref(container),
            std::ref(keep_running),
            consumer_period
        );

        std::thread producer_thread(
             counted_producer<container_type, emplace_constructor_args...>,
             std::ref(container),
             std::ref(keep_running),
             producer_period
        );

        producer_thread.join();
        atomic_store_explicit(&keep_running, false, std::memory_order_release);
        consumer_thread.join();
    }

    assert((!cues::test::counted<counted_counter_type>::constructor_destructor_mismatch() && "constructor count and destructor count should match."));
    assert(((cues::test::counted<counted_counter_type>::default_constructed_count() > 0) && "counted object was never default-constructed."));
    // copy and move elision are allowed to elide side-effects and are permitted to occur in too many circumstances with
    // no portable way of disabling the elision.
    assert(((cues::test::counted<counted_counter_type>::in_place_constructed_count() > 0) && "counted object was never constructed in-place."));

    if constexpr (std::is_base_of<randomly_fails_construction, test_value_type>::value)
    {
        counted_counter_type current_default_throws = test_value_type::default_constructor_thrown_count();
        counted_counter_type current_copy_throws    = test_value_type::copy_constructor_thrown_count();

        assert(((current_default_throws > previous_default_throws) && "expected to throw on default constructor but did not."));
        assert(((current_copy_throws    > previous_copy_throws)    && "expected to throw on copy constructor but did not."));
    }

    return cues::test::counted<counted_counter_type>::constructor_destructor_mismatch();

}

}
}


#endif // CUES_TEST_COMMON_BUFFER_FUNCTIONS_HPP_INCLUDED
