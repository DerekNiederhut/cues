#ifndef CUES_DEREFERENCED_OPERATORS_HPP_INCLUDED
#define CUES_DEREFERENCED_OPERATORS_HPP_INCLUDED

#pragma once

#include <type_traits>      //for static assertions
#include <functional>
#include <utility>

#include "cues/version_helpers.hpp"

namespace cues
{
//! functions and functors for use with, for example standard associative containers and algorithms
//! default arguments use the typical std::less and assume raw pointers.
//! for iterators, use T=const_iterator, U=value_type and optionally also set Value_Function
//! for smart pointers, use the smart pointer as T, U=T::element_type, and optionally also set Value_Function
template <class T, class U = typename std::remove_pointer<T>::type, class Value_Function = std::less<U>>
constexpr bool operator_wrapper_dereference(T lhs, T rhs) noexcept(noexcept((Value_Function{})(*lhs,*rhs)))
{
    return (Value_Function{})(*lhs,*rhs);
}

template <class T, class U = typename std::remove_pointer<T>::type, class Value_Function = std::less<U>>
struct functor_wrapper_dereference
{
    public:
        constexpr bool operator() (T lhs, T rhs) const noexcept( noexcept(operator_wrapper_dereference<T,U,Value_Function>(lhs, rhs)) )
        {
            return operator_wrapper_dereference<T,U,Value_Function>(lhs, rhs);
        }
};

// like dereferenced_operator, but for pair
// could be useful when working with non-standard associative-like containers
template <class T1, class T2, class Value_Function = std::less<T1>>
constexpr bool operator_wrapper_first_of_pair(std::pair<T1, T2> const & lhs, std::pair<T1, T2> const & rhs) noexcept(noexcept((Value_Function{})(lhs.first, rhs.first)))
{
    return (Value_Function{})(lhs.first, rhs.first);
}

// overloads that permit using a constant of the first type with a pair
template <class T1, class T2, class Value_Function = std::less<T1>>
constexpr bool operator_wrapper_first_of_pair(std::pair<T1, T2> const & lhs, T1 const & rhs) noexcept(noexcept((Value_Function{})(lhs.first, rhs)))
{
    return (Value_Function{})(lhs.first, rhs);
}

template <class T1, class T2, class Value_Function = std::less<T1>>
constexpr bool operator_wrapper_first_of_pair(T1 const & lhs, std::pair<T1,T2> const & rhs) noexcept(noexcept((Value_Function{})(lhs, rhs.first)))
{
    return (Value_Function{})(lhs, rhs.first);
}



// functor for first of pair
template <class T1, class T2, class Value_Function = std::less<T1>>
struct functor_wrapper_first_of_pair
{
    public:

    constexpr bool operator() (std::pair<T1, T2> const & lhs, std::pair<T1, T2> const & rhs) const noexcept(noexcept(operator_wrapper_first_of_pair<T1,T2, Value_Function>(lhs, rhs)))
    {
        return operator_wrapper_first_of_pair<T1,T2, Value_Function>(lhs, rhs);
    }

    constexpr bool operator() (std::pair<T1, T2> const & lhs, T1 const & rhs) const noexcept(noexcept(operator_wrapper_first_of_pair<T1,T2, Value_Function>(lhs, rhs)))
    {
        return operator_wrapper_first_of_pair<T1,T2, Value_Function>(lhs, rhs);
    }

    constexpr bool operator() (T1 const & lhs, std::pair<T1, T2> const & rhs) const noexcept(noexcept(operator_wrapper_first_of_pair<T1,T2, Value_Function>(lhs, rhs)))
    {
        return operator_wrapper_first_of_pair<T1,T2, Value_Function>(lhs, rhs);
    }

};


}

#endif // CUES_DEREFERENCED_OPERATORS_HPP_INCLUDED
