#ifndef CUES_TEST_OPTIONAL_TEST_HPP_INCLUDED
#define CUES_TEST_OPTIONAL_TEST_HPP_INCLUDED

#include <cstdint>
#include "cues/version_helpers.hpp"

#if ((__cplusplus < 201703L ) && (!defined ( __cpp_lib_launder) || !defined( __cpp_lib_optional)))
    #pragma message "NOTE: skipping test of cues::optional because it requires std::launder and std::in_place_t, which are not available."
    #define CUES_TEST_OPTIONAL 0
    namespace cues
    {
    namespace test
    {
        std::size_t check_optional_test_runtime_assertions();
    }
    }
#else
    #define CUES_TEST_OPTIONAL 1
#include <type_traits>
#include <algorithm>

#include "cues/optional.hpp"
#include "helper_types.hpp"

namespace cues
{
namespace test
{

template<class T>
struct is_copy_assignable_and_constructible : public std::integral_constant<
    bool,
    std::is_copy_constructible<T>::value && std::is_copy_assignable<T>::value
>
{};

template<class T>
struct is_move_assignable_and_constructible : public std::integral_constant<
    bool,
    std::is_move_constructible<T>::value && std::is_move_assignable<T>::value
>
{};

template<class T>
struct optional_size_matches_expectations : public std::integral_constant<
    bool,
    (sizeof(cues::optional<T>) <= (sizeof(T) + (std::max)(alignof(T), sizeof(bool))))
>
{};


using move_only_array_type = move_only[4];


static_assert(optional_size_matches_expectations<std::uint_least8_t>::value,
        "unexpected wasted space.");

static_assert(is_copy_assignable_and_constructible<cues::optional<std::uint_least8_t>>::value,
              "unexpected un-copyable optional");

static_assert(is_move_assignable_and_constructible<cues::optional<std::uint_least8_t>>::value,
               "unexpected un-movable optional");

static_assert(optional_size_matches_expectations<std::uint_least16_t>::value,
        "unexpected wasted space.");

static_assert(optional_size_matches_expectations<std::uint_least32_t>::value,
        "unexpected wasted space.");

static_assert(optional_size_matches_expectations<std::uint_least64_t>::value,
        "unexpected wasted space.");

static_assert(optional_size_matches_expectations<std::uint_least64_t[8]>::value,
        "unexpected wasted space.");

static_assert(is_copy_assignable_and_constructible<cues::optional<std::uint_least64_t[8]>>::value,
              "unexpected un-copyable optional");

static_assert(is_move_assignable_and_constructible<cues::optional<std::uint_least64_t[8]>>::value,
               "unexpected un-movable optional");

static_assert(optional_size_matches_expectations<std::uint_least32_t[3][8][15]>::value,
        "unexpected wasted space.");

static_assert(is_copy_assignable_and_constructible<cues::optional<std::uint_least32_t[3][8][15]>>::value,
              "unexpected un-copyable optional");

static_assert(is_move_assignable_and_constructible<cues::optional<std::uint_least32_t[3][8][15]>>::value,
               "unexpected un-movable optional");

static_assert(optional_size_matches_expectations<scoped_only>::value,
        "unexpected wasted space.");

static_assert(!is_copy_assignable_and_constructible<cues::optional<scoped_only>>::value,
              "unexpected copyable optional");

static_assert(!is_move_assignable_and_constructible<cues::optional<scoped_only>>::value,
               "unexpected movable optional");

static_assert(optional_size_matches_expectations<move_only>::value,
        "unexpected wasted space.");

static_assert(!is_copy_assignable_and_constructible<cues::optional<move_only>>::value,
              "unexpected copyable optional");

static_assert(is_move_assignable_and_constructible<cues::optional<move_only>>::value,
               "unexpected movable optional");

std::size_t check_optional_test_runtime_assertions();

}
}
#endif
#endif // CUES_TEST_OPTIONAL_TEST_HPP_INCLUDED
